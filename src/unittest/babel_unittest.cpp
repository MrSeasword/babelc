/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Sep 2, 2016
 */


#include "babel_unittest.h"

#include <cstdio>
#include <stdexcept>

namespace babel { namespace unittest {

std::vector<UnitTest*> &getAllUnitTests() {
    static std::vector<UnitTest*> theUnitTests;

    return theUnitTests;
}

GrandTotal UnitTest::runAllTests() {
#ifdef BABEL_UNITTEST
    bool totalSuccess = true;

    for (UnitTest *theUnitTest : getAllUnitTests()) {
        try {
            theUnitTest->run();
            totalSuccess = (totalSuccess && (theUnitTest->itsTotalNumberOfTests == theUnitTest->itsTotalNumberOfSucceededTests));
        }
        catch (std::exception &theException) {
            theUnitTest->itsExceptionReason = theException.what();
            totalSuccess = false;
        }
    }
    return GrandTotal( totalSuccess, getAllUnitTests() );
#else
    throw std::runtime_error("Program not compiled with unit tests enabled!");
#endif

}

void UnitTest::performCheck( const char*        theFileName,
                             const std::size_t  theLineNumber,
                             const std::string &theConditionToBeTrue,
                             const bool         theOutcome ) {

    itsTotalNumberOfTests += 1;

    if (theOutcome) {
        itsTotalNumberOfSucceededTests += 1;
    }
    else {
        (void)fprintf(stderr, "%s:%lu Test '%s' failed.\n", theFileName, theLineNumber, theConditionToBeTrue.c_str() );
    }
}

UnitTest::UnitTest(const std::string& theName)
: itsName(theName),
  itsExceptionReason(),
  itsTotalNumberOfTests(0),
  itsTotalNumberOfSucceededTests(0) {
    getAllUnitTests().push_back(this);
}

}}

