/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Sep 2, 2016
 */

#ifndef UNITTEST_BABEL_UNITTEST_H_
#define UNITTEST_BABEL_UNITTEST_H_

#include <vector>
#include <string>
#include <sstream>

#define BABEL_UNITTEST_CHECK( theConditionToBeTrue ) \
do { \
    performCheck( __FILE__, __LINE__, #theConditionToBeTrue, (theConditionToBeTrue)); \
} while (0)

#define BABEL_UNITTEST_CHECK_EQUAL( theLeftSide, theRightSide ) \
do {                                                            \
    auto const theLeftValue  = (theLeftSide);                   \
    auto const theRightValue = (theRightSide);                  \
    std::stringstream theErrorMessage;                          \
    theErrorMessage << #theLeftSide " == " #theRightSide " (" << theLeftValue << " != " << theRightValue << ")";   \
    performCheck( __FILE__, __LINE__, theErrorMessage.str(), (theLeftValue == theRightValue)); \
} while (0)

#define BABEL_UNITTEST_CHECK_NOT_EQUAL( theLeftSide, theRightSide ) \
do {                                                            \
    auto const theLeftValue  = (theLeftSide);                   \
    auto const theRightValue = (theRightSide);                  \
    std::stringstream theErrorMessage;                          \
    theErrorMessage << #theLeftSide " != " #theRightSide " (" << theLeftValue << " == " << theRightValue << ")";   \
    performCheck( __FILE__, __LINE__, theErrorMessage.str(), (theLeftValue != theRightValue)); \
} while (0)

namespace babel { namespace unittest {

class UnitTest;

class GrandTotal {
public:
    const bool                          itsSuccess;
    const std::vector<UnitTest *>       itsTests;

    GrandTotal( const bool theSuccess, std::vector<UnitTest *> &theTests) : itsSuccess(theSuccess), itsTests(theTests) {}
};

class UnitTest {
public:
    static GrandTotal runAllTests();

    const std::string &getItsName()                         const;
    std::size_t        getItsTotalNumberOfTests()           const;
    std::size_t        getItsTotalNumberOfSucceededTests()  const;
    const std::string &getItsExceptionReason()              const;


protected:
    virtual void run() = 0;

    void performCheck( const char *theFileName, const std::size_t theLineNumber, const std::string &theConditionToBeTrue, const bool theOutcome );

    const std::string itsName;
    std::string       itsExceptionReason;
    std::size_t       itsTotalNumberOfTests;
    std::size_t       itsTotalNumberOfSucceededTests;

    UnitTest( const std::string &theName );

    virtual ~UnitTest() {};
};

// Inlines ------------------------------------------------------------

inline const std::string &UnitTest::getItsName()                         const {
    return itsName;
}
inline std::size_t        UnitTest::getItsTotalNumberOfTests()           const {
    return itsTotalNumberOfTests;
}
inline std::size_t        UnitTest::getItsTotalNumberOfSucceededTests()  const {
    return itsTotalNumberOfSucceededTests;
}
inline const std::string &UnitTest::getItsExceptionReason()              const {
    return itsExceptionReason;
}

}
}

#endif /* UNITTEST_BABEL_UNITTEST_H_ */
