/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 19, 2016
 */

#ifndef PLUGINS_BABEL_PLUGINS_H_
#define PLUGINS_BABEL_PLUGINS_H_

#include <set>
#include <list>
#include <string>

#include <common/babel_common.h>

namespace babel { namespace plugins {

class GeneratorInterface {
public:
    using Container = std::list<GeneratorInterface*>;

    /**
     * Realizes the compilation unit found in the given file.
     * @param theInputFile
     * @param theCompilationUnit
     * @param theOutputDirectory
     */
    virtual void realize( const CompilationUnit &theCompilationUnit, 
                          const BabelcOptionSet &theOptions, 
                          const babel::FilePath &theOutputDirectory ) = 0;

    /**
     * Returns true if the target file needs to be realized given the input file, 
     * the options and the dependent files.
     */
    virtual bool needsRealization( const BabelcOptionSet       &theGivenBabelcOptions, 
                                   const FilePath              &theInputFile,
                                   const FilePath              &theOutputDirectory,
                                   const FilePaths             &theDependentFiles ) = 0;    
    /**
     * Accessor method for the name of the generator.
     * @return The name of the generator.
     */
    virtual const std::string &getItsName() const = 0;
    
    virtual FilePath getItsFilePath( const FilePath &basedUponThisPath ) const = 0;

protected:
    virtual ~GeneratorInterface() {};
};

/**
 * Factory method for getting a container of generators indicated by forTheOptions. It will throw an exception in case of failure.
 * @param forTheOptions The names that determine what generator(s) to return. If a nullptr is passes, all available generators are returned.
 * @return The generators to use. Note that the list may very well be empty.
 */
GeneratorInterface::Container getGenerators( const std::set<std::string>  * const theGeneratorNames );


}}

#endif /* PLUGINS_BABEL_PLUGINS_H_ */
