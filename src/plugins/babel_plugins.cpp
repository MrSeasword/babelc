/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 21, 2016
 */



#include <common/babel_common.h>
#include "babel_plugins.h"
#include <plugins/generators/babel_plugins_generator.h>

namespace babel { namespace plugins {


GeneratorInterface::Container getGenerators( const std::set<std::string>  * const theGeneratorNames ) {
    return generator::GeneratorImplementationBase::getGenerators( theGeneratorNames );
}

} }

