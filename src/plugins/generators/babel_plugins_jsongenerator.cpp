/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 21, 2016
 */

#include <cstdio>
#include <iterator>
#include <vector>
#include <map>
#include <numeric>
#include <util/babel_logger.h>
#include <json/babel_json_parser.h>
#include <unittest/babel_unittest.h>

#include "babel_plugins_generator.h"


namespace babel { namespace plugins { namespace generator { namespace json {
    const char theParser[] = 
#include <babel_json_parser.h.txt>
    ;

    const std::string theNameSpaceName = "babel::JSON";

    enum class SyntacticElements {
        Declaration,
        Implementation
    };
    
    class CodeTemplate {
        std::string  itsDeclaration;
        std::string  itsBody;
        
    public:
        CodeTemplate( const std::string theDeclaration, const std::string theBody ) 
        : itsDeclaration(theDeclaration),
          itsBody(theBody) {
        }

        void output( OutputFile &toFile, SyntacticElements theSyntacticElement ) const {
            toFile.printf("%s", itsDeclaration.c_str() );

            if (theSyntacticElement == SyntacticElements::Declaration) {
                toFile.printf(";\n\n");
            }
            else {
                toFile.printf(" %s\n", itsBody.c_str());
            }
        }
    };

    using CodeTemplates = std::vector<CodeTemplate >;

    static std::string createWriteTemplateDeclaration( const Type &forTheType, bool needsIndent ) {
        return util::format(
                    "template <class CharT, class Traits>\n"
                    "std::basic_ostream<CharT,Traits>&\n"
                    "writeJSON( std::basic_ostream<CharT,Traits>&theStream, const %s &theValue, %s::Indent %s)",
                    forTheType.getItsSimpleName().c_str(),
                    theNameSpaceName.c_str(),
                    (needsIndent ? "theIndent " : "")
                );
    }
    
    static std::string createInterpretTemplateDeclaration( const Type &forTheType ) {
        return util::format(
                    "template <class CharT>\n"
                    "void\n"
                    "interpretJSON( const babel::JSON::parser::Value &theJSONValue, %s &theValue)",
                    forTheType.getItsSimpleName().c_str()
                );
    }
    
    static std::vector<std::string> JSONTypeNameOf( const Type *theType ) {
        using StringVector = std::vector<std::string>;
        
        if (const auto theBuiltInType = recursive_cast<BuiltInType>(theType)) {
            return StringVector{ (theBuiltInType->itsName == "bool") ? "boolean" : "numeric" };
        }
        else if (recursive_cast<StdString>(theType)) {
            return StringVector{ "string" };
        }
        else if (const auto theEnumType = recursive_cast<EnumeratedType>(theType)) {
            return StringVector{ theEnumType->itsValues.empty() ? "numeric" : "string" };
        }
        else if (const auto theStructureType = recursive_cast<StructType>(theType)) {
            return StringVector{theStructureType->isVacuous() ? "null" : "object" };
        }
        else if (recursive_cast<MapContainer>(theType)) {
            return StringVector{ "object" };
        }
        else if (const auto theOptional = recursive_cast<OptionalType>(theType)) {
            auto theIndirectType = JSONTypeNameOf(theOptional->itsElementType);
            theIndirectType.push_back("null");
            return theIndirectType;
        }
        else if (recursive_cast<Container>(theType)) {
            return StringVector( 1, "array" );
        }
        else if (const auto thePointer = recursive_cast<PointerType>(theType)) {
            auto theIndirectType = JSONTypeNameOf(thePointer->itsReferredType);
            theIndirectType.push_back("null");
            return theIndirectType;
        }
        else {
            return StringVector{ "unhandled" };
        }
    }
    
    static void sanityCheck( const VariantType &theVariant ) {
        using TypeVector = std::vector< const Type *>;
        std::map< std::string, TypeVector > theTypeMap =  
            {
                std::make_pair(std::string("numeric"), TypeVector()),
                std::make_pair(std::string("boolean"), TypeVector()),
                std::make_pair(std::string("object"),  TypeVector()),
                std::make_pair(std::string("array"),   TypeVector()),
                std::make_pair(std::string("null"),    TypeVector()),
                std::make_pair(std::string("string"),  TypeVector())
            };
        
        for (auto theType : theVariant.itsTypes) {
            for (const auto theTypeName : JSONTypeNameOf(theType) ) {
               const auto theNameTypeLocation = theTypeMap.find(theTypeName);

                if (theNameTypeLocation == theTypeMap.end()) {
                    throw std::runtime_error(theType->itsLocation + ": babelc internal error. Unhandled JSON variant check.");
                }
                else {
                    theNameTypeLocation->second.push_back(theType);
                }
            }
        }
            
        {
            std::size_t theNoOfWarnings = 0;

            for (const auto theCase : theTypeMap) {
                if (theCase.second.size() > 1) {
                    theNoOfWarnings += 1;
                    auto theTypeIterator = theCase.second.begin();
                    GeneratorImplementationBase::reportTypeWarning( 
                            **(theTypeIterator++), 
                            "", 
                            "has JSON type '" + theCase.first + "' but this is also true for:");
                    do {
                        GeneratorImplementationBase::reportTypeWarning( **theTypeIterator, "...", "");
                    } while (++theTypeIterator != theCase.second.end());
                }
            }
            if (theNoOfWarnings != 0) {
                GeneratorImplementationBase::reportTypeError(theVariant, "is the type context for the above warnings");
            }
        }
        {
            static const char theIncompleteAttributeName[] = "intentionally_incomplete_variant";
            
            std::string theMissingTypes;
            
            for (const auto theCase  : theTypeMap) {
                if (theCase.second.empty()) {
                    if (!theMissingTypes.empty()) {
                        theMissingTypes += ", ";
                    }
                    theMissingTypes += theCase.first;
                }
            }
            const auto theIncompleteAttribute = theVariant.getAttributeValue(theIncompleteAttributeName);
            
            if (theMissingTypes.empty()) {
                if (theIncompleteAttribute) {
                    GeneratorImplementationBase::reportTypeWarning(
                        theVariant,
                        "",
                        util::format("variant type has [babelc::%s] attribute, but the variant is 'JSON complete'. Remove attribute to get rid of warning.",
                                     theIncompleteAttributeName ));
                }
            }
            else if (!theIncompleteAttribute) {
                GeneratorImplementationBase::reportTypeWarning(
                    theVariant,
                    "",
                    util::format("variant type is not 'JSON complete' (it lacks type%s for %s). Apply attribute [babelc::%s] to get rid of warning.",
                                 ((theMissingTypes.find(',') == std::string::npos) ? "" : "s"),
                                 theMissingTypes.c_str(),
                                 theIncompleteAttributeName ));
            }
        }
    }
        
    static bool addTypeTemplates( CodeTemplates &, const VariantType &forTheVariant ) {
        sanityCheck(forTheVariant);
        return false;
    }

    static bool addTypeTemplates( CodeTemplates &theCodeTemplates, const StructType &forTheStruct ) {
        {
            std::string theRequiredFields;
            std::string theFields;
            
            {
                int theNoOfRequiredFields = 0;

                for (const auto &theField : forTheStruct.itsFields) {
                    const auto isRequired = !theField.isInitialized;
                    if (isRequired) {
                        if (!theRequiredFields.empty()) {
                            theRequiredFields.append(",\n");
                        }
                        theRequiredFields.append( "\t\tBABEL_JSON_PARSER_STRING_LITERAL(CharT,\"" + theField.itsName + "\")");
                    }
                    if (!theFields.empty()) {
                        theFields.append("\n\t\telse ");
                    }
                    theFields.append(util::format(
                        "if (thePair.first == %s) {\n"
                        "\t\t\tinterpretJSON<CharT>( thePair.second, theActual.%s );\n"
                        "%s"
                        "\t\t}",
                        isRequired ? 
                            util::format("theRequiredFields[%d]",theNoOfRequiredFields).c_str() :
                            util::format("BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"%s\")", theField.itsName.c_str()).c_str(),
                        theField.itsName.c_str(),
                        isRequired ? 
                            util::format("\t\t\ttheUsedRequiredFields[%d] = true;\n", theNoOfRequiredFields).c_str() :
                            ""
                    ));
                    if (isRequired) {
                        theNoOfRequiredFields += 1;
                    }
                }
            }
            
            const std::string theRequiredFieldsDeclarations = 
                theRequiredFields.empty() ? 
                    "" 
                        :
                    util::format(
                        "\tstatic const CharT * const theRequiredFields[] = {\n"
                        "%s\n"
                        "\t};"
                        "\n"
                        "\tbool       theUsedRequiredFields[sizeof(theRequiredFields)/sizeof(theRequiredFields[0])]{};\n"
                        "\n",
                        theRequiredFields.c_str()
                    );
            
            const std::string theRequiredFieldsCheck = 
                theRequiredFields.empty() ? 
                    "" 
                        :
                    util::format(
                        "\tfor (std::size_t i = 0; i < sizeof(theUsedRequiredFields)/sizeof(theUsedRequiredFields[0]); i++) {\n"
                        "\t\tif (!theUsedRequiredFields[i]) {\n"
                        "\t\t\tif (theMissingFields.empty()) { theMissingFields.append(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"missing field(s):\")); }\n"
                        "\t\t\ttheMissingFields.append(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\" \")).append(theRequiredFields[i]);\n"
                        "\t\t}\n"
                        "\t}\n"
                        "\n"
                    );
            
            theCodeTemplates.emplace_back(
                createInterpretTemplateDeclaration(forTheStruct),
                util::format(
                    "{\n"
                    "\tusing StringType = std::basic_string<CharT>;\n"
                    "\n"
                    "%s"
                    "\tStringType theExtraFields;\n"
                    "\t%s theActual;\n"
                    "\n"
                    "\tfor (const auto &thePair : BABEL_JSON_PARSER_UPCAST( Object<std::basic_string<CharT>>,theJSONValue ).itsValue) {\n"
                    "\t\t%s\n"
                    "\t\telse {\n"
                    "\t\t\tif (theExtraFields.empty()) { theExtraFields.append(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"unknown field(s):\")); }\n"
                    "\t\t\ttheExtraFields.append(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\" \")).append(thePair.first);\n"
                    "\t\t}\n"
                    "\t}\n"
                    "\n"
                    "\tStringType theMissingFields;\n"
                    "\n"
                    "%s"
                    "\n"
                    "\tif (!theMissingFields.empty() || !theExtraFields.empty()) {\n"
                    "\t\tusing CharTraits = ::babel::JSON::parser::CharTraits<char>;\n"
                    "\t\tif (!theMissingFields.empty() && !theExtraFields.empty()) { theMissingFields.append(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\", \")); }\n"
                    "\t\tthrow std::invalid_argument(theJSONValue->getLocation() + \"Type %s has errors: \" + CharTraits::adjust(theMissingFields) + CharTraits::adjust(theExtraFields) );\n"
                    "\t}\n"
                    "\n"
                    "#pragma GCC diagnostic push\n"
                    "#pragma GCC diagnostic ignored \"-Wmaybe-uninitialized\"\n"
                     "\ttheValue = std::move(theActual);\n"
                    "#pragma GCC diagnostic pop\n"
                    "}\n",
                    theRequiredFieldsDeclarations.c_str(),
                    forTheStruct.getItsSimpleName().c_str(),
                    theFields.c_str(),
                    theRequiredFieldsCheck.c_str(),
                    forTheStruct.itsName.c_str()
                )
            );
        }
        {
            const auto &theFields          = forTheStruct.itsFields;
            const auto theFieldNumberWidth = static_cast<int>(std::to_string(theFields.size()-1).size());
            const auto theMaxFieldWidth    = static_cast<int>( std::accumulate(BABEL_UTIL_RANGE(theFields), 0, 
                                                                    []( std::size_t maxLength, const StructType::Field &theField) {
                                                                        return std::max( maxLength, theField.itsName.length()); 
                                                                    }));
            std::string  theShortHeaders;
            std::string  theLongHeaders;
            std::string  theBody;
            unsigned int theFieldNumber = 0;
            for (const auto &theField : theFields) {
                const auto theFieldLength = static_cast<int>(theField.itsName.length());
                if (theFieldNumber != 0) {
                    theShortHeaders.append(",\n");
                    theLongHeaders.append(",\n");
                    theBody.append(" << ',' << theContentIndent;\n\t");
                }
                theShortHeaders.append(util::format("\t\t\tBABEL_JSON_PARSER_STRING_LITERAL(CharT,\"\\\"%s\\\":\")", theField.itsName.c_str()));
                theLongHeaders.append(util::format("\t\t\tBABEL_JSON_PARSER_STRING_LITERAL(CharT,\"\\\"%s\\\"%*s)",theField.itsName.c_str(),(3+(theMaxFieldWidth-theFieldLength)),": \""));
                theBody.append(
                    util::format(
                        "writeJSON( theStream << theLabels[%*u], theValue.%s,%*stheNextIndent )",
                        theFieldNumberWidth,
                        theFieldNumber,
                        theField.itsName.c_str(),
                        (theMaxFieldWidth-theFieldLength)+1,
                        ""
                        )
                );
                theFieldNumber++; 
            }
            theBody.append(" << theIndent;\n");
            theCodeTemplates.emplace_back(
                createWriteTemplateDeclaration(forTheStruct, true),
                babel::util::format(
                    "{\n"
                    "\tstatic const CharT * const theLabelSet[2][%lu] = {\n"
                    "\t\t{ \n"
                    "%s\n"
                    "\t\t},\n"
                    "\t\t{\n"
                    "%s\n"
                    "\t\t}\n"
                    "\t};\n"
                    "\tconst auto theContentIndent = theIndent.next();\n"
                    "\tconst auto theNextIndent    = theContentIndent.next();\n"
                    "\tconst auto theLabels        = theLabelSet[theIndent.isActive()];\n"
                    "\ttheStream << '{' << theContentIndent;\n"
                    "\t%s"
                    "\ttheStream << '}';\n"
                    "\treturn theStream;\n"
                    "}\n",
                    theFields.size(),
                    theShortHeaders.c_str(),
                    theLongHeaders.c_str(),
                    theBody.c_str()
                )
            );
        }
        return true;
    }
    
    static bool addTypeTemplates( CodeTemplates &theCodeTemplates, const EnumeratedType &forTheEnum ) {
        std::string theInterpretBody;
        std::string theWriteBody;
        
        if (forTheEnum.itsValues.empty()) {
            theInterpretBody = util::format( "{\n"
                                             "\ttheValue = static_cast<%1$s>(BABEL_JSON_PARSER_UPCAST(Number<std::basic_string<CharT>>,theJSONValue).template getNumericValue<std::underlying_type_t<%1$s>>());\n"
                                             "}",
                                             forTheEnum.itsName.c_str());
            theWriteBody = util::format( "{\n"
                                         "\treturn writeJSON(theStream, static_cast<std::underlying_type_t<%s>>(theValue), theIndent);\n"
                                         "}",
                                         forTheEnum.itsName.c_str() );
        }
        else {
            std::string theLookups;
            std::string theCases;
            const int   theMaxValueLength = std::max_element( BABEL_UTIL_RANGE(forTheEnum.itsValues),
                                                              []( const std::string &left, const std::string &right ) {
                                                                 return left.size() < right.size(); 
                                                              })->size();
            for (auto theValue : forTheEnum.itsValues) {
                if (!theLookups.empty()) {
                    theLookups.append(",\n");
                }
                theLookups.append( util::format("\t\t{ %1$s::%2$-*3$s, BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"%2$s\") }",
                                                forTheEnum.itsName.c_str(),
                                                theValue.c_str(),
                                                theMaxValueLength ) );
                theCases.append( util::format(
                    "\tcase %1$s::%2$s: {\n"
                    "\t\tstatic const CharT * const theImage = BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"%2$s\");\n" // TODO: Must be handled correctly for wchar_t
                    "\t\treturn writeJSON( theStream, theImage, (theImage+%3$lu), theIndent );\n"
                    "\t}\n",  
                    forTheEnum.getItsSimpleName().c_str(),
                    theValue.c_str(),
                    theValue.size()));
            }
            theInterpretBody = babel::util::format(
                    "{\n"
                    "\tstatic const struct Lookup { %1$s itsValue; const CharT *itsImage; } theLookupTable[] = {\n"
                    "%2$s\n"
                    "\t};\n"
                    "\n"
                    "\tconst auto &theActual = BABEL_JSON_PARSER_UPCAST(String<std::basic_string<CharT>>,theJSONValue).itsValue;\n"
                    "\n"
                    "\tfor (const auto &thePair : theLookupTable) {\n"
                    "\t\tif (thePair.itsImage == theActual) {\n"
                    "\t\t\ttheValue = thePair.itsValue;\n"
                    "\t\t\treturn;\n"
                    "\t\t}\n"
                    "\t}\n"
                    "\tthrow std::invalid_argument(theJSONValue->getLocation() + ::babel::JSON::parser::CharTraits<char>::adjust(theActual) + \" is not a valid value for %1$s\" );\n"
                    "}\n",
                    forTheEnum.itsName.c_str(),
                    theLookups.c_str()
            );
            theWriteBody = babel::util::format(
                    "{\n"
                    "\tswitch (theValue) {\n"
                    "%s"
                    "\t}\n"
                    "\t(writeJSON( theStream << BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"Invalid %s (\"), static_cast<%s>(theValue), theIndent ) << BABEL_JSON_PARSER_STRING_LITERAL(CharT,\")\")).setstate(std::ios_base::failbit); \n"
                    "\treturn theStream;\n"
                    "}\n",
                    theCases.c_str(),
                    forTheEnum.itsName.c_str(),
                    forTheEnum.itsUnderlyingType->itsName.c_str()
                );
            
        }
        theCodeTemplates.emplace_back(
            createInterpretTemplateDeclaration(forTheEnum),
            theInterpretBody
        );
        theCodeTemplates.emplace_back(
            createWriteTemplateDeclaration(forTheEnum, true),
            theWriteBody
        );
        return true;
    }
    
    
    template <class C> 
    bool derivedTypeAdded( const Type *theType, CodeTemplates &toTheTemplates ) {
        auto const theDerivedInstance = dynamic_cast<const C *>(theType);
        
        if (theDerivedInstance) {
            return addTypeTemplates( toTheTemplates, *theDerivedInstance );
        }
        else {
            return false;
        }
    }
    
    // Recursively get all referred types of a type.
    static void getReferredTypes( Types& types, const Type& type ) {       
        struct NoFilter : public ReferenceFilter {
          bool breakAt( const Type * ) override {
              return false;
          }
        } thePassAllFilter;
        
        type.getItsReferredTypes(types, thePassAllFilter);
    }
    
    static void addTypeTemplates( OutputFile &theOutputFile, const Types &theTypes, SyntacticElements theSyntacticElement ) {
        static std::map< const Type *, CodeTemplates > theTypeTemplates;
        
        if (theTypeTemplates.empty()) {
            for (auto theType : theTypes) {
                CodeTemplates theCodeTemplates;
                auto const theTypeIsHandled = 
                    derivedTypeAdded<StructType>    (theType.second, theCodeTemplates ) ||
                    derivedTypeAdded<EnumeratedType>(theType.second, theCodeTemplates ) ||
                    derivedTypeAdded<VariantType>   (theType.second, theCodeTemplates );    
                
                if (theTypeIsHandled) {
                    theTypeTemplates[theType.second] = theCodeTemplates;
                }
            }
        }
        for (const auto &theTypeInfo : theTypeTemplates) {
            
            ScopedGuard     theTemplateGuard( theOutputFile,
                                              std::string("have_") + theNameSpaceName + "__" +
                                                theTypeInfo.first->itsName +
                                                (theSyntacticElement == SyntacticElements::Declaration ? "_declarations" : "_implementations"));
            ScopedNamespace theTemplateNamespace( theOutputFile,
                                                  theTypeInfo.first->getItsNamespace());
            const std::string thisNamespace = NamespaceToString(theTypeInfo.first->getItsNamespace());
            
            theOutputFile.printf("using %s::readJSON;\n", theNameSpaceName.c_str());
            theOutputFile.printf("using %s::writeJSON;\n", theNameSpaceName.c_str());
            theOutputFile.printf("using %s::interpretJSON;\n\n", theNameSpaceName.c_str());
            
            for (const auto &theTemplate : theTypeInfo.second) {
                if (theSyntacticElement == SyntacticElements::Implementation) {
                  
                    Types theReferredTypes;
                    getReferredTypes(theReferredTypes, *theTypeInfo.first);
                    
                    // Get the set of used namespaces.
                    std::set<std::string> theNamespaces;
                    for ( const auto& t : theReferredTypes ) {
                        if (theTypeTemplates.find(t.second) != theTypeTemplates.end()) {
                            // The type is handled.
                            const std::string theNamespace = NamespaceToString(t.second->getItsNamespace());
                            // Add to list of needed namespaces if its not the current namespace.
                            if (theNamespace != thisNamespace) {
                                theNamespaces.emplace(theNamespace);
                            }
                        }
                    }
                    
                    for (auto theNamespace : theNamespaces) {
                            theOutputFile.printf("using %sreadJSON;\n", theNamespace.c_str());
                            theOutputFile.printf("using %swriteJSON;\n", theNamespace.c_str());
                            theOutputFile.printf("using %sinterpretJSON;\n\n", theNamespace.c_str());
                    }
                }
                theTemplate.output(theOutputFile, theSyntacticElement );
            }
        }
    }
    
    static void addPredefinedBabelTemplates( OutputFile                 &theOutputFile, 
                                             SyntacticElements           theSyntacticElement,
                                             const std::set<std::string> theOptionals ) {
    
        static std::map< std::string, CodeTemplates > theTemplates;
        
        if (theTemplates.empty()) {
            CodeTemplates &theBabelTemplates = theTemplates[theNameSpaceName];
        
            theBabelTemplates.emplace_back(
                          std::string( "template <class CharT, typename T>\n"
                                       "std::enable_if_t< std::is_arithmetic< T >::value && !std::numeric_limits<T>::has_quiet_NaN && !std::is_same< T, bool >::value>\n"
                                       "interpretJSON( const babel::JSON::parser::Value &theJSONValue, T &theValue )" ),
                          std::string( "{\n"
                                       "\ttheValue = BABEL_JSON_PARSER_UPCAST( Number<std::basic_string<CharT>>, theJSONValue ).template getNumericValue<T>();\n"
                                       "}\n" ) );

            theBabelTemplates.emplace_back(
                          std::string( "template <class CharT, typename T>\n"
                                       "std::enable_if_t< std::is_arithmetic< T >::value && std::numeric_limits<T>::has_quiet_NaN && !std::is_same< T, bool >::value>\n"
                                       "interpretJSON( const babel::JSON::parser::Value &theJSONValue, T &theValue )" ),
                          std::string( "{\n"
                                       "\tif (dynamic_cast<const babel::JSON::parser::Null *>(theJSONValue.get())) {\n"
                                       "\t\ttheValue = std::numeric_limits<T>::quiet_NaN();\n"
                                       "\t}\n"
                                       "\telse {\n"
                                       "\t\ttheValue = BABEL_JSON_PARSER_UPCAST( Number<std::basic_string<CharT>>, theJSONValue ).template getNumericValue<T>();\n"
                                       "\t}"
                                       "}\n" ) );

            theBabelTemplates.emplace_back(
                std::string( "template <class CharT, typename T>\n"
                             "std::enable_if_t< std::is_same< T, std::basic_string<CharT> >::value>\n"
                             "interpretJSON( const babel::JSON::parser::Value &theJSONValue, T &theValue )" ),
                std::string( "{\n"
                             "\ttheValue = BABEL_JSON_PARSER_UPCAST( String<std::basic_string<CharT>>, theJSONValue ).itsValue;\n"
                             "}\n" ) 
            );

            theBabelTemplates.emplace_back(
                std::string( "template <class CharT, typename T>\n"
                             "std::enable_if_t< std::is_same< T, typename ::babel::JSON::parser::CharTraits<CharT>::OtherStringType >::value>\n"
                             "interpretJSON( const babel::JSON::parser::Value &theJSONValue, T &theValue )" ),
                std::string( "{\n"
                             "\ttheValue = ::babel::JSON::parser::CharTraits< typename ::babel::JSON::parser::CharTraits<CharT>::OtherCharType >::adjust( BABEL_JSON_PARSER_UPCAST( String<std::basic_string<CharT>>, theJSONValue ).itsValue );\n"
                             "}\n" ) 
            );

            theBabelTemplates.emplace_back(
                std::string( "template <class CharT, typename T>\n"
                             "std::enable_if_t< std::is_same< T, bool >::value >\n"
                             "interpretJSON( const babel::JSON::parser::Value &theJSONValue, T &theValue )" ),
                std::string( "{\n"
                             "\ttheValue = BABEL_JSON_PARSER_UPCAST( Boolean, theJSONValue ).itsValue;\n"
                             "}\n" ) 
            );

            theBabelTemplates.emplace_back(
                std::string( "template <class CharT, typename T>\n"
                             "std::enable_if_t< is_supported_container<T>::value && !is_fixed_container<T>::value && !is_map_container<T>::value >\n"
                             "interpretJSON( const babel::JSON::parser::Value &theJSONValue, T &theValue )" ),
                std::string( "{\n"
                             "\tconst auto &theActual = BABEL_JSON_PARSER_UPCAST( Array, theJSONValue );\n"
                             "\ttheValue.resize(0);\n"
                             "\tfor (const auto &theJSONElement : theActual.itsValue) {\n"
                             "\t\ttheValue.resize(theValue.size() + 1);\n"
                             "\t\tinterpretJSON<CharT>( theJSONElement, theValue.back() );\n"
                             "\t}\n"
                             "}\n" ) 
            );

            theBabelTemplates.emplace_back(
                std::string( "template <class CharT, typename T>\n"
                             "std::enable_if_t< is_fixed_container<T>::value >\n"
                             "interpretJSON( const babel::JSON::parser::Value &theJSONValue, T &theValue )" ),
                std::string( "{\n"
                             "\tconst auto &theActual = BABEL_JSON_PARSER_UPCAST( Array, theJSONValue );\n"
                             "\tif (theValue.size() != theActual.itsValue.size()) {\n"
                             "\t\tthrow std::invalid_argument(theJSONValue->getLocation() + \"Fixed array of size \" + std::to_string(theValue.size()) + \" cannot receive array of size \" + std::to_string(theActual.itsValue.size()));\n"
                             "\t}\n"
                             "\tfor (typename T::size_type i = 0; i < theValue.size(); i++) {\n"
                             "\t\tinterpretJSON<CharT>( theActual.itsValue[i], theValue[i] );\n"
                             "\t}\n"
                             "}\n" ) 
            );

            theBabelTemplates.emplace_back(
                std::string( "template <class CharT, typename T>\n"
                             "std::enable_if_t< is_map_container<T>::value  && std::is_same<typename T::key_type,std::basic_string<CharT>>::value>\n"
                             "interpretJSON( const babel::JSON::parser::Value &theJSONValue, T &theValue )" ),
                std::string( "{\n"
                             "\tconst auto &theActual = BABEL_JSON_PARSER_UPCAST( Object<std::basic_string<CharT>>, theJSONValue );\n"
                             "\ttheValue.clear();\n"
                             "\tfor (const auto &theJSONPair : theActual.itsValue) {\n"
                             "\t\ttypename T::value_type theNewItem{ theJSONPair.first, typename T::value_type::second_type{} };\n"
                             "\t\tinterpretJSON<CharT>( theJSONPair.second, theNewItem.second );\n"
                             "\t\ttheValue.emplace( theNewItem );\n"
                             "\t}\n"
                             "}\n" ) 
            );

            theBabelTemplates.emplace_back(
                std::string( "template <class CharT, typename T>\n"
                             "std::enable_if_t< is_map_container<T>::value  && ! std::is_same<typename T::key_type,std::basic_string<CharT>>::value>\n"
                             "interpretJSON( const babel::JSON::parser::Value &theJSONValue, T &theValue )" ),
                std::string( "{\n"
                             "\tconst auto &theActual = BABEL_JSON_PARSER_UPCAST( Object<std::basic_string<CharT>>, theJSONValue );\n"
                             "\ttheValue.clear();\n"
                             "\tfor (const auto &theJSONPair : theActual.itsValue) {\n"
                             "\t\ttypename T::key_type           theKey;\n"
                             "\t\tstd::basic_stringstream<CharT> theStream(theJSONPair.first);\n"
                             "\t\treadJSON( theStream, theKey );\n"
                             "\t\ttypename T::value_type theNewItem{ theKey, typename T::value_type::second_type{} };\n"
                             "\t\tinterpretJSON<CharT>( theJSONPair.second, theNewItem.second );\n"
                             "\t\ttheValue.emplace( theNewItem );\n"
                             "\t}\n"
                             "}\n" ) 
            );

            {
                const std::string theOptionalName = "boost::optional";                

                if (theOptionals.find(theOptionalName) != theOptionals.end()) {
                    CodeTemplates &theOptionalTemplates = theTemplates[theOptionalName];
                    
                    theOptionalTemplates.emplace_back(
                        std::string( "template <class CharT, typename T>\n"
                                     "void\n"
                                     "interpretJSON( const babel::JSON::parser::Value &theJSONValue, boost::optional<T> &theValue )" ),
                        std::string( "{\n"
                                     "\tif (dynamic_cast<const babel::JSON::parser::Null*>(theJSONValue.get())) {\n"
                                     "\t\ttheValue = boost::optional<T>();\n"
                                     "\t}\n"
                                     "\telse if (theValue) {\n"
                                     "\t\tinterpretJSON<CharT>( theJSONValue, *theValue );\n"
                                     "\t}\n"
                                     "\telse {\n"
                                     "\t\tT theTemporary;\n"
                                     "\t\tinterpretJSON<CharT>( theJSONValue, theTemporary );\n"
                                     "\t\ttheValue = std::move(theTemporary);\n"
                                     "\t}\n"
                                     "}\n" )
                    );

                    theOptionalTemplates.emplace_back( 
                        std::string( "template <class CharT, class Traits, class T>\n"
                                     "std::basic_ostream<CharT,Traits>&\n"
                                     "writeJSON( std::basic_ostream<CharT,Traits>&theStream, const boost::optional<T> &theValue, Indent theIndent)"),

                        std::string( "{\n"
                                     "\treturn theValue ? writeJSON( theStream, *theValue, theIndent ) : (theStream << \"null\");\n"
                                     "}\n")
                    );

                }
            }
            
            {
                const std::string theVariantName = "boost::variant";
                
                if (theOptionals.find(theVariantName) != theOptionals.end()) {
                    CodeTemplates &theVariantTemplates = theTemplates[theVariantName];
                    
                    theVariantTemplates.emplace_back(
                        std::string( "template <class CharT, typename ResultVariant>\n"
                                     "void\n"
                                     "interpretJSONVariant( const babel::JSON::parser::Value &theJSONValue, ResultVariant & )" ),
                        std::string( "{\n"
                                     "\tthrow std::invalid_argument(theJSONValue->getLocation() + \"No fitting variant type.\");\n"
                                     "}\n" )
                    );

                    theVariantTemplates.emplace_back(
                        std::string( "template <class CharT, typename ResultVariant, typename Attempt, typename... theRest>\n"
                                     "void\n"
                                     "interpretJSONVariant( const babel::JSON::parser::Value &theJSONValue, ResultVariant &theValue )" ),
                        std::string( "{\n"
                                     "\ttry {\n"
                                     "\t\tAttempt theAttempt;\n"
                                     "\t\tinterpretJSON<CharT>( theJSONValue, theAttempt );\n"
                                     "\t\ttheValue = theAttempt;\n"
                                     "\t}\n"
                                     "\tcatch (const std::invalid_argument &) {\n"
                                     "\t\tinterpretJSONVariant<CharT, ResultVariant, theRest...>( theJSONValue, theValue );\n"
                                     "\t}\n"
                                     "}\n" )
                    );

                    theVariantTemplates.emplace_back(
                        std::string( "template <class CharT, typename... T>\n"
                                     "void\n"
                                     "interpretJSON( const babel::JSON::parser::Value &theJSONValue, boost::variant<T...> &theValue )" ),
                        std::string( "{\n"
                                     "\tinterpretJSONVariant<CharT, boost::variant<T...>, T...>( theJSONValue, theValue );\n"
                                     "}\n" )
                    );

                    theVariantTemplates.emplace_back(
                        std::string( "template <class CharT, class Traits, typename... T>\n"
                                     "std::basic_ostream<CharT,Traits>&\n"
                                     "writeJSON( std::basic_ostream<CharT,Traits>&toStream, const boost::variant<T...> &theValue, Indent theIndent )" ),
                        std::string( "{\n"
                                     "\treturn boost::apply_visitor( [&toStream,&theIndent] ( const auto &theActual ) -> std::basic_ostream<CharT,Traits>&  {\n"
                                     "\t                                 return writeJSON( toStream, theActual, theIndent );\n"
                                     "\t                             },\n"
                                     "\t                             theValue );\n"
                                     "}\n" )
                    );
                }
            }
            
            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::basic_ostream<CharT,Traits>&\n"
                             "writeJSON( std::basic_ostream<CharT,Traits>&theStream, const std::shared_ptr<T> &theValue, Indent theIndent)"),

                std::string( "{\n"
                             "\treturn theValue ? writeJSON( theStream, *theValue, theIndent ) : (theStream << \"null\");\n"
                             "}\n")
            );

            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class T>\n"
                             "void\n"
                             "interpretJSON( const babel::JSON::parser::Value &theJSONValue, std::shared_ptr<T> &theValue )"),

                std::string( "{\n"
                             "\tif (dynamic_cast<const babel::JSON::parser::Null*>(theJSONValue.get())) {\n"
                             "\t\ttheValue = std::shared_ptr<T>();\n"
                             "\t}\n"
                             "\telse {\n"
                             "\t\tT theTemporary;\n"
                             "\t\tinterpretJSON<CharT>( theJSONValue, theTemporary );\n"
                             "\t\ttheValue = std::make_shared<T>(std::move(theTemporary));\n"
                             "\t}\n"
                             "}\n")
            );

            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::basic_ostream<CharT,Traits>&\n"
                             "writeJSON( std::basic_ostream<CharT,Traits>&theStream, const std::unique_ptr<T> &theValue, Indent theIndent)"),

                std::string( "{\n"
                             "\treturn theValue ? writeJSON( theStream, *theValue, theIndent ) : (theStream << \"null\");\n"
                             "}\n")
            );

            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class T>\n"
                             "void\n"
                             "interpretJSON( const babel::JSON::parser::Value &theJSONValue, std::unique_ptr<T> &theValue )"),

                std::string( "{\n"
                             "\tif (dynamic_cast<const babel::JSON::parser::Null*>(theJSONValue.get())) {\n"
                             "\t\ttheValue = std::unique_ptr<T>();\n"
                             "\t}\n"
                             "\telse {\n"
                             "\t\tT theTemporary;\n"
                             "\t\tinterpretJSON<CharT>( theJSONValue, theTemporary );\n"
                             "\t\ttheValue = std::make_unique<T>(std::move(theTemporary));\n"
                             "\t}\n"
                             "}\n")
            );

            theBabelTemplates.emplace_back(
                std::string("template <class CharT, class Traits, class T>\n"
                            "std::basic_istream<CharT,Traits>&\n"
                            "readJSON( std::basic_istream<CharT,Traits> &fromStream, T &theValue )"),
                std::string("{\n"
                            "if (const auto theJSONValue = babel::JSON::parser::readValue( fromStream )) {\n"
                            "\t\tinterpretJSON<CharT>( theJSONValue, theValue );\n"
                            "\t}\n"
                            "\telse {\n"
                            "\t\tthrow std::invalid_argument(\"Failed to read JSON from stream\");\n"
                            "\t}"
                            "\treturn fromStream;\n"
                            "}\n")
            );
            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_supported_container<T>::value, std::basic_ostream<CharT,Traits>&>\n"
                             "writeJSON( std::basic_ostream<CharT,Traits>&theStream, const T &theValues, Indent theIndent )"),

                std::string( "{\n"
                             "\tconstexpr char theStart = is_map_container<T>::value ? '{' : '[';\n"
                             "\tconstexpr char theEnd   = is_map_container<T>::value ? '}' : ']';\n"
                             "\n"
                             "\tif (theValues.empty()) {\n"
                             "\t\treturn theStream << theStart << theEnd;\n"
                             "\t}\n"
                             "\telse {\n"
                             "\t\tconst auto theNextIndent = theIndent.next();\n"
                             "\t\tauto       theValue      = theValues.begin();\n"
                             "\n"
                             "\t\ttheStream << theIndent << theStart << theNextIndent;\n"
                             "\n"
                             "\t\tfor (;;) {\n"
                             "\t\t\twriteJSON( theStream, *theValue, theNextIndent );\n"
                             "\n"
                             "\t\t\tif (++theValue == theValues.end()) {\n"
                             "\t\t\t    break;\n"
                             "\t\t\t}\n"
                             "\n"
                             "\t\t\ttheStream << ',' << theNextIndent;\n"
                             "\t\t}\n"
                             "\n"
                             "\t\treturn theStream << theIndent << theEnd;\n"
                             "\t}\n"
                             "}\n")
            );

            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_supported_ptr<T>::value, std::basic_ostream<CharT,Traits>&>\n"
                             "writeJSON(std::basic_ostream<CharT,Traits>& toStream, const T &theValue, Indent theIndent )"),

                std::string( "{\n"
                             "\treturn theValue ?\n"
                             "\t\t\twriteJSON( toStream, *theValue, theIndent.next() )\n"
                             "\t\t\t\t:\n"
                             "\t\t\t(toStream << \"null\");\n"
                             "}\n")
            );

            theBabelTemplates.emplace_back( 
                std::string( "// = Predefined arithmetic non-NaN type support =======================\n\n"
                             "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_arithmetic<T>::value && !std::numeric_limits<T>::has_quiet_NaN, std::basic_ostream<CharT,Traits>&>\n"
                             "writeJSON(std::basic_ostream<CharT,Traits>& toStream, const T &theValue, Indent )"),

                std::string( "{\n"
                             "\tusing IOType = std::conditional_t<\n"
                             "\t\t(std::is_same<T,std::uint8_t>::value || std::is_same<T,std::uint16_t>::value),\n"
                             "\t\tstd::uint32_t,\n"
                             "\t\tstd::conditional_t< (std::is_same<T,std::int8_t>::value || std::is_same<T,std::int16_t>::value),\n"
                             "\t\t\tstd::int32_t,\n"
                             "\t\t\tT> >;\n"
                             "\n"   
                             "\treturn (toStream << static_cast<IOType>(theValue));\n"
                             "}\n" )
            );

            theBabelTemplates.emplace_back( 
                std::string( "// = Predefined arithmetic NaN type support =======================\n\n"
                             "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_arithmetic<T>::value && std::numeric_limits<T>::has_quiet_NaN, std::basic_ostream<CharT,Traits>&>\n"
                             "writeJSON(std::basic_ostream<CharT,Traits>& toStream, const T &theValue, Indent )"),

                std::string( "{\n"
                             "\tusing IOType = std::conditional_t<\n"
                             "\t\t(std::is_same<T,std::uint8_t>::value || std::is_same<T,std::uint16_t>::value),\n"
                             "\t\tstd::uint32_t,\n"
                             "\t\tstd::conditional_t< (std::is_same<T,std::int8_t>::value || std::is_same<T,std::int16_t>::value),\n"
                             "\t\t\tstd::int32_t,\n"
                             "\t\t\tT> >;\n"
                             "\n"   
                             "\treturn std::isnan(theValue) ? (toStream << \"null\") : (toStream << static_cast<IOType>(theValue));\n"
                             "}\n" )
            );

            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits>\n"
                             "std::basic_ostream<CharT,Traits>&\n"
                             "writeJSON( std::basic_ostream<CharT,Traits>&theStream, const bool &theValue, Indent )"),

                std::string( "{\n"
                             "\treturn theStream << (theValue ? \"true\" : \"false\" );\n"
                             "}\n")
            );

            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class CharIter>\n"
                             "std::basic_ostream<CharT,Traits>&\n"
                             "writeJSON( std::basic_ostream<CharT,Traits>&theStream, CharIter theStart, const CharIter theEnd, Indent )"),

                std::string( "{\n"
                             "\tusing CharTraits = ::babel::JSON::parser::CharTraits<CharT>;\n"
                             "\ttheStream << BABEL_JSON_PARSER_CHAR_LITERAL(CharT,'\"');\n"
                             "\twhile (theStart != theEnd) {\n"
                             "\t\tconst CharT theChar = CharTraits::adjust(*theStart++);\n"
                             "\t\tswitch(theChar) {\n"
                             "\t\tcase BABEL_JSON_PARSER_CHAR_LITERAL(CharT,'\\b'): theStream.write(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"\\\\b\"),2);  break;\n"
                             "\t\tcase BABEL_JSON_PARSER_CHAR_LITERAL(CharT,'\\t'): theStream.write(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"\\\\t\"),2);  break;\n"
                             "\t\tcase BABEL_JSON_PARSER_CHAR_LITERAL(CharT,'\\f'): theStream.write(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"\\\\f\"),2);  break;\n"
                             "\t\tcase BABEL_JSON_PARSER_CHAR_LITERAL(CharT,'\\n'): theStream.write(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"\\\\n\"),2);  break;\n"
                             "\t\tcase BABEL_JSON_PARSER_CHAR_LITERAL(CharT,'\\r'): theStream.write(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"\\\\r\"),2);  break;\n"
                             "\t\tcase BABEL_JSON_PARSER_CHAR_LITERAL(CharT,'\\\\'): theStream.write(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"\\\\\\\\\"),2); break;\n"
                             "\t\tcase BABEL_JSON_PARSER_CHAR_LITERAL(CharT,'\"') : theStream.write(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"\\\\\\\"\"),2); break;\n"
                             "\t\tdefault: {\n"
                             "\t\t\tif (std::isprint(theChar)) {\n"
                             "\t\t\t\ttheStream.put(theChar);\n"
                             "\t\t\t}\n"
                             "\t\t\telse {\n"
                             "\t\t\t\ttheStream.write(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"\\\\u00\"),4);\n"
                             "\t\t\t\ttheStream.put(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"0123456789ABCDEF\")[(theChar >> 4) & 0xFu]);\n"
                             "\t\t\t\ttheStream.put(BABEL_JSON_PARSER_STRING_LITERAL(CharT,\"0123456789ABCDEF\")[(theChar >> 0) & 0xFu]);\n"
                             "\t\t\t}\n"
                             "\t\t}\n"
                             "\t\tbreak;\n"
                             "\t\t}\n"
                             "\t}\n"
                             "\treturn theStream << BABEL_JSON_PARSER_CHAR_LITERAL(CharT,'\"');\n"
                             "}\n")
            );

            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits>\n"
                             "std::basic_ostream<CharT,Traits>&\n"
                             "writeJSON( std::basic_ostream<CharT,Traits>&theStream, const std::string &theValue, Indent theIndent)"),

                std::string( "{\n"
                             "\treturn writeJSON( theStream, &*theValue.begin(), &*theValue.end(), theIndent );\n"
                             "}\n")
            );

            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits>\n"
                             "std::basic_ostream<CharT,Traits>&\n"
                             "writeJSON( std::basic_ostream<CharT,Traits>&theStream, const std::wstring &theValue, Indent theIndent)"),

                std::string( "{\n"
                             "\treturn writeJSON( theStream, &*theValue.begin(), &*theValue.end(), theIndent );\n"
                             "}\n")
            );

            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class K, class V>\n"
                             "std::enable_if_t< std::is_same<std::remove_cv_t<K>,std::basic_string<CharT>>::value, std::basic_ostream<CharT,Traits>&>\n"
                             "writeJSON( std::basic_ostream<CharT,Traits>&theStream, const std::pair<K,V> &theValue, Indent theIndent )"),

                std::string( "{\n"
                             "\tconst auto theNextIndent = theIndent.next();\n"
                             "\n"
                             "\treturn writeJSON( (writeJSON( theStream, theValue.first, theNextIndent) << (theIndent.isActive() ? \" : \" : \":\")), theValue.second, theNextIndent );\n"
                             "}\n")
            );
            theBabelTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class K, class V>\n"
                             "std::enable_if_t< !std::is_same<std::remove_cv_t<K>,std::basic_string<CharT>>::value, std::basic_ostream<CharT,Traits>&>\n"
                             "writeJSON( std::basic_ostream<CharT,Traits>&theStream, const std::pair<K,V> &theValue, Indent theIndent )"),

                std::string( "{\n"
                             "\tstd::ostringstream theKeyStream;\n"
                             "\tconst auto         theNextIndent = theIndent.next();\n"
                             "\n"
                             "\tif (writeJSON( theKeyStream, theValue.first, Indent(0) )) {\n"
                             "\t\t(void)writeJSON( (writeJSON( theStream, theKeyStream.str(), theNextIndent) << (theIndent.isActive() ? \" : \" : \":\")), theValue.second, theNextIndent );\n"
                             "\t}\n"
                             "\telse {\n"
                             "\t\ttheStream.setstate(std::ios::failbit);\n"
                             "\t}\n"
                             "\treturn theStream;\n"
                             "}\n")
            );
        }
        
        for (const auto &theScopedTemplate : theTemplates) {
            ScopedGuard     theTemplateGuard( theOutputFile,
                                              std::string("have_") + theScopedTemplate.first + 
                                                (theSyntacticElement == SyntacticElements::Declaration ? "_declarations" : "_implementations"));
            ScopedNamespace theTemplateNamespace( theOutputFile,
                                                  StringToNamespace(theNameSpaceName));

            for (const auto &theTemplate : theScopedTemplate.second) {
                theTemplate.output(theOutputFile, theSyntacticElement );
            }
        }
    }
    
    static void addIndent( OutputFile &theOutputFile ) {
        ScopedGuard     theIndentClassGuard( theOutputFile, std::string("have_") + theNameSpaceName + "_Indent");
        ScopedNamespace theNameSpace( theOutputFile, StringToNamespace(theNameSpaceName));
   
        theOutputFile.printf(
            "class Indent {\n"
            "public:\n"
            "\texplicit Indent  ( unsigned int theStep )       __attribute__((weak));\n"
            "\tIndent   next    ()                       const __attribute__((weak));\n"
            "\tbool     isActive()                       const __attribute__((weak));\n"
            "\n"
            "\ttemplate <class CharT, class Traits>\n"
            "\tfriend std::basic_ostream<CharT,Traits>& operator <<( std::basic_ostream<CharT,Traits>&theStream, const Indent &theIndent );\n"
            "private:\n"
            "\tunsigned int itsStep;\n"
            "};\n"
            "\n"
            "inline Indent::Indent( unsigned int theStep ) : itsStep(theStep) {}\n"
            "\n"
            "inline Indent Indent::next() const { return Indent( itsStep ? (itsStep+1) : 0 ); }\n"
            "\n"
            "inline bool Indent::isActive() const { return itsStep != 0; }\n"
            "\n"
            "template <class CharT, class Traits>\n"
            "std::basic_ostream<CharT,Traits>& operator <<( std::basic_ostream<CharT,Traits> &theStream, const Indent &theIndent ) {\n"
            "\tstatic const char theIndentStep[] = \"\t\";\n"
            "\tif (theIndent.itsStep) {\n"
            "\t\ttheStream << std::endl;\n"
            "\t\tfor (auto i = 0u; i < (theIndent.itsStep-1); i++) {\n"
            "\t\t\ttheStream << theIndentStep;\n"
            "\t\t}\n"
            "\t}\n"
            "\treturn theStream;\n"
            "}"
        );
    }

class Generator : public GeneratorImplementationBase {
public:
    Generator() : GeneratorImplementationBase(
        "Json",
        {
            "memory",
            "type_traits",
            "algorithm",
            "ostream",
            "sstream",
            "cctype"
        },
        true) 
    {
    }

    void doRealize( const CompilationUnit &theCompilationUnit,
                    OutputFile            &theOutputFile,
                    const BabelcOptionSet &theOptions ) override final;

};


void Generator::doRealize( const CompilationUnit &theCompilationUnit,
                           OutputFile            &theOutputFile,
                           const BabelcOptionSet & ) {
    
    theOutputFile.printf("%s\n", theParser );

    addIndent(theOutputFile);
    
    std::set<std::string> theOptionals;
    
    {
        struct InterfaceFilter : public ReferenceFilter {
            bool breakAt( const Type *theType ) override {
                return (dynamic_cast<const InterfaceType*>(theType) != nullptr);
            }
        };
        
        Types           theTypeClosure;
        InterfaceFilter theInterfaceFilter;
        
        for (auto theType : theCompilationUnit.itsTypes) {
            theType.second->getItsReferredTypes(theTypeClosure, theInterfaceFilter);
        }
        BABEL_LOG("Type closure is:");

        for (auto theType : theTypeClosure) {
            BABEL_LOG("----------------------------------------------");
            BABEL_LOGF("%s", theType.second->toString("").c_str());
            if (recursive_cast<VariantType>(theType.second)) {
                (void)theOptionals.insert("boost::variant");
            }
            else if (recursive_cast<OptionalType>(theType.second)) {
                (void)theOptionals.insert("boost::optional");
            }
        }
        
        declareHelpers( theOutputFile, theTypeClosure );
    
        addPredefinedBabelTemplates( theOutputFile, SyntacticElements::Declaration, theOptionals );
        
        addTypeTemplates(theOutputFile, theTypeClosure, SyntacticElements::Declaration);
        addTypeTemplates(theOutputFile, theTypeClosure, SyntacticElements::Implementation);
    }
    addPredefinedBabelTemplates( theOutputFile, SyntacticElements::Implementation, theOptionals );
}

Generator theInstance;


//============================================================================================
//
// Hereafter are conditional unit test code
//
#ifdef BABEL_UNITTEST

class JSONUnitTest : public babel::unittest::UnitTest {
public:
    JSONUnitTest() : UnitTest("JSON") {}
protected:
    template <typename T, typename CharT> 
    void testFromString( const T theAnswer, const bool expectSuccess, const CharT *theImage ) {
        T          theAttempt;
        const bool theSuccess = babel::JSON::parser::isNumericValue(theAttempt, theImage);

        if (expectSuccess) {
            BABEL_UNITTEST_CHECK_EQUAL(theAnswer,theAttempt);
        }
        else {
            BABEL_UNITTEST_CHECK(!theSuccess);
        }
    }

    template <typename T, typename CharT> 
    void doArithmeticTests() {
        using Limits = std::numeric_limits<T>;
        static const T theValues[]= {
            Limits::lowest(), 
            Limits::lowest()+1, 
            0, 
            1,
            Limits::max()-1,
            Limits::max()
        };
        for (auto theValue : theValues  ) {
            std::basic_stringstream<CharT> theStream;
            theStream << theValue;
            testFromString(theValue, true, theStream.str().c_str());
        }
        if (Limits::is_signed) {
            std::basic_stringstream<CharT> theStream;
            theStream << Limits::lowest() << '0';
            testFromString(static_cast<T>(0), false, theStream.str().c_str());
        }
        {
            std::basic_stringstream<CharT> theStream;
            theStream << Limits::max() << '0';
            testFromString(static_cast<T>(0), false, theStream.str().c_str());
        }
    }
    template <typename CharT>
    void doTests() {
      using StringType = std::basic_string<std::remove_cv_t<std::remove_reference_t<CharT>>>;
        
      {
          doArithmeticTests<unsigned long,      CharT>();
          doArithmeticTests<signed long,        CharT>();
          doArithmeticTests<unsigned long long, CharT>();
          doArithmeticTests<signed long long,   CharT>();
          doArithmeticTests<std::uint64_t,      CharT>();
          doArithmeticTests<std::int64_t,       CharT>();
      }
      {
        std::basic_istringstream<CharT> theStream( BABEL_JSON_PARSER_STRING_LITERAL(CharT,"null") );
        auto theInput = babel::JSON::parser::makePositionedStream(theStream);

        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Null*>(babel::JSON::parser::readValue( theInput ).get()) );
      }

      {
        std::basic_istringstream<CharT> theStream( BABEL_JSON_PARSER_STRING_LITERAL(CharT,"true") );
        auto theInput = babel::JSON::parser::makePositionedStream(theStream);

        auto theValue = babel::JSON::parser::readValue( theInput );

        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Boolean*>(theValue.get()) );
        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Boolean*>(theValue.get())->itsValue );
      }

      {
        std::basic_istringstream<CharT> theStream( BABEL_JSON_PARSER_STRING_LITERAL(CharT,"false") );
        auto theInput = babel::JSON::parser::makePositionedStream(theStream);

        auto theValue = babel::JSON::parser::readValue( theInput );

        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Boolean*>(theValue.get()) );
        BABEL_UNITTEST_CHECK( !dynamic_cast<const babel::JSON::parser::Boolean*>(theValue.get())->itsValue );
      }

      {
        const StringType theAnswer( BABEL_JSON_PARSER_STRING_LITERAL(CharT,"Nisse Hult Was here") );
        std::basic_istringstream<CharT> theStream( BABEL_JSON_PARSER_STRING_LITERAL(CharT,"\"") + theAnswer + BABEL_JSON_PARSER_STRING_LITERAL(CharT,"\"") );
        auto theInput = babel::JSON::parser::makePositionedStream(theStream);

        auto theValue = babel::JSON::parser::readValue( theInput );

        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::String<StringType>*>(theValue.get()) );
        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::String<StringType>*>(theValue.get())->itsValue == theAnswer );
      }

      {
#define NUMPAIR(n) {  BABEL_JSON_PARSER_STRING_LITERAL(CharT,#n), n }

        static const struct { const CharT *theString; double theDouble; } theTestValues[] = {
          NUMPAIR( -1 ),
          NUMPAIR( 0 ),
          NUMPAIR( 0.0 ),
          NUMPAIR( 0.0e0 ),
          NUMPAIR( 42 ),
          NUMPAIR( 26.5 ),
          NUMPAIR( 2347.234e23 ),
          NUMPAIR( -42 ),
          NUMPAIR( -26.5 ),
          NUMPAIR( -2347.234e23 )
        };

        for (const auto &v : theTestValues) {
          std::basic_istringstream<CharT> theStream( v.theString );
          auto theInput = babel::JSON::parser::makePositionedStream(theStream);

          auto theValue = babel::JSON::parser::readValue( theInput );
      
          BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Number<StringType>*>(theValue.get()) );
          BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Number<StringType>*>(theValue.get())->template getNumericValue<double>() == v.theDouble);
        }
      }
      {
        std::basic_istringstream<CharT> theStream( BABEL_JSON_PARSER_STRING_LITERAL(CharT,"[]") );
        auto theInput = babel::JSON::parser::makePositionedStream(theStream);

        auto theValue = babel::JSON::parser::readValue( theInput );

        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Array*>(theValue.get()));
        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Array*>(theValue.get())->itsValue.size() == 0 );
      }
      {
        std::basic_istringstream<CharT> theStream( BABEL_JSON_PARSER_STRING_LITERAL(CharT,"[ 22 ]") );
        auto theInput = babel::JSON::parser::makePositionedStream(theStream);

        auto theValue = babel::JSON::parser::readValue( theInput );
        auto theArray = dynamic_cast<const babel::JSON::parser::Array*>(theValue.get());
        BABEL_UNITTEST_CHECK( theArray );
        BABEL_UNITTEST_CHECK( theArray->itsValue.size() == 1 );
        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Number<StringType>*>(theArray->itsValue[0].get())->template getNumericValue<double>() == 22 );
      }

      {
        std::basic_istringstream<CharT> theStream( BABEL_JSON_PARSER_STRING_LITERAL(CharT,"[ -1, 42 ]") );
        auto theInput = babel::JSON::parser::makePositionedStream(theStream);

        auto theValue = babel::JSON::parser::readValue( theInput );
        auto theArray = dynamic_cast<const babel::JSON::parser::Array*>(theValue.get());
        BABEL_UNITTEST_CHECK( theArray );
        BABEL_UNITTEST_CHECK( theArray->itsValue.size() == 2 );
        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Number<StringType>*>(theArray->itsValue[0].get())->template getNumericValue<double>() == -1 );
        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Number<StringType>*>(theArray->itsValue[1].get())->template getNumericValue<double>() == 42 );
      }

      {
        using Object = babel::JSON::parser::Object<StringType>;

        std::basic_istringstream<CharT> theStream( BABEL_JSON_PARSER_STRING_LITERAL(CharT,"{}") );
        auto theInput = babel::JSON::parser::makePositionedStream(theStream);

        auto theValue = babel::JSON::parser::readValue( theInput );

        BABEL_UNITTEST_CHECK( dynamic_cast<const Object*>(theValue.get()));
        BABEL_UNITTEST_CHECK( dynamic_cast<const Object*>(theValue.get())->itsValue.size() == 0 );
      }

      
      
      
      {
        using Object = babel::JSON::parser::Object<StringType>;

        std::basic_istringstream<CharT> theStream( BABEL_JSON_PARSER_STRING_LITERAL(CharT,"{ \"one\" : 1 }") );
        auto theInput = babel::JSON::parser::makePositionedStream(theStream);

        auto theValue  = babel::JSON::parser::readValue( theInput );
        auto theObject = dynamic_cast<const Object*>(theValue.get());
        BABEL_UNITTEST_CHECK( theObject );
        BABEL_UNITTEST_CHECK( theObject->itsValue.size() == 1 );

        auto theIter = theObject->itsValue.find(BABEL_JSON_PARSER_STRING_LITERAL(CharT,"one"));
        BABEL_UNITTEST_CHECK( theIter != theObject->itsValue.end() );
        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Number<StringType>*>(theIter->second.get() ) );
        BABEL_UNITTEST_CHECK( dynamic_cast<const babel::JSON::parser::Number<StringType>*>(theIter->second.get() )->template getNumericValue<double>() == 1 );
      }      
    }
    
    void run() override {
        doTests<char>();
        doTests<wchar_t>();
    }


};

JSONUnitTest theJSONUnitTest;

#endif

} } } }
