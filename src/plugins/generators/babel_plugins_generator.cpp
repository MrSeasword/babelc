/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 21, 2016
 */


#include <sys/stat.h>

#include "babel_plugins_generator.h"
#include "util/babel_logger.h"

namespace babel { namespace plugins { namespace generator {


std::map<std::string, GeneratorInterface *> GeneratorImplementationBase::ourGenerators;

char lastCharacterOf( const char * const theCString ) {
    const char *theRunner = theCString;
    
    while (*theRunner) { theRunner++; }
    
    if (theRunner == theCString) {
        throw std::runtime_error("Can't get last character of empty string!");
    }
    
    return *--theRunner;
}

std::string makeDefineConstant( std::string ofString ) {
    std::transform( BABEL_UTIL_RANGE(ofString), ofString.begin(),
                    []( char c ){ return (::isalnum(c)) ? ::toupper(c) : '_'; } );

    ofString.insert( 0, "__");
    ofString.insert( ofString.size(), "__");

    return ofString;
}

void ensureDirectoryExistence( const FilePath &theDirectory ) {
    if (!(theDirectory.empty() || theDirectory.exists())) {
        ensureDirectoryExistence(theDirectory.directoryPart());
        
        const auto creationStatus = mkdir(theDirectory.string().c_str(), S_IRWXU | S_IRGRP | S_IROTH);
        
        if ((creationStatus < 0) && (errno != EEXIST)) {
            throw std::runtime_error("Failed to create directory " + theDirectory.string() + ": " + strerror(errno));
        }
    }
}
FILE *openFile( const FilePath &thePath ) {
    ensureDirectoryExistence(thePath.directoryPart());
    
    FILE * const theStream = std::fopen( thePath.string().c_str(), "w");

    if (!theStream) {
        throw std::runtime_error(babel::util::format("Failed to open file %s", thePath.string().c_str()));
    }

    return theStream;
}

void writeFile( const FilePath &thePath, FILE *theStream, const void *theData, std::size_t theDataSize ) {
    if (std::fwrite( theData, 1, theDataSize, theStream) != theDataSize) {
        throw std::runtime_error( babel::util::format("Failed to write to %s", thePath.string().c_str()));
    }
}

void GeneratorImplementationBase::realize( const CompilationUnit &theCompilationUnit, 
                                           const BabelcOptionSet &theOptions, 
                                           const babel::FilePath &theOutputDirectory ) {
    BABEL_LOGF("This is %s generator running for file %s", getItsName().c_str(), theCompilationUnit.itsSourceFile.string().c_str());

    const auto theOutputPath = getFilePath(theCompilationUnit.itsSourceFile, theOutputDirectory);
    
    try {
        OutputFile theOutputFile( theOutputPath, theOptions );

        if (useIncludeGuard) {
            ScopedGuard theIncludeGuard( theOutputFile, theOutputPath.string() );

            for (const auto &theIncludeFile : itsIncludeFiles) {
                theOutputFile.printf("#include <%s>\n", theIncludeFile.c_str());
            }

            theOutputFile.printf("\n");
            theOutputFile.printf("#include \"%s\"\n\n", theCompilationUnit.itsSourceFile.string().c_str());

            doRealize(theCompilationUnit, theOutputFile, theOptions );
        }
        else {
            doRealize(theCompilationUnit, theOutputFile, theOptions );
        }
    }
    catch (...) {
        if (theOutputPath.exists()) {
            theOutputPath.remove();
        }
        throw;
    }
}

bool GeneratorImplementationBase::needsRealization( const BabelcOptionSet   &, 
                                                    const FilePath          &theInputFile, 
                                                    const FilePath          &theOutputDirectory, 
                                                    const FilePaths         &theDependentFiles ) {
    
    const auto theOutputPath = getFilePath(theInputFile, theOutputDirectory);

    if (theOutputPath.exists()) {
        const auto  theOutputTimeStamp    = theOutputPath.getModifiedTime();
        std::size_t theNumberOfNewerFiles =  0;
        
        for (const auto &theFile : theDependentFiles) {
            if (theOutputTimeStamp <= theFile.getModifiedTime()) {
                BABEL_LOGF( "Need to realize %s because %s is newer", theOutputPath.string().c_str(), theFile.string().c_str());
                theNumberOfNewerFiles += 1;
            }
        }
        
        if (theNumberOfNewerFiles == 0) {
            BABEL_LOGF( "No need to realize %s because it is up to date", theOutputPath.string().c_str());
        }
        
        return (theNumberOfNewerFiles > 0);
    }
    else {
        BABEL_LOGF( "Need to realize %s because it does not exist", theOutputPath.string().c_str());
        return true;
    }
}

std::string GeneratorImplementationBase::formatTypeWarning(const Type& theType, const std::string &thePrefix, const std::string& theMessage) {
    return util::format( "%s: warning: %s%s %s", theType.itsLocation.c_str(), thePrefix.c_str(), theType.itsName.c_str(), theMessage.c_str() );
}

void GeneratorImplementationBase::reportTypeWarning(const Type& theType, const std::string &thePrefix, const std::string& theMessage) {
    report( formatTypeWarning(theType, thePrefix, theMessage.c_str() ) );
}

void GeneratorImplementationBase::report(const std::string& theMessage) {
    (void)fprintf( stderr, "%s\n", theMessage.c_str() );
}

void GeneratorImplementationBase::reportTypeError(const Type& theType, const std::string& theMessage) {
    (void)fprintf(stderr, "%s: error: %s %s\n", theType.itsLocation.c_str(), theType.itsName.c_str(), theMessage.c_str() );
    throw std::runtime_error("C++ code does not follow babelc rules for the chosen generator.");
}

FilePath GeneratorImplementationBase::getFilePath( const babel::FilePath &theSourceFile,
                                                   const babel::FilePath &inOutputDirectory  ) {

    return getItsFilePath(FilePath( inOutputDirectory, 
                                    theSourceFile.sansDirectory().replace_extension( ".TEMP" + theSourceFile.extension() ).string() ));
}

FilePath GeneratorImplementationBase::getItsFilePath( const FilePath &basedUponThisPath ) const {
    std::string     theLowerCasedName( itsName );

    std::transform(BABEL_UTIL_RANGE(theLowerCasedName), theLowerCasedName.begin(), ::tolower );

    return basedUponThisPath.sansExtension().replace_extension(("." + theLowerCasedName + basedUponThisPath.extension()));
}
//=========================================================================================

GeneratorImplementationBase::GeneratorImplementationBase( const std::string &theName,
                                                             const std::vector<std::string> &theIncludeFiles,
                                                             bool                            useIncludeGuard_) 
: itsName(theName), itsIncludeFiles(theIncludeFiles), useIncludeGuard(useIncludeGuard_)  {
    if ( ! ourGenerators.insert(std::make_pair( itsName, this )).second) {
        throw std::invalid_argument(babel::util::format("Internal: Duplicate generator '%s' added", itsName.c_str()));
    }
}

//=========================================================================================

void GeneratorImplementationBase::declareHelpers(OutputFile& theOutputFile, const Types &forTheTypes ) {
    
    Types allTypes;
    {
        struct NoFilter : public ReferenceFilter {
            bool breakAt( const Type * ) override {
                return false;
            }
        } thePassAllFilter;

        for (const auto &theType : forTheTypes) {
            theType.second->getItsReferredTypes(allTypes, thePassAllFilter);
        }
    }
    
    {
        ScopedGuard     theHelperGuard( theOutputFile, "babel_helpers_first");
        ScopedNamespace theBabelNamespace( theOutputFile, StringToNamespace("babel") );

        theOutputFile.printf(
            "template < class T > struct is_supported_ptr_helper                     : std::false_type {};\n"
            "template < class T > struct is_supported_ptr_helper<std::shared_ptr<T>> : std::true_type {};\n"
            "template < class T > struct is_supported_ptr_helper<std::unique_ptr<T>> : std::true_type {};\n"
            "template < class T > struct is_supported_ptr                            : is_supported_ptr_helper< std::remove_cv_t<T> > {};\n"
            "\n"
            "template < class T > struct is_supported_container_helper               : std::false_type {};\n"
            "template < class T > struct is_map_container_helper                     : std::false_type {};\n"
            "template < class T > struct is_fixed_container_helper                   : std::false_type {};\n" );
    }
    
    {
        const struct Specialization {
            const char *theTypeName;
            const char *P1;
            const char *P2;
            const char *theIncludeFile;
            const char *attributes[3];
        } theSpecializations[]  = {
            { "std::array",              "class T", "size_t S", "array",         { "supported", "fixed", nullptr }},
            { "std::vector",             "class T", nullptr,    "vector",        { "supported", nullptr, nullptr }},
            { "std::deque",              "class T", nullptr,    "deque",         { "supported", nullptr, nullptr }},
            { "std::list",               "class T", nullptr,    "list",          { "supported", nullptr, nullptr }},
            { "std::forward_list",       "class T", nullptr,    "forward_list",  { "supported", nullptr, nullptr }},
            { "std::set",                "class T", nullptr,    "set",           { "supported", nullptr, nullptr }},
            { "std::multiset",           "class T", nullptr,    "set",           { "supported", nullptr, nullptr }},
            { "std::unordered_set",      "class T", nullptr,    "unordered_set", { "supported", nullptr, nullptr }},
            { "std::unordered_multiset", "class T", nullptr,    "unordered_set", { "supported", nullptr, nullptr }},
            { "std::map",                "class K", "class V",  "map",           { "supported", nullptr, "map"   }},
            { "std::multimap",           "class K", "class V",  "map",           { "supported", nullptr, "map"   }},
            { "std::unordered_map",      "class K", "class V",  "unordered_map", { "supported", nullptr, "map"   }},
            { "std::unordered_multimap", "class K", "class V",  "unordered_map", { "supported", nullptr, "map"   }}
        };
        
        std::set<std::string> theIncludedFiles;
        
        for (const auto &theSpecialization : theSpecializations) {
            if (std::find_if( BABEL_UTIL_RANGE(allTypes), 
                              [&theSpecialization](const Types::value_type &v) {
                                  const auto theContainer = recursive_cast<babel::Container>(v.second);
                                  return theContainer && (theContainer->getItsStlType() == theSpecialization.theTypeName);
                              }) != allTypes.end()) {
                ScopedGuard theHelperGuard( theOutputFile, std::string("babel_helpers_") + theSpecialization.theTypeName);
                
                if (theIncludedFiles.find(theSpecialization.theIncludeFile) == theIncludedFiles.end()) {
                    theOutputFile.printf("#include <%s>\n\n", theSpecialization.theIncludeFile);
                    theIncludedFiles.insert(theSpecialization.theIncludeFile);
                }
                
                ScopedNamespace theBabelNamespace( theOutputFile, StringToNamespace("babel") );
                
                for (auto theAttribute : theSpecialization.attributes) {
                    if (theAttribute) {
                        theOutputFile.printf( "template < %s", theSpecialization.P1 );
                        if (theSpecialization.P2) {
                            theOutputFile.printf( ", %s ", theSpecialization.P2);
                        }
                        theOutputFile.printf( "> struct is_%s_container_helper<%s<%c", 
                                              theAttribute, theSpecialization.theTypeName, lastCharacterOf(theSpecialization.P1));
                        if (theSpecialization.P2) {
                            theOutputFile.printf(",%c", lastCharacterOf(theSpecialization.P2));
                        }
                        theOutputFile.printf(">> : std::true_type {};\n");
                    }
                }
            }
            else {
                BABEL_LOGF("No include needed for %s. std::array = %d, FixedArray = %d",
                            theSpecialization.theTypeName,
                            (std::string("std::array") == theSpecialization.theTypeName),
                            (std::find_if(BABEL_UTIL_RANGE(allTypes), [](const Types::value_type &v){return recursive_cast<FixedArray>(v.second);}) != allTypes.end())
                            );
            }
        }
    }
    {
        ScopedGuard     theHelperGuard( theOutputFile, "babel_helpers_last");
        ScopedNamespace theBabelNamespace( theOutputFile, StringToNamespace("babel") );

        theOutputFile.printf(
            "template < class T >  struct is_supported_container : is_supported_container_helper< std::remove_cv_t<T>> {};\n"
            "template < class T >  struct is_fixed_container     : is_fixed_container_helper< std::remove_cv_t<T>> {};\n"
            "template < class T >  struct is_map_container       : is_map_container_helper< std::remove_cv_t<T>> {};\n"
            "\n"
            "using std::is_arithmetic;\n"
            "\n"
            "template < class T >\n"
            "using is_supported = std::conditional<\n"
            "\t\t( is_arithmetic<T>::value | is_supported_ptr<T>::value | is_supported_container<T>::value),\n"
            "\t\t  std::true_type, std::false_type\n"
            "\t>;\n" 
        );
    }
    
}
//=========================================================================================

GeneratorInterface::Container GeneratorImplementationBase::getGenerators( const std::set<std::string>  * const theGeneratorNames ) {
    GeneratorInterface::Container theGenerators;

    if (theGeneratorNames == nullptr) {
        for (auto theGenerator : ourGenerators ) {
            theGenerators.push_back( theGenerator.second );
        }
    }
    else {
        std::string theInvalidNames;

        for (auto theName : *theGeneratorNames) {
            auto const theGenerator = ourGenerators.find( theName );
            if (theGenerator != ourGenerators.end()) {
                theGenerators.push_back( theGenerator->second);
            }
            else {
                theInvalidNames = theInvalidNames + (theInvalidNames.empty() ? "" : ", ") + theName;
            }
        }

        if (!theInvalidNames.empty()) {
            throw std::invalid_argument("Unknown code generators given: " + theInvalidNames );
        }
    }
    return theGenerators;
}

OutputFile::OutputFile(const FilePath& thePath, const BabelcOptionSet theOptions)
: itsFilePath(thePath),
  itsIndentChar((theOptions.find(BabelcOptions::UseTabs) != theOptions.end()) ? '\t' : ' '),
  itsOutputFile(openFile(thePath)) {
}

OutputFile::~OutputFile() {

    if (std::fclose(itsOutputFile)) {
        throw std::runtime_error(babel::util::format("Failed to close file %s", itsFilePath.string().c_str()));
    }
}

void OutputFile::printf( const char *theFormat, ...) {

    va_list theArguments;
    va_start( theArguments, theFormat );

    const auto theResult = babel::util::vformat( theFormat, theArguments );
    
    auto theStart = theResult.begin();

    while (theStart != theResult.end()) {
        auto       theEnd      = std::find_if( theStart, theResult.end(), [](char theChar){ return (theChar == '\t') || (theChar == '\n'); } );
        const auto theLastChar = ((theEnd == theResult.end()) ? '\0' : *theEnd);

        if (lastCharWasNewLine && (*theStart != '\n')) {
            writeFile( itsFilePath, itsOutputFile, &itsIndent[0], itsIndent.size() );
        }
        
        lastCharWasNewLine = (theLastChar == '\n');
        
        if (lastCharWasNewLine || ((theLastChar == '\t') && (itsIndentChar != ' '))) {
            ++theEnd; // include NL or TAB if wanted
        }
        
        writeFile( itsFilePath, itsOutputFile, &(*theStart), (theEnd - theStart) );
        
        if ((theLastChar == '\t') && (itsIndentChar == ' ')) {
            for (std::size_t i = theIndentStep(); i != 0; i--) {
                writeFile( itsFilePath, itsOutputFile, &itsIndentChar, 1 );
            }
            ++theEnd; // Skip the TAB char since it is expanded
        }
        
        theStart = theEnd;
    }
}

void OutputFile::increaseIndent() { itsIndent.insert(itsIndent.end(), theIndentStep(), itsIndentChar); }
void OutputFile::decreaseIndent() { itsIndent.erase( 0, theIndentStep() ); }

ScopedNamespace::ScopedNamespace( OutputFile &theOutputFile, const Namespace &theNamespace )
: itsOutputFile(theOutputFile),
  itsNamespace(theNamespace) {
    itsOutputFile.printf("#if __cplusplus < 201700L\n");
    
    for (auto theName : itsNamespace ) {
      itsOutputFile.printf( "namespace %s { ", theName.c_str() );
    }
    
    itsOutputFile.printf("\n#else\n");
    
    {    
        const auto &theCpp17Namespace = NamespaceToString(theNamespace);
    
        itsOutputFile.printf( "namespace %s {\n", theCpp17Namespace.substr( 2, theCpp17Namespace.length() - 4 ).c_str() );
    }
    
    itsOutputFile.printf("#endif\n\n");
    itsOutputFile.increaseIndent();
}

ScopedNamespace::~ScopedNamespace() {
    itsOutputFile.decreaseIndent();
    itsOutputFile.printf("\n#if __cplusplus < 201700L\n");
    itsOutputFile.printf("%s\n", std::string( itsNamespace.size(), '}' ).c_str() );
    itsOutputFile.printf("#else\n");
    itsOutputFile.printf("}\n" );
    itsOutputFile.printf("#endif\n\n");
}


ScopedType::ScopedType( OutputFile& theOutputFile,
                        const Type& theType,
                        const std::string theGuardSuffix )
: itsOutputFile(theOutputFile),
  itsIncludeGuard(itsOutputFile, "HAVE_" + theType.itsName + theGuardSuffix),
  itsNamespace(itsOutputFile, theType.getItsNamespace()) {
}

ScopedType::~ScopedType() {
}

ScopedGuard::ScopedGuard( OutputFile &theOutputFile, const std::string &theName )
: itsOutputFile(theOutputFile) {

    auto const theGuard = makeDefineConstant(theName);

    itsOutputFile.printf("\n#ifndef %s\n",   theGuard.c_str());
    itsOutputFile.printf("#define %s\n\n", theGuard.c_str());
}

ScopedGuard::~ScopedGuard() {
    itsOutputFile.printf( "\n#endif\n" );
}

}
}
}

