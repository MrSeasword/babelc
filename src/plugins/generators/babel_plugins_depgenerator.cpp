/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Poppy Day, 2017
 */
#include <cstdio>
#include <vector>
#include <map>
#include <numeric>
#include <util/babel_logger.h>

#include "babel_plugins_generator.h"

namespace babel { namespace plugins { namespace generator { namespace dep {

class Generator : public GeneratorImplementationBase {
public:
    Generator() : GeneratorImplementationBase("Dep", {}, false ) {}

    void doRealize( const CompilationUnit &theCompilationUnit,
                    OutputFile            &theOutputFile,
                    const BabelcOptionSet &theOptions ) override final;

};
  
void Generator::doRealize( const CompilationUnit &theCompilationUnit,
                           OutputFile            &theOutputFile,
                           const BabelcOptionSet & ) {
    
    std::string thePossibleTargets;
    
    for (auto theGenerator : getGenerators(nullptr)) {
        if (theGenerator != this) {
            theOutputFile.printf("%s ", theGenerator->getItsFilePath(theOutputFile.itsFilePath).string().c_str());
        }
    }
    
    theOutputFile.printf(":");
    
    for (const auto theDepencency : theCompilationUnit.itsDependentFiles) {
        theOutputFile.printf(" \\\n\t%s", theDepencency.string().c_str());
    }
    
    theOutputFile.printf("\n");
    
}

Generator theInstance;

}}}}
