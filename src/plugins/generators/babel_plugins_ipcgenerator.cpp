/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA 
 * or see <http://www.gnu.org/licenses/>. 
 */

#include <cstdio>
#include <vector>
#include <map>
#include <iterator>
#include <util/babel_logger.h>
#include <assert.h>
#include <cstdio>

#include "babel_plugins_generator.h"

namespace babel { namespace plugins { namespace generator { namespace ipc {
    const auto theNameSpaceName   = "babel::ipc";
    const auto theTupleViewSuffix = std::string("_TupleView");
    const auto theArrayViewSuffix = std::string("_ArrayView");
    
    enum class SyntacticElements {
        Declaration,
        Implementation
    };
    
    class CodeTemplate {
        std::string itsDeclaration;
        std::string itsBody;
        
    public:
        CodeTemplate( const std::string theDeclaration, const std::string theBody ) 
        : itsDeclaration(theDeclaration),
          itsBody(theBody) {
        }
        bool hasImplementation() const { return !itsBody.empty(); }
        
        void output( OutputFile &toFile, SyntacticElements theSyntacticElement ) const {

            if (theSyntacticElement == SyntacticElements::Declaration) {
                toFile.printf("%s;\n\n", itsDeclaration.c_str() );
            }
            else  if (hasImplementation()) {
                const auto theAttributePosition = itsDeclaration.find("__attribute__");
                toFile.printf("%s %s\n", 
                              itsDeclaration.substr(0, theAttributePosition).c_str(), 
                              itsBody.c_str());
            }
        }
    };

    using CodeTemplates = std::vector<CodeTemplate >;

    static bool isValueConvertibleType( const Type * theType ) {
        const auto thePointer = recursive_cast<PointerType>(theType);
        
        return (thePointer && isValueConvertibleType(thePointer->itsReferredType)) ||
                recursive_cast<EnumeratedType>(theType) ||
                recursive_cast<StdString>(theType)     ||
                (theType->getItsBaseType()->itsName == "bool");
    }
    
    bool isComplicatedType( const Type *theType) {
        const auto theIpcType = theType->getAttributeValue("ipc_type");
        
        if (theIpcType) {
            return true;
        }
        else if (is_any_of<StructType,PointerType,VariantType,OptionalType>(theType)) {
            return true;
        }
        else if (const auto theContainer = recursive_cast<Container>(theType)) {
            
            if (const auto theArray = recursive_cast<FixedArray>(theContainer)) {
                return isComplicatedType(theArray->itsElementType);
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }

    void declareIpcHelpers(OutputFile& theOutputFile, const Types & ) {
        BABEL_LOG_SCOPE();
        
        ScopedGuard     theHelperGuard( theOutputFile,
                                          std::string("have_") + theNameSpaceName + "_helpers" );
        ScopedNamespace theHelperNamespace( theOutputFile, StringToNamespace(theNameSpaceName) );
        theOutputFile.printf("%s",
            "//======================================================================\n"
            "// Templates for providing a value view of an array\n"
            "//\n"
            "template<std::size_t N, typename Enum, std::size_t... Indices> \n"
            "inline auto ArrayValue( const std::array<std::enable_if_t<std::is_enum<Enum>::value,Enum>,N>& theArray, std::index_sequence< Indices...>&& ) {\n"
            "\tstatic_assert( sizeof(theArray)   == sizeof(std::array<std::underlying_type_t<Enum>,N>), \"Woops, something wrong with enum array size\" );\n"
            "\tstatic_assert( sizeof(theArray[0]) == sizeof(std::underlying_type_t<Enum>), \"Woops, something wrong with enum size\" );\n"
            "\n"
            "\treturn reinterpret_cast<const std::array<std::underlying_type_t<Enum>,N>&>(theArray);\n"
            "}\n"
            "\n"
            "template<std::size_t N, typename, std::size_t... Indices> \n"
            "auto ArrayValue( const std::array<std::string,N>& theArray, std::index_sequence< Indices...>&& ) {\n"
            "\treturn std::array<const char *, N>{ theArray[Indices].c_str()... };\n"
            "}\n"
            "\n"
            "template<std::size_t N, typename Boolean, std::size_t... Indices> \n"
            "auto ArrayValue( const std::array<bool,N>& theArray, std::index_sequence< Indices...>&& ) {\n"
            "\treturn std::array<Boolean, N>{ static_cast<Boolean>(theArray[Indices])... };\n"
            "}\n"
            "\n"
            "template <typename E, std::size_t N, typename B = int> \n"
            "inline auto ArrayValue( const std::array<E,N>& theArray, B = B{}) {\n"
            "\treturn ArrayValue<N,B>( theArray, std::make_index_sequence<N>() );\n"
            "}\n"
            "\n"
            "//======================================================================\n"
            "// Templates for references of some types\n"
            "//\n"
            "template <typename Boolean>\n"
            "struct BooleanReference {\n"
            "\tbool    &itsValue;\n"
            "\tBoolean  itsValueView;\n"
            "\tBooleanReference( bool &theValue ) : itsValue{theValue},itsValueView{static_cast<Boolean>(theValue)} {}\n"
            "\t~BooleanReference() { itsValue = (itsValueView != Boolean{}); }\n"
            "\tauto operator&() { return &itsValueView; }\n"
            "};\n"
            "\n"
            "template <typename Enum, void (*validate) ( Enum &, Enum, std::string& ) > \n"
            "struct EnumReference {\n"
            "\tusing ViewType = std::underlying_type_t<Enum>;\n"
            "\tEnum        &itsValue;\n"
            "\tstd::string &itsErrorString;\n"
            "\tViewType     itsValueView;\n"
            "#pragma GCC diagnostic push\n"
            "#pragma GCC diagnostic ignored \"-Wmaybe-uninitialized\"\n"
            "\tEnumReference( Enum &theValue, std::string &theErrorString ) : itsValue{theValue}, itsErrorString(theErrorString), itsValueView{static_cast<ViewType>(theValue)} {}\n"
            "#pragma GCC diagnostic pop\n"
            "\t~EnumReference() { validate(itsValue, static_cast<Enum>(itsValueView), itsErrorString ); }\n"
            "\tauto operator&() { return &itsValueView; }\n"
            "};\n"
            "\n"
            "struct StringReference {\n"
            "\tstd::string &itsValue;\n"
            "\tconst char  *itsValueView;\n"
            "\tStringReference(std::string &theValue) : itsValue{theValue}, itsValueView{theValue.c_str()} {}\n"
            "\t~StringReference() { if (itsValue.c_str() != itsValueView) itsValue = itsValueView; }\n"
            "\tauto operator&() { return &itsValueView; }\n"
            "};\n"
            "\n"
            "\n"
            "//======================================================================\n"
            "// Templates for references to array values\n"
            "//\n"
            "template <typename ValueType, typename ReferenceType, std::size_t N, std::size_t... Indices>\n"
            "struct ArrayReference {\n"
            "\tstd::array<ReferenceType,  N> itsArrayView;\n"
            "\n"
            "\tArrayReference( std::array<ValueType,N>& theArray ) \n"
            "\t  : itsArrayView{theArray[Indices]...} {\n"
            "\t}\n"
            "\n"
            "\tauto operator&() { return std::array<decltype(&itsArrayView[0]),N>{&itsArrayView[Indices]...}; }\n"
            "};\n"
            "\n"
            "template <typename Enum, void (*validate) ( Enum &, Enum, std::string& ), std::size_t N >\n"
            "struct EnumArrayReference {\n"
            "\tusing UnderlyingArrayType = std::array<std::underlying_type_t<Enum>,N>;\n"
            "\tUnderlyingArrayType &itsUnderlyingArray;\n"
            "\tstd::string         &itsErrorString;\n"
            "\n"
            "\tEnumArrayReference(std::array<Enum, N> &theArray, std::string &theErrorString )\n"
            "\t\t: itsUnderlyingArray(reinterpret_cast<UnderlyingArrayType&>(theArray)),itsErrorString(theErrorString) {\n"
            "\t}\n"
            "\tauto operator &() { return &itsUnderlyingArray; }\n"
            "\t~EnumArrayReference() {\n"
            "\tEnum notUsed; for (auto v : itsUnderlyingArray ) { validate( notUsed, static_cast<Enum>(v), itsErrorString ); }\n"
            "\t}\n"
            "};\n"
            "\n"
            "template< typename ValueType, typename ReferenceType, std::size_t N, std::size_t... Indices > \n"
            "inline auto MakeArrayReferenceImpl( std::array<ValueType,N>& theArray, std::index_sequence<Indices...> ) {\n"
            "\treturn ArrayReference< ValueType, ReferenceType, N, Indices... >( theArray );\n"
            "}\n"
            "\n"
            "template <typename Boolean, std::size_t N, typename Indices = std::make_index_sequence<N> > \n"
            "inline auto MakeBooleanArrayReference( std::array<Boolean,N>& theArray ) {\n"
            "\treturn MakeArrayReferenceImpl< bool, BooleanReference< Boolean >>( theArray, Indices() );\n"
            "}\n"
            "\n"
            "template <typename Enum, void (*validate)( Enum &, Enum, std::string& ), std::size_t N >\n"
            "inline auto MakeEnumArrayReference( std::array<Enum,N>& theArray, std::string &theErrorString ) {\n"
            "\treturn EnumArrayReference<Enum,validate,N>( theArray, theErrorString );\n"
            "}\n"
            "\n"
            "template< std::size_t N, typename Indices = std::make_index_sequence<N> > \n"
            "inline auto MakeStringArrayReference( std::array<std::string,N>& theArray ) {\n"
            "\treturn MakeArrayReferenceImpl<std::string, StringReference>( theArray, Indices() );\n"
            "}\n"
            "\n"
        );
    }

    static std::shared_ptr<const std::string> getSpecifiedIpcType( const Type *forTheType ) {
        const auto theIpcType = forTheType->getAttributeValue("ipc_type");
        
        return theIpcType ? std::make_shared<std::string>("typename IpcPolicy::" + *theIpcType) : theIpcType;
    }
    
    static std::string makeInitValue( const Type *theType ) {
        const auto thePointer = recursive_cast<PointerType>(theType);
        
        return thePointer ?
            util::format("(std::make_%s<%s>())",
                                (thePointer->isShared ? "shared" : "unique"),
                                 thePointer->itsReferredType->itsName.c_str())
            :
            std::string("");
    }

    template <typename Iterator >
    std::string getArgumentsImage( Iterator             theArgumentBegin,
                                   Iterator             theArgumentEnd,            
                                   const std::string   &theArgumentsPrefix,
                                   const std::string   &theDelimiter,
                                   const std::string   &theIndent,
                                   std::string (*theTransformer)( const Type*, const std::string&, const std::string&));
    struct ArgumentUpdate {
        std::string itsCode;
        bool        needsWarningSuppression;
    };

    template <typename Iterator>
    ArgumentUpdate updateArguments(Iterator           theArgumentStart,
                                Iterator           theArgumentEnd,
                                const std::string &theArgumentsPrefix,
                                const std::string &theAccessorMethod,
                                const std::string &theMessageArgument,
                                const std::string &theIndent,
                                std::string (*theTransformer)( const Type*, const std::string&, const std::string),
                                std::string (*theIndirector)( const std::string &, const Type*, const std::string&, const std::string& ));
    
    std::string complicatedTypeIO(const std::string &theBaseCall, const std::string &theMessageName, const Type *theType, const std::string &theExpression, const std::string &theIndent) {
        if (const auto theIpcType = theType->getAttributeValue("ipc_type")) {
            return util::format("static_assert( IpcPolicy::%2$s::template Compatible<typename %1$s>::value, \"You cant use type %1$s as a IpcPolicy::%2$s\");\n"
                                "%3$sIpcPolicy::%2$s::%6$s( %4$s, %5$s )", 
                                theType->itsName.c_str(),
                                theIpcType->c_str(), 
                                theIndent.c_str(),
                                theMessageName.c_str(), 
                                theExpression.c_str(),
                                theBaseCall.c_str());
        }
        else if (is_any_of<StructType,Container,PointerType,VariantType>(theType)) {
            return util::format( "%1$s%5$s<IpcPolicy, typename IpcPolicy::Boolean>(\n%4$s\t%2$s,\n%4$s\t%3$s\n%4$s)",
                                  NamespaceToString(theType->getItsNamespace()).c_str(),
                                  theMessageName.c_str(),
                                  theExpression.c_str(),
                                  theIndent.c_str(),
                                  theBaseCall.c_str());
        }
        else {
            return "";
        }
    }
    std::string complicatedTypeReader( const std::string &theMessageName, const Type *theType, const std::string &theExpression, const std::string &theIndent ) {
        return complicatedTypeIO("IpcReadContainer", theMessageName, theType, theExpression, theIndent);
    }
    
    std::string complicatedTypeWriter( const std::string &theMessageName, const Type *theType, const std::string &theExpression, const std::string &theIndent  ) {
        return complicatedTypeIO("IpcWriteContainer", theMessageName, theType, theExpression, theIndent);
    }
    
    static std::string asIpcTemplateTypeName( const Type *theType );
    
    static std::string asIpcValueTypeName( const Type *theType ) {
        BABEL_LOG_SCOPE();

        std::shared_ptr<const std::string> theResult;
        
        if (const auto theIpcType = getSpecifiedIpcType(theType)) {
            theResult = theIpcType;
        }
        else if (theType->getItsBaseType()->itsName == "bool") {
            theResult = std::make_shared<std::string>("Boolean");
        }
        else if (recursive_cast<StdString>(theType)) {
            theResult = std::make_shared<std::string>("const char *");
        }
        else if (auto const theEnum = recursive_cast<EnumeratedType>(theType)) {
            theResult = std::make_shared<std::string>( theEnum->itsUnderlyingType->itsName );
        }
        else if (auto const theOptional = recursive_cast<OptionalType>(theType)) {
            theResult = std::make_shared<std::string>(util::format( "std::array<%s,1>", asIpcTemplateTypeName(theOptional->itsElementType).c_str() ));
        }
        else {
            theResult = std::make_shared<std::string>(theType->itsName);
        }

        BABEL_LOGF("Returning %s as %s", theType->itsName.c_str(), theResult->c_str());
        
        return *theResult;
    }
    
    static std::string asIpcTemplateTypeName( const Type *theType ) {
        BABEL_LOG_SCOPE();

        std::shared_ptr<const std::string> theResult;
        
        if (const auto theIpcType = getSpecifiedIpcType(theType)) {
            theResult =  theIpcType;
        }
        else if (const auto thePointer = recursive_cast<PointerType>(theType)) {
            theResult = std::make_shared<std::string>(asIpcTemplateTypeName(thePointer->itsReferredType));
        }
        else if (recursive_cast<StructType>(theType)) {
            theResult = std::make_shared<std::string>(theType->itsName + theTupleViewSuffix + "<Boolean,IpcPolicy>");
        }
        else if (recursive_cast<VariantType>(theType)) {
            theResult = std::make_shared<std::string>("typename IpcPolicy::VariantType");
        }
        else if (recursive_cast<OptionalType>(theType)) {
            theResult = std::make_shared<std::string>(asIpcValueTypeName(theType));
        }
        else if (recursive_cast<MapContainer>(theType)) {
            theResult = std::make_shared<std::string>( util::format("%s%s<Boolean,IpcPolicy>", theType->itsName.c_str(), theArrayViewSuffix.c_str()));
        }
        else {
            const auto theContainer = recursive_cast<Container>(theType);
            
            if (theContainer && (isValueConvertibleType(theContainer->itsElementType) ||
                                recursive_cast<MapContainer>(theContainer->itsElementType) ||
                                recursive_cast<VariantType>(theContainer->itsElementType) ||
                                recursive_cast<StructType>(theContainer->itsElementType))) {
                theResult = std::make_shared<std::string>(theType->itsName + theArrayViewSuffix + "<Boolean,IpcPolicy>");
            }
            else {
                theResult = std::make_shared<std::string>(asIpcValueTypeName(theType));
            }
        }

        BABEL_LOGF("Returning %s as %s", theType->itsName.c_str(), theResult->c_str());
        
        return *theResult;
    }
    
    template <typename Iterator>
    std::string asIpcTemplateTypeNames( Iterator theBegin, Iterator theEnd ) {
        std::string theNames;
        while (theBegin != theEnd) {
            if (!theNames.empty()) {
                theNames.append(", ");
            }
            theNames.append( asIpcTemplateTypeName((theBegin++)->itsType) );
        }
        return theNames;
    }
    
    static std::string asIpcArrayValue( const Type *theArrayElementType, const std::string &theExpression, std::string) {
        BABEL_LOG_SCOPE();
        if(const auto theSpecifiedType = getSpecifiedIpcType(theArrayElementType)) {
            return util::format( "std::array<%s,1>{}", theSpecifiedType->c_str() );
        }
        else if (recursive_cast<StdString>(theArrayElementType)) {
            return util::format( "%s::ArrayValue( %s )", theNameSpaceName, theExpression.c_str() );
        }
        else if (const auto theEnum = recursive_cast<EnumeratedType>(theArrayElementType)) {
                return util::format( "%s::ArrayValue( %s, %s{} )", theNameSpaceName, theExpression.c_str(), theEnum->itsName.c_str() );
        }
        else if (theArrayElementType->getItsBaseType()->itsName == "bool") {
            return util::format( "%s::ArrayValue( %s, Boolean{} )", theNameSpaceName, theExpression.c_str() );
        }
        else if (const auto theStruct = recursive_cast<StructType>(theArrayElementType)) {
            return util::format( "std::array<%s%s<Boolean,IpcPolicy>,1>{}", theStruct->itsName.c_str(), theTupleViewSuffix.c_str());
        }
        else if (const auto theVariant = recursive_cast<VariantType>(theArrayElementType)) {
            return util::format( "std::array<%s,1>{}", asIpcTemplateTypeName(theVariant).c_str());
        }
        else {
            return theExpression;
        }
    }
    
    static std::string asIpcValue( const Type *theType, const std::string &theExpression, std::string theIndent = "" ) {
        BABEL_LOG_SCOPE();
        
        if (const auto theIpcType = getSpecifiedIpcType(theType)) {
            return *theIpcType + "(" + theExpression + ")";
        }
        else if (theType->getItsBaseType()->itsName == "bool") {
            return "static_cast<Boolean>(" + theExpression + ")";
        }
        else if (recursive_cast<StdString>(theType)) {
            return theExpression + ".c_str()";
        }
        else if (auto const theStruct = recursive_cast<StructType>(theType)) {
            
            std::string theTuple;

            for (const auto &theField : theStruct->itsFields) {
                if (theTuple.size()) {
                    theTuple += ",";
                }
                theTuple += "\n" + theIndent + "\t";
                theTuple += asIpcValue( theField.itsType, theExpression + "." + theField.itsName, theIndent + "\t" );
            }
            return "std::make_tuple(" + theTuple + "\n" + theIndent + ")";
        }
        else if (auto const theEnum = recursive_cast<EnumeratedType>(theType)) {
                return "static_cast<" + theEnum->itsUnderlyingType->itsName + ">(" + theExpression + ")";
        }
        else if (auto const theArray = recursive_cast<FixedArray>(theType)) {
            return asIpcArrayValue(theArray->itsElementType, theExpression, theIndent );
        }
        else if (auto const thePointer = recursive_cast<PointerType>(theType)) {
            return asIpcValue(thePointer->itsReferredType, "(*(" + theExpression + "))", theIndent);
        }
        else {
            return theExpression;
        }
    }
    
    static std::string asIpcArrayReference( const FixedArray *theArray, const std::string &theExpression, std::string ) {
        BABEL_LOG_SCOPE();

        if (recursive_cast<StdString>(theArray->itsElementType)) {
            return util::format( "&%s::MakeStringArrayReference( %s )", theNameSpaceName, theExpression.c_str() );
        }
        else if (const auto theEnum = recursive_cast<EnumeratedType>(theArray->itsElementType)) {
            return util::format( "&%s::MakeEnumArrayReference<%s,%sIpcEnumValidate>( %s, theErrorString )", 
                                theNameSpaceName, 
                                theEnum->itsName.c_str(),
                                NamespaceToString(theEnum->getItsNamespace()).c_str(),
                                theExpression.c_str() );
        }
        else if (theArray->itsElementType->getItsBaseType()->itsName == "bool") {
            return util::format( "&%s::MakeBooleanArrayReference( %s )", theNameSpaceName, theExpression.c_str() );
        }
        else {
            return theExpression;
        }
    }
    
    static std::string asIpcReference( const Type *theType, const std::string &theExpression, std::string theIndent = "" ) {
        BABEL_LOG_SCOPE();

        if (const auto theIpcType = getSpecifiedIpcType(theType)) {
            return "&" + *theIpcType + "(" + theExpression + ")";
        }
        else if (auto const theStruct = recursive_cast<StructType>(theType)) {
            std::string theTuple;
            for (const auto &theField : theStruct->itsFields) {
                if (theTuple.size()) {
                    theTuple += ",";
                }
                theTuple += ("\n" + theIndent + "\t");
                theTuple += asIpcReference( theField.itsType, theExpression + "." + theField.itsName, theIndent + "\t" );
            }
            return util::format( "std::make_tuple(%s\n%s)", theTuple.c_str(), theIndent.c_str());
        }
        else if (auto const theEnum = recursive_cast<EnumeratedType>(theType)) {
                return std::string("&") + theNameSpaceName + "::EnumReference<\n" + 
                        theIndent + "\t" + theEnum->itsName + ",\n" + 
                        theIndent + "\t" +  NamespaceToString(theEnum->getItsNamespace()) + "IpcEnumValidate\n" +
                        theIndent + ">(" + theExpression + ", theErrorString )";
        }
        else if (theType->getItsBaseType()->itsName == "bool") {
            return std::string("&") + theNameSpaceName + "::BooleanReference<typename IpcPolicy::Boolean>(" + theExpression + ")";
        }
        else if (recursive_cast<StdString>(theType)) {
            return std::string("&") + theNameSpaceName + "::StringReference(" + theExpression + ")";
        }
        else if (auto const theArray = recursive_cast<FixedArray>(theType)) {
            return asIpcArrayReference( theArray, theExpression, theIndent );
        }
        else if (const auto thePointer = recursive_cast<PointerType>(theType)) {
            return asIpcReference(thePointer->itsReferredType,"(*(" + theExpression + "))", theIndent);
        }
        else {
            return "&" + theExpression;
        }
    }
    
    static std::string Tupleize( const StructType &theStruct, std::string theIndent ) {
        BABEL_LOG_SCOPE();

        std::string theTuple;
        
        for (const auto &theField : theStruct.itsFields ) {
            if (!theTuple.empty()) {
                theTuple += ",";
            }
            
            theTuple += ("\n" + theIndent + "\t");
            
            if (auto const theSubStruct = recursive_cast<StructType>(theField.itsType)) {
                theTuple += Tupleize(*theSubStruct, theIndent+"\t");
            }
            else {
                theTuple += asIpcTemplateTypeName(theField.itsType);
            }
        }
        return "std::tuple< " + theTuple + "\n" + theIndent + ">";
    }
    static bool addTypeTemplates( CodeTemplates &theCodeTemplates, const StructType &forTheStruct ) {
        BABEL_LOG_SCOPE();

        if (forTheStruct.isVacuous()) {
            BABEL_LOGF("The struct type %s is vacuous - no code generated.",
                       forTheStruct.itsName.c_str() );
            return false;
        }
        // A tuple view of the struct contents is needed to convey type information
        //
        theCodeTemplates.emplace_back(
            util::format( "template <typename Boolean,typename IpcPolicy>\nusing %s%s = %s", 
                            forTheStruct.getItsSimpleName().c_str(), 
                            theTupleViewSuffix.c_str(),
                            Tupleize(forTheStruct, "\t").c_str()),
            ""
        );
        // There is also a need for readers and writers
        
        theCodeTemplates.emplace_back(
            util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                          "void IpcReadContainer( MessageT &&theMessage, %s &theStruct )", 
                          forTheStruct.itsName.c_str() ),
            util::format( "{\n"
                          "\ttypename IpcPolicy::template ReadContainerScope<%s> theContainer( theMessage, false );\n"
                          "\n"
                          "%s"
                          "}",
                          asIpcTemplateTypeName(&forTheStruct).c_str(),
                          updateArguments( BABEL_UTIL_RANGE(forTheStruct.itsFields),
                                           "theStruct.",
                                           "getMessageArguments", 
                                           "theMessage", 
                                           "\t",
                                           asIpcReference, 
                                           complicatedTypeReader).itsCode.c_str()
                        )
        );
        theCodeTemplates.emplace_back(
            util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                            "void IpcWriteContainer(  MessageT &&theMessage, const %s &theStruct )", 
                            forTheStruct.itsName.c_str() ),
            util::format( "{\n"
                          "\ttypename IpcPolicy::template WriteContainerScope<%s> theContainer( theMessage );\n"
                          "\n"
                          "%s"
                          "}",
                          asIpcTemplateTypeName(&forTheStruct).c_str(),
                          updateArguments( BABEL_UTIL_RANGE(forTheStruct.itsFields),
                                           "theStruct.",
                                           "appendMessageArguments", 
                                           "theMessage", 
                                           "\t",
                                           asIpcValue, 
                                           complicatedTypeWriter).itsCode.c_str()
                        )
        );
        return true;
    }
    static bool addTypeTemplates( CodeTemplates &theCodeTemplates, const VariantType &forTheVariant ) {
        BABEL_LOG_SCOPE();
        
        static const char theDiscriminantAttribute[] = "use_discriminated_variant";
        
        std::map<std::string, std::vector<const Type*>> theTypeSpecs;
        std::vector<std::string>                        theWarnings;
        
        for (auto theType : forTheVariant.itsTypes) {
            if (theType->isVacuous()) {
                theWarnings.push_back( GeneratorImplementationBase::formatTypeWarning( *theType, "", "is vacuous (see context below).") );
            }
            else {
                theTypeSpecs[theType->getItsTypeSpecification()].push_back(theType);
            }
        }
        for (const auto &theTypeSpec : theTypeSpecs) {
            const auto &theSameTypes = theTypeSpec.second;
            if (theSameTypes.size() > 1) {
                auto theNextType  = theSameTypes.begin();
                
                theWarnings.push_back( GeneratorImplementationBase::formatTypeWarning( **(theNextType++), "", "has the same type signature as (see context below):") );
                
                do {
                    theWarnings.push_back( GeneratorImplementationBase::formatTypeWarning( **(theNextType++), "... ", "") );
                } while (theNextType != theSameTypes.end());
            }
        }

        const bool useDiscriminant = !!forTheVariant.getAttributeValue(theDiscriminantAttribute);
        
        if (theWarnings.size() > 0) {
            if (!useDiscriminant) {
                std::for_each( BABEL_UTIL_RANGE(theWarnings), GeneratorImplementationBase::report );
                GeneratorImplementationBase::reportTypeError(
                    forTheVariant,
                    util::format( "is the context for the above warning%s" 
                                  " (use attribute [[babelc::%s]] to force code generation).",
                                  (theWarnings.size() == 1) ? "" : "s",
                                  theDiscriminantAttribute ).c_str() );
            }
        } 
        else if (useDiscriminant) {
            GeneratorImplementationBase::reportTypeWarning( 
                forTheVariant, 
                "", 
                util::format("[[babelc::%s]] specified but variant holds no vacuous types or types with the same type signature.",
                             theDiscriminantAttribute));
        }
        
        // There is a need for readers and writers
        std::string theReaderBody;
        std::string theWriterBody;
        
        if (useDiscriminant) {
            {
                std::string theCases;
                for (auto i = 0LU; i < forTheVariant.itsTypes.size(); i++) {
                    const auto thisType = forTheVariant.itsTypes[i];

                    theCases.append(theCases.empty() ? "" : "\n" );
                    
                    const std::vector<StructType::Field> theElement = {
                        StructType::Field( "theTypedValue_", thisType, false )
                    };

                    theCases.append(util::format("\tcase %1$lu: {\n"
                                                 "\t\t%2$s %3$s; %4$s\n"
                                                 "%5$s"
                                                 "\t\ttheVariant = %3$s;\n"
                                                 "\t}\n"
                                                 "\tbreak;",
                                                 i,
                                                 thisType->itsName.c_str(),
                                                 theElement.front().itsName.c_str(),
                                                 (thisType->isVacuous() ? "// This is a vacuous type" : ""),
                                                 (thisType->isVacuous() ?
                                                    "\n" :
                                                     updateArguments( BABEL_UTIL_RANGE(theElement),
                                                                       "",
                                                                       "getMessageArguments", 
                                                                       "theMessage", 
                                                                       "\t\t",
                                                                       asIpcReference, 
                                                                       complicatedTypeReader).itsCode.c_str())
                                                 ));
                }
                theReaderBody = util::format(
                                  "\t%s theReturnValue;\n"
                                  "\n"
                                  "\ttypename IpcPolicy::template ReadVariantScope<>                  theVariantScope( theMessage );\n"
                                  "\ttypename IpcPolicy::template ReadContainerScope<std::tuple<int>> theContainerScope( theMessage, true );\n"
                                  "\n"
                                  "\tdecltype(theVariant.%s()) theIndex;\n"
                                  "\n"
                                  "\tIpcPolicy::getMessageArguments(theMessage, theIndex);\n"
                                  "\n"
                                  "\tswitch (theIndex) {\n"
                                  "%s"
                                  "\tdefault:\n"
                                  "\t\tthrow std::invalid_argument(\"Unsupported variant type received\");\n"
                                  "\t}\n",
                                  forTheVariant.itsName.c_str(),
                                  forTheVariant.itsIndexMethod.c_str(),
                                  theCases.c_str()
                                );
            }
            {
                std::string theCases;
                for (auto i = 0LU; i < forTheVariant.itsTypes.size(); i++) {
                    const auto thisType = forTheVariant.itsTypes[i];
                    
                    theCases.append(theCases.empty() ? "" : "\n" );

                    const std::vector<StructType::Field> theElement = {
                        StructType::Field( util::format("%s<%s>(theVariant)",
                                                        forTheVariant.itsGetMethod.c_str(), 
                                                        thisType->itsName.c_str()),
                                           forTheVariant.itsTypes[i],
                                           false )
                    };

                    const auto theVariant = 
                        util::format( "std::tuple<decltype(theIndex)%s>",
                            thisType->isVacuous() ? "" : util::format(",%s", asIpcTemplateTypeName(thisType).c_str()).c_str());
                    
                    theCases.append(util::format("\tcase %lu: {\n"
                                                 "\t\t\ttypename IpcPolicy::template WriteVariantScope<%s>   theVariantScope( theMessage );\n"
                                                 "\t\t\ttypename IpcPolicy::template WriteContainerScope<%s> theContainerScope( theMessage );\n"
                                                 "\n"
                                                 "\t\t\tIpcPolicy::appendMessageArguments(theMessage,theIndex);\n"
                                                 "%s"
                                                 "\t\t}\n"
                                                 "\t\tbreak;",
                                                 i,
                                                 theVariant.c_str(),
                                                 theVariant.c_str(),
                                                 thisType->isVacuous() ? 
                                                     "" :
                                                     updateArguments( BABEL_UTIL_RANGE(theElement),
                                                                    "",
                                                                    "appendMessageArguments", 
                                                                    "theMessage", 
                                                                    "\t\t\t",
                                                                    asIpcValue, 
                                                                    complicatedTypeWriter).itsCode.c_str()
                                                 ));
                }
                theWriterBody = util::format(
                                  "\tconst auto theIndex = theVariant.%s();\n"
                                  "\n"
                                  "\tswitch(theIndex) {\n" 
                                  "%s\n"
                                  "\t}",
                                  forTheVariant.itsIndexMethod.c_str(),
                                  theCases.c_str()
                                );
            }
        }
        else {
            {
                std::string theCases;
                for (auto i = 0LU; i < forTheVariant.itsTypes.size(); i++) {
                    theCases.append( theCases.empty() ? "\t": "\n\telse ");
                    
                    const std::vector<StructType::Field> theElement = {
                        StructType::Field( "theTypedValue_", forTheVariant.itsTypes[i], false )
                    };

                    theCases.append(util::format("if (theScope.template containsType<%1$s>()) {\n"
                                                 "\t\t%2$s theTypedValue_;\n"
                                                 "%3$s"
                                                 "\t\ttheVariant = theTypedValue_;\n"
                                                 "\t}",
                                                 asIpcTemplateTypeName(forTheVariant.itsTypes[i]).c_str(),
                                                 forTheVariant.itsTypes[i]->itsName.c_str(),
                                                 updateArguments( BABEL_UTIL_RANGE(theElement),
                                                                    "",
                                                                    "getMessageArguments", 
                                                                    "theMessage", 
                                                                    "\t\t",
                                                                    asIpcReference, 
                                                                    complicatedTypeReader).itsCode.c_str()
                                                 ));
                }
                theCases.append("\n\telse {\n"
                                "\t\tthrow std::invalid_argument(\"Unsupported variant type received\");\n"
                                "\t}"
                );
                theReaderBody = util::format(
                                  "\ttypename IpcPolicy::template ReadVariantScope<> theScope( theMessage );\n"
                                  "\n"
                                  "%s",
                                  theCases.c_str()
                                );
            }
            {
                std::string theCases;
                for (auto i = 0LU; i < forTheVariant.itsTypes.size(); i++) {
                    theCases.append(theCases.empty() ? "" : "\n" );

                    const std::vector<StructType::Field> theElement = {
                        StructType::Field( util::format("%s<%s>(theVariant)",
                                                        forTheVariant.itsGetMethod.c_str(), 
                                                        forTheVariant.itsTypes[i]->itsName.c_str()),
                                           forTheVariant.itsTypes[i],
                                           false)
                    };

                    theCases.append(util::format("\tcase %lu: {\n"
                                                 "\t\t\ttypename IpcPolicy::template WriteVariantScope<%s> theScope( theMessage );\n"
                                                 "%s"
                                                 "\t\t}\n"
                                                 "\t\tbreak;",
                                                 i,
                                                 asIpcTemplateTypeName(forTheVariant.itsTypes[i]).c_str(),
                                                 updateArguments( BABEL_UTIL_RANGE(theElement),
                                                                    "",
                                                                    "appendMessageArguments", 
                                                                    "theMessage", 
                                                                    "\t\t\t",
                                                                    asIpcValue, 
                                                                    complicatedTypeWriter).itsCode.c_str()
                                                 ));
                }
                theWriterBody = util::format(
                                  "\tswitch(theVariant.%s()) {\n" 
                                  "%s\n"
                                  "\t}",
                                  forTheVariant.itsIndexMethod.c_str(),
                                  theCases.c_str()
                                );
            }
        }
        theCodeTemplates.emplace_back(
            util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                          "void IpcReadContainer( MessageT &&theMessage, %s &theVariant )", 
                          forTheVariant.itsName.c_str()),
            util::format( "{\n"
                          "%s\n"
                          "}",
                          theReaderBody.c_str()
                        )
        );
        theCodeTemplates.emplace_back(
            util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                            "void IpcWriteContainer(  MessageT &&theMessage, const %s &theVariant )", 
                            forTheVariant.itsName.c_str()),
            util::format( "{\n"
                          "%s\n"
                          "}",
                          theWriterBody.c_str()
                        )
        );

        return true;
    }
    static StructType::Field asValue( const std::string theName, const Type *theType ) {
        const auto thePointer = recursive_cast<PointerType>(theType);
        return StructType::Field( thePointer ? "(*(" + theName + "))"        : theName,
                                  thePointer ? thePointer->itsReferredType : theType,
                                  false);
    }
    
    static bool addTypeTemplates( CodeTemplates &theCodeTemplates, const FixedArray &forTheArray ) {
        BABEL_LOG_SCOPE();

        std::size_t additions = 0;
        
        const auto theExpression = forTheArray.getItsSimpleName() + "()";
        const auto theConversion = asIpcArrayValue(forTheArray.itsElementType,theExpression,"");
        
        BABEL_LOGF("Array conversion: '%s<%s>' => '%s'", 
                    theExpression.c_str(), 
                    forTheArray.itsElementType->itsName.c_str(), 
                    theConversion.c_str() );
        
        if (theConversion != theExpression) {
            theCodeTemplates.emplace_back(
                util::format( "template <typename Boolean,typename IpcPolicy>\nusing %1$s%2$s = decltype (%3$s)", 
                                     forTheArray.getItsSimpleName().c_str(), 
                                     theArrayViewSuffix.c_str(),
                                     theConversion.c_str() ),
                ""
            );
            
            additions += 1;
        }

        if (is_any_of<StructType,PointerType>(forTheArray.itsElementType) || getSpecifiedIpcType(forTheArray.itsElementType)) {
            // There is also a need for readers and writers
            const std::vector<StructType::Field> theElement( 1, asValue( "theArray[i]", forTheArray.itsElementType ) );

            theCodeTemplates.emplace_back(
                util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                              "void IpcReadContainer( MessageT &&theMessage, %s &theArray )", 
                              forTheArray.itsName.c_str() ),
                util::format( "{\n"
                              "\ttypename IpcPolicy::template ReadContainerScope<%1$s> theContainer( theMessage, false );\n"
                              "\n"
                              "\tstd::size_t i = 0;\n"
                              "\n"
                              "\twhile ((i < theArray.size()) && !IpcPolicy::AtEndOfContainer(theMessage)) {\n"
                              "%2$s\n"
                              "\t\ti += 1;\n"
                              "\t}\n"
                              "\tif (i < theArray.size()) throw std::invalid_argument(\"Too small %3$s array received\");\n"
                              "\tif (!IpcPolicy::AtEndOfContainer(theMessage)) throw std::invalid_argument(\"Too large %3$s array received\");\n"
                              "}\n",
                              asIpcTemplateTypeName(&forTheArray).c_str(),
                              updateArguments( BABEL_UTIL_RANGE(theElement),
                                                 "",
                                                 "getMessageArguments", 
                                                 "theMessage", 
                                                 "\t\t",
                                                 asIpcReference, 
                                                 complicatedTypeReader).itsCode.c_str(),
                              forTheArray.itsName.c_str()
                            )
            );
            theCodeTemplates.emplace_back(
                util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                                "void IpcWriteContainer(  MessageT &&theMessage, const %s &theArray )", 
                                forTheArray.itsName.c_str() ),
                util::format( "{\n"
                              "\ttypename IpcPolicy::template WriteContainerScope<%s> theContainer( theMessage );\n"
                              "\n"
                              "\tfor (std::size_t i = 0; i < theArray.size(); i++) {\n"
                              "%s\n"
                              "\t}\n"
                              "}\n",
                              asIpcTemplateTypeName(&forTheArray).c_str(),
                              updateArguments( BABEL_UTIL_RANGE(theElement),
                                                 "",
                                                 "appendMessageArguments", 
                                                 "theMessage", 
                                                 "\t",
                                                 asIpcValue, 
                                                 complicatedTypeWriter).itsCode.c_str()
                              
                            )
            );

            additions += 1;
        }
        
        return (additions != 0);
    }

    static bool addTypeTemplates( CodeTemplates &theCodeTemplates, const OptionalType &forTheOptional ) {
        // Optionals are modelled as arrays with length 0 or 1.
        //
        BABEL_LOG_SCOPE();

        const auto theExpression = "std::array<" + forTheOptional.itsElementType->itsName + ",1>()";
        const auto theConversion = asIpcArrayValue(forTheOptional.itsElementType,theExpression,"");
        
        BABEL_LOGF("Optional conversion: '%s<%s>' => '%s'", 
                    theExpression.c_str(), 
                    forTheOptional.itsElementType->itsName.c_str(), 
                    theConversion.c_str() );
        
        if (theConversion != theExpression) {
            theCodeTemplates.emplace_back(
                util::format( "template <typename Boolean,typename IpcPolicy>\nusing %1$s%2$s = decltype (%3$s)", 
                                     forTheOptional.getItsSimpleName().c_str(), 
                                     theArrayViewSuffix.c_str(),
                                     theConversion.c_str() ),
                ""
            );
        }

        // There is also a need for readers and writers
        {
            const std::vector<StructType::Field> theElement( 1, asValue( "theValue", forTheOptional.itsElementType ) );

            theCodeTemplates.emplace_back(
                util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                              "void IpcReadContainer( MessageT &&theMessage, %s &theOptional )", 
                              forTheOptional.itsName.c_str() ),
                util::format( "{\n"
                              "\ttypename IpcPolicy::template ReadContainerScope<%1$s> theContainer( theMessage, false );\n"
                              "\n"
                              "\t%2$s theValue;\n"
                              "\n"
                              "\tif (IpcPolicy::AtEndOfContainer(theMessage)) {\n"
                              "\t\ttheOptional = %3$s();\n"
                              "\t}"
                              "\telse {\n"
                              "%4$s\n"
                              "\t\tif (!IpcPolicy::AtEndOfContainer(theMessage)) throw std::invalid_argument(\"Got more than one optional object\");\n"
                              "\t\ttheOptional = theValue;\n"
                              "\t}\n"
                              "}\n",
                              asIpcTemplateTypeName(&forTheOptional).c_str(),
                              forTheOptional.itsElementType->itsName.c_str(),
                              forTheOptional.itsName.c_str(),
                              updateArguments( BABEL_UTIL_RANGE(theElement),
                                                 "",
                                                 "getMessageArguments", 
                                                 "theMessage", 
                                                 "\t\t",
                                                 asIpcReference, 
                                                 complicatedTypeReader).itsCode.c_str()
                            )
            );
        }
        {
            const std::vector<StructType::Field> theElement( 1, asValue( "(*theOptional)", forTheOptional.itsElementType ) );

            theCodeTemplates.emplace_back(
                util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                                "void IpcWriteContainer(  MessageT &&theMessage, const %s &theOptional )", 
                                forTheOptional.itsName.c_str() ),
                util::format( "{\n"
                              "\ttypename IpcPolicy::template WriteContainerScope<%s> theContainer( theMessage );\n"
                              "\n"
                              "\tif (theOptional) {\n"
                              "%s"
                              "\t}\n"
                              "}\n",
                              asIpcTemplateTypeName(&forTheOptional).c_str(),
                              updateArguments( BABEL_UTIL_RANGE(theElement),
                                                 "",
                                                 "appendMessageArguments", 
                                                 "theMessage", 
                                                 "\t\t",
                                                 asIpcValue, 
                                                 complicatedTypeWriter).itsCode.c_str()

                            )
            );
        }
        return true;
    }

    static bool addTypeTemplates( CodeTemplates &theCodeTemplates, const VaryingLengthContainer &forTheContainer ) {
        BABEL_LOG_SCOPE();
        
        theCodeTemplates.emplace_back(
            util::format( "template <typename Boolean,typename IpcPolicy>\nusing %1$s%2$s = std::array<%3$s,1>", 
                                 forTheContainer.getItsSimpleName().c_str(), 
                                 theArrayViewSuffix.c_str(),
                                 asIpcTemplateTypeName(forTheContainer.itsElementType).c_str() ),
            ""
        );

        const auto                           containsBuiltinType = (recursive_cast<BuiltInType>(forTheContainer.itsElementType) != nullptr);
        const std::vector<StructType::Field> theElement( 1, asValue( "theElement", forTheContainer.itsElementType ) );
            
        
        std::string theReaderBody;
        std::string theWriterBody;
        
        if (containsBuiltinType) {
            
            theReaderBody = 
                    util::format(
                        "{\n"
                        "\tconst %1$s::value_type *theValues     = nullptr;\n"
                        "\t%1$s::size_type         theNoOfValues = 0;\n"
                        "\n"
                        "\tIpcPolicy::getMessageArray( theMessage, theValues, theNoOfValues );\n"
                        "\n"
                        "\ttheVaryingLengthContainer.assign( theValues, (theValues+theNoOfValues) );\n"
                        "}",
                        forTheContainer.itsName.c_str()
                    );
        }
        else {
            const std::string theElementDeclaration = 
                util::format("\t\t%s::value_type theElement%s;\n",
                            forTheContainer.itsName.c_str(),
                            makeInitValue(forTheContainer.itsElementType).c_str());
            theReaderBody = 
                util::format( "{\n"
                              "\ttypename IpcPolicy::template ReadContainerScope<%1$s> theContainer( theMessage, false );\n"
                              "\n"
                              "\twhile (!IpcPolicy::AtEndOfContainer(theMessage)) {\n"
                              "%2$s"
                              "%3$s\n"
                              "\t\ttheVaryingLengthContainer.emplace_back(theElement);\n"
                              "\t}\n"
                              "}\n",
                              asIpcTemplateTypeName(&forTheContainer).c_str(),
                              theElementDeclaration.c_str(),
                              updateArguments( BABEL_UTIL_RANGE(theElement),
                                                 "",
                                                 "getMessageArguments", 
                                                 "theMessage", 
                                                 "\t\t",
                                                 asIpcReference, 
                                                 complicatedTypeReader).itsCode.c_str()
                            );
        }
        
        if (containsBuiltinType && forTheContainer.isConsecutive) {
            theWriterBody =
                    util::format(
                        "{\n"
                        "\tIpcPolicy::appendMessageArray(\n"
                        "\t\ttheMessage,\n"
                        "\t\t&theVaryingLengthContainer.front(),\n"
                        "\t\ttheVaryingLengthContainer.size()\n"
                        "\t);\n"
                        "}"
                    );
        }
        else {
            theWriterBody = 
                util::format( "{\n"
                              "\ttypename IpcPolicy::template WriteContainerScope<%s> theContainer( theMessage );\n"
                              "\n"
                              "\tfor (const auto &theElement : theVaryingLengthContainer) {\n"
                              "%s\n"
                              "\t}\n"
                              "}\n",
                              asIpcTemplateTypeName(&forTheContainer).c_str(),
                              updateArguments( BABEL_UTIL_RANGE(theElement),
                                                 "",
                                                 "appendMessageArguments", 
                                                 "theMessage", 
                                                 "\t",
                                                 asIpcValue, 
                                                 complicatedTypeWriter).itsCode.c_str()
                              
                            );
        }
        theCodeTemplates.emplace_back(
            util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                          "void IpcReadContainer( MessageT &&theMessage, %s &theVaryingLengthContainer )", 
                          forTheContainer.itsName.c_str() ),
            theReaderBody
        );
        theCodeTemplates.emplace_back(
            util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                            "void IpcWriteContainer(  MessageT &&theMessage, const %s &theVaryingLengthContainer )", 
                            forTheContainer.itsName.c_str() ),
            theWriterBody
        );
        
        return true;
    }
    
    static bool addTypeTemplates( CodeTemplates &theCodeTemplates, const MapContainer &forTheMap ) {
        BABEL_LOG_SCOPE();

        
        theCodeTemplates.emplace_back(
            util::format( "template <typename Boolean,typename IpcPolicy>\nusing %1$s%2$s = std::array<std::pair<%3$s,%4$s>,1>", 
                                 forTheMap.getItsSimpleName().c_str(), 
                                 theArrayViewSuffix.c_str(),
                                 asIpcTemplateTypeName(forTheMap.itsKeyType).c_str(),
                                 asIpcTemplateTypeName(forTheMap.itsElementType).c_str() ),
            ""
        );
            

        const std::vector<StructType::Field> thePair{ asValue("thisPair.first", forTheMap.itsKeyType),
                                                      asValue("thisPair.second", forTheMap.itsElementType)};

        theCodeTemplates.emplace_back(
            util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                          "void IpcReadContainer( MessageT &&theMessage, %s &theMap )", 
                          forTheMap.itsName.c_str() ),
            util::format( "{\n"
                          "\tusing TheIpcPair = std::pair<%1$s,%2$s>;\n"
                          "\tusing ThePair    = std::pair<%3$s,%4$s>;\n"
                          "\n"
                          "\ttypename IpcPolicy::template ReadContainerScope<%5$s> theMapContainer( theMessage, false );\n"
                          "\n"
                          "\twhile (!IpcPolicy::AtEndOfContainer(theMessage)) {\n"
                          "\t\ttypename IpcPolicy::template ReadContainerScope<TheIpcPair> thePairContainer( theMessage, false );\n"
                          "\n"
                          "\t\tThePair thisPair;\n"
                          "\n"
                          "%6$s\n"
                          "\n"
                          "\t\ttheMap.emplace( thisPair );\n"
                          "\t}\n"
                          "}\n",
                          asIpcTemplateTypeName(forTheMap.itsKeyType).c_str(),
                          asIpcTemplateTypeName(forTheMap.itsElementType).c_str(),
                          forTheMap.itsKeyType->itsName.c_str(),
                          forTheMap.itsElementType->itsName.c_str(),
                          asIpcTemplateTypeName(&forTheMap).c_str(),
                          updateArguments( BABEL_UTIL_RANGE(thePair),
                                             "",
                                             "getMessageArguments", 
                                             "theMessage", 
                                             "\t\t",
                                             asIpcReference, 
                                             complicatedTypeReader).itsCode.c_str()
                        )
        );
        theCodeTemplates.emplace_back(
            util::format( "template <class IpcPolicy, typename Boolean, typename MessageT>\n"
                            "void IpcWriteContainer(  MessageT &&theMessage, const %s &theMap )", 
                            forTheMap.itsName.c_str() ),
            util::format( "{\n"
                          "\tusing TheIpcPair = std::pair<%1$s,%2$s>;\n"
                          "\n"
                          "\ttypename IpcPolicy::template WriteContainerScope<%3$s> theMapContainer( theMessage );\n"
                          "\n"
                          "\tfor (const auto &thisPair : theMap) {\n"
                          "\t\ttypename IpcPolicy::template WriteContainerScope<TheIpcPair> thePairContainer( theMessage );\n"
                          "\n"
                          "%4$s\n"
                          "\t}\n"
                          "}\n",
                          asIpcTemplateTypeName(forTheMap.itsKeyType).c_str(),
                          asIpcTemplateTypeName(forTheMap.itsElementType).c_str(),
                          asIpcTemplateTypeName(&forTheMap).c_str(),
                          updateArguments( BABEL_UTIL_RANGE(thePair),
                                             "",
                                             "appendMessageArguments", 
                                             "theMessage", 
                                             "\t\t",
                                             asIpcValue, 
                                             complicatedTypeWriter).itsCode.c_str()

                        )
        );

        return true;
    }

    static bool addTypeTemplates( CodeTemplates &theCodeTemplates, const EnumeratedType &forTheEnum ) {
        BABEL_LOG_SCOPE();

        static const char theTargetName[]   = "theTarget";
        static const char theArgumentName[] = "theValue";
        static const char theErrorString[]  = "theErrorString";
        
        std::string       theBody;
        
        if (forTheEnum.itsValues.empty()) {
            theBody = util::format( "{\n"
                                    "\t %1$s = %2$s;\n"
                                    "}",
                                    theTargetName,
                                    theArgumentName );
        }
        else {
            theBody = util::format("{\n"
                                   "\tswitch (%s) {\n",
                                   theArgumentName );
            
            for (auto theValue : forTheEnum.itsValues) {
                theBody.append( util::format(
                    "\tcase %s::%s:\n",  
                    forTheEnum.getItsSimpleName().c_str(),
                    theValue.c_str()));
            }
           
            theBody.append( 
                util::format( "\t\t%1$s = %2$s;\n"
                              "\t\treturn;\n"
                              "\t}\n"
                              "\tif (!%3$s.empty()) { %3$s += \", \"; };"
                              "\t%3$s += (std::to_string(static_cast<%4$s>(%2$s)) + \" not valid for %5$s\");\n"
                              "}\n",
                              theTargetName,
                              theArgumentName,
                              theErrorString, 
                              forTheEnum.itsUnderlyingType->itsName.c_str(),
                              forTheEnum.itsName.c_str() ) );            
        }
        theCodeTemplates.emplace_back(
                util::format( "void IpcEnumValidate( %1$s &%2$s, %1$s %3$s, std::string &%4$s ) __attribute__((weak))",  
                              forTheEnum.getItsSimpleName().c_str(),
                              theTargetName,
                              theArgumentName,
                              forTheEnum.itsValues.empty() ? "" : theErrorString ),
                theBody
            );

        return true;
    }
    
    
    template <class C> 
    bool derivedTypeAdded( const Type *theType, CodeTemplates &toTheTemplates ) {
        BABEL_LOG_SCOPE();

        if (auto const theDerivedInstance = recursive_cast<C>(theType)) {
            return addTypeTemplates( toTheTemplates, *theDerivedInstance );
        }
        else {
            return false;
        }
    }
    
    static void addTypeTemplates( OutputFile &theOutputFile, const Types &theTypes, SyntacticElements theSyntacticElement, std::set<std::string> &theNamespaces ) {
        BABEL_LOG_SCOPE();

        using TypeTemplates = std::vector< std::pair< const Type *, CodeTemplates> >;
        
        static TypeTemplates theTypeTemplates;
        
        if (theTypeTemplates.empty()) {
            
            for (auto theType : theTypes) {
                TypeTemplates::value_type  thePair;
                BABEL_LOGF("Trying type %s...", theType.second->itsName.c_str());

                auto const theTypeIsHandled = 
                    derivedTypeAdded<StructType>            (theType.second, thePair.second ) ||
                    derivedTypeAdded<EnumeratedType>        (theType.second, thePair.second ) ||
                    derivedTypeAdded<VaryingLengthContainer>(theType.second, thePair.second ) ||
                    derivedTypeAdded<MapContainer>          (theType.second, thePair.second ) ||
                    derivedTypeAdded<FixedArray>            (theType.second, thePair.second ) ||
                    derivedTypeAdded<VariantType>           (theType.second, thePair.second ) ||
                    derivedTypeAdded<OptionalType>          (theType.second, thePair.second );

                BABEL_LOGF("The type %s was %shandled (total %lu types now)", 
                            theType.first.c_str(), 
                            (theTypeIsHandled ? ""  : "not "),
                            theTypeTemplates.size() + 1 );
                
                if (theTypeIsHandled) {
                    const std::size_t theSizeBefore = theTypeTemplates.size();

                    thePair.first = theType.second;
                    
                    for (TypeTemplates::iterator i = theTypeTemplates.begin(); i != theTypeTemplates.end(); ++i) {
                        if (i->first->isReferred(thePair.first)) {
                            theTypeTemplates.insert(i,thePair);
                            break;
                        }
                    }
                    if (theSizeBefore == theTypeTemplates.size()) {
                        theTypeTemplates.emplace_back(thePair);
                    }
                    theNamespaces.insert(NamespaceToString(theType.second->getItsNamespace()));
                }
            }
        }
        for (const auto &theTypeInfo : theTypeTemplates) {
            BABEL_LOGF("Sorted types: %s", theTypeInfo.first->itsName.c_str() );
        }
        for (const auto &theTypeInfo : theTypeTemplates) {
            if (theSyntacticElement == SyntacticElements::Declaration) {
                ScopedGuard     theTemplateGuard( theOutputFile,
                                                  std::string("have_") + theNameSpaceName + "__" +
                                                    theTypeInfo.first->itsName + "_ipc_declarations" );
                ScopedNamespace theTemplateNamespace( theOutputFile,
                                                      theTypeInfo.first->getItsNamespace());
                
                for (const auto &theTemplate : theTypeInfo.second) {
                    theTemplate.output(theOutputFile, theSyntacticElement );
                }
            }
            else {
                std::unique_ptr<ScopedGuard>     theTemplateGuard;
                std::unique_ptr<ScopedNamespace> theTemplateNamespace;
                
                for (const auto &theTemplate : theTypeInfo.second) {
                    if (theTemplate.hasImplementation() && !theTemplateGuard) {
                        theTemplateGuard.reset(new ScopedGuard( theOutputFile,
                                                                std::string("have_") + theNameSpaceName + "__" +
                                                                theTypeInfo.first->itsName + "_ipc_implementations"));
                        theTemplateNamespace.reset(new ScopedNamespace( theOutputFile, theTypeInfo.first->getItsNamespace()));
                    }
                    theTemplate.output(theOutputFile, theSyntacticElement );
                }
            }
        }
    }
    
    static void addIpcHelpersTemplates( OutputFile &theOutputFile, const std::string &theClassName ) {
        BABEL_LOG_SCOPE();
       
        theOutputFile.printf( "\t%1$s( const %1$s &)             = delete;\n"
                              "\t%1$s( %1$s &&)                  = delete;\n"
                              "\t%1$s &operator=( const %1$s & ) = delete;\n"
                              "\t%1$s &operator=( %1$s && )      = delete;\n\n",
                              theClassName.c_str() );
    }
    
    struct Policy {
        const InterfaceType &itsInterface;
        const bool           isMethodInterface;
        virtual std::string getItsName()                       const = 0;
        virtual std::string getReceiverSideFactoryMethod()     const = 0;
        virtual std::string getTransmitterSideFactoryMethod()  const = 0;
        virtual std::string getReceiverSideClassName()         const = 0;
        virtual std::string getTransmitterSideClassName()      const = 0;
        virtual std::string getReceiverSideFactoryComment()    const = 0;
        virtual std::string getTransmitterSideFactoryComment() const = 0;
        virtual std::string getReceiverSideRegisterMethod()    const = 0;
        virtual std::string getTransmitterSideInvocation()     const = 0;
        virtual std::string getTransmitterSideCreation()       const = 0;
        virtual std::string getRegisterMethod()                const = 0;
        
        std::string getOptionalWarning() const {
            if (!itsInterface.hasReturnValues()) {
                return  "// ***** WARNING *** WARNING *** WARNING *** WARNING *** WARNING **********\n"
                        "// * " + itsInterface.itsName +  " can be registered both as a broadcast \n"
                        "// * and a call interface since none of its methods has a return value.\n"
                        "// * Be sure to use the correct register functions for both client and server!\n"
                        "// ***** WARNING *** WARNING *** WARNING *** WARNING *** WARNING **********\n"
                        "//\n";
            }
            else {
                return "";
            }
        }
        Policy( const InterfaceType &theInterface, bool isMethodInterface_ ) : itsInterface(theInterface), isMethodInterface(isMethodInterface_){}
    };
    
    struct MethodPolicy : public Policy {
        std::string getItsName()                      const override { return "MethodPolicy"; }
        std::string getReceiverSideFactoryMethod()    const override { return "register" +  itsInterface.getItsSimpleName() + "Implementation"; }
        std::string getTransmitterSideFactoryMethod() const override { return "lookup" + itsInterface.getItsSimpleName() + "Implementation"; }
        std::string getReceiverSideClassName()        const override { return itsInterface.getItsSimpleName() + "ImplementationProxy"; }
        std::string getTransmitterSideClassName()     const override { return itsInterface.getItsSimpleName() + "ClientProxy";}
        std::string getReceiverSideRegisterMethod()   const override { return "addMethod"; }
        std::string getTransmitterSideInvocation()    const override { return "sendMessage"; }
        std::string getTransmitterSideCreation()      const override { return "createMethodMessage"; }
        std::string getRegisterMethod()               const override { return "registerImplementor"; }

        virtual std::string getReceiverSideFactoryComment()    const override {
            return  "// Registers an implementation of the " + itsInterface.itsName + " interface at the passed IPC Policy.\n"
                    "// All remote IPC calls will be forwarded to the passed instance.\n"
                    "// Returns a shared pointer to the \"stub class\" which fulfills the same interface. Calls to the\n"
                    "// methods of the returned instance will also be forwarded to the implementation passed as second argument.\n"
                    "// \"Forgetting\" the returned instance will deregister the implementation from the bus interface.\n"
                    "//\n" + getOptionalWarning();
        }
        virtual std::string getTransmitterSideFactoryComment() const override {
            return  "// Looks up a remote implementation of " + itsInterface.itsName +  " using the passed IPC Policy.\n"
                    "// Returns a stub which will forward its calls to the remote implementation.\n"
                    "//\n" + getOptionalWarning();
        }

        
        explicit MethodPolicy( const InterfaceType &theInterface ) : Policy(theInterface,true) {}
    };
    
    struct SignalPolicy : public Policy {
        std::string getItsName()                      const override { return "SignalPolicy"; }
        std::string getReceiverSideFactoryMethod()    const override { return "register" +  itsInterface.getItsSimpleName() + "BroadcastListener"; }
        std::string getTransmitterSideFactoryMethod() const override { return "get" + itsInterface.getItsSimpleName() + "BroadcastProxy"; }
        std::string getReceiverSideClassName()        const override { return itsInterface.getItsSimpleName() + "BroadcastListener"; }
        std::string getTransmitterSideClassName()     const override { return itsInterface.getItsSimpleName() + "BroadcastProxy";}
        std::string getReceiverSideRegisterMethod()   const override { return "addSignal"; }
        std::string getTransmitterSideInvocation()    const override { return "emitSignal"; }
        std::string getTransmitterSideCreation()      const override { return "createSignalMessage"; }
        std::string getRegisterMethod()               const override { return "registerListener"; }
        
        virtual std::string getReceiverSideFactoryComment()    const override {
            return  "// Registers an implementation of the " + itsInterface.itsName + " interface at the passed IPC Policy.\n"
                    "// All broadcast IPC calls to the interface will be forwarded to the passed instance.\n"
                    "// Returns a shared pointer to the \"stub class\" which fulfills the same interface. Calls to the\n"
                    "// methods of the returned instance will also be forwarded to the implementation passed as second argument.\n"
                    "// \"Forgetting\" the returned instance will deregister the implementation from the bus interface.\n"
                    "//\n" +
                    getOptionalWarning();
        }
        virtual std::string getTransmitterSideFactoryComment() const override {
            return  "// Creates a proxy implementing the " + itsInterface.itsName + " interface using the passed IPC Policy.\n"
                    "// The proxy could be used to broadcast messages from the given interface.\n"
                    "//\n" +
                    getOptionalWarning();
        }

        explicit SignalPolicy( const InterfaceType &theInterface ) : Policy( theInterface, false) {}
    };

    static std::size_t addMethodOverrideStart( OutputFile &theOutputFile, const InterfaceType::Method &theMethod ) {
        BABEL_LOG_SCOPE();
        
        const bool hasAnyConstArguments = std::count_if( BABEL_UTIL_RANGE(theMethod.itsArguments), [](const InterfaceType::Method::Argument &a){return a.isConst;} ); 
        const bool hasAnyRefArguments   = std::count_if( BABEL_UTIL_RANGE(theMethod.itsArguments), [](const InterfaceType::Method::Argument &a){return a.isReference;} ); 

        const char * const theConstPad = hasAnyConstArguments ? "      " : "";
        const char * const theRefPad   = hasAnyRefArguments   ? " " : "";
        const auto theArgumentWithLongestTypeName = std::max_element(BABEL_UTIL_RANGE(theMethod.itsArguments), 
                                                         []( const InterfaceType::Method::Argument &theLeft,
                                                             const InterfaceType::Method::Argument &theRight) {
                                                             return theLeft.itsType->itsName.size() < theRight.itsType->itsName.size();
                                                         });
        const std::size_t theMaxTypeLength = 
            theArgumentWithLongestTypeName == theMethod.itsArguments.end() ? 
                0 : theArgumentWithLongestTypeName->itsType->itsName.size();
                                                         
        theOutputFile.printf( "%s %s(", theMethod.itsReturnType->itsName.c_str(), theMethod.itsName.c_str() );
                
        switch (theMethod.itsArguments.size()) {
            case 0:
                break;
            case 1:
                {
                    const auto &theArg = *theMethod.itsArguments.begin();
                    theOutputFile.printf(" %s%s%s%s ", 
                                            (theArg.isConst ? "const " : ""),
                                            theArg.itsType->itsName.c_str(),
                                            (theArg.isReference ? " &" : " "),
                                            theArg.itsName.c_str() );
                }
                break;
            default:
                {
                    const char *theAppendString = "\n";

                    for (const auto &theArgument : theMethod.itsArguments) {
                        theOutputFile.printf("%s\t%s%*s %s%s",
                                             theAppendString, 
                                            (theArgument.isConst ? "const " : theConstPad),
                                            -static_cast<int>(theMaxTypeLength),
                                            theArgument.itsType->itsName.c_str(),
                                            (theArgument.isReference ? "&" : theRefPad),
                                            theArgument.itsName.c_str() );
                        theAppendString = ",\n";
                    }
                    theOutputFile.printf("\n");
                }
                break;
        }


        theOutputFile.printf(") override {\n");
        
        return theMaxTypeLength;
    }

    template <typename Iterator >
    std::string getArgumentsImage( Iterator             theArgumentBegin,
                                   Iterator             theArgumentEnd,
                                   const std::string   &theArgumentsPrefix,
                                   const std::string   &theDelimiter,
                                   const std::string   &theIndent,
                                   std::string (*theTransformer)( const Type*, const std::string&, const std::string)) {
        std::string theArgumentValues;
        while (theArgumentBegin != theArgumentEnd) {
            if (!theArgumentValues.empty()) {
                theArgumentValues.append(theDelimiter);
            }
            theArgumentValues.append(theIndent);
            theArgumentValues.append((*theTransformer)( theArgumentBegin->itsType, theArgumentsPrefix + theArgumentBegin->itsName, theIndent ));
            theArgumentBegin++;
        }
        return theArgumentValues;
    }
    
    template <typename Iterator>
    ArgumentUpdate updateArguments(Iterator           theArgumentStart,
                                   Iterator           theArgumentEnd,
                                   const std::string &theArgumentsPrefix,
                                   const std::string &theAccessorMethod,
                                   const std::string &theMessageArgument,
                                   const std::string &theIndent,
                                   std::string (*theTransformer)( const Type*, const std::string&, const std::string),
                                   std::string (*theIndirector)( const std::string&, const Type*, const std::string&, const std::string& )) {
        BABEL_LOG_SCOPE();
        
        std::string theResult;

        const auto needsErrorCheck = (theAccessorMethod == "getMessageArguments") &&
                                        (theArgumentEnd != 
                                            std::find_if(theArgumentStart, theArgumentEnd, 
                                                        [](const decltype(*theArgumentStart)&v) {
                                                            const FixedArray * const theArray = recursive_cast<FixedArray>(v.itsType);
                                                            return recursive_cast<EnumeratedType>(v.itsType) ||
                                                                    (theArray && recursive_cast<EnumeratedType>(theArray->itsElementType));
                                                        } ));
        if (needsErrorCheck) {
            theResult.append(util::format("%sstd::string theErrorString;\n\n", theIndent.c_str()));
        }

        for (;;) {
            const auto theFirstComplicatedArgument = std::find_if( 
                theArgumentStart, 
                theArgumentEnd, 
                []( const decltype(*theArgumentStart)&v ){ return isComplicatedType(v.itsType); }
            );

            if (theFirstComplicatedArgument != theArgumentStart) {
                BABEL_LOGF("%s is not a complicated argument", theArgumentStart->itsName.c_str() );
                theResult.append( util::format("%sIpcPolicy::%s(\n", theIndent.c_str(), theAccessorMethod.c_str()));

                {
                    theResult.append( util::format("%1$s\t%2$s,\n"
                                                   "%3$s",
                                                   theIndent.c_str(),
                                                   theMessageArgument.c_str(),
                                                   getArgumentsImage(
                                                     theArgumentStart, theFirstComplicatedArgument,
                                                     theArgumentsPrefix,
                                                     ",\n", 
                                                     theIndent + "\t",
                                                     theTransformer).c_str()));
                }
                theResult.append(util::format("\n%s);\n", theIndent.c_str()));
            }

            if (theFirstComplicatedArgument == theArgumentEnd) {
                break;
            }

            BABEL_LOGF("%s is a complicated argument", theFirstComplicatedArgument->itsName.c_str() );

            theResult.append( util::format("%s%s;\n", 
                                            theIndent.c_str(),
                                            (*theIndirector)( theMessageArgument, 
                                                              theFirstComplicatedArgument->itsType, 
                                                              theArgumentsPrefix + theFirstComplicatedArgument->itsName,
                                                              theIndent).c_str() ));
            theArgumentStart = (theFirstComplicatedArgument + 1);
        }

        theResult.append("\n");

        if (needsErrorCheck) {
            theResult.append(util::format("%sif (!theErrorString.empty()) { throw std::invalid_argument( theErrorString ); };\n", theIndent.c_str()));
        }

        return { theResult, needsErrorCheck };
    }
    
    static std::string interfaceNameOf( const InterfaceType &theInterface ) {
        if (const auto theAttributedName = theInterface.getAttributeValue("interface_name")) {
            return *theAttributedName;
        }
        else {
            auto theInterfaceName = util::replaceAll("::", theInterface.itsName, "." );
            
            return (theInterfaceName.at(0) == '.') ? theInterfaceName.erase(0,1) : theInterfaceName;
        }
    }
    static void addReceiverStub( OutputFile &theOutputFile, const InterfaceType &theInterface, const Policy &thePolicy ) {
        BABEL_LOG_SCOPE();

        const std::string theFixedArgumentType = "std::shared_ptr<Policy>";

        const auto theStubClassName = thePolicy.getReceiverSideClassName();
        const auto theVariableArgument = util::format("std::shared_ptr<%s>", theInterface.itsName.c_str());
        const auto theTypeWidth = -(static_cast<int>( std::max(theFixedArgumentType.size(), theVariableArgument.size())) + 1);

        theOutputFile.printf(   "template <typename IpcPolicy>\n"
                                "struct %1$s final :\n"
                                "\tpublic %2$s,\n"
                                "\tpublic IpcPolicy::Implementor {\n",
                                theStubClassName.c_str(),
                                theInterface.itsName.c_str() );
        {
            ScopedIndent thePublicSection(theOutputFile);

            theOutputFile.printf("\n"
                                 "~%1$s() {\n"
                                 "\titsIpcPolicy->deregisterImplementor( itsObjectPath.c_str(), \"%2$s\", this);\n"
                                 "}\n\n",
                                 theStubClassName.c_str(),
                                 interfaceNameOf( theInterface ).c_str());
        }
        addIpcHelpersTemplates(theOutputFile, theStubClassName );
        theOutputFile.printf("private:\n\n");
        {
            ScopedIndent thePrivateSection(theOutputFile);

            theOutputFile.printf(
                "using IpcPolicyPtr    = std::shared_ptr<IpcPolicy>;\n"
                "using Implementor     = typename IpcPolicy::Implementor;\n"
                "using ImplementorPtr  = const typename Implementor::SharedPtr &;\n"
                "using Boolean         = typename IpcPolicy::Boolean;\n"
                "using Message         = typename IpcPolicy::Message;\n"
                "using InterfacePtr    = std::shared_ptr<%1$s>;\n"
                "\n"
                "template <typename Policy>\n"
                "friend std::shared_ptr<%1$s>\n"
                "%2$s(\n"
                "\t%3$*5$s theIpcPolicy,\n"
                "\t%7$*5$s theObjectPath,\n"
                "\t%4$*5$s theImplementation\n"
                ");\n"
                "\n"
                "IpcPolicyPtr      itsIpcPolicy;\n"
                "InterfacePtr      itsImplementation;\n"
                "const std::string itsObjectPath;\n"
                "\n"
                "%6$s( IpcPolicyPtr theIpcPolicy, const char *theObjectPath, InterfacePtr theImplementation )\n"
                ": itsIpcPolicy(theIpcPolicy), itsImplementation(theImplementation), itsObjectPath(theObjectPath) {\n"
                "}\n\n",
                theInterface.itsName.c_str(),
                thePolicy.getReceiverSideFactoryMethod().c_str(),
                theFixedArgumentType.c_str(),
                theVariableArgument.c_str(),
                theTypeWidth, 
                theStubClassName.c_str(),
                "const char *"
            );

            for (const InterfaceType::Method &theMethod : theInterface.itsMethods) {
                const std::size_t theMaxTypeLength = addMethodOverrideStart(theOutputFile, theMethod );
                
                theOutputFile.printf("\t%sitsImplementation->%s(", 
                                        (recursive_cast<Void>(theMethod.itsReturnType) ? "" : "return "),
                                        theMethod.itsName.c_str());
                {
                    const char *theDelimiter = "";
                    for (const auto &theArgument : theMethod.itsArguments) {
                        theOutputFile.printf("%s%s", theDelimiter, theArgument.itsName.c_str());
                        theDelimiter = ", ";
                    }
                }
                theOutputFile.printf(");\n");
                theOutputFile.printf("}\n\n");
                theOutputFile.printf("static void %s( ImplementorPtr theThis, Message &%s, Message &%s ) {\n"
                                     "\tauto &theProxy = dynamic_cast<%s&>(*theThis);\n\n",
                                     theMethod.itsName.c_str(),
                                    theMethod.itsArguments.empty() ? "" : "theArguments",
                                    (theMethod.itsReturnType->itsName == "void") ? "" : "theReturnValue",
                                     theStubClassName.c_str() );
                {
                    ScopedIndent theMethodBodyIndent(theOutputFile);
                    
                    for (const auto &theArgument : theMethod.itsArguments) {
                        theOutputFile.printf("%*s %s;\n",                                                             
                                             -static_cast<int>(theMaxTypeLength),
                                            theArgument.itsType->itsName.c_str(),
                                            theArgument.itsName.c_str() );
                    }
                    
                    const auto theArgumentUpdate = updateArguments( BABEL_UTIL_RANGE(theMethod.itsArguments),
                                                                    "",
                                                                    "getMessageArguments", 
                                                                    "theArguments",
                                                                    "",
                                                                   asIpcReference,
                                                                   complicatedTypeReader );
                    
                    theOutputFile.printf("\n%s", theArgumentUpdate.itsCode.c_str() );

                    std::string theProxyCall = util::format("theProxy.%s(", theMethod.itsName.c_str());

                    if (theMethod.itsArguments.empty()) {
                        theProxyCall += ")";
                    }
                    else {                            
                        const char *theSeparator = "\n\t";

                        for (const auto &theArgument : theMethod.itsArguments) {
                            theProxyCall += util::format("%s%s",
                                                        theSeparator,
                                                        theArgument.itsName.c_str());
                            theSeparator = ",\n\t";
                        }
                        theProxyCall += "\n)";
                    }

                    if (theArgumentUpdate.needsWarningSuppression) {
                        theOutputFile.printf("\n#pragma GCC diagnostic push\n"
                                             "#pragma GCC diagnostic ignored \"-Wmaybe-uninitialized\"\n");
                    }
                    
                    if (theMethod.itsReturnType->itsName == "void") {
                        theOutputFile.printf( "%s;\n", theProxyCall.c_str() );
                    }
                    else {
                        std::vector<typename StructType::Field> theReturnValue( 1, asValue( theProxyCall, theMethod.itsReturnType ) );
                        
                        theOutputFile.printf("\n%s",
                                                updateArguments( BABEL_UTIL_RANGE(theReturnValue),
                                                                 "",
                                                                 "appendMessageArguments", 
                                                                 "theReturnValue",
                                                                 "",
                                                                 asIpcValue,
                                                                 complicatedTypeWriter ).itsCode.c_str() );
                    }

                    if (theArgumentUpdate.needsWarningSuppression) {
                        theOutputFile.printf("#pragma GCC diagnostic pop\n");
                    }
                    
                }
                theOutputFile.printf("}\n\n");
            }
        }
        theOutputFile.printf("};\n\n");
    }
    
    static void addTransmitterWrapper( OutputFile &theOutputFile, const InterfaceType &theInterface, const Policy &thePolicy ) {
        BABEL_LOG_SCOPE();
        
        if (dynamic_cast<const MethodPolicy*>(&thePolicy)) {
            const std::string theClassName = thePolicy.getTransmitterSideClassName() + "Wrapper";

            theOutputFile.printf(
                "\n\n"
                "template <typename IpcPolicy>\n"
                "struct %1$s final :\n"
                "\tpublic %2$s,\n"
                "\tpublic IpcPolicy::Client {\n"
                "\n"
                "private:\n"
                "\tconst std::shared_ptr<%2$s> itsTransmitter;\n"
                "\tstd::weak_ptr<%2$s>         itsShortcut;\n"
                "\n"
                "\tauto getItsTransmitter() const {\n"
                "\t\tconst auto theShortcut = itsShortcut.lock();\n"
                "\t\treturn theShortcut ? theShortcut : itsTransmitter;\n"
                "\t}\n\n"
                "\ttemplate <typename Policy>\n"
                "\tfriend std::shared_ptr<%2$s>\n"
                "\t%3$s( std::shared_ptr<Policy>, const char * );\n"
                "\n"
                "\texplicit %1$s( const std::shared_ptr<%2$s> theTransmitter )\n"
                "\t\t: itsTransmitter(theTransmitter),\n"
                "\t\t  itsShortcut() {\n"
                "\t}\n"
                "\n",
                theClassName.c_str(),
                theInterface.itsName.c_str(),
                thePolicy.getTransmitterSideFactoryMethod().c_str() );

            {
                ScopedIndent theIndent( theOutputFile );

                for (const auto &theMethod : theInterface.itsMethods) {
                    const auto theReturnTypeName = theMethod.itsReturnType->getItsBaseType()->itsName;
                    const bool hasReturnValue = (theReturnTypeName != "void");

                    addMethodOverrideStart(theOutputFile, theMethod);

                    std::string theArguments;
                    
                    for (const auto theArgument : theMethod.itsArguments) {
                        theArguments.append( theArguments.empty() ? " " : ", " );
                        theArguments.append( theArgument.itsName );
                    }

                    theOutputFile.printf("\t%sgetItsTransmitter()->%s(%s%s);\n",
                                        (hasReturnValue ? "return " : ""),
                                        theMethod.itsName.c_str(),
                                        theArguments.c_str(),
                                        theArguments.empty() ? "" : " ");
                    theOutputFile.printf("}\n\n");
                }
                theOutputFile.printf("void implementorIsRegistered( const typename IpcPolicy::Implementor::SharedPtr &theImplementor ) override {\n"
                                     "\titsShortcut = std::dynamic_pointer_cast<%s>(theImplementor);\n"
                                     "}\n\n",
                                     theInterface.itsName.c_str());
            }

            theOutputFile.printf("};\n"
                                 "\n");
        }
    }
    
    static void addTransmitterStub( OutputFile &theOutputFile, const InterfaceType &theInterface, const Policy &thePolicy ) {
        BABEL_LOG_SCOPE();

        const std::string theClassName = thePolicy.getTransmitterSideClassName();
        
        theOutputFile.printf(
            "template <typename IpcPolicy>\n"
            "struct %1$s final : public %2$s {\n"
            "\n",
            theClassName.c_str(),
            theInterface.itsName.c_str() );

        addIpcHelpersTemplates(theOutputFile, theClassName );
        
        theOutputFile.printf(
            "private:\n"
            "\tstatic const char theInterfaceName[];\n"
            "\n"
            "\tusing IpcPolicyPtr = std::shared_ptr<IpcPolicy>;\n"
            "\tusing Boolean      = typename IpcPolicy::Boolean;\n"
            "\tusing Message      = typename IpcPolicy::Message;\n"
            "\n"
            "\ttemplate <typename Policy>\n"
            "\tfriend std::shared_ptr<%2$s>\n"
            "\t%3$s( std::shared_ptr<Policy>, const char * );\n"
            "\t\n"
            "\tIpcPolicyPtr      itsIpcPolicy;\n"
            "\tconst std::string itsObjectPath;\n"
            "\t\n"
            "\t%1$s( IpcPolicyPtr theIpcPolicy, const char *theObjectPath )\n"
            "\t\t: itsIpcPolicy(theIpcPolicy),\n"
            "\t\t  itsObjectPath(theObjectPath) {\n"
            "\t}\n"
            "\n",
            theClassName.c_str(),
            theInterface.itsName.c_str(),
            thePolicy.getTransmitterSideFactoryMethod().c_str());
        {
            ScopedIndent theIndent( theOutputFile );
            
            for (const auto &theMethod : theInterface.itsMethods) {
                static const char theReturnValueName[] = "theIpcReturnValue";
                static const char theMessageTypeName[] = "Message";
                static const char theMessageName[]     = "theIpcMessage";
                
                const auto theReturnTypeName = theMethod.itsReturnType->getItsBaseType()->itsName;
                const bool hasReturnValue = (theReturnTypeName != "void");
                const int  theLocalVariableTypeWidth = static_cast<int>(std::max( theReturnTypeName.size(), (sizeof(theMessageTypeName)-1)));
                
                addMethodOverrideStart(theOutputFile, theMethod);
                {
                    ScopedIndent theMethodBodyIndent( theOutputFile );

                    if (hasReturnValue) {
                        theOutputFile.printf("%*s %s%s;\n", 
                                                -theLocalVariableTypeWidth, 
                                                theReturnTypeName.c_str(),
                                                theReturnValueName,
                                                makeInitValue( theMethod.itsReturnType ).c_str());
                    }

                    theOutputFile.printf("%*s %s =\n"
                                         "\titsIpcPolicy->%s(\n"
                                         "\t\titsObjectPath.c_str(),\n"
                                         "\t\ttheInterfaceName,\n"
                                         "\t\t\"%s\"\n"
                                         "\t);\n\n",
                                        -theLocalVariableTypeWidth, 
                                        theMessageTypeName, 
                                        theMessageName,
                                        thePolicy.getTransmitterSideCreation().c_str(),
                                        theMethod.itsName.c_str());

                    theOutputFile.printf("%s",
                                         updateArguments( BABEL_UTIL_RANGE(theMethod.itsArguments),
                                                            "",
                                                            "appendMessageArguments", 
                                                            theMessageName,
                                                            "",
                                                            asIpcValue,
                                                            complicatedTypeWriter ).itsCode.c_str() );

                    const std::string theProxyCall = util::format( "itsIpcPolicy->%s( %s )",
                                                                   thePolicy.getTransmitterSideInvocation().c_str(),
                                                                   theMessageName );

                    if (hasReturnValue) {
                        static const char theMessageResult[] = "theMessageReturnValue";
                        
                        theOutputFile.printf( "\nauto %s = %s;\n", theMessageResult, theProxyCall.c_str());
                        
                        std::vector<typename StructType::Field> theReturnValue( 1, asValue( theReturnValueName, theMethod.itsReturnType ) );
                        
                        theOutputFile.printf("\n%s",
                                               updateArguments( BABEL_UTIL_RANGE(theReturnValue),
                                                                 "",
                                                                 "getMessageArguments", 
                                                                 theMessageResult, 
                                                                 "",
                                                                 asIpcReference,
                                                                 complicatedTypeReader ).itsCode.c_str() );
                        theOutputFile.printf("\nreturn %s;\n", theReturnValueName);
                    }
                    else {
                        theOutputFile.printf("(void)%s;\n", theProxyCall.c_str());
                    }
                }
                theOutputFile.printf("}\n\n");
            }
        }
        theOutputFile.printf("};\n"
                             "\n"
                             "template <typename IpcPolicy>\n"
                             "const char %s<IpcPolicy>::theInterfaceName[] = \"%s\";\n",
                             theClassName.c_str(),
                             interfaceNameOf( theInterface ).c_str());

        if (dynamic_cast<const MethodPolicy*>(&thePolicy)) {
            addTransmitterWrapper(theOutputFile,theInterface,thePolicy);
        }
    }
    
    
    static void addStubClasses( OutputFile &theOutputFile, const std::vector<const InterfaceType *> &theInterfaces ) {
        BABEL_LOG_SCOPE();

        for (auto theInterface : theInterfaces) {
            ScopedGuard                          theStubGuard( theOutputFile, std::string("have_") + theInterface->itsName + "_stubs");
            ScopedNamespace                      theStubNameSpace( theOutputFile, theInterface->getItsNamespace());
            std::vector<std::unique_ptr<Policy>> thePolicies;
            
            thePolicies.emplace_back(std::make_unique<MethodPolicy>(*theInterface));
            
            if (!theInterface->hasReturnValues()) {
                thePolicies.emplace_back(std::make_unique<SignalPolicy>(*theInterface));
            }
            
            for (const auto &thePolicy : thePolicies) {
                addReceiverStub( theOutputFile, *theInterface, *thePolicy );
                addTransmitterStub( theOutputFile, *theInterface, *thePolicy );
            }
        }
    }
    
    static bool needsBoolean( const Type *theType ) {
        return (theType->getItsBaseType()->itsName == "bool")   ||
               recursive_cast<StructType>(theType)              || 
               //recursive_cast<Container>(theType)               ||
                (recursive_cast<PointerType>(theType) && needsBoolean(recursive_cast<PointerType>(theType)->itsReferredType));    
    }
    
    static void addFactoryTemplates( OutputFile &theOutputFile, const std::vector<const InterfaceType *> &theInterfaces, SyntacticElements theSyntacticElement ) {
        BABEL_LOG_SCOPE();

    
        static CodeTemplates theTemplates;
        
        if (theTemplates.empty()) {
            const std::string theFixedArgumentType("std::shared_ptr<IpcPolicy>");
            
            for (auto theInterface : theInterfaces) {
                const auto theVariableArgument = util::format("std::shared_ptr<%s>", theInterface->itsName.c_str());
                const auto theTypeWidth = -(static_cast<int>( std::max(theFixedArgumentType.size(), theVariableArgument.size())) + 1);

                std::vector<std::unique_ptr<Policy>> thePolicies;
            
                thePolicies.emplace_back(std::make_unique<MethodPolicy>(*theInterface));
            
                if (!theInterface->hasReturnValues()) {
                    thePolicies.emplace_back(std::make_unique<SignalPolicy>(*theInterface));
                }

                BABEL_LOGF("Making factory for %s which has %lu policies", theInterface->itsName.c_str(), thePolicies.size());
            
                for (const auto &thePolicy : thePolicies) {
                    std::map<std::size_t, std::size_t>                         theColumnWidths;
                    std::map< std::size_t, std::map<std::size_t, std::string>> theLinesAndColumns;
                    std::size_t                                                theWidestMethodName = 0;
                    std::size_t                                                theLine             = 0;

                    BABEL_LOGF("Making factory for policy %s", thePolicy->getItsName().c_str());
                
                    for  (const InterfaceType::Method &theMethod : theInterface->itsMethods ) {
                        theWidestMethodName = std::max( theWidestMethodName, theMethod.itsName.size() );

                        auto        &theColumns      = theLinesAndColumns[theLine++];
                        std::size_t  theColumnNumber = 0;

                        if (thePolicy->isMethodInterface) {
                            theColumns[theColumnNumber++] = asIpcTemplateTypeName(theMethod.itsReturnType);
                        }

                        if (theMethod.itsArguments.empty()) {
                            theColumns[theColumnNumber++] = "void";
                        }
                        else {
                            for (const auto &theArgument : theMethod.itsArguments) {
                                theColumns[theColumnNumber++] = asIpcTemplateTypeName(theArgument.itsType);
                            }
                        }
                        for (std::size_t i = 0; i < theColumns.size(); i++) {
                            theColumnWidths[i] = std::max( theColumnWidths[i], theColumns[i].size() );
                        }
                    }                    
                    std::string theMethodTable;
                    theLine = 0;

                    for  (const InterfaceType::Method &theMethod : theInterface->itsMethods ) {
                        auto        &theColumns      = theLinesAndColumns[theLine++];
                        std::size_t theColumn = 0;
                        theMethodTable += "\t\t.template " + thePolicy->getReceiverSideRegisterMethod() + "< ";
                        theMethodTable += util::format( "%*s", -static_cast<int>(theColumnWidths[theColumn]), theColumns[theColumn].c_str());

                        while (++theColumn < theColumns.size()) {
                            theMethodTable += util::format(", %*s", -static_cast<int>(theColumnWidths[theColumn]), theColumns[theColumn].c_str());
                        }
                        while (theColumn < theColumnWidths.size()) {
                            theMethodTable += util::format("  %*s", -static_cast<int>(theColumnWidths[theColumn]), "");
                            theColumn += 1;
                        }
                        theMethodTable += util::format(" >( \"%s\", %*sImplementationProxy::%s )\n", 
                                                        theMethod.itsName.c_str(), 
                                                        static_cast<int>(theWidestMethodName-theMethod.itsName.size()), 
                                                        "",
                                                        theMethod.itsName.c_str());
                    }
                    
                    const bool needsBooleanTypedef = std::regex_search(
                                                        BABEL_UTIL_RANGE(theMethodTable),
                                                        std::regex("\\bBoolean\\b")
                                                     );

                    theTemplates.emplace_back(
                        util::format(   "%1$s"
                                        "template <typename IpcPolicy>\n"
                                        "std::shared_ptr<%2$s>\n"
                                        "%3$s(\n"
                                        "\t%4$*7$stheIpcPolicy,\n"
                                        "\t%5$*7$stheObjectPath,\n"
                                        "\t%6$*7$stheImplementation\n" 
                                        ")",
                                        thePolicy->getReceiverSideFactoryComment().c_str(),
                                        theInterface->itsName.c_str(),
                                        thePolicy->getReceiverSideFactoryMethod().c_str(),
                                        theFixedArgumentType.c_str(),
                                        "const char*",
                                        theVariableArgument.c_str(),
                                        theTypeWidth ),
                        util::format(   "\n{\n" 
                                        "\tusing ImplementationProxy = %s<IpcPolicy>;\n"
                                        "%s"
                                        "\n"
                                        "\tauto theProxy = std::shared_ptr<ImplementationProxy>(\n"
                                        "\t\tnew ImplementationProxy(theIpcPolicy, theObjectPath, theImplementation)\n"
                                        "\t);\n"
                                        "\n"
                                        "\ttheIpcPolicy->%s(theObjectPath, \"%s\", theProxy )\n"
                                        "%s"
                                        "\t\t.publish();\n"
                                        "\n"
                                        "\treturn theProxy;\n"
                                        "}",
                                        thePolicy->getReceiverSideClassName().c_str(),
                                        needsBooleanTypedef ? "\tusing Boolean             = typename IpcPolicy::Boolean;\n" : "",
                                        thePolicy->getRegisterMethod().c_str(),
                                        interfaceNameOf( *theInterface ).c_str(),
                                        theMethodTable.c_str()) 
                    );
                    theTemplates.emplace_back(
                        util::format(   "%s"
                                        "template <typename IpcPolicy>\n"
                                        "std::shared_ptr<%s>\n"
                                        "%s( %s theIpcPolicy, const char *theObjectPath )",
                                        thePolicy->getTransmitterSideFactoryComment().c_str(),
                                        theInterface->itsName.c_str(),
                                        thePolicy->getTransmitterSideFactoryMethod().c_str(),
                                        theFixedArgumentType.c_str() ),
                        dynamic_cast<const SignalPolicy*>(thePolicy.get()) ?
                            (util::format(  "\n{\n"
                                            "\treturn std::shared_ptr<%s>(new %s<IpcPolicy>(theIpcPolicy,theObjectPath));\n"
                                            "}",
                                            theInterface->getItsSimpleName().c_str(),
                                            thePolicy->getTransmitterSideClassName().c_str()) )
                            :
                            (util::format(  "\n{\n"
                                            "\tconst auto theProxy = std::shared_ptr<%1$s>(\n"
                                            "\t\tnew %1$s( \n"
                                            "\t\t\tstd::shared_ptr<%2$s>(\n"
                                            "\t\t\t\tnew %2$s(theIpcPolicy,theObjectPath)\n"
                                            "\t\t\t)\n"
                                            "\t\t)\n"
                                            "\t);\n"
                                            "\ttheProxy->itsShortcut = \n"
                                            "\t\tstd::dynamic_pointer_cast<%3$s>(\n"
                                            "\t\t\ttheIpcPolicy->registerClient( theProxy, theObjectPath, %2$s::theInterfaceName )\n"
                                            "\t\t);\n"
                                            "\treturn theProxy;\n"
                                            "}",
                                            util::format("%sWrapper<IpcPolicy>", thePolicy->getTransmitterSideClassName().c_str()).c_str(),
                                            util::format("%s<IpcPolicy>", thePolicy->getTransmitterSideClassName().c_str()).c_str(),
                                            theInterface->itsName.c_str())));
                }
            }
        }
        
        std::size_t i = 0;
        for (auto theInterface : theInterfaces) {
            ScopedGuard     theTemplateGuard( theOutputFile,
                                              std::string("have_") + theInterface->itsName + 
                                                (theSyntacticElement == SyntacticElements::Declaration ? "_declarations" : "_implementations"));
            ScopedNamespace theTemplateNamespace( theOutputFile,
                                                  theInterface->getItsNamespace());
            theTemplates[i++].output(theOutputFile, theSyntacticElement );
            theTemplates[i++].output(theOutputFile, theSyntacticElement );
            
            if (!theInterface->hasReturnValues()) {
                // These kind of interfaces has four factory methods
                //
                theTemplates[i++].output(theOutputFile, theSyntacticElement );
                theTemplates[i++].output(theOutputFile, theSyntacticElement );
            }

        }
    }
    

class Generator : public GeneratorImplementationBase {
public:
    Generator() : GeneratorImplementationBase(
        "Ipc",
        {
            "memory",
            "type_traits",
            "algorithm",
            "tuple",
            "array"
        },
        true)   
    {
    }

    void doRealize( const CompilationUnit &theCompilationUnit,
                    OutputFile            &theOutputFile,
                    const BabelcOptionSet &theOptions ) override final;

};


void Generator::doRealize( const CompilationUnit &theCompilationUnit,
                           OutputFile            &theOutputFile,
                           const BabelcOptionSet & ) {    
    BABEL_LOG_SCOPE();
    
    {
        struct InterfaceFilter : public ReferenceFilter {
            bool breakAt( const Type * ) override {
                return false;
            }
        };
        
        std::vector<const InterfaceType *> theInterfaces;
        Types                              theTypeClosure;
        InterfaceFilter                    theInterfaceFilter;
        
        for (auto theType : theCompilationUnit.itsTypes) {
            auto theInterface = dynamic_cast<const InterfaceType*>(theType.second);
            if (theInterface) {
                BABEL_LOGF( "Found interface %s", theInterface->itsName.c_str() );
                theInterfaces.push_back(theInterface);
                theInterface->getItsReferredTypes(theTypeClosure, theInterfaceFilter);
            }
        }

        if (theInterfaces.empty()) {
            throw std::runtime_error("No interfaces declared for ipc generator usage.");
        }
        else {
            unsigned int theErrorCount = 0;
            
            for (auto theInterface : theInterfaces) {
                for (auto theMethod : theInterface->itsMethods) {
                    for (auto theArgument : theMethod.itsArguments ) {
                        if (theArgument.isReference && !theArgument.isConst) {
                            fprintf( stderr, "%s: Non-const reference argument '%s' not supported by babelc/dbus\n", theArgument.itsLocation.c_str(), theArgument.itsName.c_str() );
                            theErrorCount += 1;
                        }
                    }
                }
            }
            
            if (theErrorCount > 0) {
                throw std::invalid_argument("Argument specification(s) not compatible with DBUS backend");
            }
            
        }
        
        BABEL_LOG("Type closure is:");

        for (auto theType : theTypeClosure) {
            BABEL_LOG("----------------------------------------------");
            BABEL_LOGF("%s", theType.second->toString("").c_str());
        }
        BABEL_LOG("End of closure");
        
        addFactoryTemplates( theOutputFile, theInterfaces, SyntacticElements::Declaration );

        theOutputFile.printf( "\n"
                              "//%s\n"
                              "// Below here is only implementation code which may be perused at leisure\n"
                              "// by an interested reader.\n",
                              std::string(78, '=').c_str());
        
        declareHelpers(theOutputFile, theTypeClosure );
        declareIpcHelpers( theOutputFile, theTypeClosure );
        
        std::set<std::string> theNamespaces;
        addTypeTemplates(theOutputFile, theTypeClosure, SyntacticElements::Declaration, theNamespaces);
        addTypeTemplates(theOutputFile, theTypeClosure, SyntacticElements::Implementation, theNamespaces);
        
        addStubClasses(theOutputFile, theInterfaces );
        addFactoryTemplates( theOutputFile, theInterfaces, SyntacticElements::Implementation );
    }
    //addPredefinedBabelTemplates( theOutputFile, SyntacticElements::Implementation );
}

Generator theInstance;

} } } }
