/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 21, 2016
 */

#ifndef PLUGINS_GENERATORS_BABEL_PLUGINS_GENERATOR_H_
#define PLUGINS_GENERATORS_BABEL_PLUGINS_GENERATOR_H_

#include <map>
#include <string>
#include <cstdio>
#include <memory>
#include <plugins/babel_plugins.h>
#include <util/babel_util.h>
#include <common/babel_common.h>

namespace babel { namespace plugins { namespace generator {

class OutputFile;


class GeneratorImplementationBase : public GeneratorInterface {
public:

    /**
     * Accessor for defined generators. Throws an exception if some names aren't valid generator names.
     * @param theGeneratorNames See return value.
     * @return The generators matching the passed names or all all generators if nullptr is passed.
     */
    static GeneratorInterface::Container getGenerators( const std::set<std::string>  * const theGeneratorNames );

    /**
     * @return The name of this generator.
     */
    const std::string &getItsName() const override final {
        return itsName;
    }

    FilePath getItsFilePath( const FilePath &basedUponThisPath ) const override final;
    
    void realize( const CompilationUnit &theCompilationUnit, 
                  const BabelcOptionSet &theOptions, 
                  const babel::FilePath &theOutputDirectory ) override final;
    
    bool needsRealization( const BabelcOptionSet& theGivenBabelcOptions, 
                           const FilePath& theInputFile, 
                           const FilePath& theOutputDirectory, 
                           const FilePaths& theDependentFiles ) override final;

    static std::string formatTypeWarning( const Type &theType, const std::string &thePrefix, const std::string &theMessage ); 
    static void        reportTypeWarning( const Type &theType, const std::string &thePrefix, const std::string &theMessage ); 
    static void        report( const std::string &theMessage );
    
    [[noreturn]]
    static void reportTypeError( const Type &theType, const std::string &theMessage ); 

protected:
    GeneratorImplementationBase( const std::string              &theName,
                                    const std::vector<std::string> &theIncludeFiles,
                                    bool                            useIncludeGuard );
    
    void declareHelpers( OutputFile &theOutputFile, const Types &forTheTypes );

    virtual void doRealize( const CompilationUnit &theCompilationUnit,
                            OutputFile            &theOutputFile,
                            const BabelcOptionSet &theOptions )     = 0;

private:
    /**
     * Creates a path for the passed compilation unit suitable for this generator.
     * @param forCompilationUnit The compilation unit whose path to return
     * @return The file path to use for the given compilation unit.
     */
    FilePath getFilePath( const babel::FilePath &forCompilationUnit,
                          const babel::FilePath &inOutputDirectory );

    const std::string        itsName;
    std::vector<std::string> itsIncludeFiles;
    const bool               useIncludeGuard;

    static std::map<std::string, GeneratorInterface *> ourGenerators;
};

/**
 * Helper class for a #define guard. The name of the guard
 * will be capitalized and non-alphanumerics will be replaced
 * with underscores. Double underscores will also be prepended
 * and appended.
 */
class ScopedGuard {
public:
    ScopedGuard( OutputFile &theOutputFile, const std::string &theName );
    ~ScopedGuard();
private:
    OutputFile &itsOutputFile;
};

/**
 * Encapsulation of an output file with indent handling.
 */
class OutputFile {
public:
    /**
     * Opens and truncates the file identified by the path parameter. Throws an exception if the file cannot be opened.
     * @param thePath The file to open.
     */
    OutputFile( const FilePath &thePath, const BabelcOptionSet theOptions);

    OutputFile( const OutputFile &) = delete;
    
    /**
     * Flushes and closes the output file. Throws an exception upon failure.
     */
    ~OutputFile();

    void printf( const char *theFormat, ...) __attribute__((format (printf,2,3)));

    void increaseIndent();
    void decreaseIndent();
    
    const FilePath itsFilePath;

private:
    std::string::size_type theIndentStep() const {
        return (itsIndentChar == '\t') ? 1 : 4;
    }

    const char                   itsIndentChar;
    FILE * const                 itsOutputFile;
    std::string                  itsIndent;
    bool                         lastCharWasNewLine = false;
};

/**
 * Helper class for a scoped indentation. Indent is increased by the
 * constructor and decreased by the destructor.
 */
class ScopedIndent {
public:
    ScopedIndent( OutputFile &theOutputFile ) : itsOutputFile( theOutputFile ) {
        itsOutputFile.increaseIndent();
    }
    ~ScopedIndent() {
        itsOutputFile.decreaseIndent();
    }
private:
    OutputFile &itsOutputFile;
};

/**
 * Helper class for a name space. Will add namespace declaration in
 * the constructor and conclude it in the destructor. The indent
 * level will also be increased/decreased.
 */
class ScopedNamespace {
public:
    ScopedNamespace( OutputFile &theOutputFile, const Namespace &theNamespace );

    ~ScopedNamespace();
private:
    OutputFile       &itsOutputFile;
    const Namespace   itsNamespace;
};

/**
 * Helper class for a type. Will add include guard and namespace declaration in
 * the constructor and conclude them in the destructor.
 */
class ScopedType {
public:
    ScopedType( OutputFile &theOutputFile, const Type &theType, const std::string theGuardSuffix );

    ~ScopedType();
private:
    OutputFile      &itsOutputFile;
    ScopedGuard      itsIncludeGuard;
    ScopedNamespace  itsNamespace;
};


} } }



#endif /* PLUGINS_GENERATORS_BABEL_PLUGINS_GENERATOR_H_ */
