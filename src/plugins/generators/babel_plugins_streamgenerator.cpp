/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 21, 2016
 */

#include <cstdio>
#include <vector>
#include <map>
#include <numeric>
#include <util/babel_logger.h>

#include "babel_plugins_generator.h"

namespace babel { namespace plugins { namespace generator { namespace stream {
    const auto theNameSpaceName = "babel::stream";

    enum class SyntacticElements {
        Declaration,
        Implementation
    };
    
    class CodeTemplate {
        std::string itsDeclaration;
        std::string itsBody;
        
    public:
        CodeTemplate( const std::string theDeclaration, const std::string theBody ) 
        : itsDeclaration(theDeclaration),
          itsBody(theBody) {
        }

        void output( OutputFile &toFile, SyntacticElements theSyntacticElement ) const {
            toFile.printf("%s", itsDeclaration.c_str() );

            if (theSyntacticElement == SyntacticElements::Declaration) {
                toFile.printf(";\n\n");
            }
            else {
                toFile.printf(" %s\n", itsBody.c_str());
            }
        }
    };

    using CodeTemplates = std::vector<CodeTemplate >;

    static void addTypeDeclarationTemplate(CodeTemplates &theCodeTemplates, const Type &forTheType) {

        theCodeTemplates.emplace_back(
            babel::util::format(
                "template <class CharT, class Traits>\n"
                "std::basic_ostream<CharT,Traits>&\n"
                "operator <<( std::basic_ostream<CharT,Traits>&theStream, const %s &theValue )",
                forTheType.getItsSimpleName().c_str()
            ),
            babel::util::format(
                "{\n"
                "\treturn appendValue( theStream, theValue, %s::Indent(0));\n"
                "}\n",
                theNameSpaceName
            )
        );
        theCodeTemplates.emplace_back(
            babel::util::format(
                "template <class CharT, class Traits>\n"
                "std::basic_istream<CharT,Traits>&\n"
                "operator >>( std::basic_istream<CharT,Traits>&theStream, %s &theValue )",
                forTheType.getItsSimpleName().c_str()
            ),
            babel::util::format(
                "{\n"
                "\treturn readValue( theStream, theValue );\n"
                "}\n"
            )
        );
        
    }
    
    static std::string createAppendTemplateDeclaration( const Type &forTheType, bool needsIndent ) {
        return util::format(
                    "template <class CharT, class Traits>\n"
                    "std::basic_ostream<CharT,Traits>&\n"
                    "appendValue( std::basic_ostream<CharT,Traits>&theStream, const %s &%s, %s::Indent %s)",
                    forTheType.getItsSimpleName().c_str(),
                    (forTheType.isVacuous() ? "" : "theValue"),
                    theNameSpaceName,
                    (needsIndent && !forTheType.isVacuous() ? "theIndent " : "")
                );
    }
    
    static std::string createReadTemplateDeclaration( const Type &forTheType) {
        return util::format(
                    "template <class CharT, class Traits>\n"
                    "std::basic_istream<CharT,Traits>&\n"
                    "readValue( std::basic_istream<CharT,Traits>&fromStream, %s &%s)",
                    forTheType.getItsSimpleName().c_str(),
                    (forTheType.isVacuous() ? "" : "theValue "));
    }

    static void addTypeTemplates( CodeTemplates &theCodeTemplates, const StructType &forTheStruct ) {
        
        addTypeDeclarationTemplate( theCodeTemplates, forTheStruct );
        
        std::string theWriteBody;
        std::string theReadBody;
        
        if (forTheStruct.isVacuous()) {
            theWriteBody = "{\n"
                           "\treturn (theStream << \"{}\");\n"
                           "}";
            theReadBody = "{\n"
                          "\treturn babel::stream::expect( fromStream, \"{}\");\n"
                          "}";
        }
        else {
            const auto &theFields          = forTheStruct.itsFields;
            const auto theFieldNumberWidth = static_cast<int>(std::to_string(theFields.size()-1).size());
            const auto theMaxFieldWidth    = static_cast<int>( std::accumulate(BABEL_UTIL_RANGE(theFields), 0, 
                                                                    []( std::size_t maxLength, const StructType::Field &theField) {
                                                                        return std::max( maxLength, theField.itsName.length()); 
                                                                    }));
            std::string  theShortHeaders;
            std::string  theLongHeaders;
            std::string  theBody;
            unsigned int theFieldNumber = 0;
            int          thePreviousFieldLength = 0;
            for (const auto &theField : theFields) {
                const auto theFieldLength = static_cast<int>(theField.itsName.length());
                if (theFieldNumber != 0) {
                    theShortHeaders.append(util::format("%-*s",(3+(theMaxFieldWidth-thePreviousFieldLength)),", "));
                    theLongHeaders.append(", ");
                    theBody.append(" << ',' << theContentIndent;\n\t");
                }
                theShortHeaders.append(util::format("\"%s:\"", theField.itsName.c_str()));
                theLongHeaders.append(util::format("\"%s%*s",theField.itsName.c_str(),(3+(theMaxFieldWidth-theFieldLength)),": \""));
                theBody.append(
                    util::format(
                        "appendValue( theStream << theLabels[%*u], theValue.%s,%*stheContentIndent )",
                        theFieldNumberWidth,
                        theFieldNumber,
                        theField.itsName.c_str(),
                        (theMaxFieldWidth-theFieldLength)+1,
                        ""
                        )
                );
                thePreviousFieldLength = theFieldLength;
                theFieldNumber++; 
            }
            theBody.append(" << theIndent;\n");
            theWriteBody =
                babel::util::format(
                    "{\n"
                    "\tstatic const char * const theLabelSet[2][%lu] = {\n"
                    "\t\t{ %s },\n"
                    "\t\t{ %s }\n"
                    "\t};\n"
                    "\tconst auto theContentIndent = theIndent.next();\n"
                    "\tconst auto theLabels        = theLabelSet[theIndent.isActive()];\n"
                    "\ttheStream << '{' << theContentIndent;\n"
                    "\t%s"
                    "\ttheStream << '}';\n"
                    "\treturn theStream;\n"
                    "}\n",
                    theFields.size(),
                    theShortHeaders.c_str(),
                    theLongHeaders.c_str(),
                    theBody.c_str()
                );

            std::string theReadStatements;
            
            for (const auto &theField : theFields) {
                if (!theReadStatements.empty()) {
                    theReadStatements.append(" babel::stream::expect( fromStream, \",\" ) &&\n");
                }
                theReadStatements.append(babel::util::format(
                    "\t\tBABEL_STREAM_READ_FIELD( %2$-*1$s ) &&",
                        theMaxFieldWidth,
                        theField.itsName.c_str()
                ));
            }

            theReadBody =
                babel::util::format(
                    "{\n"
                    "\t#define BABEL_STREAM_READ_FIELD( name ) \\\n"
                    "\t\treadValue( babel::stream::expect( babel::stream::expect( fromStream, #name ), \":\" ), theValue.name )\n"
                    "\n"
                    "\t(void)(\n"
                    "\t\tbabel::stream::expect( fromStream, \"{\" ) &&\n"
                    "%s\n"
                    "\t\tbabel::stream::expect( fromStream, \"}\" )\n"
                    "\t);\n"
                    "\n"
                    "\treturn fromStream;\n"
                    "\n"
                    "\t#undef BABEL_STREAM_READ_FIELD\n"
                    "}\n",
                    theReadStatements.c_str()
                );
        }
        theCodeTemplates.emplace_back( createAppendTemplateDeclaration(forTheStruct, true),
                                       theWriteBody );
        theCodeTemplates.emplace_back( createReadTemplateDeclaration(forTheStruct),
                                       theReadBody );
    }
    
    static void addTypeTemplates( CodeTemplates &theCodeTemplates, const VariantType &forTheVariant ) {
        addTypeDeclarationTemplate( theCodeTemplates, forTheVariant );
        
        {
            std::string theCases;
            
            for (std::size_t i = 0; i < forTheVariant.itsTypes.size(); i++) {
                theCases.append(
                  util::format("\tcase %lu: return appendValue( theStream, %s<%s>(theValue), theContentIndent) << theIndent << '}';\n",
                                i, 
                                forTheVariant.itsGetMethod.c_str(),
                                forTheVariant.itsTypes[i]->itsName.c_str())
                );
            }
            theCodeTemplates.emplace_back(
                createAppendTemplateDeclaration(forTheVariant, true),
                babel::util::format(
                    "{\n"
                    "\tconst auto theContentIndent = theIndent.next();\n"
                    "\ttheStream << '{' << theContentIndent;\n"
                    "\ttheStream << theValue.%1$s() << ',' << theContentIndent;\n"
                    "\tswitch (theValue.%1$s()) {\n"
                    "%2$s"
                    "\t}\n"
                    "\tthrow std::runtime_error(\"Unknown part of variant %3$s\");\n"
                    "}\n",
                    forTheVariant.itsIndexMethod.c_str(),
                    theCases.c_str(),
                    forTheVariant.itsName.c_str()
                )
            );
        }
        {
            std::string theCases;
            
            for (std::size_t i = 0; i < forTheVariant.itsTypes.size(); i++) {
                if (!theCases.empty()) {
                    theCases.append( "\n" );
                }
                theCases.append(
                  util::format("\tcase %lu: {\n"
                               "\t\t%s theCase;\n"
                               "\t\tif (readValue(fromStream,theCase)) {\n"
                               "\t\t\ttheValue = theCase;\n"
                               "\t\t}\n"
                               "\t}\n"
                               "\tbreak;",
                                i, 
                                forTheVariant.itsTypes[i]->itsName.c_str())
                );
            }
            theCodeTemplates.emplace_back(
                createReadTemplateDeclaration(forTheVariant),
                babel::util::format(
                    "{\n"
                    "\tdecltype(theValue.%s()) theIndex;\n"
                    "\tif (babel::stream::expect(readValue(babel::stream::expect(fromStream,\"{\"),theIndex),\",\")) {\n"
                    "\t\tswitch (theIndex) {\n"
                    "%s\n"
                    "\t\tdefault: throw std::runtime_error(\"Unknown part of variant %s\");\n"
                    "\t\t}\n"
                    "\t}\n"
                    "\treturn fromStream ? babel::stream::expect(fromStream,\"}\") : fromStream;\n"
                    "}",
                    forTheVariant.itsIndexMethod.c_str(),
                    theCases.c_str(),
                    forTheVariant.itsName.c_str()
                ) 
            );
        }
    }

    static void addTypeTemplates( CodeTemplates &theCodeTemplates, const OptionalType &forTheOptional ) {
        addTypeDeclarationTemplate( theCodeTemplates, forTheOptional );
        
        {
            theCodeTemplates.emplace_back(
                createAppendTemplateDeclaration(forTheOptional, true),
                babel::util::format(
                    "{\n"
                    "\tconst auto theContentIndent = theIndent.next();\n"
                    "\treturn theValue ? (appendValue( (theStream << '{' << theContentIndent), *theValue, theContentIndent) << theIndent << '}') : (theStream << \"{}\");"
                    "}\n"
                )
            );
        }
        {
            theCodeTemplates.emplace_back(
                createReadTemplateDeclaration(forTheOptional),
                babel::util::format(
                    "{\n"
                    "\tif (babel::stream::expect(fromStream,\"{\")) {\n"
                    "\t\tif (fromStream.peek() == '}') {\n"
                    "\t\t\tif (fromStream.get()) { theValue = %s(); }\n"
                    "\t\t}\n"
                    "\t\telse {\n"
                    "\t\t\t%s theActualValue;\n"
                    "\t\t\tif (babel::stream::expect( readValue(fromStream, theActualValue), \"}\")) { theValue = theActualValue; }"
                    "\t\t}\n"
                    "\t}\n"
                    "\treturn fromStream;\n"
                    "}",
                    forTheOptional.itsName.c_str(),
                    forTheOptional.itsElementType->itsName.c_str()
                )
            );
        }
    }
    
    static void addTypeTemplates( CodeTemplates &theCodeTemplates, const EnumeratedType &forTheEnum ) {
        addTypeDeclarationTemplate( theCodeTemplates, forTheEnum );
        
        {
            const auto &theValues        = forTheEnum.itsValues;
            const auto  theMaxValueWidth = static_cast<int>(std::accumulate(BABEL_UTIL_RANGE(theValues), 0, 
                                                            []( std::size_t theMaxSoFar, const std::string &theValue){
                                                                return std::max( theMaxSoFar, theValue.length());
                                                            }));
            std::string theCases;
            std::string theLookups;
            
            for (auto theValue : theValues) {
                if (!theCases.empty()) {
                    theCases.append("\n\t");
                    theLookups.append(",\n\t\t");
                }
                
                theCases.append( util::format(
                    "case %1$s::%2$-*3$s: return (theStream << \"%1$s::%2$s\");",  
                    forTheEnum.getItsSimpleName().c_str(),
                    theValue.c_str(),
                    theMaxValueWidth));
                theLookups.append( util::format(
                    "{ %1$s::%2$-*3$s, \"%2$s\" %4$*5$s",
                    forTheEnum.getItsSimpleName().c_str(),
                    theValue.c_str(),
                    theMaxValueWidth,
                    "}",
                    static_cast<int>(theMaxValueWidth - theValue.length())));
            }
            theCodeTemplates.emplace_back(
                createAppendTemplateDeclaration(forTheEnum, true),
                theCases.empty() ?
                    babel::util::format(
                        "{\n"
                        "\treturn babel::stream::appendValue( theStream, static_cast<std::underlying_type_t<%s>>(theValue), theIndent );\n"
                        "}\n",
                        forTheEnum.itsName.c_str()
                    )
                        :
                    babel::util::format(
                        "{\n"
                        "\tswitch (theValue) {\n"
                        "\t%1$s\n"
                        "\t};\n"
                        "\n"
                        "\tbabel::stream::appendValue( (theStream << \"<Invalid %2$s (\"), static_cast<std::underlying_type_t<%2$s>>(theValue), theIndent ) << \")>\";\n"
                        "\n"
                        "\ttheStream.setstate(std::ios::failbit);\n"
                        "\n"
                        "\treturn theStream;\n"
                        "}\n",
                        theCases.c_str(),
                        forTheEnum.itsName.c_str()
                    )
            );
            theCodeTemplates.emplace_back(
                createReadTemplateDeclaration(forTheEnum),
                theLookups.empty() ?
                    babel::util::format(
                        "{\n"
                        "\tstd::underlying_type_t<%1$s> theTemporary;\n"
                        "\tif (babel::stream::readValue( fromStream, theTemporary )) {\n"
                        "\t\ttheValue = static_cast<%1$s>(theTemporary);\n"
                        "\t}\n"
                        "\treturn fromStream;\n"
                        "}\n",
                        forTheEnum.itsName.c_str()
                    )
                        :
                    babel::util::format(
                        "{\n"
                        "\n"
                        "\tstatic const char theScope[] = \"%1$s::\";\n"
                        "\n"
                        "\tstatic const struct Lookup { %1$s itsValue; const char *itsImage; } theLookupTable[] = {\n"
                        "\t\t%2$s\n"
                        "\t};\n"
                        "\n"
                        "\tchar theIdentifier[%3$d];\n"
                        "\t\n"
                        "\tif (babel::stream::expectIdentifier( babel::stream::expect(fromStream,theScope), std::begin(theIdentifier), std::end(theIdentifier))) {\n"
                        "\t\tfor (const auto &theLookup : theLookupTable) {\n"
                        "\t\t\tif (std::strcmp( theLookup.itsImage, theIdentifier ) == 0) {\n"
                        "\t\t\t\ttheValue = theLookup.itsValue;\n"
                        "\t\t\t\treturn fromStream;\n"
                        "\t\t\t}\n"
                        "\t\t}\n"
                        "\t}\n"
                        "\n"
                        "\tfromStream.setstate(std::ios::failbit);\n"
                        "\n"
                        "\treturn fromStream;\n"
                        "}\n",
                        forTheEnum.getItsSimpleName().c_str(),
                        theLookups.c_str(),
                        (theMaxValueWidth+1)
                    )
            );
        }
    }
    
    
    template <class C> 
    bool derivedTypeAdded( const Type *theType, CodeTemplates &toTheTemplates ) {
        auto const theDerivedInstance = dynamic_cast<const C *>(theType);
        
        if (theDerivedInstance) {
            addTypeTemplates( toTheTemplates, *theDerivedInstance );
            return true;
        }
        else {
            return false;
        }
    }
    
    static void addTypeTemplates( OutputFile &theOutputFile, const Types &theTypes, SyntacticElements theSyntacticElement, std::set<std::string> &theNamespaces ) {
        static std::map< const Type *, CodeTemplates > theTypeTemplates;
        
        if (theTypeTemplates.empty()) {
            for (auto theType : theTypes) {
                CodeTemplates theCodeTemplates;
                auto const theTypeIsHandled = 
                    derivedTypeAdded<StructType>    (theType.second, theCodeTemplates ) ||
                    derivedTypeAdded<VariantType>   (theType.second, theCodeTemplates)  ||
                    derivedTypeAdded<OptionalType>  (theType.second, theCodeTemplates)  ||
                    derivedTypeAdded<EnumeratedType>(theType.second, theCodeTemplates );
                
                if (theTypeIsHandled) {
                    theTypeTemplates[theType.second] = theCodeTemplates;
                    theNamespaces.insert(NamespaceToString(theType.second->getItsNamespace()));
                }
            }
        }
        for (const auto &theTypeInfo : theTypeTemplates) {
            
            ScopedGuard     theTemplateGuard( theOutputFile,
                                              std::string("have_") + theNameSpaceName + "__" +
                                                theTypeInfo.first->itsName +
                                                (theSyntacticElement == SyntacticElements::Declaration ? "_declarations" : "_implementations"));
            ScopedNamespace theTemplateNamespace( theOutputFile,
                                                  theTypeInfo.first->getItsNamespace());
            const std::string thisNamespace = NamespaceToString(theTypeInfo.first->getItsNamespace());
            
            theOutputFile.printf("using %s::operator <<;\n\n", theNameSpaceName);
            theOutputFile.printf("using %s::operator >>;\n\n", theNameSpaceName);
            theOutputFile.printf("using %s::readValue;\n\n", theNameSpaceName);
            
            for (const auto &theTemplate : theTypeInfo.second) {
                if (theSyntacticElement == SyntacticElements::Implementation) {
                    for (auto theNamespace : theNamespaces) {
                        if (theNamespace != thisNamespace) {
                            theOutputFile.printf("using %sappendValue;\n", theNamespace.c_str());
                        }
                    }
                    if(!theNamespaces.empty()) {
                        theOutputFile.printf("\n");
                    }
                }
                theTemplate.output(theOutputFile, theSyntacticElement );
            }
        }
    }
    static void addPredefinedBabelTemplates( OutputFile &theOutputFile, SyntacticElements theSyntacticElement ) {
    
        static CodeTemplates theTemplates;
        
        if (theTemplates.empty()) {
            theTemplates.emplace_back( 
                std::string( "// = Predefined container support =============================\n\n"
                             "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_supported_container<T>::value, std::basic_ostream<CharT,Traits>&>\n"
                             "operator <<( std::basic_ostream<CharT,Traits>&theStream, const T &theValues )"),

                std::string( "{\n"
                             "   return appendValue( theStream, theValues, Indent(0) );\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_supported_container<T>::value, std::basic_istream<CharT,Traits>&>\n"
                             "operator >>( std::basic_istream<CharT,Traits>&fromStream, T &theValues )"),

                std::string( "{\n"
                             "   return readValue( fromStream, theValues );\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_supported_container<T>::value, std::basic_ostream<CharT,Traits>&>\n"
                             "appendValue( std::basic_ostream<CharT,Traits>&theStream, const T &theValues, Indent theIndent )"),

                std::string( "{\n"
                             "\tif (theValues.empty()) {\n"
                             "\t\treturn theStream << \"{}\";\n"
                             "\t}\n"
                             "\telse {\n"
                             "\t\tconst auto theNextIndent = theIndent.next();\n"
                             "\t\tauto       theValue      = theValues.begin();\n"
                             "\n"
                             "\t\ttheStream << theIndent << '{' << theNextIndent;\n"
                             "\n"
                             "\t\tfor (;;) {\n"
                             "\t\t\tappendValue( theStream, *theValue, theNextIndent );\n"
                             "\n"
                             "\t\t\tif (++theValue == theValues.end()) {\n"
                             "\t\t\t    break;\n"
                             "\t\t\t}\n"
                             "\n"
                             "\t\t\ttheStream << ',' << theNextIndent;\n"
                             "\t\t}\n"
                             "\n"
                             "\t\treturn theStream << theIndent << '}';\n"
                             "\t}\n"
                             "}\n")
            );
            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_fixed_container<T>::value, std::basic_istream<CharT,Traits>&>\n"
                             "readValue( std::basic_istream<CharT,Traits>&fromStream, T &theValues )"),

                std::string( "{\n"
                             "\tif (babel::stream::expect( fromStream, \"{\" ) && theValues.size()) {\n"
                             "\t\tsize_t i = 0;\n"
                             "\t\twhile (readValue( fromStream, theValues[i++])) {\n"
                             "\t\t\tif (i == theValues.size()) {\n"
                             "\t\t\t\tbreak;\n"
                             "\t\t\t}\n"
                             "\t\t\telse {\n"
                             "\t\t\t\tbabel::stream::expect( fromStream, \",\" );\n"
                             "\t\t\t}\n"
                             "\t\t}\n"
                             "\t}\n"
                             "\treturn babel::stream::expect( fromStream, \"}\" );\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_map_container<T>::value, std::basic_istream<CharT,Traits>&>\n"
                             "readValue( std::basic_istream<CharT,Traits>&fromStream, T &theValues )"),

                std::string( "{\n"
                             "\ttheValues.clear();\n"
                             "\n"
                             "\tif (expect(fromStream,\"{\")) {\n"
                             "\t\tif (fromStream.peek() == '}') {\n"
                             "\t\t\t(void)fromStream.get();\n"
                             "\t\t}\n"
                             "\t\telse {\n"
                             "\t\t\tstd::pair<typename T::key_type,typename T::mapped_type> theTemporary;\n"
                             "\t\t\tchar                                                    theNextDelimiter = ',';\n"
                             "\n"
                             "\t\t\twhile ((theNextDelimiter == ',') && readValue( fromStream, theTemporary )) {\n"
                             "\t\t\t\t(void)theValues.emplace( theTemporary );\n"
                             "\t\t\t\tbabel::stream::expectAnyOf( fromStream, theNextDelimiter, \",}\" );\n"
                             "\t\t\t}\n"
                             "\t\t}\n"
                             "\t}\n"
                             "\treturn fromStream;\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_supported_container<T>::value && !is_fixed_container<T>::value && !is_map_container<T>::value, std::basic_istream<CharT,Traits>&>\n"
                             "readValue( std::basic_istream<CharT,Traits>&fromStream, T &theValues )"),

                std::string( "{\n"
                             "\ttheValues.clear();\n"
                             "\n"
                             "\tif (expect(fromStream,\"{\")) {\n"
                             "\t\tif (fromStream.peek() == '}') {\n"
                             "\t\t\t(void)fromStream.get();\n"
                             "\t\t}\n"
                             "\t\telse {\n"
                             "\t\t\ttypename T::value_type theTemporary;\n"
                             "\t\t\tchar                     theNextDelimiter = ',';\n"
                             "\n"
                             "\t\t\twhile ((theNextDelimiter == ',') && readValue( fromStream, theTemporary )) {\n"
                             "\t\t\t\ttheValues.insert( theValues.end(), theTemporary );\n"
                             "\t\t\t\tbabel::stream::expectAnyOf( fromStream, theNextDelimiter, \",}\" );\n"
                             "\t\t\t}\n"
                             "\t\t}\n"
                             "\t}\n"
                             "\treturn fromStream;\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "// = Predefined pointer support ===============================\n\n"
                             "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_supported_ptr<T>::value, std::basic_ostream<CharT,Traits>&>\n"
                             "appendValue(std::basic_ostream<CharT,Traits>& toStream, const T &theValue, Indent theIndent )"),

                std::string( "{\n"
                             "\treturn theValue ?\n"
                             "\t\t\t(appendValue( toStream << '(' << theValue.get() << (theIndent.isActive() ? \" -> \" : \"->\"), *theValue, theIndent.next() ) << ')')\n"
                             "\t\t\t\t:\n"
                             "\t\t\t(toStream << \"(nullptr)\");\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_supported_ptr<T>::value, std::basic_istream<CharT,Traits>&>\n"
                             "readValue(std::basic_istream<CharT,Traits>& fromStream, T &theValue)"),

                std::string( "{\n"
                             "\tif (babel::stream::expect(fromStream, \"(\")) {\n"
                             "\t\tstd::string thePointer;\n"
                             "\t\tchar        theNextChar;\n"
                             "\n"
                             "\t\twhile (fromStream.get(theNextChar) && std::isprint(theNextChar) && (theNextChar != ')') && (theNextChar != '-')) {\n"
                             "\t\t\tthePointer.append(1, theNextChar);\n"
                             "\t\t}\n"
                             "\t\tif (fromStream) {\n"
                             "\t\t\tif ((thePointer == \"nullptr\") && (theNextChar == ')')) {\n"
                             "\t\t\t\ttheValue.reset();\n"
                             "\t\t\t}\n"
                             "\t\t\telse if (theNextChar == '-') {\n"
                             "\t\t\t\ttheValue.reset( new typename T::element_type() );\n"
                             "\t\t\t\tbabel::stream::expect( readValue( babel::stream::expect( fromStream, \">\" ), *theValue ), \")\" );\n"
                             "\t\t\t}\n"
                             "\t\t\telse {\n"
                             "\t\t\t\tfromStream.setstate(std::ios::failbit);\n"
                             "\t\t\t}\n"
                             "\t\t}\n"
                             "\t}\n"
                             "\treturn fromStream;\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "// = Predefined arithmetic type support =======================\n\n"
                             "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_arithmetic<T>::value, std::basic_ostream<CharT,Traits>&>\n"
                             "appendValue(std::basic_ostream<CharT,Traits>& toStream, const T &theValue, Indent )"),

                std::string( "{\n"
                             "\tusing IOType = std::conditional_t<\n"
                             "\t\t(std::is_same<T,std::uint8_t>::value || std::is_same<T,std::uint16_t>::value),\n"
                             "\t\tstd::uint32_t,\n"
                             "\t\tstd::conditional_t< (std::is_same<T,std::int8_t>::value || std::is_same<T,std::int16_t>::value),\n"
                             "\t\t\tstd::int32_t,\n"
                             "\t\t\tT> >;\n"
                             "\n"   
                             "\treturn (toStream << static_cast<IOType>(theValue));\n"
                             "}\n" )
            );

            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_arithmetic<T>::value && (sizeof(T)<=2),\n"
                             "                  std::basic_istream<CharT,Traits>&>\n"
                             "readValue(std::basic_istream<CharT,Traits>& fromStream, T &theValue )"),

                std::string( "{\n"
                             "\tusing IOType = std::conditional_t<\n"
                             "\t\t(std::is_same<T,std::uint8_t>::value || std::is_same<T,std::uint16_t>::value),\n"
                             "\t\t std::uint32_t, std::int32_t >;\n"
                             "\n"
                             "\tIOType theTemporary;\n"
                             "\n"
                             "\tif (fromStream >> theTemporary) {\n"
                             "\t\tif (static_cast<IOType>(static_cast<T>(theTemporary)) == theTemporary) {\n"
                             "\t\t\ttheValue = static_cast<T>(theTemporary);\n"
                             "\t\t}\n"
                             "\t\telse {\n"
                             "\t\t\tfromStream.setstate(std::ios::failbit);\n"
                             "\t\t}\n"
                             "\t}\n"
                             "\treturn fromStream;\n"
                             "}\n")
            );
            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class T>\n"
                             "std::enable_if_t< is_arithmetic<T>::value && (sizeof(T)>2),\n"
                             "                  std::basic_istream<CharT,Traits>&>\n"
                             "readValue(std::basic_istream<CharT,Traits>& fromStream, T &theValue )"),

                std::string( "{\n"
                             "\treturn (fromStream >> theValue);\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "// = Predefined boolean support ===============================\n\n"
                             "template <class CharT, class Traits>\n"
                             "std::basic_ostream<CharT,Traits>&\n"
                             "appendValue( std::basic_ostream<CharT,Traits>&theStream, const bool &theValue, Indent )"),

                std::string( "{\n"
                             "\treturn theStream << (theValue ? \"True\" : \"False\" );\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits>\n"
                             "std::basic_istream<CharT,Traits>&\n"
                             "readValue( std::basic_istream<CharT,Traits>&fromStream, bool &theValue )"),

                std::string( "{\n"
                             "\tchar theIdentifier[6];\n"
                             "\tif (babel::stream::expectIdentifier(fromStream, std::begin(theIdentifier), std::end(theIdentifier))) {\n"
                             "\t\tif (std::strcmp(\"True\",theIdentifier) == 0) {\n"
                             "\t\t\ttheValue = true;\n"
                             "\t\t}\n"
                             "\t\telse if (std::strcmp(\"False\",theIdentifier) == 0) {\n"
                             "\t\t\ttheValue = false;\n"
                             "\t\t}\n"
                             "\t\telse {\n"
                             "\t\t\tfromStream.setstate(std::ios::failbit);\n"
                             "\t\t}\n"
                             "\t}\n"
                             "\treturn fromStream;\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "// = Predefined string support ================================\n\n"
                             "template <class CharT, class Traits>\n"
                             "std::basic_ostream<CharT,Traits>&\n"
                             "appendValue( std::basic_ostream<CharT,Traits>&theStream, const std::string &theValue, Indent )"),

                std::string( "{\n"
                             "\ttheStream << '\"';\n"
                             "\tfor (const auto theChar : theValue) {\n"
                             "\t\tif (std::isprint(theChar) && (theChar != '\"') && (theChar != '\\\\')) {\n"
                             "\t\t\ttheStream.put(theChar);\n"
                             "\t\t}\n"
                             "\t\telse {\n"
                             "\t\t\tstatic const char theHexDigits[] = \"0123456789ABCDEF\";\n"
                             "\n"
                             "\t\t\ttheStream.put('\\\\');\n"
                             "\t\t\ttheStream.put('x');\n"
                             "\t\t\ttheStream.put(theHexDigits[(theChar >> 4) & 0xFu]);\n"
                             "\t\t\ttheStream.put(theHexDigits[(theChar >> 0) & 0xFu]);\n"
                             "\t\t}\n"
                             "\t}\n"
                             "\treturn theStream << '\"';\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits>\n"
                             "std::basic_istream<CharT,Traits>&\n"
                             "readValue( std::basic_istream<CharT,Traits>&fromStream, std::string &theValue )"),

                std::string( "{\n"
                             "\tif (babel::stream::expect( fromStream, \"\\\"\" )) {\n"
                             "\t\tchar theNextChar;\n"
                             "\n"
                             "\t\ttheValue.clear();\n"
                             "\n"
                             "\t\twhile ((fromStream.get(theNextChar)) && (theNextChar != '\"')) {\n"
                             "\t\t\tif (theNextChar != '\\\\') {\n"
                             "\t\t\t\ttheValue.push_back(theNextChar);\n"
                             "\t\t\t}\n"
                             "\t\t\telse {\n"
                             "\t\t\t\tstatic const char theHexDigits[] = \"0123456789ABCDEF\";\n"
                             "\n"
                             "\t\t\t\tchar        theX;\n"
                             "\t\t\t\tchar        theMSD;\n"
                             "\t\t\t\tchar        theLSD;\n"
                             "\t\t\t\tconst char *theMSDPtr;\n"
                             "\t\t\t\tconst char *theLSDPtr;\n"
                             "\n"
                             "\t\t\t\tif ((fromStream.get(theX).get(theMSD).get(theLSD)) &&\n"
                             "\t\t\t\t    (theX == 'x') &&\n"
                             "\t\t\t\t    ((theMSDPtr = index(theHexDigits,theMSD)) != nullptr) &&\n"
                             "\t\t\t\t    ((theLSDPtr = index(theHexDigits,theLSD)) != nullptr)) {\n"
                             "\t\t\t\t\ttheValue.push_back(static_cast<char>(16*(theMSDPtr-std::begin(theHexDigits))+(theLSDPtr-std::begin(theHexDigits))));\n"
                             "\t\t\t\t}\n"
                             "\t\t\t\telse {\n"
                             "\t\t\t\t\tfromStream.setstate(std::ios::failbit);\n"
                             "\t\t\t\t}\n"
                             "\t\t\t}\n"
                             "\t\t}\n"
                             "\t}\n"
                             "\treturn fromStream;\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "// = Predefined std::pair support =============================\n\n"
                             "template <class CharT, class Traits, class K, class V>\n"
                             "std::basic_ostream<CharT,Traits>&\n"
                             "appendValue( std::basic_ostream<CharT,Traits>&theStream, const std::pair<K,V> &theValue, Indent theIndent )"),

                std::string( "{\n"
                             "\tconst auto theNextIndent = theIndent.next();\n"
                             "\n"
                             "\treturn appendValue( (appendValue( theStream << \"[\", theValue.first, theNextIndent) << (theIndent.isActive() ? \" : \" : \":\")), theValue.second, theNextIndent ) << \"]\";\n"
                             "}\n")
            );

            theTemplates.emplace_back( 
                std::string( "template <class CharT, class Traits, class K, class V>\n"
                             "std::basic_istream<CharT,Traits>&\n"
                             "readValue( std::basic_istream<CharT,Traits>&fromStream, std::pair<K,V> &theValue )"),

                std::string( "{\n"
                             "\treturn babel::stream::expect(readValue( babel::stream::expect(readValue( babel::stream::expect(fromStream,\"[\"), theValue.first ), \":\"), theValue.second ), \"]\");\n"
                             "}\n")
            );
        }
        
        ScopedGuard     theTemplateGuard( theOutputFile,
                                          std::string("have_") + theNameSpaceName + 
                                            (theSyntacticElement == SyntacticElements::Declaration ? "_declarations" : "_implementations"));
        ScopedNamespace theTemplateNamespace( theOutputFile,
                                              StringToNamespace(theNameSpaceName));
        
        for (const auto &theTemplate : theTemplates) {
            theTemplate.output(theOutputFile, theSyntacticElement );
        }
    }
    
    static void addIndent( OutputFile &theOutputFile ) {
        ScopedGuard     theIndentClassGuard( theOutputFile, std::string("have_") + theNameSpaceName + "_Indent");
        ScopedNamespace theNameSpace( theOutputFile, StringToNamespace(theNameSpaceName));
   
        theOutputFile.printf(
            "class Indent {\n"
            "public:\n"
            "\texplicit Indent  ( unsigned int theStep )       __attribute__((weak));\n"
            "\tIndent   next    ()                       const __attribute__((weak));\n"
            "\tIndent   previous()                       const __attribute__((weak));\n"
            "\tbool     isActive()                       const __attribute__((weak));\n"
            "\n"
            "\ttemplate <class CharT, class Traits>\n"
            "\tfriend std::basic_ostream<CharT,Traits>& operator <<( std::basic_ostream<CharT,Traits>&theStream, const Indent &theIndent );\n"
            "private:\n"
            "\tunsigned int itsStep;\n"
            "};\n"
            "\n"
            "inline Indent::Indent( unsigned int theStep ) : itsStep(theStep) {}\n"
            "\n"
            "inline Indent Indent::next() const { return Indent( itsStep ? (itsStep+1) : 0 ); }\n"
            "\n"
            "inline Indent Indent::previous() const { return Indent( itsStep ? (itsStep-1) : 0 ); }\n"
            "\n"
            "inline bool Indent::isActive() const { return itsStep != 0; }\n"
            "\n"
            "template <class CharT, class Traits>\n"
            "std::basic_ostream<CharT,Traits>& operator <<( std::basic_ostream<CharT,Traits> &theStream, const Indent &theIndent ) {\n"
            "\tstatic const char theIndentStep[] = \"\t\";\n"
            "\tif (theIndent.itsStep) {\n"
            "\t\ttheStream << std::endl;\n"
            "\t\tfor (auto i = 0u; i < (theIndent.itsStep-1); i++) {\n"
            "\t\t\ttheStream << theIndentStep;\n"
            "\t\t}\n"
            "\t}\n"
            "\treturn theStream;\n"
            "}\n"
            "\n"
            "template <class CharT, class Traits>\n"
            "std::basic_istream<CharT,Traits>& expect( std::basic_istream<CharT,Traits> &theStream, const char * const theExpectedCharacters ) {\n"
            "\tchar        theNextChar;\n"
            "\tconst char *theExpectedChar = theExpectedCharacters;\n"
            "\n"
            "\twhile (*theExpectedChar && theStream.get(theNextChar)) {\n"
            "\t\tif (*theExpectedChar == theNextChar) {\n"
            "\t\t\ttheExpectedChar++;\n"
            "\t\t}\n"
            "\t\telse if (!std::isspace(theNextChar) || (theExpectedChar != theExpectedCharacters)) {\n"
            "\t\t\ttheStream.setstate(std::ios::failbit);\n"
            "\t\t}\n"
            "\t}\n"
            "\treturn theStream;\n"
            "}\n"
            "\n"
            "template <class CharT, class Traits>\n"
            "std::basic_istream<CharT,Traits>& expectAnyOf( std::basic_istream<CharT,Traits> &theStream, char &theChar, const char * const theExpectedCharacters ) {\n"
            "\twhile (theStream.get(theChar)) {\n"
            "\t\tif (index(theExpectedCharacters,theChar)) {\n"
            "\t\t\tbreak;\n"
            "\t\t}\n"
            "\t\telse if (!std::isspace(theChar)) {\n"
            "\t\t\ttheStream.setstate(std::ios::failbit);\n"
            "\t\t}\n"
            "\t}\n"
            "\treturn theStream;\n"
            "}\n"
            "\n"
            "template <class CharT, class Traits>\n"
            "std::basic_istream<CharT,Traits>& expectIdentifier( std::basic_istream<CharT,Traits> &theStream, char *begin, const char *end ) {\n"
            "\n"
            "\tif (begin == end) {\n"
            "\t\ttheStream.setstate(std::ios::failbit);\n"
            "\t}\n"
            "\telse {\n"
            "\t\tchar theNextChar;\n"
            "\n"
            "\t\twhile (theStream.get(theNextChar) && std::isspace(theNextChar)) continue;\n"
            "\n"
            "\t\tend -= 1;\n"
            "\n"
            "\t\twhile (theStream && (begin != end) && (std::isalnum(theNextChar) || (theNextChar == '_'))) {\n"
            "\t\t\t*begin++ = theNextChar;\n"
            "\t\t\ttheStream.get(theNextChar);"
            "\t\t}\n"
            "\n"
            "\t\t*begin = '\\0';\n"
            "\t}\n"
            "\n"
            "\treturn theStream.unget();\n"
            "}\n"
        );
    }

class Generator : public GeneratorImplementationBase {
public:
    Generator() : GeneratorImplementationBase(
        "Stream",
        {
            "memory",
            "type_traits",
            "algorithm",
            "ostream",
            "cctype",
            "cstring"
        },
        true) 
    {
    }

    void doRealize( const CompilationUnit &theCompilationUnit,
                    OutputFile            &theOutputFile,
                    const BabelcOptionSet &theOptions ) override final;

};


void Generator::doRealize( const CompilationUnit &theCompilationUnit,
                           OutputFile            &theOutputFile,
                           const BabelcOptionSet & ) {
    addIndent(theOutputFile);
    {
        struct InterfaceFilter : public ReferenceFilter {
            bool breakAt( const Type *theType ) override {
                return (dynamic_cast<const InterfaceType*>(theType) != nullptr);
            }
        };
        
        Types           theTypeClosure;
        InterfaceFilter theInterfaceFilter;
        
        for (auto theType : theCompilationUnit.itsTypes) {
            theType.second->getItsReferredTypes(theTypeClosure, theInterfaceFilter);
        }
        BABEL_LOG("Type closure is:");

        for (auto theType : theTypeClosure) {
            BABEL_LOG("----------------------------------------------");
            BABEL_LOGF("%s", theType.second->toString("").c_str());
        }
        
        declareHelpers( theOutputFile, theTypeClosure );
    
        addPredefinedBabelTemplates( theOutputFile, SyntacticElements::Declaration );
        
        std::set<std::string> theNamespaces;
        
        addTypeTemplates(theOutputFile, theTypeClosure, SyntacticElements::Declaration, theNamespaces);
        addTypeTemplates(theOutputFile, theTypeClosure, SyntacticElements::Implementation, theNamespaces);
    }
    addPredefinedBabelTemplates( theOutputFile, SyntacticElements::Implementation );
}

Generator theInstance;

} } } }
