/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2016/08/28
 */

#include "babel_parser.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <cctype>
#include <iterator>
#include <vector>
#include <stdexcept>
#include <unistd.h>
#include <util/babel_util.h>
#include <util/babel_logger.h>
#include <util/babel_suppress.h>
#include <unittest/babel_unittest.h>
#include <map>
#include <set>
#include <memory>
#include <regex>
#include <algorithm>
#include <numeric>

namespace babel { namespace parser {

using Files           = std::map< std::shared_ptr<std::string>, LinesPtr >;
using Namespaces      = std::map< std::string, LinesPtr >;
using SplittedStrings = std::vector< std::pair< LinesIterator, LinesIterator > >;

#define ALL_TYPE_VARIANTS( theType )    #theType, "unsigned " #theType, "signed " #theType
#define WITH_AND_WITHOUT_STD( theType ) "std::" #theType, #theType

static const std::set<std::string> theDisallowedTypeNames = {
        ALL_TYPE_VARIANTS(char),
        ALL_TYPE_VARIANTS(wchar_t),
        ALL_TYPE_VARIANTS(short),
        ALL_TYPE_VARIANTS(int),
        ALL_TYPE_VARIANTS(long),
        ALL_TYPE_VARIANTS(long int),
        ALL_TYPE_VARIANTS(long long),
        ALL_TYPE_VARIANTS(long long int),
        WITH_AND_WITHOUT_STD( size_t ),
        WITH_AND_WITHOUT_STD( ssize_t ),
        WITH_AND_WITHOUT_STD( ptrdiff_t ),
        WITH_AND_WITHOUT_STD( nullptr_t )
};


/**
 * This function prints a columnized version of the names of the built in types on stderr.
 */
static void printBuiltInTypeNames() {
    constexpr int theMaxLineLength = 80;

    static const auto theBuiltInTypeNameWidth =
            1 + std::min( theMaxLineLength,
                          std::accumulate( BABEL_UTIL_RANGE(theBuiltInTypes),
                                           0,
                                           []( int theMaxSoFar, const Types::value_type &thePair) {
                                                 return std::max( theMaxSoFar, static_cast<int>(thePair.second->itsName.length()));
                                           }));

    static const auto theBuiltInTypesNumberOfColumns = std::max( 1, (theMaxLineLength-1)/theBuiltInTypeNameWidth );


    {
        std::remove_const<decltype(theBuiltInTypesNumberOfColumns)>::type theColumn = 0;

        std::for_each( BABEL_UTIL_RANGE(theBuiltInTypes),
                       [&theColumn]( const Types::value_type &thePair ) {
                          (void)fprintf(stderr, "%*s%s", -theBuiltInTypeNameWidth, thePair.second->itsName.c_str(),
                                                 ((++theColumn % theBuiltInTypesNumberOfColumns) == 0) ? "\n" : "");
                        });
    }

    if ((theBuiltInTypes.size() % theBuiltInTypesNumberOfColumns) != 0) {
        (void)fprintf(stderr,"\n");
    }
}
/**
 * Global counter of the number of parse errors encountered so far
 */
static unsigned int theNumberOfParseErrors = 0;

/**
 * Global counter of the number of type errors encountered so far
 */
static unsigned int theNumberOfTypeErrors  = 0;


/**
 * Trims all whitespace characters from both ends of the range [theBegin, theEnd). After the call the range will either
 * be empty (theBegin == theEnd) or theBegin will refer to the first non-whitespace character and theEnd will refer to
 * the position after the last non-whitespace character.
 * @param theBegin Iterator to the first character to consider
 * @param theEnd End of the range to scan for whitespace
 */
template <class Iterator>
void trimWhiteSpace( Iterator &theBegin, Iterator &theEnd ) {

    while ((theBegin != theEnd) && isspace(*theBegin)) {
        ++theBegin;
    }
    Iterator theTrialEnd = theEnd;
    while ((theBegin != theTrialEnd) && isspace(*--theTrialEnd)) {
        theEnd = theTrialEnd;
    }
}

/**
 * Returns its argument with leading and trailing whitespace removed.
 */
std::string trimWhiteSpace( const std::string &theString ) {
    std::string::const_iterator theBegin = theString.begin();
    std::string::const_iterator theEnd   = theString.end();

    trimWhiteSpace( theBegin, theEnd );

    return std::string( theBegin, theEnd );
}

/**
 * Helper function to check if the character argument is legal initial character of a C++ identifier.
 * @param c
 * @return true if it is
 */
inline bool isInitialIdentfierChar( char c) {
    return isalpha(c) || (c == '_');
}

/**
 * Helper function to check if the character argument is legal later part of a C++ identifier.
 * @param c
 * @return true if it is
 */
inline bool isLaterIdentfierChar( char c ) {
    return isalnum(c) || (c == '_');
}
/**
 * Unifies the format of a string, which means that all whitespace are replaced with space characters, all multiple space characters
 * are replaces with a single space, all remaining space characters that is not between two C++ identifiers are removed.
 * E.g. "  std::array< unsigned   int > *" is replaced by "std::array<unsigned int>*" (the only remaining space is that
 * between 'unsigned' and 'int' since removing that would change the meaning of the expression).
 * @param theStringToUnify The string to unify
 * @return The unified string.
 */
std::string unify( const std::string &theStringToUnify) {
    std::string theUnifiedString( trimWhiteSpace(theStringToUnify) );

    std::string::iterator       theResult             = theUnifiedString.begin();
    std::string::const_iterator theScan               = theResult;

    // Pre-requisite for this loop is that it is trimmed, i.e. the last and first character (if any,
    // otherwise the loop won't be entered) is non-space. This makes it safe to examine the
    // previous value of an iterator pointing to a space and also to increment an iterator until a
    // non-space is found without checking for a boundary. Otherwise logic follows the method
    // description.
    //
    while (theScan != theUnifiedString.end()) {
        if (isspace(*theScan)) {
            while (isspace(*++theScan)) {}

            if (isLaterIdentfierChar(*std::prev(theResult)) && isLaterIdentfierChar(*theScan)) {
                *theResult++ = ' ';
            }
        }
        *theResult++ = *theScan++;
    }

    theUnifiedString.erase(theResult, theUnifiedString.end());

    return theUnifiedString;
}
/**
 * Splits the interval [theBegin,theEnd) into intervals delimited by theDelimiter regexp.
 * The intervals will be trimmed from leading and trailing whitespace and empty intervals will
 * not be included into the result. E.g. an input of "A; B ;;C;  " with ';' as delimiter will
 * return the container ("A","B","C")
 * @param theBegin     The start of the input interval
 * @param theEnd       The end of the input interval
 * @param theDelimiter The delimiter character
 * @return The subintervals as described above.
 */
template <typename Iterator>
std::vector< std::pair<Iterator,Iterator> >  splitByDelimiterRegexp( Iterator theBegin, Iterator theEnd, const std::regex &theRegexp) {
    using ReturnType = decltype(splitByDelimiterRegexp(theBegin,theEnd,theRegexp));
    
    ReturnType theResult;

    do {
        std::match_results< Iterator > theMatcher;
        
        typename ReturnType::value_type theValue( theBegin, regex_search( theBegin, theEnd, theMatcher, theRegexp) ? theMatcher[0].first : theEnd );

        const bool timeToBreak = (theValue.second == theEnd);

        theBegin = theValue.second;

        trimWhiteSpace( theValue.first, theValue.second);

        if (theValue.first != theValue.second) {
            theResult.push_back(theValue);
        }

        if (timeToBreak) {
            break;
        }
        else {
            theBegin = theMatcher[0].second;
        }

    } while (true);

    return theResult;
}

/**
 * Splits the interval [theBegin,theEnd) into intervals delimited by theDelimiter character.
 * The intervals will be trimmed from leading and trailing whitespace and empty intervals will
 * not be included into the result. E.g. an input of "A; B ;;C;  " with ';' as delimiter will
 * return the container ("A","B","C")
 * @param theBegin     The start of the input interval
 * @param theEnd       The end of the input interval
 * @param theDelimiter The delimiter character
 * @return The subintervals as described above.
 */
template <typename Iterator>
std::vector< std::pair<Iterator,Iterator> > splitByDelimiter( Iterator theBegin, Iterator theEnd, const char theDelimiter) {
    return splitByDelimiterRegexp( theBegin, theEnd, std::regex( util::format("%c", theDelimiter), std::regex::ECMAScript) );
}


/**
 * Function for determining if the first match for theRegexp is starting directly at theBegin.
 * @param theRegexp   The regexp to look for
 * @param theMatcher  The resultant matcher for the regexp (only valid if true is returned)
 * @param theBegin    Iterator to start looking at
 * @param theEnd      End of the range to scan
 * @return true if theRegexp is found directly at theBegin.
 */
bool lookingAt( const std::regex &theRegexp, LinesMatcher &theMatcher, const LinesIterator &theBegin, const LinesIterator &theEnd) {
    return regex_search(theBegin, theEnd, theMatcher, theRegexp) && (theMatcher[0].first == theBegin);
}

/**
 * Report a parse error on standard error and increment the global parse error counter.
 */
void parseError( const std::string    &theLocation,
                 const std::string    &theError ) {
    (void)fprintf( stderr, "%s: %s\n", theLocation.c_str(), theError.c_str());
    theNumberOfParseErrors += 1;
}

/**
 * Report a parse error on standard error and increment the global parse error counter.
 */
void parseError( const LinesIterator &theLocation,
                 const std::string    &theError ) {
    parseError( theLocation.getLocation(), theError );
}

/**
 * Report a type error on standard error and increment the global parse and type error counters.
 */
void typeError( const LinesIterator &theLocation, const std::string &theError ) {
    parseError( theLocation, theError);
    theNumberOfTypeErrors += 1;
}
/**
 * As parseError, but will throw an instance of std::runtime_error.
 */
[[noreturn]] void fatalError( const LinesIterator &theLocation,
                              const std::string   &theError ) {
    parseError( theLocation, theError );
    throw std::runtime_error("C++ code does not follow babelc rules.");
}

LinesIterator skipToClosingBrace(LinesIterator theScan, const LinesIterator &theEnd ) {
    unsigned int theBraceDepth = 0;

    while (theScan != theEnd) {
        switch (*theScan) {
        case '{':
            theBraceDepth += 1;
            break;
        case '}':
            if (theBraceDepth == 0) {
                return theScan;
            }
            else {
                theBraceDepth -= 1;
            }
            break;
        }
        ++theScan;
    }
    fatalError( theEnd, "Failed to balance curly braces for namespaces" );
}

/**
 * This is a forward declaration since the parsing is quite recursive.
 * Attempt to validate the passed type spec and if successful, returns a pointer to the type.
 * @param theTypeSpec The textual representation of the type.
 * @param theSourceFile The source file for the type
 * @param theLocation The location within the source file of the type
 * @param theKnownTypes The local types found so far.
 * @return
 */
const Type *lookupType( std::string                     theTypeSpec,
                        bool                            allowInstantiation,
                        const LinesIterator            &theLocation,
                        const Types                    &theKnownTypes,
                        const BabelcOptionSet          &theOptions,
                        const Namespace                &theEnclosingNamespace,
                        const Namespaces               &theIncludedNamespaces );

void recurseNamespaces( const LinesPtr                 &theLines,
                        const std::string              &theParentNamespace,
                        Namespaces                     &theNamespaces,
                        const BabelcOptionSet          &theOptions ) {

    static const std::regex theNamespaceRegexp( "namespace[[:space:]]+([A-Za-z_][A-Za-z_0-9]*([[:space:]]*::[[:space:]]*[A-Za-z_][A-Za-z_0-9]*)*)[[:space:]]*(__attribute__[^{]+)?\\{", std::regex::ECMAScript);

    static const std::set<std::string> theUnusedNamespaces{
      "std",
      "__cxx11",
      "__gnu_cxx",
      "__cxxabiv1",
      "boost",
      "__gnu_debug"
    };

    static const std::string theUnusedNameSpaceName("0");

    auto         theScan = LinesIterator::begin(theLines);
    auto         theEnd = LinesIterator::end(theLines);
    LinesMatcher theMatcher;

    while (regex_search(theScan, theEnd, theMatcher, theNamespaceRegexp)) {
        const auto theFoundName      = theMatcher[1].str();
        auto       theNamespaceBegin = theMatcher[0].first;
        auto       theNamespaceEnd   = skipToClosingBrace(theMatcher[0].second, theEnd);
        const auto thisNamespace     = theNamespaces.insert(
                    std::make_pair( theParentNamespace == theNamespaceDelimiter ?
                                        (theUnusedNamespaces.count(theFoundName) ? theUnusedNameSpaceName : (theNamespaceDelimiter + theFoundName)) :
                                        (theParentNamespace + theNamespaceDelimiter+ theFoundName),
                                    std::make_shared<Lines>())).first;



        BABEL_LOGF("Found namespace %s, storing it as %s", theFoundName.c_str(), thisNamespace->first.c_str());


        if (thisNamespace->first == theUnusedNameSpaceName) {
            splice( theNamespaceBegin, ++theNamespaceEnd, *thisNamespace->second );
        }
        else {
            auto  theNamespaceBodyBegin = theMatcher[0].second;
            splice( theNamespaceBodyBegin, theNamespaceEnd, *thisNamespace->second );
            recurseNamespaces( thisNamespace->second, thisNamespace->first, theNamespaces, theOptions );
            Lines theGarbageBin;
            splice( theNamespaceBegin, ++theNamespaceEnd, theGarbageBin );
        }

        theScan = theNamespaceEnd;
    }
}

Namespaces parseNamespaces( const FilePath                 &theSourceFile,
                            const LinesPtr                 &theLines,
                            const BabelcOptionSet          &theOptions ) {
    Namespaces           theNameSpaces{ std::make_pair(theNamespaceDelimiter, theLines ) };

    auto theCurrentNameSpace = theNameSpaces.begin();

    recurseNamespaces(theCurrentNameSpace->second, theCurrentNameSpace->first, theNameSpaces, theOptions );

    for (auto theNameSpace : theNameSpaces) {
        dumpLines( theOptions, theSourceFile, (std::string(".namespace.") + util::replaceAll( theNamespaceDelimiter, theNameSpace.first, ".")).c_str(), *theNameSpace.second );
    }

    return theNameSpaces;
}

/**
 * Parses the given input [theScan,theEndOfInput) to determine the namespace of it. The passed interval will be updated to be
 * only the body part of the namespace (i.e. the text between the curly braces).
 */
Namespace parseNamespace( LinesIterator                &theScan,
                            LinesIterator                &theEndOfInput,
                            const BabelcOptionSet          &BABEL_UNUSED(theOptions) ) {

    BABEL_LOG_SCOPE();

    static const std::regex theNamespace( "namespace[[:space:]]+([A-Za-z_][A-Za-z_0-9]*([[:space:]]*::[[:space:]]*[A-Za-z_][A-Za-z_0-9]*)*)[[:space:]]*(__attribute__[^{]+)?\\{", std::regex::ECMAScript);
    Namespace        thePrimaryNamespace;
    LinesMatcher   theMatcher;

    while (lookingAt(theNamespace, theMatcher, theScan, theEndOfInput )) {
        for (const auto &theNamespacePart : splitByDelimiterRegexp(theMatcher[1].first, theMatcher[1].second, std::regex( "::" ))) {
            thePrimaryNamespace.push_back( std::string( theNamespacePart.first, theNamespacePart.second ));
        }

        if (*--theEndOfInput != '}') {
           fatalError( theEndOfInput, "Expected end of namespace here. babelc requires everything in the same namespace.");
        }
        trimWhiteSpace((theScan = theMatcher[0].second), theEndOfInput);
    }
    if (thePrimaryNamespace.empty()) {
        fatalError( theScan, "Expected namespace here. babelc requires at least one enclosing namespace.");
    }

    return thePrimaryNamespace;
}

InterfaceType::Method::Arguments parseArguments( const Types           &theKnownTypes,
												 LinesIterator          theScan,
												 const LinesIterator   &theEndOfInput,
												 const BabelcOptionSet &theOptions,
												 const Namespace       &theEnclosingNamespace,
												 const Namespaces      &theIncludedNamespaces ) {

    //                                          1                    2                              3
    static const std::regex theArgumentRegexp( "(const[[:space:]]+)?(:?:?[A-Za-z_][A-Za-z_0-9: \t<>,]*)([*& \t]+)"
    //                                          4
                                               "([A-Za-z_][A-Za-z_0-9]*)[[:space:]]*"
    //                                          5      6
                                               "(\\[\\[([^\\]]+)\\]\\])?"
    //                                                      7
                                               "[[:space:]]*(=[[:space:]]*(.+))?");

	// Regular expressions won't suffice at all on C++ argument lists, so scan instead
	//
	InterfaceType::Method::Arguments theArguments;
	unsigned int					 theNoOfVoidArguments = 0;

	for (;;) {
		while ((theScan != theEndOfInput) && std::isspace(*theScan)) {
			++theScan;
		}

		if (theScan == theEndOfInput) {
			break;
		}

		LinesIterator theArgumenStart    = theScan;
		unsigned int  insideTemplateList = 0;

		for (;;) {
			++theScan;

			if ((theScan == theEndOfInput) || (*theScan == ',')) {
				if (insideTemplateList) {
					fatalError(theArgumenStart, "Internal: Failed to parse template type argument.");
				}
				// This ought to be the end of an argument!
				LinesMatcher theMatcher;

				if (!regex_match(theArgumenStart, theScan, theMatcher, theArgumentRegexp)) {
                                    const auto theArgument = unify(std::string(theArgumenStart,theScan));
                                    
                                    if ((theArgument != "void") || (++theNoOfVoidArguments > 1)) {
                                        parseError(theArgumenStart, "Internal: Failed to parse argument '" + theArgument + "'");
                                    }
				}
				else {
					const auto isConstArgument     = theMatcher[1].matched;
					const auto theAccessSpecifier  = unify(theMatcher[3].str());
					const auto isReferenceArgument = (theAccessSpecifier == "&");
					const auto theArgumentName     = theMatcher[4].str();
					const auto theArgumentType     = unify(theMatcher[2].str());
                                        const auto theAttributes       = theMatcher[6].matched ? unify(theMatcher[6].str()) : std::string{};
                                        
					if (!isReferenceArgument && !theAccessSpecifier.empty()) {
						parseError(theMatcher[3].second, "babelc only accepts by value or by r-value reference arguments to methods.");
					}
                                        if (!theAttributes.empty()) {
                                            if (theAttributes.find("babelc::") != 0) {
                                                parseError(theMatcher[5].second, "Unsupported attribute: " + theAttributes);
                                            }
                                        }
					if (theMatcher[7].matched) {
						parseError(theMatcher[7].second, "babelc does not allow default values for arguments.");
					}

                                        auto theActualArgumentType = lookupType(theArgumentType, false, theMatcher[2].first, theKnownTypes, theOptions, theEnclosingNamespace, theIncludedNamespaces);
					
                                        if (!theAttributes.empty()) {
                                            theActualArgumentType = theActualArgumentType->addAttributes(theAttributes);
                                        }

                                        theArguments.emplace_back(
							theArgumentName,
							theActualArgumentType,
							isConstArgument,
							isReferenceArgument,
                                                        theMatcher[4].second.getLocation());

				}
				if (theScan != theEndOfInput) {
					++theScan; // Skip the ','
				}
				break;
			}
			else if (*theScan == '<') {
				insideTemplateList += 1;
			}
			else if (*theScan == '>') {
				insideTemplateList -= 1;
			}
		}
	}
	return theArguments;
}
/**
 * Attempts to parse a 'struct' where the text to be parsed, [theScan,theEndOfInput), is assumed to start at the tag
 * name of the struct. A babelc struct could either be a POD structure or a pure virtual interface.
 */
Type* parseStruct(const Types            &theKnownTypes,
                  LinesIterator          &theScan,
                  const LinesIterator    &theEndOfInput,
                  const BabelcOptionSet  &theOptions,
				  const Namespace        &theEnclosingNamespace,
				  const Namespaces       &theIncludedNamespaces ) {
    BABEL_LOG_SCOPE();

    static const std::regex theAccessRegexp("((public|protected|private)[[:space:]]*:[[:space:]]*).*", std::regex::ECMAScript);
    static const std::regex theStructStartRegexp( "([A-Za-z_][A-Za-z_0-9]*)[[:space:]]*(\\[\\[([^\\]]+)\\]\\])?[[:space:]]*\\{[[:space:]]*", std::regex::ECMAScript);
    static const std::regex theStructEndRegexp( "\\}[[:space:]]*(\\[\\[([^\\]]+)\\]\\])?[[:space:]]*;" );
    static const std::regex theMethodRegexp( "(virtual[[:space:]]+)?(:?:?[A-Za-z_][A-Za-z_0-9: \t<>,]*)([*& \t]+)"
                                             "([A-Za-z_][A-Za-z_0-9]*)[[:space:]]*\\((.*)\\)[[:space:]]*"
                                             "(const[[:space:]]*)?(=[[:space:]]*0)?" );
    static const std::regex theMemberRegexp( "((const|volatile)[[:space:]]+)*(:?:?[A-Za-z_][A-Za-z_0-9: \t<>,]*[*& \t]+)"
                                             "([A-Za-z_][A-Za-z_0-9]*)[[:space:]]*(=[[:space:]]*(.*))?");

    LinesMatcher theStructMatcher;

    if ( ! lookingAt( theStructStartRegexp, theStructMatcher, theScan, theEndOfInput )) {
        fatalError( theScan, "Expected a named struct without inheritance here.");
    }
    const auto        theNameLocation = theStructMatcher[1].first;
    const std::string theStructName(theStructMatcher[1].str());
    const std::string theAttributesSpec(theStructMatcher[3].matched ? theStructMatcher[3].str() : std::string{} );
    
    const std::regex theConDeStructorRegexp( "((virtual[[:space:]]*)?~)?" + theStructName + 
                                                "[[:space:]]*\\(([^)]*)\\)([[:space:]]*=[[:space:]]*0[[:space:]]*)?");

    BABEL_LOGF("Found struct '%s'", theStructName.c_str());
    
    auto theOpeningBrace = theStructMatcher[0].second;
    auto theClosingBrace = skipToClosingBrace(theStructMatcher[0].second, theEndOfInput);
    
    if (!lookingAt(theStructEndRegexp, theStructMatcher, theClosingBrace, theEndOfInput)) {
        fatalError( theClosingBrace, "Expected a structure to end here.");
    }

    const auto theAttributes = (theStructMatcher[1].matched) ? theStructMatcher[1].str() : std::string("");
 
    theScan = theStructMatcher[0].second;
    
    const auto theFieldTexts = splitByDelimiterRegexp(theOpeningBrace, theClosingBrace, std::regex(";|\\{[[:space:]]*\\}", std::regex::ECMAScript) );

    unsigned int theNumberOfConstructors     = 0;
    unsigned int theNumberOfDestructors      = 0;
    bool         theDestructorIsVirtual      = 0;
    bool         theDestructorIsPure         = 0;
    
    babel::logger::IndentIncrease theFieldIndent;
    StructType::Fields            theFields;
    InterfaceType::Methods        theMethods;

    for (auto theFieldText : theFieldTexts ) {
        LinesMatcher theFieldMatcher;

        if (regex_match(theFieldText.first, theFieldText.second, theFieldMatcher, theAccessRegexp)) {
            parseError( theFieldText.first, "babelc does not support access specifications.");
            theFieldText.first = theFieldMatcher[1].second;
            if (theFieldText.first == theFieldText.second) {
                continue;
            }
        }

        if (regex_match(theFieldText.first, theFieldText.second, theFieldMatcher, theMethodRegexp )) {
        	auto const theMethodName = theFieldMatcher[4].str();
            BABEL_LOGF("Found method '%s'", theMethodName.c_str());

            std::vector<std::string> theErrors;

            if ((theFieldMatcher[1].length() == 0) || (theFieldMatcher[7].length() == 0)) {
                theErrors.push_back("be pure virtual");
            }
            if (theFieldMatcher[6].length() != 0) {
                theErrors.push_back("be non-const");
            }
            const std::string  theReturnTypeImage = trimWhiteSpace(theFieldMatcher[2].str());
            const Type        *theReturnType = lookupType(theReturnTypeImage, false, theFieldMatcher[2].first, theKnownTypes, theOptions, theEnclosingNamespace, theIncludedNamespaces );

            if (!trimWhiteSpace(theFieldMatcher[3].str()).empty()) {
                theErrors.push_back("return by value");
            }

            if ( ! theErrors.empty()) {
                std::string theMessage("babelc methods must ");
                for (std::size_t i = 0; i < theErrors.size(); i++) {
                    if (i > 0) {
                        theMessage += (i == (theErrors.size()-1)) ? " and " : ", ";
                    }
                    theMessage += theErrors[i];
                }
                parseError( theFieldMatcher[0].first, theMessage );
            }

            auto const theArgumentList = theFieldMatcher[5];

            theMethods.emplace_back( theReturnType, theMethodName, parseArguments( theKnownTypes, theArgumentList.first, theArgumentList.second, theOptions, theEnclosingNamespace, theIncludedNamespaces));
        }
        else if (regex_match(theFieldText.first, theFieldText.second, theFieldMatcher, theConDeStructorRegexp)) {
            const char *theTypeOfSpecialMethod;
            
            if (theFieldMatcher[1].length()) {
                theTypeOfSpecialMethod = "destructor";
                theNumberOfDestructors += 1;
                theDestructorIsVirtual = theFieldMatcher[2].matched;
                theDestructorIsPure    = theFieldMatcher[4].matched;
            }
            else {
                theTypeOfSpecialMethod = "constructor";
                theNumberOfConstructors += 1;
            }
            
            BABEL_LOGF("Found %s '%s'", theTypeOfSpecialMethod, theFieldMatcher[0].str().c_str());
        }
        else if (regex_match(theFieldText.first, theFieldText.second, theFieldMatcher, theMemberRegexp)) {
        	auto const theFieldName = theFieldMatcher[4].str();
        	auto const theFieldType = unify( theFieldMatcher[3].str() );
                auto const isInitialized = theFieldMatcher[5].matched;

            BABEL_LOGF("Found %sinitialized data member '%s' of type %s", (isInitialized ? "" : "non-"), theFieldName.c_str(), theFieldType.c_str());
            theFields.emplace_back( theFieldName, 
                                    lookupType( theFieldType, false, theFieldMatcher[3].first, theKnownTypes, theOptions, theEnclosingNamespace, theIncludedNamespaces ),
                                    isInitialized );
        }
        else {
            parseError(theFieldText.first, "This is not recognized as neither constructor, destructor, method nor data member.");
        }
    }

    auto const  theTypeName = NamespaceToString(theEnclosingNamespace) + theStructName;
    const char *theStructError;
    Type       *theResult;

    if (theMethods.size() == 0) {
        theStructError = nullptr; //TODO check for default constructor!
        theResult      = new StructType( theTypeName, theFields, theNameLocation.getLocation() );
    }
    else {
        if (theFields.size() != 0) {
            theStructError = "babelc structs must be either PODs or pure virtual interfaces.";
        }
        else if (theNumberOfConstructors) {
            theStructError = "babelc pure virtual interfaces are not allowed constructors";
        }
        else if ((theNumberOfDestructors == 0) || !theDestructorIsVirtual || theDestructorIsPure) {
            theStructError = "babelc pure virtual interfaces must have a non-pure virtual destructor";
        }
        else {
            theStructError = nullptr;
        }
    
        theResult = new InterfaceType( theTypeName, theMethods, theNameLocation.getLocation() );
    }

    if (theStructError) {
        parseError(theNameLocation, theStructError );
    }

    if (!theAttributes.empty()) {
        theResult = theResult->addAttributes(theAttributes);
    }
    
    return theResult;
}

/**
 * Attempts to parse a 'typedef' where the text to be parsed, [theScan,theEndOfInput), is assumed to start at the
 * type definitions (i.e. after the token 'typedef' and following whitespace).
 */
Type* parseTypedef(const bool                      isOldStyleTypedef,
                   const Types                    &theKnownTypes,
                   LinesIterator                  &theScan,
                   const LinesIterator            &theEndOfInput,
                   const BabelcOptionSet          &theOptions,
                   const Namespace                &theEnclosingNamespace,
                   const Namespaces               &theIncludedNamespaces ) {

    BABEL_LOG_SCOPE();

    LinesMatcher theMatcher;
    int          theNameIndex;
    int          theTypeIndex;
    int          theAttributeIndex;

    if (isOldStyleTypedef) {
        static const std::regex theTypedefRegexp( "([^;]+[ \t>*])([A-Za-z_][A-Za-z_0-9]*)[[:space:]]*(\\[\\[([^\\]]+)\\]\\])?[[:space:]]*;", std::regex::ECMAScript);


        if ( ! lookingAt( theTypedefRegexp, theMatcher, theScan, theEndOfInput )) {
            fatalError( theScan, "Expected a typedef acceptable by babelc here.");
        }

        theNameIndex      = 2;
        theTypeIndex      = 1;
        theAttributeIndex = 3;
    }
    else {
        static const std::regex theUsingRegexp( "([A-Za-z_][A-Za-z_0-9]*)[[:space:]]*=[[:space:]]*([^;[]+)(\\[\\[([^\\]]+)\\]\\])?[[:space:]]*;" );


        if ( ! lookingAt( theUsingRegexp, theMatcher, theScan, theEndOfInput )) {
            static const std::regex theNamespaceRegexp( "namespace[[:space:]]" );

            if (lookingAt(theNamespaceRegexp, theMatcher, theScan, theEndOfInput)) {
                fatalError(theScan, "babelc does not support 'using namespace'.");
            }
            else {
                fatalError(theScan, "Expected a 'using type' declaration acceptable by babelc here." + std::string( theScan, theEndOfInput));
            }
        }

        theNameIndex      = 1;
        theTypeIndex      = 2;
        theAttributeIndex = 3;
    }

    theScan = theMatcher[0].second;


    const auto theTypeName = NamespaceToString(theEnclosingNamespace) + theMatcher[theNameIndex].str();
    const auto theType     = lookupType(theMatcher[theTypeIndex].str(), true, theMatcher[theTypeIndex].first, theKnownTypes, theOptions, theEnclosingNamespace, theIncludedNamespaces );
    const auto theLocation = theMatcher[theNameIndex].first.getLocation();
    const auto theBaseType = is_any_of<Container,PointerType,VariantType,OptionalType>(theType) ? 
                                theType->clone(theTypeName, theLocation) : 
                                   new Typedef( theTypeName, theType, theLocation );
    
    if (theMatcher[theAttributeIndex].matched) {
        return theBaseType->addAttributes(theMatcher[theAttributeIndex+1].str());
    }
    else {
        return theBaseType;
    }
}

/**
 * Attempts to parse an 'enum' where the text to be parsed, [theScan,theEndOfInput), is assumed to start at the
 * type definitions (i.e. after the token 'enum' and following whitespace).
 */
Type* parseEnum( const Types            &theKnownTypes,
                 LinesIterator          &theScan,
                 const LinesIterator    &theEndOfInput,
                 const BabelcOptionSet  &theOptions,
                 const Namespace        &theEnclosingNamespace,
                 const Namespaces       &theIncludedNamespaces ) {

    BABEL_LOG_SCOPE();

    //                                           12
    static const std::regex theEnumBodyRegexp ( "((struct|class)[[:space:]]+)?"
    //                                           3
                                                "([A-Za-z_][A-Za-z_0-9]*)"
    //                                           4                         5
                                                "([[:space:]]*:[[:space:]]*([A-Za-z_][A-Za-z_0-9:]*))?[[:space:]]*"
    //                                           6      7
                                                "(\\[\\[([^\\]]+)\\]\\])?[[:space:]]*"
    //                                              8
                                                "\\{([^}]*)\\}[[:space:]]*;", std::regex::ECMAScript);
    static const std::regex theEnumValueRegexp( "([A-Za-z_][A-Za-z_0-9]*)([[:space:]]*=[^=].*)?" );

    LinesMatcher theEnumTypeMatcher;

    if ( ! lookingAt( theEnumBodyRegexp, theEnumTypeMatcher, theScan, theEndOfInput )) {
        fatalError(theScan, "Expected a named enum here.");
    }

    const std::string  theEnumTypeName   = theEnumTypeMatcher[3].str();
    auto               theClass          = theEnumTypeMatcher[2];
    auto               theUnderlyingType = theEnumTypeMatcher[5];

    BABEL_LOGF("Found enum type '%s'", theEnumTypeName.c_str());
    theScan = theEnumTypeMatcher[0].second;

    if ( ! (theClass.matched && theUnderlyingType.matched)) {
        parseError(theEnumTypeMatcher[0].first,
                    babel::util::format("babelc only supports scoped enums with underlying type (rewrite as 'enum %s %s : %s {...};')",
                            (theClass.matched ?  theClass.str().c_str() : "struct"),
                            theEnumTypeName.c_str(),
                            (theUnderlyingType.matched ?  theUnderlyingType.str().c_str() : "<type>")));
    }

    EnumeratedType::Values theValues;
    const auto theValueTexts = splitByDelimiter( theEnumTypeMatcher[8].first, theEnumTypeMatcher[8].second, ',');

    babel::logger::IndentIncrease theValueIndent;

    for (auto theValueText : theValueTexts) {
        LinesMatcher theValueMatcher;

        if (regex_match(theValueText.first, theValueText.second, theValueMatcher, theEnumValueRegexp )) {
            theValues.push_back(theValueMatcher[1].str());
            BABEL_LOGF("Enum value '%s'", (*std::prev(theValues.end())).c_str());
        }
        else {
            parseError(theEnumTypeMatcher[8].first,
                        ("internal: babelc can't identify '" + std::string(theValueText.first, theValueText.second) + "' as an enum value") );
        }
    }


    const auto theEnumeratedType = 
         new EnumeratedType( (NamespaceToString(theEnclosingNamespace) + theEnumTypeName),
                               theUnderlyingType.matched ?
                                       lookupType(theUnderlyingType.str(), false, theUnderlyingType.first, theKnownTypes, theOptions, theEnclosingNamespace, theIncludedNamespaces) :
                                       nullptr,
                               theValues,
                                theEnumTypeMatcher[3].first.getLocation());
    
    if (theEnumTypeMatcher[6].matched) {
        return theEnumeratedType->addAttributes(theEnumTypeMatcher[7].str());
    }

    return theEnumeratedType;
}

Types parseTypes( LinesIterator              &theScan,
                  LinesIterator              &theEndOfInput,
                  const BabelcOptionSet      &theOptions,
                  const Namespace            &theEnclosingNamespace,
                  const Namespaces           &theIncludedNamespaces ) {
    BABEL_LOG_SCOPE();

    static const std::regex theToken( "([a-z]+)[[:space:]]+", std::regex::ECMAScript);

    Types            theTypes{ theBuiltInTypes };
    LinesMatcher   theMatcher;

    while (lookingAt(theToken, theMatcher, theScan, theEndOfInput )) {
        const std::string  theKeyword = theMatcher[1].str();
        Type              *theType;

        theScan = theMatcher[0].second;

        if (theKeyword == "struct") {
            theType = parseStruct(theTypes, theScan, theEndOfInput, theOptions, theEnclosingNamespace, theIncludedNamespaces );
        }
        else if ((theKeyword == "typedef") || (theKeyword == "using")) {
            theType = parseTypedef((theKeyword == "typedef"), theTypes, theScan, theEndOfInput, theOptions, theEnclosingNamespace, theIncludedNamespaces );
            if (theType != nullptr) {
                theTypes[theType->itsName] = theType;
            }
        }
        else if (theKeyword == "enum") {
            theType = parseEnum(theTypes, theScan, theEndOfInput, theOptions, theEnclosingNamespace, theIncludedNamespaces );
        }
        else if (theKeyword == "class") {
            parseError( theScan, "babelc only support struct's, not classes.");
            theType = parseStruct(theTypes, theScan, theEndOfInput, theOptions, theEnclosingNamespace, theIncludedNamespaces );
        }
        else {
            fatalError( theScan, "Internal babelc inconsistency error!");
        }

        theTypes[theType->itsName] = theType;

        trimWhiteSpace(theScan, theEndOfInput );
    }

    if (theScan != theEndOfInput) {
        fatalError( theScan, "Extra text found after initial namespace. babelc only allows one namespace." );
    }

    // Remove the predefined types added before
    //
    for (auto theBuiltInType : theBuiltInTypes ) {
    	theTypes.erase(theBuiltInType.first);
    }
    return theTypes;
}

/**
 * Searches the theIncludedNamespaces for theQualifiedTypeName and returns a pointer to instance of it if found, otherwise nullptr
 * @param theQualifiedTypeName The name of the type with full name space qualification.
 * @param theIncludedNamespaces The namespaces included in the compilation.
 * @return
 */
const Type *searchForType( const std::string       theQualifiedTypeName,
                           const Types            &theKnownTypes,
                           const BabelcOptionSet  &theOptions,
                           const Namespaces       &theIncludedNamespaces ) {

    const auto theNamespaceNameEnd = theQualifiedTypeName.find_last_of(theNamespaceDelimiter);

    if (theNamespaceNameEnd == std::string::npos) {
        BABEL_LOGF("The type '%s' is not qualified.", theQualifiedTypeName.c_str() );
        return nullptr;
    }

    auto const theAlreadyKnownType = theKnownTypes.find(theQualifiedTypeName);

    if (theAlreadyKnownType != theKnownTypes.end()) {
        BABEL_LOGF("The type '%s' was already known.", theQualifiedTypeName.c_str() );
    	return theAlreadyKnownType->second;
    }

    const auto theNamespaceName = theQualifiedTypeName.substr(0, theNamespaceNameEnd - (theNamespaceDelimiter.size()-1));
    const auto theTypeName      = theQualifiedTypeName.substr( theNamespaceNameEnd + 1 );
    const auto theNamespace     = theIncludedNamespaces.find(theNamespaceName);

    if (theNamespace == theIncludedNamespaces.end()) {
        BABEL_LOGF("The namespace '%s' is not known.", theNamespaceName.c_str());
        return nullptr;
    }

    const Namespace     theEnclosingNamespace = StringToNamespace(theNamespaceName);
    const LinesIterator theEnd                = LinesIterator::end(theNamespace->second);

    {
        LinesMatcher        theMatcher;
        LinesIterator       theScan               = LinesIterator::begin(theNamespace->second);
        //                                  123       4              5    6            7                 8                    9
        const std::regex theTypeRegexp( "\\b(((using)|(struct|class)|(enum([[:space:]]+(struct|class))?)|(typedef[[:space:]]+)([^;]+))[[:space:]]+)" +
                                        theTypeName +
        //                                           10
                                        "[[:space:]]*([:;{=])", std::regex::ECMAScript );

        while(regex_search(theScan, theEnd , theMatcher, theTypeRegexp)) {
            BABEL_LOGF("Attempting match with prelude '%s'", theMatcher[1].str().c_str());

            auto const theRightAnchor = *theMatcher[10].first;

            if (theMatcher[3].matched && (theRightAnchor == '=')) { // using...
                theScan = theMatcher[1].second;
                return parseTypedef(false, theKnownTypes, theScan, theEnd, theOptions, theEnclosingNamespace, theIncludedNamespaces );
            }
            else if (theMatcher[4].matched && (theRightAnchor != '=')) { // struct/class
                // Skip if it is a forward declaration
                //
                if (theRightAnchor != ';') {
                	theScan = theMatcher[1].second;
                	return parseStruct( theKnownTypes, theScan, theEnd, theOptions, theEnclosingNamespace, theIncludedNamespaces );
                }
            }
            else if (theMatcher[5].matched && (theRightAnchor != '=')) { // enum...
                // Skip if it is a forward declaration
                //
                if (theRightAnchor != ';') {
                    theScan = theMatcher[7].first;
                    return parseEnum(theKnownTypes, theScan, theEnd, theOptions, theEnclosingNamespace, theIncludedNamespaces );
                }

            }
            else if (theMatcher[8].matched && (theRightAnchor == ';')) {
                theScan = theMatcher[8].second;
                return parseTypedef(true, theKnownTypes, theScan, theEnd, theOptions, theEnclosingNamespace, theIncludedNamespaces );
            }
            else {
                fatalError(theMatcher[0].first, "Internal: Failed to parse this as a babelc typedef/using/struct/class/enum.");
            }
            // Coming here the input was accepted but didn't fit (e.g. a forward declaration).
            // Skip the match and retry.
            //
            theScan = theMatcher[0].second;
        }
    }

    BABEL_LOGF("Giving up on type '%s'.", theQualifiedTypeName.c_str());

    return nullptr;
}

/**
 * See function declaration above.
 */
const Type *lookupType( std::string                     theTypeSpec,
                        bool                            allowInstantiation,
                        const LinesIterator            &theLocation,
                        const Types                    &theKnownTypes,
                        const BabelcOptionSet          &theOptions,
                        const Namespace                &theEnclosingNamespace,
                        const Namespaces               &theIncludedNamespaces ) {
    static const std::regex theTemplateRegexp("([^<]+)(<(.*)>)?");

    BABEL_LOG_SCOPE();

    const auto theInitialTypeErrorCount = theNumberOfTypeErrors;

    theTypeSpec = unify(theTypeSpec);
#if 0
    if ((theTypeSpec.size() > 2) && (theTypeSpec.find("::") == 0)) {
        theTypeSpec.erase(0,2); // Discard leading ::
    }
#endif    
    if (theDisallowedTypeNames.find(theTypeSpec) != theDisallowedTypeNames.end()) {
        typeError( theLocation, babel::util::format("Type '%s' is not allowed.", theTypeSpec.c_str()));
    }
    if (theTypeSpec.back() == '*') {
        typeError( theLocation, "Raw pointer types are not allowed.");
    }

    if (theInitialTypeErrorCount != theNumberOfTypeErrors) {
        return nullptr;
    }
    else {
        std::smatch theMatcher;

        if (!regex_match(theTypeSpec, theMatcher, theTemplateRegexp )) {
            typeError( theLocation, babel::util::format( "Internal: WTF is this type '%s'?",
                                                                         theTypeSpec.c_str()));
            return nullptr;
        }
        // This is not a complete template parsing - it will fail if shift and relational ops are used.
        //
        auto const theTypePart          = theMatcher[1].str();
        auto const theTemplatePart      = theMatcher[3].str();
        auto const theTemplateArguments = splitByDelimiter(theMatcher[3].first, theMatcher[3].second, ',');
        const Type *theType;
        // The check for name space is simplified and relies upon that we are looking
        // at pre-processed and semantically consistent C++! Check if it is within the same name space
        // as the type itself or if it is within a parent name space.
        //
        auto theNameSpace = theEnclosingNamespace;

        for (;;) {
            auto const theQualifiedName = NamespaceToString(theNameSpace) + theTypePart;

            {
                auto const theTypeIterator = theKnownTypes.find(theQualifiedName);

                if (theTypeIterator != theKnownTypes.end()) {
                    BABEL_LOGF("'%s' is a known type", theTypePart.c_str());
                    theType = theTypeIterator->second;
                }
                else {
                    theType = searchForType( theQualifiedName, theKnownTypes, theOptions, theIncludedNamespaces);
                }
            }

            if (theType || theNameSpace.empty()) {
                break;
            }

            theNameSpace.pop_back();
        }

        if (theType == nullptr) {
            typeError( theLocation, babel::util::format("Can't identify type '%s'.", theTypePart.c_str()));
        }

        if (theType && !theTemplatePart.empty()) {
            if (!allowInstantiation) {
                parseError(theLocation, "babelc does not allow inline instantiations here. Make instantiation as a namespace level type definition instead.");
            }

            auto const theTemplateType = dynamic_cast<const Template*>(theType);

            if (theTemplateType == nullptr) {
                typeError(theLocation, babel::util::format("Internal: Type %s is not recognized as a template type.", theTypePart.c_str()) );
            }
            else {
                BABEL_LOGF("'%s' is a template with arguments '%s'", theTypePart.c_str(), theTemplatePart.c_str());

                if ((std::count(BABEL_UTIL_RANGE(theTemplatePart), '<') > 0) ||
                    (std::count(BABEL_UTIL_RANGE(theTemplatePart), '>') > 0)) {
                    typeError( theLocation, babel::util::format( "Internal: This template argument is too complicated for babelc '%s'?",
                                                                                theTemplatePart.c_str()));
                    theType = nullptr;
                }
                else {
                    std::vector< const Type * >theTemplateTypes;
                    
                    for (const auto &theArgument : theTemplateArguments) {
                        if (theTemplateTypes.size() == theTemplateType->itsExpectedNoOfArguments) {
                            break;
                        }
                        const auto thisTemplateArgument = std::string(theArgument.first, theArgument.second );
                        BABEL_LOGF("Template argument is '%s'", thisTemplateArgument.c_str());
                        theTemplateTypes.emplace_back(
                            lookupType( thisTemplateArgument,
                                        false,
                                        theLocation, 
                                        theKnownTypes, 
                                        theOptions, 
                                        theEnclosingNamespace, 
                                        theIncludedNamespaces)
                        );
                    }

                    theType = theTemplateType->instantiate( theTemplateTypes );
                }
            }
        }

        return theType;
    }

}

/**
 *
 * @param theSourceFile
 * @param theSourceLines
 * @param theIncludedFiles
 * @param theOptions
 * @return
 */
CompilationUnit::Ptr parsePrimary(  const FilePath           &theSourceFile,
                                    const LinesPtr           &theSourceLines,
                                    const LinesPtr           &theIncludedFiles,
                                    const BabelcOptionSet    &theOptions,
                                    const FilePaths          &theDependencies ) {
    BABEL_LOG_SCOPE();

    LinesIterator theScan       = LinesIterator::begin( theSourceLines );
    LinesIterator theEndOfInput = LinesIterator::end  ( theSourceLines );

    trimWhiteSpace(theScan, theEndOfInput);

    const Namespace  thePrimaryNamespace  { parseNamespace( theScan, theEndOfInput, theOptions ) };
    const Namespaces theIncludedNamespaces{ parseNamespaces( theSourceFile, theIncludedFiles, theOptions ) };
    const Types      theTypes             { parseTypes( theScan, theEndOfInput, theOptions, thePrimaryNamespace, theIncludedNamespaces ) };

    if (theNumberOfParseErrors != 0) {
        if (theNumberOfTypeErrors != 0) {
            (void)fprintf( stderr, "babelc: The allowed built-in types are\n");
            printBuiltInTypeNames();
        }
        throw std::runtime_error("C++ code does not follow babelc rules.");
    }
    return new CompilationUnit( theSourceFile, thePrimaryNamespace, theTypes, theDependencies );
}

/**
 * Utility function for changing curly braces into spaces if they are found within single or double quotes. All ordinary characters
 * within quotations is also made upper case. This is made on the source text to ease parsing when skipping over braced regions and when
 * looking for c++ keywords.
 * @param theScan Where to start the transformation...
 * @param theEnd  ...and where to end it.
 * @return Returns true if everything went OK and false if there are e.g. unbalanced quotations.
 */
static bool cleanup( std::string::iterator theScan, std::string::iterator theEnd ) {
    enum { Idle, WithinQuotes, WithinAttribute, EscapeSeen } theScanState = Idle;
    char                                  theDelimiter     = '\0';
    char                                  theLastCharacter = '\0';
    while (theScan != theEnd ) {
        char &theCharacter = *theScan++;

        switch (theScanState) {
        case Idle:
            if ((theCharacter == '"') || (theCharacter == '\'')) {
                theDelimiter = theCharacter;
                theScanState = WithinQuotes;
            }
            else if ((theCharacter == '[') && (theLastCharacter == '[')) {
                theScanState = WithinAttribute;
            }
            break;
        case WithinQuotes:
            if (theCharacter == '\\') {
                theScanState = EscapeSeen;
            }
            else if (theCharacter == theDelimiter) {
                theScanState = Idle;
            }
            else if ((theCharacter == '{') || (theCharacter == '}')) {
                theCharacter = ' ';
            }
            else {
                theCharacter = static_cast<char>(std::toupper(theCharacter));
            }
            break;
        case WithinAttribute:
            if ((theCharacter == ']') && (theLastCharacter == ']')) {
                theScanState = Idle;
            }
            break;
        case EscapeSeen:
            theScanState = WithinQuotes;
            break;
        }
        theLastCharacter = theCharacter;
    }

    return (theScanState == Idle);
}

/**
 * Accepts the pre-processed source code in fromTheLines and washes away preprocessor file and line number information which is instead added
 * to the lines themselves. Lines from the file specified as theSource will be removed from fromTheLines and are used as return value
 * from the funtion.
 */
LinesPtr extractSourceFile( const BabelcOptionSet &theOptions, const FilePath &theSource, const  LinesPtr &fromTheLines) {
    BABEL_LOG_SCOPE();

    std::regex                           theFileSpecRegexp(std::string("# ([[:digit:]]+) \"([^\"]+)\".*"), std::regex::ECMAScript);
    std::regex                           theBlankOrPragmaLine("[[:space:]]*(#pragma[[[:space:]].*)?", std::regex::ECMAScript );
    std::shared_ptr<std::string>         theMostRecentFileName;
    std::size_t                          theMostRecentLineNumber;
    babel::util::StringInterner          theFileNames;

    const LinesPtr theSourceContents = std::make_shared<Lines>();
    auto theLine = fromTheLines->begin();

    while (theLine != fromTheLines->end()) {
        std::smatch theMatchResult;

        if (regex_match(BABEL_UTIL_RANGE(theLine->getItsContents()), theMatchResult, theBlankOrPragmaLine)) {
            theLine = fromTheLines->erase(theLine); // Simply dump it
            theMostRecentLineNumber++;
        }
        else if (regex_match(BABEL_UTIL_RANGE(theLine->getItsContents()), theMatchResult, theFileSpecRegexp)) {
            theMostRecentFileName   = theFileNames.intern(theMatchResult[2].str());
            theMostRecentLineNumber = babel::util::lexical_cast<std::size_t>( theMatchResult[1].str());
            theLine = fromTheLines->erase(theLine);
        }
        else if (!theMostRecentFileName) {
            throw std::runtime_error("First line of preprocessed file does not contain a # in the first position");
        }
        else {
            theLine->setItsLocation( theMostRecentFileName, theMostRecentLineNumber++ );

            if (!cleanup(BABEL_UTIL_RANGE(theLine->getItsContents()))) {
                parseError(babel::util::format("%s:%lu", theMostRecentFileName->c_str(), theMostRecentLineNumber), "Failed to parse quotations");
                throw std::runtime_error("Internal babelc error.");
            }

            if (*theMostRecentFileName == theSource.string()) {
                theSourceContents->splice( theSourceContents->end(), *fromTheLines, theLine++ );
            }
            else {
                theLine++;
            }
        }
    }

    dumpLines(theOptions, theSource, ".included", *fromTheLines );
    dumpLines(theOptions, theSource, ".primary", *theSourceContents );
            
    return theSourceContents;
}

/**
 *
 * @param theLines
 * @param theGccOptions
 */
void sematicCheck( const LinesPtr                  &theLines,
                   const std::string               &theCompiler,
                   const std::vector< char *>      &theGccOptions ) {
    BABEL_LOG_SCOPE();

    int  thePipe[2];

    if (pipe(thePipe) != 0) {
        throw std::runtime_error("Failed to create pipe to gcc!");
    }

    const int &thePipeReadEnd  = thePipe[0];
    const int &thePipeWriteEnd = thePipe[1];

    const pid_t thePid = fork();

    if (thePid < 0) {
        throw std::runtime_error("Failed to fork gcc!");
    }
    else if (thePid == 0) {
        // This is the child process.
        //
        if (close( thePipeWriteEnd ) || (dup2( thePipeReadEnd, STDIN_FILENO ) < 0)) {
            throw std::runtime_error("Failed to redirect standard in for gcc!");
        }

        // Build an argument vector for gcc. The strdup's aren't leaks since we will exec.
        //
        auto theArgumentVector = theGccOptions;
        
        
        util::push_front( theArgumentVector, strdup(theCompiler.c_str()));

        theArgumentVector.push_back( strdup("-xc++-cpp-output") ); // Input is preprocessed C++
        theArgumentVector.push_back( strdup("-fsyntax-only") );    // No output expected here!
        theArgumentVector.push_back( strdup("-") );                // Compile standard input
        theArgumentVector.push_back( nullptr );

        (void)execvp(theArgumentVector[0], &theArgumentVector[0]);

        throw std::runtime_error("Failed to exec gcc!"); // Otherwise we wouldn't be here!
    }
    else {
        const char *theError = nullptr;

        (void)close(thePipeReadEnd);

        {
            const char theNewLine = '\n';

            for (auto theLine : *theLines ) {
                if ((write( thePipeWriteEnd, theLine.getItsContents().c_str(), theLine.getItsContents().size()) < 0) ||
                    (write( thePipeWriteEnd, &theNewLine, sizeof(theNewLine)) != sizeof(theNewLine))) {
                    theError = "Failed to write to gcc pipe!";
                    break;
                }
            }
            (void)close(thePipeWriteEnd);
        }
        {
            int theExitStatus = 0;

            const pid_t theDeceasedCompiler = waitpid( thePid, &theExitStatus, 0 );

            if ((theDeceasedCompiler != thePid) || !WIFEXITED(theExitStatus) || WEXITSTATUS(theExitStatus)) {
                throw std::runtime_error("There are syntactic or semantic errors!");
            }
        }

        if (theError) {
            throw std::runtime_error(theError);
        }
    }
}

CompilationUnit parse( const FilePath               &theSourceFile,
                       const LinesPtr               &theLines,
                       const std::string            &theCompiler,
                       const std::vector< char *>   &theGccOptions,
                       const BabelcOptionSet        &theOptions,
                       const FilePaths              &theDependentFiles) {

    BABEL_LOG_SCOPE();

    // Let g++ make semantic check of the preprocessed file...
    //
    sematicCheck(theLines, theCompiler, theGccOptions );

    // Coming here, the preprocessor and g++ had nothing to complain about!
    // Let the babelc front end chew on it
    //

    const auto theSourceContents = extractSourceFile(theOptions, theSourceFile, theLines);

    if (theSourceContents->empty()) {
        throw std::runtime_error("Empty source file?");
    }
    
    auto const theCompilationUnit = parsePrimary( theSourceFile, theSourceContents, theLines, theOptions, theDependentFiles );

    if (theOptions.find(BabelcOptions::Summary) != theOptions.end()) {
        const std::string theBigRuler( 79, '=' );
        const std::string theSmallRuler( theBigRuler.size(), '-');

        (void)fprintf(stdout, "\n%s\nSummary:\n\n", theBigRuler.c_str());
        (void)fprintf(stdout, "Input header : %s\n", theCompilationUnit->itsSourceFile.string().c_str());
        (void)fprintf(stdout, "Namespace    : %s\n", babel::NamespaceToString(theCompilationUnit->itsNameSpace).c_str());

        for (auto theType : theCompilationUnit->itsTypes) {
            (void)fprintf(stdout, "%s\n\n%s\n", theSmallRuler.c_str(), theType.second->toString("").c_str());
        }

        (void)fprintf(stdout, "\nEnd of summary\n%s\n", theBigRuler.c_str());
    }

    if ((theNumberOfParseErrors == 0) && (theNumberOfTypeErrors == 0)) {
        return *theCompilationUnit;
    }
    else {
        throw std::invalid_argument("Compilation error(s) will prevent code generation.");
    }
}

//============================================================================================
//
// Hereafter are conditional unit test code
//
#ifdef BABEL_UNITTEST

class ParserUnitTest : public babel::unittest::UnitTest {
public:
    ParserUnitTest() : UnitTest("Parser") {}
protected:
    void run() override {
        runUnifyTests();
        runFileTests();
        runNamespaceTests();
        runTypeSpecTests();
        runParseC17NameSpace();
    }

private:
    void runTypeSpecTests() {
        static const char *theInputLines[] =
        {
            "namespace TypeSpecTest {",
            "  using Int32    = std::int32_t;",
            "  using Uint32   = std::uint32_t;",
            "  using MyString = std::string;",
            "  enum struct Enum : std::uint16_t {};"
            "  using DoubleArray = std::array<double,10>;"
            "  using DoubleList  = std::list<double>;"
            "  using StringMap1  = std::map<std::string,Int32>;"
            "  using StringMap2  = std::map<std::string,std::int32_t>;"
            "",
            "struct First {",
            "  std::int32_t  adam;",
            "  std::uint32_t bertil;",
            "  std::string   caesar;",
            "  std::uint16_t david;",
            "  DoubleArray   eirk;",
            "  StringMap1    filip;",
            "",
            "};",
            "",
            "struct Second {",
            "  Int32      alpha;",
            "  Uint32     bravo;",
            "  MyString   charlie;",
            "  Enum       delta;",
            "  DoubleList echo;",
            "  StringMap2 foxtrot;",
            "};",
            "",
            "}"
        };
        static const char *theIncludedFiles[] = 
        {
            ""
        };
        auto const theInput = makeLines(BABEL_UTIL_RANGE(theInputLines));
        auto const theInclusions = makeLines(BABEL_UTIL_RANGE(theIncludedFiles));

        theNumberOfParseErrors = theNumberOfTypeErrors = 0;

        
        auto const theCompilationUnit = parsePrimary(FilePath(""),theInput,theInclusions,BabelcOptionSet(),FilePaths());
        
        BABEL_UNITTEST_CHECK(theCompilationUnit);
        
        const auto theFirst  = theCompilationUnit->itsTypes.find("::TypeSpecTest::First");
        const auto theSecond = theCompilationUnit->itsTypes.find("::TypeSpecTest::Second");
        const auto theEnd    = theCompilationUnit->itsTypes.end();
        
        BABEL_UNITTEST_CHECK( theFirst  != theEnd );
        BABEL_UNITTEST_CHECK( theSecond != theEnd );
        BABEL_UNITTEST_CHECK( theFirst != theSecond );
        BABEL_UNITTEST_CHECK( theFirst->second != theSecond->second );

        BABEL_UNITTEST_CHECK( theFirst->second->getItsTypeSpecification() == 
                                "{ " 
                                    "std::int32_t " 
                                    "std::uint32_t "
                                    "std::string "
                                    "std::uint16_t "
                                    "double[] "
                                    "( std::string . std::int32_t ) "
                                "}");
        BABEL_UNITTEST_CHECK( theFirst->second->getItsTypeSpecification() == theSecond->second->getItsTypeSpecification());
        
    }
    
    void runParseC17NameSpace() {
        static const char *theInputLines[] =
        {
            "namespace Type::Spec::Test  {",
            "  using Int32    = std::int32_t;",
            "  using Uint32   = std::uint32_t;",
            "  using MyString = std::string;",
            "  enum struct Enum : std::uint16_t {};"
            "  using DoubleArray = std::array<double,10>;"
            "  using DoubleList  = std::list<double>;"
            "  using StringMap1  = std::map<std::string,Int32>;"
            "  using StringMap2  = std::map<std::string,std::int32_t>;"
            "",
            "struct First {",
            "  std::int32_t  adam;",
            "  std::uint32_t bertil;",
            "  std::string   caesar;",
            "  std::uint16_t david;",
            "  DoubleArray   eirk;",
            "  StringMap1    filip;",
            "",
            "};",
            "",
            "struct Second {",
            "  Int32      alpha;",
            "  Uint32     bravo;",
            "  MyString   charlie;",
            "  Enum       delta;",
            "  DoubleList echo;",
            "  StringMap2 foxtrot;",
            "};",
            "",
            "}"
        };
        static const char *theIncludedFiles[] =
        {
            ""
        };
        auto const theInput = makeLines(BABEL_UTIL_RANGE(theInputLines));
        auto const theInclusions = makeLines(BABEL_UTIL_RANGE(theIncludedFiles));

        theNumberOfParseErrors = theNumberOfTypeErrors = 0;


        auto const theCompilationUnit = parsePrimary(FilePath(""),theInput,theInclusions,BabelcOptionSet(),FilePaths());

        BABEL_UNITTEST_CHECK(theCompilationUnit);

        const auto theFirstNameSpace  = theCompilationUnit->itsNameSpace[0];
        const auto theSecondNameSpace = theCompilationUnit->itsNameSpace[1];
        const auto theThirdNameSpace  = theCompilationUnit->itsNameSpace[2];

        BABEL_UNITTEST_CHECK( theFirstNameSpace  ==  "Type" );
        BABEL_UNITTEST_CHECK( theSecondNameSpace ==  "Spec" );
        BABEL_UNITTEST_CHECK( theThirdNameSpace  ==  "Test" );

        const auto theFirst  = theCompilationUnit->itsTypes.find("::Type::Spec::Test::First");
        const auto theSecond = theCompilationUnit->itsTypes.find("::Type::Spec::Test::Second");
        const auto theEnd    = theCompilationUnit->itsTypes.end();

        BABEL_UNITTEST_CHECK( theFirst  != theEnd );
        BABEL_UNITTEST_CHECK( theSecond != theEnd );
        BABEL_UNITTEST_CHECK( theFirst != theSecond );
        BABEL_UNITTEST_CHECK( theFirst->second != theSecond->second );

        BABEL_UNITTEST_CHECK( theFirst->second->getItsTypeSpecification() ==
                                "{ "
                                    "std::int32_t "
                                    "std::uint32_t "
                                    "std::string "
                                    "std::uint16_t "
                                    "double[] "
                                    "( std::string . std::int32_t ) "
                                "}");
        BABEL_UNITTEST_CHECK( theFirst->second->getItsTypeSpecification() == theSecond->second->getItsTypeSpecification());

    }

    void runNamespaceTests() {

        static const char *theInputLines[] =
            {
                "namespace std {",
                "}",
                "namespace First {",
                "  using Type8 = std::int8_t;",
                "}",
                "namespace First {",
                "  using Type16 = std::int16_t;",
                "  using IndirectFirst = Type8;",
                "  namespace Second {",
                "     using Type32 = std::int32_t;",
                "     typedef Type16 IndirectSecond;",
                "  }",
                "}",
                "namespace Third {",
                "  using Type64 = std::int64_t;",
                "  typedef Type64 IndirectThird;",
                "}",
                "namespace __cxxabiv1 {",
                "}",
                "namespace boost {",
                "}"
            };

        auto const theInput      = makeLines( BABEL_UTIL_RANGE(theInputLines));
        auto const theNameSpaces = parseNamespaces(FilePath(""), theInput, BabelcOptionSet());
        Types      theKnownTypes{ theBuiltInTypes };

        BABEL_UNITTEST_CHECK( theNameSpaces.size() == 5 );
        BABEL_UNITTEST_CHECK(theNameSpaces.find("std")             == theNameSpaces.end());
        BABEL_UNITTEST_CHECK(theNameSpaces.find("__cxxabiv1")      == theNameSpaces.end());
        BABEL_UNITTEST_CHECK(theNameSpaces.find("boost")           == theNameSpaces.end());
        BABEL_UNITTEST_CHECK(theNameSpaces.find("::")              != theNameSpaces.end());
        BABEL_UNITTEST_CHECK(theNameSpaces.find("0")               != theNameSpaces.end());
        BABEL_UNITTEST_CHECK(theNameSpaces.find("::First")         != theNameSpaces.end());
        BABEL_UNITTEST_CHECK(theNameSpaces.find("::First::Second") != theNameSpaces.end());
        BABEL_UNITTEST_CHECK(theNameSpaces.find("::Third")         != theNameSpaces.end());

        struct { const char *theName; const char *theBaseName; } theTestCases[] = {
                { "::First::Type8",                   "std::int8_t" },
                { "::First::Type16",                  "std::int16_t" },
                { "::First::Second::Type32",          "std::int32_t" },
                { "::Third::Type64",                  "std::int64_t" },
                { "::First::IndirectFirst",           "::First::Type8" },
                { "::First::Second::IndirectSecond",  "::First::Type16" },
                { "::Third::IndirectThird",           "::Third::Type64" },
        };

        for (auto theTestCase : theTestCases) {
            auto theType{ searchForType(theTestCase.theName, theKnownTypes, BabelcOptionSet(), theNameSpaces ) };
            auto theRealType = dynamic_cast<const Typedef *>(theType);

            BABEL_UNITTEST_CHECK( theType != nullptr );
            BABEL_UNITTEST_CHECK( theRealType != nullptr );
            if (theRealType) {
                BABEL_UNITTEST_CHECK( theRealType->itsBaseType != nullptr);
                if (theRealType->itsBaseType) {
                    BABEL_UNITTEST_CHECK_EQUAL( theRealType->itsBaseType->itsName, theTestCase.theBaseName );
                }
            }
        }
    }

    void runFileTests() {
        {
            static const char *theInputLines[] = {
                    "# 1 \"file1.h\"",
                    "# 1 \"file1.h\"",
                    "# 1 \"file2.h\" 1",
                    "",
                    "#pragma off",
                    "{ }",
                    "'{' }",
                    "'\\'{' }",
                    "'{' '}'",
                    "'a' a",
                    "\"abc\" abc",
                    "# 2 \"file1.h\" 2",
                    "",
                    "{ }",
                    "\"{\" }",
                    "\"\\\"{\" }",
                    "\"{ }\""
            };

            auto const theInput = makeLines(BABEL_UTIL_RANGE(theInputLines));
            
            LinesPtr   theSourceLines = extractSourceFile(BabelcOptionSet(), FilePath("file1.h"), theInput);
            BABEL_UNITTEST_CHECK_EQUAL(theSourceLines->size(), 4);
            BABEL_UNITTEST_CHECK_EQUAL(theInput->size(), 6);
            {
                Lines::const_iterator theFileContents = theSourceLines->begin();
                BABEL_UNITTEST_CHECK_EQUAL( (*theFileContents++).getItsContents() , "{ }" );
                BABEL_UNITTEST_CHECK_EQUAL( (*theFileContents++).getItsContents() , "\" \" }" );
                BABEL_UNITTEST_CHECK_EQUAL( (*theFileContents++).getItsContents() , "\"\\\" \" }" );
                BABEL_UNITTEST_CHECK_EQUAL( (*theFileContents++).getItsContents() , "\"   \"" );

            }
            {
                Lines::const_iterator theFileContents = theInput->begin();

                BABEL_UNITTEST_CHECK_EQUAL( (*theFileContents++).getItsContents() , "{ }" );
                BABEL_UNITTEST_CHECK_EQUAL( (*theFileContents++).getItsContents() , "' ' }" );
                BABEL_UNITTEST_CHECK_EQUAL( (*theFileContents++).getItsContents() , "'\\' ' }" );
                BABEL_UNITTEST_CHECK_EQUAL( (*theFileContents++).getItsContents() , "' ' ' '" );
                BABEL_UNITTEST_CHECK_EQUAL( (*theFileContents++).getItsContents() , "'A' a" );
                BABEL_UNITTEST_CHECK_EQUAL( (*theFileContents++).getItsContents() , "\"ABC\" abc" );
            }
        }
        {
            auto theInput = std::make_shared<Lines>(std::initializer_list<Lines::value_type>{
                    Line( "# 1 \"Don't worry, this error shall occur!\"" ),
                    Line( "{ \"}" ),
            });
            LinesPtr theSourceLines;

            try {
                theSourceLines = extractSourceFile(BabelcOptionSet(), FilePath(""), theInput);
            }
            catch (const std::runtime_error &) {
                theSourceLines = nullptr;
            }
            BABEL_UNITTEST_CHECK_EQUAL(theSourceLines , LinesPtr() );
        }
    }
    void runUnifyTests() {
        {
            BABEL_UNITTEST_CHECK_EQUAL( unify("")     , ""  );
            BABEL_UNITTEST_CHECK_EQUAL( unify("a")    , "a" );
            BABEL_UNITTEST_CHECK_EQUAL( unify(" a  ") , "a" );
            BABEL_UNITTEST_CHECK_EQUAL( unify( " a  \t\n\rb \f c ") , "a b c");
            BABEL_UNITTEST_CHECK_EQUAL( unify( " a . \t\n\rb. \f c ") , "a.b.c");
            BABEL_UNITTEST_CHECK_EQUAL( unify("  std::array\n< unsigned   int > *") , "std::array<unsigned int>*");
        }
    }
};

ParserUnitTest theParserUnitTest;

#endif

}}



