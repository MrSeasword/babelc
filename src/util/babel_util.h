/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2016/08/28
 */
#ifndef UTIL_BABEL_UTIL_H_
#define UTIL_BABEL_UTIL_H_

#include <string>
#include <algorithm>
#include <map>
#include <memory>
#include <cstdarg>
#include <vector>

namespace babel { namespace util {

#define BABEL_UTIL_RANGE( container ) std::begin(container), std::end(container)

/**
 * Compiler checked utility function for formating text.
 *
 * @param theFormat A standard 'printf' format string, followed by its arguments
 * @return The formatted string as large as needed and no more.
 */
std::string format( const char *theFormat, ... ) __attribute__((format (printf, 1, 2)));

/**
 * The va_list variant of the above function.
 */
std::string vformat( const char *theFormat, va_list theArguments) __attribute__((format (printf, 1, 0)));

inline bool endsWith( const std::string &theString, const std::string &theSuffix ) {
    return (theString.size() >= theSuffix.size()) &&
            std::equal( theSuffix.rbegin(), theSuffix.rend(), theString.rbegin());
}

/**
 * Replaces all instances ofString foundWithinString withString. ofString may not be empty.
 * @param ofString
 * @param foundWithinString
 * @param withString
 * @return The result of the operation
 */
std::string replaceAll( const std::string &ofString, const std::string &foundWithinString, const std::string &withString );

/**
 * @brief Some relief for that the /&%¤/&/&%/&% std::vector lacks a push_front!
 * @param c
 * @param v
 */
template <class Container>
void push_front( Container &c, const typename Container::value_type &v) {
    (void)c.insert( std::begin(c), v);
}

/**
 * Helper template to lexical_cast below
 */
template < class IntegerType >
IntegerType StringToIntegerType( const std::string theImage, std::size_t *theEnd, int theBase, std::true_type ) {
    return std::stoull( theImage, theEnd, theBase );
}

/**
 * Helper template to lexical_cast below
 */
template < class IntegerType >
IntegerType StringToIntegerType( const std::string theImage, std::size_t *theEnd, int theBase, std::false_type ) {
    return std::stoll( theImage, theEnd, theBase );
}

/**
 * Tries to convert the text argument to the desired integer and return that value.
 * Integers may be specified with "normal" C syntax (i.e. with leading 0 or 0x to specify base).
 * An exception is thrown if the number won't fit in the designated type or if the text contains
 * illegal characters.
 * @param theImage The number in text form
 * @return The interpreted number
 */
template <class IntegerType>
IntegerType lexical_cast( const std::string &theImage ) {
    using IntermediateType = typename std::conditional< std::is_unsigned<IntegerType>::value, unsigned long long, long long >::type;
    using Limits           = std::numeric_limits<IntegerType>;

    static_assert( (Limits::digits <= std::numeric_limits<IntermediateType>::digits), "Can't handle integers of this size" );

    std::size_t       theEnd    = 0;
    IntermediateType  theResult = StringToIntegerType<IntermediateType>(theImage, &theEnd, 0, typename std::is_unsigned<IntegerType>() );

    if ((theEnd == theImage.size()) && (Limits::min() <= theResult) && (theResult <= Limits::max())) {
        return static_cast<IntegerType>(theResult);
    }
    throw std::invalid_argument("Invalid string representation of integer");
}

/**
 * This is a class to "intern" strings, thus making them comparable only via a pointer.
 */
class StringInterner {
public:
    std::shared_ptr<std::string > intern( const std::string &theString ) {
        const auto thePair = itsMap.find( theString );
        return (thePair != itsMap.end()) ? thePair->second : itsMap.emplace( theString, std::make_shared<std::string>(theString) ).first->second;
    }
    
    using Contents = std::vector< std::shared_ptr<const std::string> >;
    
    void getStrings( Contents &theStrings ) const {
        for (auto thePair : itsMap) {
            theStrings.emplace_back(thePair.second);
        }
    }
private:
    std::map< std::string, std::shared_ptr< std::string > > itsMap;
};

}}


#endif /* UTIL_BABEL_UTIL_H_ */
