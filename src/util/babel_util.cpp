/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2016/08/28
 */
#include <stdarg.h>
#include <stdio.h>
#include <stdexcept>

#include "babel_util.h"
#include "babel_suppress.h"

namespace babel { namespace util {

std::string vformat( const char *theFormat, va_list theArguments) {
    static std::size_t  theBufferSize = 64*1024;
    static char        *theBuffer = new char[theBufferSize];
    
    if (theFormat == nullptr) {
        throw std::invalid_argument("Null format given");
    }

    for (;;) {
        const int theStatus = vsnprintf(theBuffer, theBufferSize, theFormat, theArguments);
 
        if (theStatus < 0) {
            throw std::invalid_argument(std::string("Bad format: ") + theFormat ); // theFormat cannot be null here
        }
        else if (static_cast<std::size_t>(theStatus) >= theBufferSize) {
            theBufferSize += 64*1024;
            delete []theBuffer;
            theBuffer = new char[theBufferSize];
        }
        else {
            return std::string(theBuffer);
        }
    }
}

std::string format(const char* theFormat, ...) {
    std::string theResult;

    va_list theArguments;
    va_start( theArguments, theFormat );

    theResult = vformat( theFormat, theArguments );

    va_end( theArguments );

    return theResult;
}

std::string replaceAll( const std::string &ofString, const std::string &foundWithinString, const std::string &withString ) {
    auto theResult              = foundWithinString;
    auto theReplacementPosition = theResult.find( ofString, 0 );

    if (ofString.empty()) {
        throw std::invalid_argument("replaceAll: String to replace cannot be empty");
    }

    while (theReplacementPosition != std::string::npos) {
        theReplacementPosition =
                theResult.erase( theReplacementPosition, ofString.size() )
                    .insert(theReplacementPosition, withString )
                        .find( ofString, (theReplacementPosition + withString.size()));
    }
    return theResult;
}

}}
