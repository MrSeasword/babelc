/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2016/08/28
 */

#ifndef UTIL_BABEL_LOGGER_H_
#define UTIL_BABEL_LOGGER_H_

#include <string>

namespace babel { namespace logger {

/**
 * Enable debug logging. By default it is disabled.
 */
void enable();

/**
 * Disable debug logging. By default it is disabled.
 */
void disable();

/**
 * Declare an instance of this class if you want to increase
 * the log indent for a scope.
 */
class IndentIncrease {
public:
    IndentIncrease();
    ~IndentIncrease();

    IndentIncrease           ( const IndentIncrease &) = delete;
    IndentIncrease &operator=( const IndentIncrease &) = delete;
};

/**
 * Macro to make a log message. Accepts printf-like style arguments (which are subject to gcc checks)
 */
#define BABEL_LOGF(theFormat, ...) \
    do { babel::logger::logMessage( __FILE__, __LINE__, __func__, theFormat, __VA_ARGS__ ); } while (0)

/**
 * Macro to make a simple unformatted message.
 */
#define BABEL_LOG(theMessage) \
    do { babel::logger::logMessage( __FILE__, __LINE__, __func__, "%s", theMessage ); } while (0)

/**
 * Macro for making a log when entering a scope and one when exiting the same scope.
 * Indent level will be automatically increased/decreased and the message will also
 * indicate the enter and exit point, so normally the arguments could be the empty
 * string. Uses printf-like arguments which are gcc checked.
 */
#define BABEL_LOG_SCOPE() \
    babel::logger::ScopedLog BABEL_LOG_MAKE_UNIQUE(__theBabelLogScope,__LINE__)(__FILE__,__LINE__,__func__ )

#define BABEL_LOG_SCOPEF(theFormat, ...) \
    babel::logger::ScopedLog BABEL_LOG_MAKE_UNIQUE(__theBabelLogScope,__LINE__)(__FILE__,__LINE__,__func__, theFormat, __VA_ARGS__ )

//==============================================================================================================
//
// Text following here is not part of the public interface, but are implementation parts for the functions and
// macros above. Please use the parts above for compatibility with future versions of this API.
//

/**
 * This method is not intended to be used directly.
 * Use the BABEL_LOG and BABEL_LOG_SCOPE instead.
 */
void logMessage( const char * const theFile,
                 const unsigned int theLine,
                 const char * const theFunction,
                 const char * const theFormat,
                 ... ) __attribute__((format(printf,4,5)));

class ScopedLog {
public:
    ScopedLog( const char * const theFile,
               const unsigned int theLine,
               const char * const theFunction );

    ScopedLog( const char * const theFile,
               const unsigned int theLine,
               const char * const theFunction,
               const char * const theFormat,
               ... ) __attribute__((format(printf,5,6)));

    ~ScopedLog();

    ScopedLog           ( const ScopedLog & ) = delete;
    ScopedLog &operator=( const ScopedLog & ) = delete;
private:
    IndentIncrease               itsIndentIncrease;
    std::string                  itsFile;
    unsigned int                 itsLine;
    std::string                  itsFunction;
};

/**
 * Kludge macros to paste two expanded macro variables.
 */
#define BABEL_LOG_MAKE_UNIQUE( left, right ) BABEL_LOG_PASTE( left, right )
#define BABEL_LOG_PASTE( left, right ) left##right

}}

#endif /* UTIL_BABEL_LOGGER_H_ */
