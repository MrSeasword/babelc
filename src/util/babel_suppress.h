/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2016/08/28
 */

#ifndef UTIL_BABEL_SUPPRESS_H_
#define UTIL_BABEL_SUPPRESS_H_

#include <assert.h>

#define BABEL_UNUSED(p) UNUSED_##p __attribute__((__unused__))

#define SUPPRESS_DO_PRAGMA( s ) _Pragma (#s)

#define SUPPRESS_SIGN_WARNING( why ) \
	SUPPRESS_DO_PRAGMA(GCC diagnostic push) \
	SUPPRESS_DO_PRAGMA(GCC diagnostic ignored "-Wsign-compare") \
	do{ assert( why ); } while(0)

#define SUPPRESS_RESTORE     SUPPRESS_DO_PRAGMA(GCC diagnostic pop) do {} while (0)

#endif /* UTIL_BABEL_SUPPRESS_H_ */
