/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2016/08/28
 */
#include <util/babel_logger.h>
#include <cstdio>
#include <limits>
#include <cstdarg>

namespace babel { namespace logger {

bool         isEnabled      = false;
unsigned int theIndentLevel = 0;

void enable() {
    isEnabled = true;
}

void disable() {
    isEnabled = false;
}

IndentIncrease::IndentIncrease() {
    if (theIndentLevel != std::numeric_limits<decltype(theIndentLevel)>::max()) {
        theIndentLevel += 1;
    }
}

IndentIncrease::~IndentIncrease() {
    if (theIndentLevel) {
        theIndentLevel -= 1;
    }
}

void vlogMessage( const char* const   theFile,
                  const unsigned int  theLine,
                  const char* const   theFunction,
                  const char* const   theFormat,
                  va_list            &theArguments ) {

    if (isEnabled) {
        (void)fprintf ( stdout, "%s:%u %*s(%s) ", theFile, theLine, 2*theIndentLevel, "", theFunction );
        (void)vfprintf( stdout, theFormat, theArguments);
        (void)fprintf ( stdout, "\n" );
    }
}

void logMessage( const char* const  theFile,
                 const unsigned int theLine,
                 const char* const  theFunction,
                 const char* const  theFormat,
                 ... ) {
    va_list theArguments;

    va_start( theArguments, theFormat );

    vlogMessage( theFile, theLine, theFunction, theFormat, theArguments );

    va_end( theArguments );
}
ScopedLog::ScopedLog( const char* const  theFile,
                      const unsigned int theLine,
                      const char* const  theFunction,
                      const char* const  theFormat,
                      ... )
: itsIndentIncrease(),
  itsFile(theFile),
  itsLine(theLine),
  itsFunction(theFunction) {

    va_list theArguments;

    va_start( theArguments, theFormat );

    vlogMessage( itsFile.c_str(), itsLine, ("ENTER " + itsFunction).c_str(), theFormat, theArguments );

    va_end( theArguments );
}
ScopedLog::ScopedLog( const char* const  theFile,
                      const unsigned int theLine,
                      const char* const  theFunction )
: itsIndentIncrease(),
  itsFile(theFile),
  itsLine(theLine),
  itsFunction(theFunction) {

  logMessage( itsFile.c_str(), itsLine, ("ENTER " + itsFunction).c_str(), " " );
}

ScopedLog::~ScopedLog() {
    logMessage( itsFile.c_str(), itsLine, ("EXIT  " + itsFunction).c_str(), " " );
}

}}
