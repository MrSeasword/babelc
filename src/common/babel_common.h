/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2016/08/28
 */
#ifndef COMMON_BABEL_COMMON_H_
#define COMMON_BABEL_COMMON_H_

#include <memory>
#include <list>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <iterator>
#include <algorithm>
#include <util/babel_util.h>
#include <regex>
#include <type_traits>

#include "babel_types.h"

namespace babel {
/**
 * Forward declaration of the iterator in order to declare the traits.
 *
 */
class LinesIterator;

}

namespace std {

template <>
class iterator_traits<babel::LinesIterator> {
public:
    typedef std::ptrdiff_t                   difference_type;
    typedef char                             value_type;
    typedef char                            *pointer;
    typedef char                            &reference;
    typedef std::bidirectional_iterator_tag  iterator_category;
};

}

namespace babel {

/**
 * The following enum gives the possible command line options.
 * If you add a new value you should also add a description line
 * in the option documentation within the babel c main program.
 * Note that the enum value is also used as the textual command
 * line option.
 */
enum class BabelcOptions {
    _FIRST,                   //!< _FIRST Just a name for the first value

    KeepIntermediate = BabelcOptions::_FIRST,//!< KeepIntermediate
    Log,                      //!< Log
    Output,                   //!< Output
    Force,                    //!< Force
    NoColor,                  //!< NoColor
    Summary,                  //!< Summary
    UseTabs,                  //!< UseTabs
    Directory,                //!< Directory
    Compiler,                 //!< Compiler
    UnitTest,                 //!< UnitTest

    _LAST = BabelcOptions::UnitTest          //!< BabelcOptions_LAST This must be updated if a new value is added last.
};

/**
 * This is the number of options
 */
static constexpr std::size_t BabelcOptions_LENGTH = (static_cast<std::size_t>(BabelcOptions::_LAST) - static_cast<std::size_t>(BabelcOptions::_FIRST) + 1);

/**
 * A bit set type for the options.
 */
using BabelcOptionSet = std::set<BabelcOptions>;


/**
 * Conditional template which evaluates to the S argument if an instance of S can be used to construct an std::string.
 */
template < class S >
using StringConvertible = std::enable_if_t< std::is_constructible< std::string, S >::value, S  >;

class Line;
using Lines      = std::list<Line>;
using LinesPtr   = std::shared_ptr< Lines >;

/**
 * Function template to create a dynamically allocated Lines instance using the provided
 * iterators to initialize it.
 * @param theBegin
 * @param theEnd
 * @return A shared pointer to the Lines instance created.
 */
template <class I>
LinesPtr makeLines( I theBegin, I theEnd ) {
    auto const theLinesPtr = std::make_shared<Lines>();
    while (theBegin != theEnd ) {
        theLinesPtr->emplace_back(*theBegin++);
    }
    return theLinesPtr;
}

/**
 * This function splices, i.e. moves, the Line instances identified by the iterators into
 * the passed Lines instance.
 * @param theBegin
 * @param theEnd
 * @param intoTheLines
 */
void splice( LinesIterator &theBegin, LinesIterator &theEnd, Lines &intoTheLines  );


/**
 * A line is a string with an associated filename and line number
 */
class Line {
private:
    friend void splice( LinesIterator &theBegin, LinesIterator &theEnd, Lines &intoTheLines );

    std::string                    itsContents;
    std::shared_ptr< std::string > itsFile;
    std::size_t                    itsLineNumber;

public:

    template < class S >
    Line( S &&theContents, const decltype(itsFile) &theFile, std::size_t theLineNumber)
    : itsContents(std::forward<StringConvertible<S>>(theContents)), itsFile(theFile), itsLineNumber(theLineNumber) {
    }

    template < class S >
    explicit Line( S &&theContents ) : Line( std::forward<StringConvertible<S>>(theContents), std::make_shared<std::string>(), 0 ) {}

    template < class S >
    Line( S &&theNewContents, const Line &forLocation ) : Line( std::forward<StringConvertible<S>>(theNewContents), forLocation.itsFile, forLocation.itsLineNumber ) {}

    Line substr( std::string::size_type theBegin, std::string::size_type theEnd ) const {
        return Line( itsContents.substr(theBegin, theEnd ), itsFile, itsLineNumber );
    }

    const std::string &getItsContents() const { return itsContents; }
    std::string       &getItsContents()       { return itsContents; }
    std::string        getItsLocation() const { return itsFile ? babel::util::format("%s:%lu", itsFile->c_str(), itsLineNumber ) : std::string(""); }

    Line &setItsLocation( const decltype(itsFile) &theFile, std::size_t theLineNumber ) {
        itsFile       = theFile;
        itsLineNumber = theLineNumber;
        return *this;
    }
};

/**
 * Simple class to encapsulate a file name and some operations on it. I know there is an excellent class in boost,
 * but this product aims to be boost-free. When the file system classes make it into the std, then there is no
 * question of what to use...
 */
class FilePath {
public:
    template <class StringCompatible, typename std::enable_if< std::is_constructible< std::string, StringCompatible>::value, int >::type = 0>
    explicit FilePath( StringCompatible &&thePathName ) : itsPath( std::forward<StringCompatible>(thePathName) ) {}

    template <class StringCompatible, typename std::enable_if< std::is_constructible< std::string, StringCompatible>::value, int >::type = 0>
    FilePath( FilePath theDirectory, StringCompatible &&theName ) : itsPath( theDirectory.itsPath + "/" + std::forward<StringCompatible>(theName) ) {}

    const std::string &string() const {
        return itsPath;
    }

    bool empty() const { return itsPath.empty(); }
    
    FilePath sansDirectory() const {
        const auto slashPos = itsPath.find_last_of("\\/");
        return (slashPos == std::string::npos) ? *this : FilePath( itsPath.substr(slashPos+1));
    }

    std::string extension() const {
        const auto   pathSansDirectory = sansDirectory();
        const auto &theName            = pathSansDirectory.itsPath;
        
        return theName.substr(std::min(theName.size(),theName.find_last_of('.')));
    }

    FilePath sansExtension() const {
        return FilePath(itsPath.substr(0,(itsPath.size()-extension().size())));
    }
    
    FilePath directoryPart() const {
        const auto slashPos = itsPath.find_last_of("\\/");
        return (slashPos == std::string::npos) ? FilePath(".") : FilePath(itsPath.substr(0, slashPos));
    }
    
    template <class StringCompatible, typename std::enable_if< std::is_constructible< std::string, StringCompatible>::value, int >::type = 0>
    FilePath replace_extension( StringCompatible && theNewExtension ) const {
        return FilePath( sansExtension().itsPath + theNewExtension );
    }

    /**
     * Returns true if the file exists and false otherwise.
     */
    bool exists() const;
    
    /**
     * Get the modified time of the file. Will throw if file is not accessible.
     */
    std::time_t getModifiedTime() const;
    
    /**
     * If the file exists it is attempted to be removed. If this fails an exception is thrown.
     * If the file does not exist, nothing happens.
     */
    void remove() const;

    /**
     * Returns true if the file exists and is a directory
     */
    bool isDirectory() const;
private:
    std::string itsPath;
};

using FilePaths = std::vector<FilePath>;

/**
 * Dumps theLines onto thePath (with new extension theExtension) according to theOptions.
 * @param theOptions
 * @param theSource
 * @param theExtension
 * @param theLines
 */
void dumpLines( const BabelcOptionSet &theOptions, const FilePath &theSource, const char *theExtension, const Lines &theLines );

/**
 * This class defines an iterator into a dynamically allocated Lines instance.
 */
class LinesIterator {
public:
    typedef char char_type;


    static LinesIterator begin(const LinesPtr &theStrings);

    static LinesIterator end(const LinesPtr &theStrings );


    char           operator* () const;
    LinesIterator &operator++();
    LinesIterator  operator++(int);
    LinesIterator &operator--();
    LinesIterator  operator--(int);
    bool           operator==( const LinesIterator &other ) const;
    bool           operator!=( const LinesIterator &other ) const;

    LinesIterator();

    std::string getLocation() const;

private:
    friend void splice( LinesIterator &theBegin, LinesIterator &theEnd, Lines &intoTheLines );

    LinesIterator( const LinesPtr        &theStrings,
                   const Lines::iterator &thePosition,
                   std::ptrdiff_t         theColumn );

    LinesPtr              itsLines;
    Lines::iterator       itsCurrentLine;
    std::ptrdiff_t        itsCurrentColumn;
};

using LinesMatcher = std::match_results< LinesIterator >;

/**
 * Here follows a slew of regex functions explicitely declared. The direct usage of templates made the eclipse parser freak out
 * and signal errors although gcc built the code without complaint. This caused dummy errors all over the place
 * and I prefer to gather them in one place.
 *
 */
bool regex_search(const LinesIterator &theBegin, const LinesIterator &theEnd, LinesMatcher &theMatcher, const std::regex &theRegexp);
bool regex_search(const std::string::const_iterator &theBegin, const std::string::const_iterator &theEnd, std::smatch &theMatcher, const std::regex &theRegexp);
bool regex_match(const std::string &theString, std::smatch &theMatcher, const std::regex &theRegexp );
bool regex_match(const LinesIterator &theBegin, const LinesIterator &theEnd, LinesMatcher &theMatcher, const std::regex &theRegexp);
bool regex_match(const std::string::const_iterator &theBegin, const std::string::const_iterator &theEnd, std::smatch &theMatcher, const std::regex &theRegexp);



/**
 * A compilation unit the the result of a succesfully parsed file.
 */
class CompilationUnit {
public:
    typedef const CompilationUnit *Ptr;

    const FilePath    itsSourceFile;
    const Namespace   itsNameSpace;
    const Types       itsTypes;
    const FilePaths   itsDependentFiles;

    CompilationUnit( const FilePath   &thePath, 
                     const Namespace  &theNamespace, 
                     const Types      &theTypes, 
                     const FilePaths  &theDependentFiles )
    : itsSourceFile(thePath), 
      itsNameSpace(theNamespace), 
      itsTypes(theTypes),
      itsDependentFiles(theDependentFiles)
    {}
};

//===================================================================================================
//
// Here after follows inline declarations.
//
//===================================================================================================


inline LinesIterator LinesIterator::begin(const LinesPtr &theContainer) {
    Lines::iterator thePosition = std::begin(*theContainer);

    while ((thePosition != std::end(*theContainer)) && (*thePosition).getItsContents().empty()) {
        thePosition++;
    }
    return LinesIterator( theContainer, thePosition, 0 );
}

inline LinesIterator LinesIterator::end(const LinesPtr &theContainer) {
    return LinesIterator( theContainer, std::end(*theContainer), 0 );
}


inline char LinesIterator::operator*() const {
    if (itsCurrentLine == std::end(*itsLines)) {
        throw std::out_of_range("Dereference of StringsIterator iterator beyond end");
    }
    else if (itsCurrentColumn < 0) {
        throw std::out_of_range("Dereference of StringIterator iterator before begin");
    }
    else {
        return (*itsCurrentLine).getItsContents()[itsCurrentColumn];
    }
}
inline LinesIterator &LinesIterator::operator++() {

    if ((++itsCurrentColumn >= 0) &&
        (itsCurrentLine   != std::end(*itsLines)) &&
        (itsCurrentColumn   >= static_cast<std::ptrdiff_t>((*itsCurrentLine).getItsContents().size()))) {
        itsCurrentColumn = 0;
        while ((++itsCurrentLine != std::end(*itsLines)) && (*itsCurrentLine).getItsContents().empty())
            ;
    }

    return *this;
}

inline LinesIterator LinesIterator::operator++(int) {
    const LinesIterator theOldValue(*this);
    ++*this;
    return theOldValue;
}

inline LinesIterator &LinesIterator::operator--() {

    if ((itsCurrentColumn == 0) && (itsCurrentLine != std::begin(*itsLines))) {
        while ((--itsCurrentLine != std::begin(*itsLines)) && (*itsCurrentLine).getItsContents().empty())
            ;
        itsCurrentColumn = (*itsCurrentLine).getItsContents().size();
    }
    itsCurrentColumn -= 1;

    return *this;
}

inline LinesIterator LinesIterator::operator--(int) {
    const LinesIterator theOldValue(*this);
    --*this;
    return theOldValue;
}

inline bool LinesIterator::operator == ( const LinesIterator &other ) const {
    if ((std::begin(*itsLines) != std::begin(*other.itsLines)) || (std::end(*itsLines) != std::end(*other.itsLines))) {
        throw std::invalid_argument("Comparing equality for iterators from different containers");
    }
    return (itsCurrentColumn == other.itsCurrentColumn) && (itsCurrentLine == other.itsCurrentLine);
}
inline bool LinesIterator::operator != ( const LinesIterator &other ) const {
	return !(*this == other);
}

inline std::string LinesIterator::getLocation() const {
    return util::format("%s:%ld", itsCurrentLine->getItsLocation().c_str(), std::max(static_cast<std::ptrdiff_t>(0), itsCurrentColumn) + 1);
}


inline LinesIterator::LinesIterator( const LinesPtr            &theStrings,
                                         const Lines::iterator &thePosition,
										 std::ptrdiff_t         theColumn )
: itsLines(theStrings), itsCurrentLine( thePosition ), itsCurrentColumn(theColumn)
{}


inline LinesIterator::LinesIterator()
: itsLines(), itsCurrentLine(), itsCurrentColumn()
{}

}


#endif /* COMMON_BABEL_COMMON_H_ */
