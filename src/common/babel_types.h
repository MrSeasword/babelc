/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 25, 2016
 */

#ifndef COMMON_BABEL_TYPES_H_
#define COMMON_BABEL_TYPES_H_

#include <vector>
#include <string>
#include <util/babel_util.h>

namespace babel {

using Namespace  = std::vector< std::string >;

extern const std::string theNamespaceDelimiter;

/**
 * Converts a Namespace instance to a string where the name space names are enumerated in sequence with a "::" following each component
 * (and thus also the last). If the namespace vector is empty, an empty string is returned.
 * @param theNamespace An instance of Namespace (possibly empty).
 * @return Its string representation.
 */
std::string NamespaceToString( const Namespace theNamespace );

/**
 * Converts a string to an instance of Namespace. Special cases are an empty string and a sole "::" both which result in an empty vector. A
 * trailing "::" does not generate an empty namespace name. An exception of type std::invalid_argument will be thrown if the C++ namespace
 * syntax is somehow violated.
 * @param theString The string representation of a namespace.
 * @return A namespace instance.
 */
Namespace StringToNamespace( const std::string &theString );

/**
 * Container for a bunch of types
 */
class Type;
typedef std::map< std::string, const Type * > Types;

struct ReferenceFilter {
    virtual bool breakAt( const Type *theType ) = 0;
};
/**
 * The root class for all types
 */
class Type {
public:
    const std::string                 itsName;
    const std::string                 itsClass;
    const std::string                 itsLocation;

    std::string         toString( const std::string &theIndent ) const;
    std::string         getItsSimpleName() const;
    Namespace           getItsNamespace() const;
    virtual const Type *getItsBaseType() const { return this; }
    virtual bool        isVacuous() const { return false; }
    virtual void        getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const = 0;
    virtual bool        isReferred( const Type *theOther ) const = 0;
    virtual            ~Type();
    virtual Type *      clone( const std::string &itsNewName, const std::string &itsNewLocation ) const = 0;
    virtual std::string getItsTypeSpecification() const = 0;
    Type *              addAttributes( const std::string &theAttributes ) const;
    std::shared_ptr<const std::string> getAttributeValue(const std::string &theAttributeName) const;
protected:
    Type( const std::string &theName, const std::string &theClass, const std::string &theLocation );
    virtual std::string derivedToString(const std::string &theIndent) const = 0;
    static std::string safeToString( const Type *theType, const std::string &theIndent );
private:
    std::map<std::string,std::shared_ptr<const std::string>> itsAttributes;
};

/**
 * A template interface of for this types that can be instantiated
 */
class Template {
public:
    const std::size_t itsExpectedNoOfArguments;
    
    virtual Type *      instantiate( const std::vector<const Type *> &theTemplateArguments ) const = 0;
protected:
    explicit Template( std::size_t theExpectedNoOfArguments) 
    : itsExpectedNoOfArguments(theExpectedNoOfArguments) {
    }

    virtual ~Template() {}
};

/**
 * The built in types that are available
 */
extern const Types theBuiltInTypes;

/**
 * Performs a dynamic cast on the given parameter to the desired type, but it
 * will also follow recursively through typedefs.
 * @param theType The type object to cast
 * @return nullptr if cast fails or a valid pointer to a derived type object
 */
template <typename TheDesiredType>
const TheDesiredType *recursive_cast( const Type *theType );

/**
 * Well, the void type is needed in this language
 */
class Void : public Type {
public:
    Void(const std::string&theName, const std::string &theLocation);
    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;
    bool isReferred(const Type* theOther) const override;
    std::string getItsTypeSpecification() const override;
    bool isVacuous() const override { return true; }

protected:
    std::string derivedToString(const std::string &) const override;

};
/**
 * A typedef type (which really is a renamed type in this strange language)
 */
class Typedef : public Type {
public:
    const Type * const itsBaseType;

    Typedef(const std::string &theName, const Type* theBaseType, const std::string &theLocation );
    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    const Type *getItsBaseType() const override { return itsBaseType->getItsBaseType(); }
    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;
    bool isReferred(const Type* theOther) const override;
    std::string getItsTypeSpecification() const override;

protected:
    std::string derivedToString(const std::string &theIndent) const override;
};

/**
 * Built-in type, i.e. those from <cstdint>, <cfloat>, <cstdbool>.
 */
class BuiltInType : public Type {
public:
    const bool        isSigned;
    const std::string itsWideningType; // If not empty, necessary type to widen to to get a numeric IO representation
    
    BuiltInType( const std::string &theName, bool hasSigned, const std::string &theWideningType, const std::string &theLocation );
    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;
    bool isReferred(const Type* theOther) const override;
    std::string getItsTypeSpecification() const override;

protected:
    std::string derivedToString(const std::string &) const override;

};

class EnumeratedType : public Type {
public:
    using Values = std::vector< std::string >;

    const Type * const itsUnderlyingType;
    const Values       itsValues;

    EnumeratedType( const std::string theName, const Type *theUnderlyingType, const Values &theValues, const std::string &theLocation );

    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;
    bool isReferred(const Type* theOther) const override;
    std::string getItsTypeSpecification() const override;

protected:
    std::string derivedToString(const std::string &) const override;
};

/**
 * Type class for std::string
 */
class StdString : public Type {
public:
    StdString( const std::string &theName, const std::string &theLocation );
    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;
    bool isReferred(const Type* theOther) const override;
    std::string getItsTypeSpecification() const override;

protected:
    std::string derivedToString(const std::string &) const override;
};

/**
 * The root class for composite types
 */
class CompositeType : public Type {
protected:
    CompositeType( const std::string &theName, const std::string &theClass, const std::string &theLocation);
};

/**
 * A POD struct
 */
class StructType : public CompositeType {
public:
    struct Field {
        const std::string  itsName;
        const Type * const itsType;
        const bool         isInitialized;

        Field(const std::string &theName, const Type *theType, bool initialized) : itsName(theName), itsType(theType), isInitialized(initialized) {}
    };

    typedef std::vector< Field > Fields;

    const Fields itsFields;

    StructType( const std::string &theName, const Fields &theFields, const std::string &theLocation );
    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    bool isReferred(const Type* theOther) const override;
    std::string getItsTypeSpecification() const override;
    bool isVacuous() const override { return itsFields.empty(); }

    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;
protected:
    std::string derivedToString(const std::string &theIndent) const override;
};

/**
 * A container with varying contents
 */
class VariantType : public CompositeType, public Template {
public:
    const std::vector<const Type *> itsTypes;
    const std::string               itsIndexMethod;
    const std::string               itsGetMethod;
    
    VariantType( const std::string &theName, const std::string &theIndexMethod, const std::string &theGetMethod, const std::string &theLocation );
    VariantType( const std::string &theName, const std::string &theIndexMethod, const std::string &theGetMethod, const std::vector<const Type *> theTypes, const std::string &theLocation );

    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    bool isReferred(const Type* theOther) const override;

    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;

    Type *instantiate( const std::vector<const Type *> &theTemplateArguments ) const override;

    std::string getItsTypeSpecification() const override;

protected:
    std::string derivedToString(const std::string &theIndent) const override;
};

class InterfaceType : public Type {
public:
    struct Method {        struct Argument {
            const std::string  itsName;
            const Type * const itsType;
            const bool         isConst;
            const bool         isReference;
            const std::string  itsLocation;

            Argument( std::string theName, const Type *theType, bool isConst_, bool isReference_, const std::string &theLocation )
            : itsName(theName), itsType(theType), isConst(isConst_), isReference(isReference_), itsLocation(theLocation) {
            }
        };
        typedef std::vector<Argument> Arguments;

        const Type * const itsReturnType; // nullptr means void
        const std::string  itsName;
        const Arguments    itsArguments;  // Empty container means void

        Method( const Type *theReturnType, const std::string &theName, const Arguments &theArguments )
        : itsReturnType(theReturnType), itsName(theName), itsArguments(theArguments) {
        }
    };

    typedef std::vector<Method> Methods;

    const Methods itsMethods;

    InterfaceType( const std::string &theName, const Methods theMethods, const std::string &theLocation );

    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;
    bool isReferred(const Type* theOther) const override;

    bool hasReturnValues() const {
        return itsMethods.size() != 
                static_cast<std::size_t>(
                    std::count_if( BABEL_UTIL_RANGE(itsMethods), 
                                  [](const Method &theMethod) { 
                                      return dynamic_cast<const Void*>(theMethod.itsReturnType); 
                                  } ) );
    }
    
    std::string getItsTypeSpecification() const override;

protected:
    std::string derivedToString(const std::string &theIndent) const override;
};

/**
 * The root class for container types
 */
class Container : public CompositeType {
public:
    const Type * const itsElementType;
    const std::string &getItsStlType() const;
    
    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    bool isReferred(const Type* theOther) const override;

protected:
    std::string  itsStlType;
    Container( const std::string &theName, const std::string theClass, const Type *theElementType, const std::string &theStlType, const std::string &theLocation );
};

/**
 * A container with optional fixed type contents
 */
class OptionalType : public Container, public Template {
public:
    OptionalType( const std::string &theName, const std::string &theLocation );
    OptionalType( const std::string &theName, const Type *theType, const std::string &theLocation );

    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;

    Type *instantiate( const std::vector<const Type *> &theTemplateArguments ) const override;

    std::string getItsTypeSpecification() const override;

protected:
    std::string derivedToString(const std::string &theIndent) const override;
};

/**
 * A fixed sized array
 */
class FixedArray : public Container, public Template {
public:
    const std::size_t itsSize;
    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;

    FixedArray( const std::string &theName, const Type *theElementType, std::size_t theSize, const std::string &theLocation );

    std::string getItsTypeSpecification() const override;

protected:

    std::string derivedToString(const std::string &theIndent) const override;

    Type *instantiate( const std::vector<const Type *> &theTemplateArguments ) const override;
};

/**
 * A container with varying size (std::vector, std::list)
 */
class VaryingLengthContainer : public Container, public Template {
public:
    const bool isConsecutive;
    
    VaryingLengthContainer( const std::string &theName, const Type *theElementType, bool isConsecutive, const std::string &theLocation );

    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;

    Type *instantiate( const std::vector<const Type *> &theTemplateArguments ) const override;

    std::string getItsTypeSpecification() const override;

protected:
    std::string derivedToString(const std::string &theIndent) const override;
};

/**
 * A container with a key and value (std::map...)
 */
class MapContainer : public Container, public Template {
public:
    const Type * const itsKeyType;
    
    MapContainer( const std::string &theName, const Type *theKeyType, const Type *theElementType, const std::string &theLocation );

    Type * clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;

    Type * instantiate( const std::vector<const Type *> &theTemplateArguments ) const override;

    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    bool isReferred(const Type* theOther) const override;
    std::string getItsTypeSpecification() const override;

protected:
    std::string derivedToString(const std::string &theIndent) const override;
};

/**
 * A general pointer type.
 */
class PointerType : public Type, public Template {
public:
    const Type * const itsReferredType;
    const bool         isShared;

    PointerType( const std::string theName, const Type *theReferredType, bool isShared_, const std::string &theLocation );

    Type *instantiate( const std::vector<const Type *> &theTemplateArguments ) const override;
    Type *clone( const std::string &itsNewName, const std::string &itsNewLocation ) const override;
    bool  isReferred(const Type* theOther) const override;

    void getItsReferredTypes( Types &theReferredTypes, ReferenceFilter &theFilter ) const override;
    std::string getItsTypeSpecification() const override;
protected:
    std::string derivedToString(const std::string &theIndent) const override;
};

// Implementation of an above declaration, which see
//

template <typename TheDesiredType>
const TheDesiredType *recursive_cast( const Type *theType ) {
    const auto theTypedef = dynamic_cast<const Typedef*>(theType);
    if (theTypedef) {
        return recursive_cast<TheDesiredType>(theTypedef->itsBaseType);
    }
    else {
        return dynamic_cast<const TheDesiredType *>(theType);
    }
}

template <typename... types>
bool is_any_of( const Type *theType ) {
    std::array<const Type*, sizeof...(types)> theCasts{ recursive_cast<types>(theType)...};
    
    return std::count(BABEL_UTIL_RANGE(theCasts), nullptr) != theCasts.size();
}
}


#endif /* COMMON_BABEL_TYPES_H_ */
