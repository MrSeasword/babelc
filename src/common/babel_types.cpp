/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 25, 2016
 */

#include <util/babel_logger.h>
#include "babel_common.h"
#include "unittest/babel_unittest.h"
#include "util/babel_suppress.h"
#include "babel_types.h"
#include <numeric>

namespace babel {

const std::string theNamespaceDelimiter = "::";
const std::string oneIndent = "   ";

/**
 * Helper function to create a pair where the first item is the name of the type following it.
 * @param theName
 * @param theTypeArgs
 * @return
 */
template <class TheType, typename... TheTypeArgs  >
Types::value_type makeTypePair(const char *theName, TheTypeArgs... theTypeArgs ) {
    return std::pair< std::string, const Type *>{ theName, new TheType( theName, theTypeArgs... ) };
}

#define CHECK_TEMPLATE_ARGC( arguments )                                      \
do {                                                                          \
    if (itsExpectedNoOfArguments != arguments.size()) {                       \
        throw std::invalid_argument("Wrong number of template arguments: " +  \
                                    std::to_string(arguments.size()) +        \
                                    ". Expected " +                           \
                                    std::to_string(itsExpectedNoOfArguments));\
    }                                                                         \
} while (0)

const Types theBuiltInTypes = {
        makeTypePair<BuiltInType>           ( "bool",           false, "",             "C++ language" ),
        makeTypePair<BuiltInType>           ( "float",          true,  "",             "C++ language" ),
        makeTypePair<BuiltInType>           ( "double",         true,  "",             "C++ language" ),
        makeTypePair<BuiltInType>           ( "std::int8_t",    true,  "std::int32_t", "<cstdint>" ),
        makeTypePair<BuiltInType>           ( "std::uint8_t",   false, "std::uint32_t","<cstdint>" ),
        makeTypePair<BuiltInType>           ( "std::int16_t",   true,  "std::int32_t", "<cstdint>" ),
        makeTypePair<BuiltInType>           ( "std::uint16_t",  false, "std::uint32_t","<cstdint>" ),
        makeTypePair<BuiltInType>           ( "std::int32_t",   true,  "",             "<cstdint>" ),
        makeTypePair<BuiltInType>           ( "std::uint32_t",  false, "",             "<cstdint>" ),
        makeTypePair<BuiltInType>           ( "std::int64_t",   true,  "",             "<cstdint>" ),
        makeTypePair<BuiltInType>           ( "std::uint64_t",  false, "",             "<cstdint>" ),
        makeTypePair<Void>                  ( "void",           "C++ language"),
        makeTypePair<PointerType>           ( "std::shared_ptr", nullptr, true, "<memory>" ),
        makeTypePair<PointerType>           ( "std::unique_ptr", nullptr, false, "<memory>" ),
        makeTypePair<FixedArray>            ( "std::array", nullptr, 0, "<array>" ),
        makeTypePair<VaryingLengthContainer>( "std::vector", nullptr, true, "<vector>" ),
        makeTypePair<VaryingLengthContainer>( "std::list", nullptr, false, "<list>" ),
        makeTypePair<MapContainer>          ( "std::map", nullptr, nullptr, "<map>" ),
        makeTypePair<StdString>             ( "std::string", "<string>" ),
        makeTypePair<VariantType>           ( "std::variant",   "index", "std::get", "<variant>" ),
        makeTypePair<VariantType>           ( "boost::variant", "which", "boost::get", "<boost/variant.hpp>" ),
        makeTypePair<OptionalType>          ( "std::optional",   "<optional>" ),
        makeTypePair<OptionalType>          ( "boost::optional", "<boost/optional.hpp>" )
};

//===============================================================================

std::string NamespaceToString( const Namespace theNamespace ) {
    switch (theNamespace.size()) {
        case 0: return std::string("");
        case 1:
            if ((theNamespace[0] == "std") || (theNamespace[0] == "boost")) {
                return theNamespace[0]  + theNamespaceDelimiter;
            }
            [[fallthrough]];
        default:
            return std::accumulate(
                    BABEL_UTIL_RANGE(theNamespace),
                    theNamespaceDelimiter,
                    []( const std::string &thePrefix, const std::string &theComponent){
                        return thePrefix + theComponent + theNamespaceDelimiter;
                    });
    }
}

//===============================================================================

Namespace StringToNamespace( const std::string &theString ) {
    Namespace        theNamespace;
    const std::regex theDelimiter( "[[:space:]]*" + theNamespaceDelimiter + "[[:space:]]*" );
    const std::regex theIdentifier( "[A-Za-z_][A-Za-z_0-9]*" );
    const auto       theIdentifierMatcher = [&theIdentifier]
                                            (const std::string& theComponent) {
                                                    std::smatch theMatcher;
                                                    return regex_match(BABEL_UTIL_RANGE(theComponent), theMatcher, theIdentifier);
                                            };

    // This will always produce at least one element in the output.
    //
    std::copy( std::sregex_token_iterator( BABEL_UTIL_RANGE(theString), theDelimiter, -1 ),
               std::sregex_token_iterator(),
               std::back_inserter(theNamespace));

    // An empty first element comes if the string begins with "::", which is allowed.
    // Simply dump it.
    //
    if (theNamespace.at(0).empty()) {
        theNamespace.erase( theNamespace.begin());
    }

    // The rest of the components must adhere to the identifier syntax
    //
    if (static_cast<Namespace::size_type>(std::count_if( BABEL_UTIL_RANGE(theNamespace), theIdentifierMatcher)) != theNamespace.size()) {
        throw std::invalid_argument("Namespace component is not a C++ identifier '" + theString + "'");
    }

    return theNamespace;
}


//===============================================================================

std::string Type::toString( const std::string &theIndent ) const {
    std::string theAttributes;
    for (const auto &theAttribute : itsAttributes) {
        if (!theAttributes.empty()) {
            theAttributes += ", ";
        }
        theAttributes += theAttribute.first + ":" + *theAttribute.second;
    }
    if (!theAttributes.empty()) {
        theAttributes = "\n" + theIndent + oneIndent + "with attributes " + theAttributes;
    }
    return itsClass + " named " + itsName + derivedToString(theIndent + oneIndent) + theAttributes;
}

std::string Type::getItsSimpleName() const {
    auto const theLastSeparator = std::find_end(BABEL_UTIL_RANGE(itsName), BABEL_UTIL_RANGE(theNamespaceDelimiter));
    return (theLastSeparator == itsName.end()) ? itsName : std::string((theLastSeparator+theNamespaceDelimiter.length()), itsName.end());
}

Namespace Type::getItsNamespace() const {
    auto const theLastSeparator = std::find_end(BABEL_UTIL_RANGE(itsName), BABEL_UTIL_RANGE(theNamespaceDelimiter));
    return StringToNamespace((theLastSeparator == itsName.end()) ? std::string() : std::string(itsName.begin(), theLastSeparator));
}

Type::~Type() {
}

Type::Type( const std::string &theName, const std::string &theClass, const std::string &theLocation)
: itsName(theName), itsClass(theClass), itsLocation(theLocation) {
}

std::string Type::safeToString( const Type *theType, const std::string &theIndent ) {
    return theType ? theType->toString(theIndent) : std::string("<UNDEFINED>");
}

std::shared_ptr<const std::string> Type::getAttributeValue(const std::string &theAttributeName) const {
    const auto theIterator = itsAttributes.find(theAttributeName);
    
    return (theIterator != itsAttributes.end()) ? theIterator->second : std::shared_ptr<const std::string>();
}

Type * Type::addAttributes( const std::string &theAttributes ) const {
    static const char theTag[] = "babelc::";
    static const auto theEmptyString = std::make_shared<const std::string>("");
    
    const auto             theCopy  = clone( itsName, itsLocation );
    std::string::size_type theStart = 0; 
    
    for (;;) {
        theStart = theAttributes.find(theTag, theStart);
        
        if (theStart == std::string::npos) {
            break;
        }
        const auto theNameStart     = theStart + (sizeof(theTag)-1);
        const auto theNameEnd       = theAttributes.find_first_of("(,", theNameStart);
        const auto theAttributeName = theAttributes.substr(theNameStart, (theNameEnd-theNameStart)); 
        
        if (theNameEnd == std::string::npos) {
            theCopy->itsAttributes[theAttributeName] = theEmptyString;
            theStart = theNameEnd;
        }
        else if (theAttributes[theNameEnd] == ',') {
            theCopy->itsAttributes[theAttributeName] = theEmptyString;
            theStart = theNameEnd + 1;
        }
        else {
            // This is dubious, but since we only work upon syntactically correct C++ code...
            //
            const auto theLastParen  = theAttributes.find(')', theNameEnd);
            const auto theFirstQuote = theAttributes.find('"', theNameEnd);
            const auto theLastQuote  = theAttributes.rfind('"', theLastParen);
            theCopy->itsAttributes[theAttributeName] = std::make_shared<std::string>(theAttributes.substr( theFirstQuote + 1, (theLastQuote - (theFirstQuote  + 1)) ));
            theStart = theLastParen + 1;
        }
    }
    
    return theCopy;
}

//===============================================================================

Void::Void(const std::string&theName, const std::string &theLocation)
: Type( theName, "Void", theLocation) {
}

std::string Void::derivedToString(const std::string &) const {
    return "";
}
std::string Void::getItsTypeSpecification() const {
    return "void";
}

void Void::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter ) const {
    if (!theFilter.breakAt(this) && (theReferredTypes.find(itsName) == theReferredTypes.end())) {
        theReferredTypes.emplace( itsName, this );
    }
}

Type* Void::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new Void( itsNewName, itsNewLocation );
}

bool Void::isReferred(const Type* theOther) const {
    return dynamic_cast<decltype(this)>(theOther);
}

//===============================================================================

Typedef::Typedef(const std::string &theName, const Type* theBaseType, const std::string &theLocation )
: Type(theName, "Typedef", theLocation), itsBaseType(theBaseType) {
}

std::string Typedef::derivedToString(const std::string &theIndent) const {
    return " with base type " + safeToString( itsBaseType , theIndent);
}

std::string Typedef::getItsTypeSpecification() const {
    return itsBaseType->getItsTypeSpecification();
}

void Typedef::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter) const {
    if (!theFilter.breakAt(this) && (theReferredTypes.find(itsName) == theReferredTypes.end())) {
        if (itsBaseType) {
            itsBaseType->getItsReferredTypes(theReferredTypes, theFilter);
        }
    }
}

Type* Typedef::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new Typedef( itsNewName, itsBaseType, itsNewLocation );
}
bool Typedef::isReferred(const Type* theOther) const {
    return dynamic_cast<decltype(this)>(theOther);
}

//===============================================================================

BuiltInType::BuiltInType( const std::string &theName, bool hasSign, const std::string &theWideningType, const std::string &theLocation )
: Type(theName, "BuiltIn", theLocation), isSigned(hasSign), itsWideningType(theWideningType) {
}

std::string BuiltInType::derivedToString(const std::string &) const {
    return "";
}

std::string BuiltInType::getItsTypeSpecification() const {
    return itsName;
}

void BuiltInType::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter) const {
    if (!theFilter.breakAt(this) && (theReferredTypes.find(itsName) == theReferredTypes.end())) {
        theReferredTypes.emplace( itsName, this );
    }
}

Type* BuiltInType::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new BuiltInType( itsNewName, isSigned, itsWideningType, itsNewLocation );
}

bool BuiltInType::isReferred(const Type* theOther) const {
    return dynamic_cast<decltype(this)>(theOther);
}


//===============================================================================


EnumeratedType::EnumeratedType( const std::string theName, const Type *theUnderlyingType, const Values &theValues, const std::string &theLocation )
: Type( theName, "EnumeratedType", theLocation ), itsUnderlyingType(theUnderlyingType), itsValues(theValues) {
}

std::string EnumeratedType::derivedToString(const std::string &indent) const {
    return " with underlying type " + safeToString( itsUnderlyingType, indent) + 
           "\n" + indent + " with values: " +
           std::accumulate( BABEL_UTIL_RANGE(itsValues), std::string(""),
                    []( const std::string &thePrevious, const std::string& theValue){return thePrevious + " " + theValue;});
}

std::string EnumeratedType::getItsTypeSpecification() const {
    return itsUnderlyingType->getItsTypeSpecification();
}

void EnumeratedType::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter) const {
    if (!theFilter.breakAt(this) && (theReferredTypes.find(itsName) == theReferredTypes.end())) {
        itsUnderlyingType->getItsReferredTypes(theReferredTypes, theFilter);
        theReferredTypes.emplace( itsName, this );
    }
}

Type* EnumeratedType::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new EnumeratedType(itsNewName, itsUnderlyingType, itsValues, itsNewLocation );
}

bool EnumeratedType::isReferred(const Type* theOther) const {
    return dynamic_cast<decltype(this)>(theOther) && itsName == theOther->itsName;
}

//===============================================================================

StdString::StdString( const std::string &theName, const std::string &theLocation )
: Type(theName, "StdString", theLocation) {
}

std::string StdString::derivedToString(const std::string &) const {
    return "";
}

void StdString::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter) const {
    if (!theFilter.breakAt(this) && (theReferredTypes.find(itsName) == theReferredTypes.end())) {
        theReferredTypes.emplace( itsName, this );
    }
}

std::string StdString::getItsTypeSpecification() const {
    return "std::string";
}

Type* StdString::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new StdString(itsNewName, itsNewLocation);
}

bool StdString::isReferred(const Type* theOther) const {
    return dynamic_cast<decltype(this)>(theOther);
}

//===============================================================================

CompositeType::CompositeType( const std::string &theName, const std::string &theClass, const std::string &theLocation)
: Type(theName, theClass, theLocation) {
}

//===============================================================================


StructType::StructType( const std::string &theName, const Fields &theFields, const std::string &theLocation )
: CompositeType( theName, "StructType", theLocation), itsFields(theFields) {
}

std::string StructType::derivedToString(const std::string &theIndent) const {
    return " with fields:" +
            std::accumulate( BABEL_UTIL_RANGE(itsFields),
                             std::string(),
                            [&theIndent](const std::string &thePrevious, const Field &theField){
                                return thePrevious + "\n" + theIndent + theField.itsName + ": " + safeToString(theField.itsType, theIndent);
                             });
    }

std::string StructType::getItsTypeSpecification() const {
    return "{" +
            std::accumulate( BABEL_UTIL_RANGE(itsFields),
                             std::string(" "),
                             []( const std::string &thePrefix, const Field &theField){
                                 return thePrefix + theField.itsType->getItsTypeSpecification() + " ";
                             }) +
           "}";                  
}

void StructType::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter ) const {
    if (!theFilter.breakAt(this) && (theReferredTypes.find(itsName) == theReferredTypes.end())) {
        for (auto theField : itsFields) {
            theField.itsType->getItsReferredTypes(theReferredTypes, theFilter);
        }
        theReferredTypes.emplace( itsName, this );
    }
}

Type* StructType::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new StructType(itsNewName, itsFields, itsNewLocation );
}

bool StructType::isReferred(const Type* theOther) const {
    if (dynamic_cast<decltype(this)>(theOther) && (itsName == theOther->itsName)) {
        return true;
    }
    for (auto theField : itsFields) {
        if (theField.itsType->isReferred(theOther)) {
            return true;
        }
    }
    return false;
}

//===============================================================================


VariantType::VariantType( const std::string &theName, const std::string &theIndexMethod, const std::string &theGetMethod, const std::string &theLocation )
: VariantType{ theName, theIndexMethod, theGetMethod, std::vector<const Type*>(), theLocation } {
}

VariantType::VariantType( const std::string &theName, const std::string &theIndexMethod, const std::string &theGetMethod, const std::vector<const Type *> theTypes, const std::string &theLocation )
: CompositeType( theName, "VariantType", theLocation), Template(1024), itsTypes(theTypes), itsIndexMethod(theIndexMethod), itsGetMethod(theGetMethod) {
}

std::string VariantType::derivedToString(const std::string &theIndent) const {
    std::string theInitialValue;
    return " with types:" +
            std::accumulate( BABEL_UTIL_RANGE(itsTypes),
                             theInitialValue,
                            [&theIndent](const std::string &thePrevious, const Type *theType){
                                return thePrevious + "\n" + theIndent + safeToString(theType, "");
                             });
    }

std::string VariantType::getItsTypeSpecification() const {
    // This is not entirely correct since variants depends upon their type arguments.
    // The return value rather reflects the existence of an "any" type.
    //
    return "std::variant";
}

void VariantType::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter ) const {
    if (!theFilter.breakAt(this) && (theReferredTypes.find(itsName) == theReferredTypes.end())) {
        for (auto theType : itsTypes) {
            theType->getItsReferredTypes(theReferredTypes, theFilter);
        }
        theReferredTypes.emplace( itsName, this );
    }
}

Type* VariantType::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new VariantType(itsNewName, itsIndexMethod, itsGetMethod, itsTypes, itsNewLocation );
}

bool VariantType::isReferred(const Type* theOther) const {
    if (dynamic_cast<decltype(this)>(theOther) && (itsName == theOther->itsName)) {
        return true;
    }
    for (auto theType : itsTypes) {
        if (theType->isReferred(theOther)) {
            return true;
        }
    }
    return false;
}
Type * VariantType::instantiate( const std::vector<const Type *> &theTemplateArguments ) const {
    return new VariantType( itsName, itsIndexMethod, itsGetMethod, theTemplateArguments, itsLocation );
}

//===============================================================================

InterfaceType::InterfaceType( const std::string &theName, const Methods theMethods, const std::string &theLocation )
: Type( theName, "Interface", theLocation ), itsMethods(theMethods) {
}

std::string InterfaceType::derivedToString(const std::string &theIndent) const {
    return " with methods:" +
            std::accumulate( BABEL_UTIL_RANGE(itsMethods),
                             std::string(""),
                            [&theIndent](const std::string &thePrevious, const Method &theMethod ){
                                return thePrevious + "\n" + theIndent + safeToString(theMethod.itsReturnType, "") +
                                        " " + theMethod.itsName + " (" +
                                        std::accumulate( BABEL_UTIL_RANGE(theMethod.itsArguments),
                                                        std::string(""),
                                                        [&theIndent](const std::string &thePrevious, const Method::Argument &theArgument) {
                                                        return thePrevious + "\n" + theIndent + oneIndent + theArgument.itsName + ": " + safeToString(theArgument.itsType, theIndent + oneIndent);} ) +
                                        "\n" + theIndent + ")";
                             });
    }

std::string InterfaceType::getItsTypeSpecification() const {
    return "I{" +
            std::accumulate( BABEL_UTIL_RANGE(itsMethods),
                             std::string(" "),
                            [](const std::string &thePrevious, const Method &theMethod ){
                                return thePrevious + theMethod.itsReturnType->getItsTypeSpecification() +
                                        " (*)( " +
                                        std::accumulate( BABEL_UTIL_RANGE(theMethod.itsArguments),
                                                        std::string(" "),
                                                        [](const std::string &thePrevious, const Method::Argument &theArgument) {
                                                          return thePrevious + theArgument.itsType->getItsTypeSpecification() + " ";} ) +
                                        ") ";
                             }) +
            "}";
}

void InterfaceType::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter) const {
    if (!theFilter.breakAt(this) && (theReferredTypes.find(itsName) == theReferredTypes.end())) {
        for (auto theMethod : itsMethods) {
            theMethod.itsReturnType->getItsReferredTypes(theReferredTypes, theFilter);
        
            for(auto theArgument : theMethod.itsArguments) {
                theArgument.itsType->getItsReferredTypes(theReferredTypes, theFilter);
            }
        }
        theReferredTypes.emplace( itsName, this );
    }
}

Type* InterfaceType::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new InterfaceType(itsNewName, itsMethods, itsNewLocation );
}

bool InterfaceType::isReferred(const Type* theOther) const {
    if (dynamic_cast<decltype(this)>(theOther) && (itsName == theOther->itsName)) {
        return true;
    }
    for (auto theMethod : itsMethods) {
        if (theMethod.itsReturnType->isReferred(theOther)) {
            return true;
        }

        for(auto theArgument : theMethod.itsArguments) {
            if (theArgument.itsType->isReferred( theOther )) {
                return true;
            }
        }
    }
    return false;
}

//===============================================================================

Container::Container( const std::string &theName, const std::string theClass, const Type *theElementType, const std::string &theStlType, const std::string &theLocation )
: CompositeType( theName, theClass, theLocation), itsElementType(theElementType), itsStlType(theStlType) {
    
}

void Container::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter) const {
    if (!theFilter.breakAt(this) && (theReferredTypes.find(itsName) == theReferredTypes.end())) {
        if (itsElementType) {
            itsElementType->getItsReferredTypes(theReferredTypes, theFilter);
        }
        theReferredTypes.emplace( itsName, this );
    }
}

const std::string& Container::getItsStlType() const {
    return itsStlType;
}

bool Container::isReferred(const Type* theOther) const {
    return (dynamic_cast<decltype(this)>(theOther) && (itsName == theOther->itsName)) || itsElementType->isReferred(theOther);
}



OptionalType::OptionalType( const std::string &theName, const std::string &theLocation )
: OptionalType{ theName, nullptr, theLocation } {
}

OptionalType::OptionalType( const std::string &theName,  const Type  *theType, const std::string &theLocation )
: Container( theName, "OptionalType", theType, "std::optional", theLocation), Template(1) {
}

std::string OptionalType::derivedToString(const std::string &) const {
    return " with type: " + safeToString(itsElementType, "");
}

std::string OptionalType::getItsTypeSpecification() const {
    return itsElementType->getItsTypeSpecification() + "?";
}

Type* OptionalType::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new OptionalType(itsNewName, itsElementType, itsNewLocation );
}

Type * OptionalType::instantiate( const std::vector<const Type *> &theTemplateArguments ) const {
    CHECK_TEMPLATE_ARGC(theTemplateArguments);
    return new OptionalType( itsName, theTemplateArguments[0], itsLocation );
}

//===============================================================================

//===============================================================================

FixedArray::FixedArray( const std::string &theName, const Type *theElementType, std::size_t theSize, const std::string &theLocation )
: Container(theName, "FixedArray", theElementType, "std::array", theLocation ), Template(1), itsSize(theSize) {
    }

Type* FixedArray::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new FixedArray( itsNewName, itsElementType, itsSize, itsNewLocation );
}

std::string FixedArray::derivedToString(const std::string &theIndent) const {
    return util::format(" referring to %lu instances of\n", itsSize ) + theIndent + safeToString(itsElementType, theIndent);
    }

std::string FixedArray::getItsTypeSpecification() const {
    return itsElementType->getItsTypeSpecification() + "[]";
}

Type * FixedArray::instantiate( const std::vector<const Type *> &theTemplateArguments ) const {
    CHECK_TEMPLATE_ARGC(theTemplateArguments);
    return new FixedArray( itsName, theTemplateArguments[0], 0, itsLocation );
}

//===============================================================================

VaryingLengthContainer::VaryingLengthContainer( const std::string &theName, const Type *theElementType, bool isConsecutive_, const std::string &theLocation )
: Container(theName, "VaryingContainer", theElementType, theName, theLocation ), Template(1), isConsecutive(isConsecutive_) {}

Type * VaryingLengthContainer::instantiate( const std::vector<const Type *> &theTemplateArguments ) const {
    CHECK_TEMPLATE_ARGC(theTemplateArguments);
    return new VaryingLengthContainer( itsName, theTemplateArguments[0], isConsecutive, itsLocation );
}

std::string VaryingLengthContainer::derivedToString(const std::string &theIndent) const {
    return " referring to instances of:\n" + theIndent + safeToString(itsElementType, theIndent);
}

std::string VaryingLengthContainer::getItsTypeSpecification() const {
    return itsElementType->getItsTypeSpecification() + "[]";
}


Type* VaryingLengthContainer::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    const auto theClone = new VaryingLengthContainer( itsNewName, itsElementType, isConsecutive, itsNewLocation );
    theClone->itsStlType = itsStlType;
    return theClone;
}

//===============================================================================

MapContainer::MapContainer( const std::string &theName, const Type *theKeyType, const Type *theElementType, const std::string &theLocation )
: Container(theName, "MapContainer", theElementType, theName, theLocation ), Template(2), itsKeyType(theKeyType) {}

Type * MapContainer::instantiate( const std::vector<const Type *> &theTemplateArguments ) const {
    CHECK_TEMPLATE_ARGC(theTemplateArguments);
    return new MapContainer( itsName, theTemplateArguments[0], theTemplateArguments[1], itsLocation);
}

std::string MapContainer::derivedToString(const std::string &theIndent) const {
    return " keyed by " + safeToString(itsKeyType, theIndent + oneIndent) +
            " referring to instances of\n" + theIndent + safeToString(itsElementType, theIndent);
    }

std::string MapContainer::getItsTypeSpecification() const {
    return "( " + itsKeyType->getItsTypeSpecification() + " . " + itsElementType->getItsTypeSpecification() + " )";
}

Type* MapContainer::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    const auto theClone = new MapContainer( itsNewName, itsKeyType, itsElementType, itsNewLocation );
    theClone->itsStlType = itsStlType;
    return theClone;
}
void MapContainer::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter) const {
    Container::getItsReferredTypes(theReferredTypes, theFilter);
    if (itsKeyType) {
        itsKeyType->getItsReferredTypes(theReferredTypes, theFilter);
    }
}

bool MapContainer::isReferred(const Type* theOther) const {
    return Container::isReferred(theOther) || itsKeyType->isReferred(theOther);
}

//===============================================================================


PointerType::PointerType( const std::string theName, const Type *theReferredType, bool isShared_, const std::string &theLocation )
: Type( theName, "PointerType", theLocation), Template(1), itsReferredType(theReferredType), isShared(isShared_) {}

Type * PointerType::instantiate( const std::vector<const Type *> &theTemplateArguments ) const {
    CHECK_TEMPLATE_ARGC(theTemplateArguments);
    return new PointerType( itsName, theTemplateArguments[0], isShared, itsLocation );
}


std::string PointerType::derivedToString(const std::string &theIndent) const {
    return " referring to\n" + theIndent + safeToString(itsReferredType, theIndent);
}

std::string PointerType::getItsTypeSpecification() const {
    return itsReferredType->getItsTypeSpecification() + "*";
}

void PointerType::getItsReferredTypes(Types& theReferredTypes, ReferenceFilter &theFilter) const {
    if (!theFilter.breakAt(this) && (theReferredTypes.find(itsName) == theReferredTypes.end())) {
        if (itsReferredType) {
            itsReferredType->getItsReferredTypes(theReferredTypes, theFilter);
        }
        theReferredTypes.emplace( itsName, this );
    }
}

Type* PointerType::clone(const std::string &itsNewName, const std::string &itsNewLocation) const {
    return new PointerType(itsNewName, itsReferredType, isShared, itsNewLocation );
}

bool PointerType::isReferred(const Type* theOther) const {
    return (dynamic_cast<decltype(this)>(theOther) && (itsName == theOther->itsName)) || itsReferredType->isReferred(theOther);
}


#ifdef BABEL_UNITTEST


class TypesUnitTest : public babel::unittest::UnitTest {
public:
    TypesUnitTest() : UnitTest("Types") {}
protected:
    void run() override {
        testNamespace();
    }

private:
    void testNamespace() {
        enum class TestResult { Success, Failure };

        struct TestCase{ const char *theTest; const char *theCorrectAnswer; TestResult theExpectedOutcome; };

        for (auto theTestCase : { TestCase{"",        "",             TestResult::Success },
                                  TestCase{"::",      "",             TestResult::Success },
                                  TestCase{"a::",     "::a::",        TestResult::Success },
                                  TestCase{"::a",     "::a::",        TestResult::Success },
                                  TestCase{"::::",    "",             TestResult::Failure },
                                  TestCase{"a::b::",  "::a::b::",     TestResult::Success },
                                  TestCase{"a::2::",  "",             TestResult::Failure },
                                  TestCase{"a::b::c", "::a::b::c::",  TestResult::Success } } ) {
            try {
                Namespace theNamespace = StringToNamespace(theTestCase.theTest);

                BABEL_UNITTEST_CHECK( theTestCase.theExpectedOutcome == TestResult::Success );

                BABEL_UNITTEST_CHECK( theTestCase.theCorrectAnswer == NamespaceToString(theNamespace));
            }
            catch (...) {
                BABEL_UNITTEST_CHECK( theTestCase.theExpectedOutcome == TestResult::Failure );
            }
        }
    }
};

TypesUnitTest theTypesUnitTest;

#endif

}


