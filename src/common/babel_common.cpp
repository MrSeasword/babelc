/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Sep 2, 2016
 */
#include "babel_common.h"
#include <unittest/babel_unittest.h>
#include <util/babel_util.h>
#include <util/babel_suppress.h>
#include <algorithm>
#include <random>
#include <limits>
#include <regex>
#include <fstream>
#include <util/babel_logger.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

namespace babel {

    void FilePath::remove() const {
        if (exists()) {
            if (unlink(itsPath.c_str())) {
                throw std::runtime_error("Failed to remove file " + itsPath);
            }
        }
    }

    
    std::time_t FilePath::getModifiedTime() const {
        struct stat theStatus;
        
        if (stat(itsPath.c_str(), &theStatus)) {
            throw std::runtime_error("Cannot stat " + itsPath);
        }
        
        return theStatus.st_mtime;
    }

    bool FilePath::exists() const {
        struct stat theStatus;
        return stat(itsPath.c_str(), &theStatus) == 0;
    }
    
    bool FilePath::isDirectory() const {
        struct stat theStatus;
        return (stat(itsPath.c_str(), &theStatus) == 0) && (theStatus.st_mode & S_IFDIR);
    }
    
void splice( LinesIterator &theBegin, LinesIterator &theEnd, Lines &intoTheLines  ) {
    if (theBegin.itsLines != theEnd.itsLines) {
        throw std::invalid_argument("Cloning iterators from different containers");
    }

    if ((theBegin.itsCurrentColumn < 0) ||
        (theEnd.itsCurrentColumn < 0)) {
        throw std::invalid_argument("Dereferencing invalid iterator in clone");
    }

    if ((theEnd.itsCurrentLine == theBegin.itsCurrentLine) && (theBegin.itsCurrentColumn >= theEnd.itsCurrentColumn)){
        /*Nothing*/ ;
    }
    else if (theBegin.itsCurrentLine == theEnd.itsCurrentLine){
        const auto theSizeToRemove =  (theEnd.itsCurrentColumn-theBegin.itsCurrentColumn);

        intoTheLines.push_back((*theBegin.itsCurrentLine).substr( theBegin.itsCurrentColumn,theSizeToRemove));
        (*theBegin.itsCurrentLine).itsContents.erase(static_cast<size_t>(theBegin.itsCurrentColumn),theSizeToRemove);
        theEnd.itsCurrentColumn -= theSizeToRemove;
    }
    else {
        intoTheLines.push_back( (*theBegin.itsCurrentLine).substr( theBegin.itsCurrentColumn, std::string::npos ) );
        (*theBegin.itsCurrentLine).itsContents.erase(static_cast<size_t>(theBegin.itsCurrentColumn), std::string::npos);

        intoTheLines.splice( intoTheLines.end(), *theBegin.itsLines, std::next(theBegin.itsCurrentLine), theEnd.itsCurrentLine );

        if (theEnd.itsCurrentLine != theEnd.itsLines->end()) {
            intoTheLines.push_back((*theEnd.itsCurrentLine).substr(0, theEnd.itsCurrentColumn ));
            (*theEnd.itsCurrentLine).itsContents.erase(0, theEnd.itsCurrentColumn);
            theEnd.itsCurrentColumn = 0;
        }
    }
    theBegin = theEnd;
}

void dumpLines( const BabelcOptionSet &theOptions, const FilePath &theSource, const char *theExtension, const Lines &theLines ) {

    if (theOptions.find(BabelcOptions::KeepIntermediate) != theOptions.end()) {
        std::ofstream theIntermediateOutput( theSource.replace_extension(theExtension).string(), std::ios_base::out );

        for (auto theLine : theLines) {
            const std::string &theContents = theLine.getItsContents();
            if (theContents.find('"') != std::string::npos) {
                BABEL_LOGF("String found: /%s/", theContents.c_str());
            }
            const std::string itsLocation = theLine.getItsLocation();
            theIntermediateOutput << itsLocation << (itsLocation.empty() ? "" : ": ") << theContents.c_str() << std::endl;
        }
    }

}

#ifdef HIDE_FROM_ECLIPSE_CDT

// Hide this from the Eclipse syntax check since they are flagged as errors but they compile fine.

bool regex_search( const LinesIterator  &theBegin,
                   const LinesIterator  &theEnd,
                   LinesMatcher         &theMatcher,
                   const std::regex       &theRegexp ) {
    return std::regex_search( theBegin, theEnd, theMatcher, theRegexp );
}

bool regex_search( const std::string::const_iterator &theBegin,
                   const std::string::const_iterator &theEnd,
                   std::smatch                       &theMatcher,
                   const std::regex &theRegexp) {
    return std::regex_search( theBegin, theEnd, theMatcher, theRegexp );
}

bool regex_match( const std::string &theString,
                  std::smatch       &theMatcher,
                  const std::regex  &theRegexp ) {
    return std::regex_match( theString, theMatcher, theRegexp );
}

bool regex_match( const LinesIterator &theBegin,
                  const LinesIterator &theEnd,
                  LinesMatcher        &theMatcher,
                  const std::regex      &theRegexp ) {
    return std::regex_match( theBegin, theEnd, theMatcher, theRegexp );
}

bool regex_match( const std::string::const_iterator &theBegin,
                  const std::string::const_iterator &theEnd,
                  std::smatch                       &theMatcher,
                  const std::regex                  &theRegexp ) {
    return std::regex_match( theBegin, theEnd, theMatcher, theRegexp );
}
#endif

#ifdef BABEL_UNITTEST

#ifdef HIDE_FROM_ECLIPSE_CDT

// Hide this from Eclipse since it is marked as unused, but it is really used below and we
// can't compile without it!

static LinesIterator operator+( LinesIterator theLeftOperand, int theRightOperand) {
    std::advance(theLeftOperand, theRightOperand);

    return theLeftOperand;
}
#endif

static std::string makeString( LinesIterator theBegin, LinesIterator theEnd ) {
    LinesPtr theNewLines = std::make_shared<Lines>();

    splice(theBegin, theEnd, *theNewLines );

    return std::string( LinesIterator::begin( theNewLines ), LinesIterator::end( theNewLines ) );
}


class CommonUnitTest : public babel::unittest::UnitTest {
public:
    CommonUnitTest() : UnitTest("Common") {}
protected:
    void run() override {
        testIterators();
        testClone();
        testRegex();
    }

private:
    void testRegex() {
        auto theTestData = std::make_shared<Lines>(std::initializer_list<Lines::value_type>{
            Line( "{" ),
            Line( "   typedef std::shared_ptr<std::int32_t> NisseHult;" ),
            Line( "}" )
        });
        LinesMatcher        theMatcher;
        LinesIterator       theScan               = LinesIterator::begin(theTestData);
        //                               1
        const std::regex theTypeRegexp( "\\b(typedef[[:space:]]+)([^;]+)[[:space:]]NisseHult[[:space:]]*;", std::regex::ECMAScript );

        BABEL_UNITTEST_CHECK(regex_search(theScan, LinesIterator::end( theTestData ) , theMatcher, theTypeRegexp) );
    }

    void testClone() {
        const auto noFileName = std::make_shared<std::string>();

        auto theTestData = std::make_shared<Lines>(std::initializer_list<Lines::value_type>{
            Line( "1234567890", noFileName, 0 ),
            Line( "ABCDEFGHIJ", noFileName, 0 ),
            Line( "abcdefghij", noFileName, 0 )
        });

        {
            const auto theBegin = LinesIterator::begin(theTestData);
            const auto theEnd   = LinesIterator::end(theTestData);

            BABEL_UNITTEST_CHECK(std::string( theBegin, theEnd)                 == "1234567890ABCDEFGHIJabcdefghij");
            BABEL_UNITTEST_CHECK(std::string( theBegin, theBegin)               == "");
            BABEL_UNITTEST_CHECK(std::string( (theBegin + 1), (theBegin +  8) ) == "2345678");
            BABEL_UNITTEST_CHECK(std::string( (theBegin + 9), (theBegin + 11) ) == "0A");
            BABEL_UNITTEST_CHECK(std::string( (theBegin + 9), (theBegin + 21) ) == "0ABCDEFGHIJa");
        }
        {
            {
                auto theOriginal = std::make_shared<Lines>( *theTestData );

                BABEL_UNITTEST_CHECK(makeString( LinesIterator::begin(theOriginal), LinesIterator::end(theOriginal) )                 == "1234567890ABCDEFGHIJabcdefghij");
                BABEL_UNITTEST_CHECK(std::string(LinesIterator::begin(theOriginal), LinesIterator::end(theOriginal))                  == "");
            }

            {
                auto theOriginal = std::make_shared<Lines>( *theTestData );

                BABEL_UNITTEST_CHECK(makeString( LinesIterator::begin(theOriginal), LinesIterator::begin(theOriginal))               == "");
                BABEL_UNITTEST_CHECK(std::string(LinesIterator::begin(theOriginal), LinesIterator::end(theOriginal))                 == "1234567890ABCDEFGHIJabcdefghij");
            }

            {
                auto theOriginal = std::make_shared<Lines>( *theTestData );

                BABEL_UNITTEST_CHECK(makeString( (LinesIterator::begin(theOriginal) + 1), (LinesIterator::begin(theOriginal) +  0) ) == "");
                BABEL_UNITTEST_CHECK(std::string(LinesIterator::begin(theOriginal), LinesIterator::end(theOriginal))                 == "1234567890ABCDEFGHIJabcdefghij");
            }

            {
                auto theOriginal = std::make_shared<Lines>( *theTestData );

                BABEL_UNITTEST_CHECK(makeString( (LinesIterator::begin(theOriginal) + 1), (LinesIterator::begin(theOriginal) +  8) ) == "2345678");
                BABEL_UNITTEST_CHECK(std::string(LinesIterator::begin(theOriginal), LinesIterator::end(theOriginal))                 == "190ABCDEFGHIJabcdefghij");
            }

            {
                auto theOriginal = std::make_shared<Lines>( *theTestData );

                BABEL_UNITTEST_CHECK(makeString( (LinesIterator::begin(theOriginal) + 9), (LinesIterator::begin(theOriginal) + 11) ) == "0A");
                BABEL_UNITTEST_CHECK(std::string(LinesIterator::begin(theOriginal), LinesIterator::end(theOriginal))                 == "123456789BCDEFGHIJabcdefghij");
            }
            {
                auto theOriginal = std::make_shared<Lines>( *theTestData );

                BABEL_UNITTEST_CHECK(makeString( (LinesIterator::begin(theOriginal) + 9), (LinesIterator::begin(theOriginal) + 21) ) == "0ABCDEFGHIJa");
                BABEL_UNITTEST_CHECK(std::string(LinesIterator::begin(theOriginal), LinesIterator::end(theOriginal))                 == "123456789bcdefghij");
            }
        }
    }

    void testIterators() {
        std::srand(19610809u);


        const std::string theTestString = "Nay, if we fight to the end, it can only be glorious!";

        std::vector<std::size_t> theSegmentLengths;

        {
            std::size_t theRemainingLength = theTestString.size();
            while (theRemainingLength > 0) {
                theSegmentLengths.push_back(std::min( theRemainingLength, static_cast<std::size_t>((static_cast<long long>(theTestString.size()/2)*std::rand())/RAND_MAX) ) );
                theRemainingLength -= *std::prev(theSegmentLengths.end());
            }
            theSegmentLengths.push_back(0);
            theSegmentLengths.push_back(0);
        }

        std::sort( BABEL_UTIL_RANGE(theSegmentLengths) );

        do {
            auto theTestStrings = std::make_shared<Lines>();

            std::size_t i = 0;

            for (std::size_t theSegmentLength : theSegmentLengths) {
                theTestStrings->push_back(
                        Line( theTestString.substr(i,theSegmentLength),
                                std::make_shared<std::string>(),
                                0 ));
                i += theSegmentLength;
            }

            testAString( LinesIterator::begin(theTestStrings),
                         LinesIterator::end(theTestStrings),
                         theTestString );
            // Let theTestStrings leak - this is a test program!
        } while(std::next_permutation(BABEL_UTIL_RANGE(theSegmentLengths)));
    }
    void testAString( const LinesIterator &theBegin, const LinesIterator &theEnd, const std::string &theTestString  ) {
        {
            auto theWalker = theBegin;

            for (auto i = theTestString.size(); i != 0; i--) {
                theWalker++;
            }
            BABEL_UNITTEST_CHECK(theWalker == theEnd);
            for (auto i = theTestString.size(); i != 0; i--) {
                theWalker--;
            }
            BABEL_UNITTEST_CHECK(theWalker == theBegin);
            for (auto i = theTestString.size(); i != 0; i--) {
                ++theWalker;
            }
            BABEL_UNITTEST_CHECK(theWalker == theEnd);
            for (auto i = theTestString.size(); i != 0; i--) {
                --theWalker;
            }
            BABEL_UNITTEST_CHECK(theWalker == theBegin);

        }
        BABEL_UNITTEST_CHECK( theTestString == std::string(theBegin, theEnd));
        {
            const std::ptrdiff_t theOffset = 4;
            auto                 theWalker = theBegin;

            for (std::ptrdiff_t i = 0; i < theOffset; i++) {
                if (i % 2) {
                    theWalker--;
                }
                else {
                    --theWalker;
                }
            }
            for (std::ptrdiff_t i = 0; i < theOffset; i++) {
                if (i % 2) {
                    ++theWalker;
                }
                else {
                    theWalker++;
                }
            }
            BABEL_UNITTEST_CHECK( theWalker == theBegin);
        }
        {
            const std::ptrdiff_t theOffset = 4;
            auto                 theWalker = theEnd;

            for (std::ptrdiff_t i = 0; i < theOffset; i++) {
                if (i % 2) {
                    theWalker++;
                }
                else {
                    ++theWalker;
                }
            }
            for (std::ptrdiff_t i = 0; i < theOffset; i++) {
                if (i % 2) {
                    --theWalker;
                }
                else {
                    theWalker--;
                }
            }
            BABEL_UNITTEST_CHECK( theWalker == theEnd);
        }
    }
};

CommonUnitTest theCommonUnitTest;

#endif

}

