/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2016/08/28
 */

#include <iostream>
#include <array>
#include <algorithm>
#include <numeric>
#include <cstring>
#include <string>
#include <cstdio>
#include <type_traits>
#include <map>
#include <regex>
#include <fstream>
#include <stdexcept>
#include <common/babel_common.h>
#include <util/babel_util.h>
#include <util/babel_logger.h>
#include <parser/babel_parser.h>
#include <preprocessor/babel_preprocessor.h>
#include <plugins/babel_plugins.h>
#include <unittest/babel_unittest.h>

namespace babel {

/**
 * The name of the compiler
 */
static const char theCompilerName[] = "babelc";

/**
 * This is the string starting each command line option
 * for this compiler.
 */
#define BABELC_OPTION_MARKER "-babelc"


/**
 * This is the information paired with the option string
 */
struct BabelcOptionInformation {
    BabelcOptions  itsName;
    bool           hasValue;
    const char     *itsDescription;
};

/**
 * The string is the command line option, the second part is the associated information.
 */
typedef std::pair< std::string, BabelcOptionInformation > BabelcOptionConfiguration;

/**
 * Constructor macro for a command line option.
 * The first argument is a value of the type @BabelcOptions.
 * The second argument is a descriptive string. The string could be as long as wanted and
 * should NOT include line breaks, since the usage function will break lines as neccessary.
 */
#define BABELC_OPTION_CONFIGURATION( n, v, description ) BabelcOptionConfiguration{ std::string(BABELC_OPTION_MARKER #n), { babel::BabelcOptions::n, v, description } }

/**
 * The canonical line length!
 */
static constexpr std::size_t THE_COMMAND_LINE_LENGTH = 80;

/**
 * And here we have the options and their descriptions:
 */
const std::map < std::string, BabelcOptionInformation > theBabelcOptions = {
        BABELC_OPTION_CONFIGURATION( KeepIntermediate, false, "Keep intermediate preprocessed header file with new extension \".ii\". "
                                                              "Non-standard name spaces will be saved in files with the extension "
                                                              "\".namespace.<namespace-name>\". Standard namespaces will be saved in "
                                                              "a file with extension \".namespace.0\". " ),
        BABELC_OPTION_CONFIGURATION( Log,              false, "Print internal debug logs to standard output."),
        BABELC_OPTION_CONFIGURATION( Output,           true,  "Specify output generator."),
        BABELC_OPTION_CONFIGURATION( Force,            false, "Force generation of output even if dependencies are older than target output." ),
        BABELC_OPTION_CONFIGURATION( NoColor,          false, "Disable the gcc coloring of diagnostic messages."),
        BABELC_OPTION_CONFIGURATION( Summary,          false, "Print a summary of the findings in the submitted header file on standard output."),
        BABELC_OPTION_CONFIGURATION( UseTabs,          false, "Use hard tabs instead of soft tabs in generated code."),
        BABELC_OPTION_CONFIGURATION( Directory,        true,  "Use this directory as output directory for generated files. If this option is NOT "
                                                              "given, the output file will be put in the same directory as the originating header."),
        BABELC_OPTION_CONFIGURATION( Compiler,         true,  "Specify which GNU g++ compiler to use."),
        BABELC_OPTION_CONFIGURATION( UnitTest,         false, "Run unit tests instead of the compiler proper. "
                                                              "This will not produce any output files. "
                                                              "Test results will appear on standard error and exit code is 0 if all tests pass.")
};


/**
 * @brief Prints out the error (if non-null) and usage information.
 *
 * @param theError A descriptive error string. nullptr means no error.
 */
__attribute__((noreturn))
static void usage(const char *theError) {

    FILE *&theStream = theError ? stderr : stdout;

    (void)fprintf( theStream, "\n" );

    if (theError) {
        (void)fprintf( theStream, "%s: %s\n\n", theCompilerName, theError );
    }

    (void)fprintf( theStream, "Usage: %s <gcc-compiler-options|babelc-options> input-file\n\n", theCompilerName );
    (void)fprintf( theStream, "where babelc options always starts with %s and can be:\n", BABELC_OPTION_MARKER );

    const std::size_t theMaxOptionLength =
            std::max_element(
                    theBabelcOptions.begin(), theBabelcOptions.end(),
                    []( const BabelcOptionConfiguration &theLeft, const BabelcOptionConfiguration &theRight) {
                        return theLeft.first.size() < theRight.first.size();
                    }
            )->first.size();

    for (const BabelcOptionConfiguration &theBabelcOption : theBabelcOptions ) {

        std::smatch        theRegexpMatch;
        const std::regex   theRegexp( util::format(" *(.{1,%lu}[^ ])( |$)", THE_COMMAND_LINE_LENGTH -((theMaxOptionLength+2)+1) ));
        std::string        theDescription(theBabelcOption.second.itsDescription);

        if (theBabelcOption.second.hasValue) {
            theDescription.append( " Values are given as " + theBabelcOption.first + "=<value>, where <value> may be ");
            std::string theValues;

            switch (theBabelcOption.second.itsName) {
            case babel::BabelcOptions::Output:
                
                for (auto theGenerator : babel::plugins::getGenerators(nullptr)) {
                    theValues.append((theValues.empty() ? "" : ", ") + theGenerator->getItsName());
                }
                theValues = "any one of " + theValues;
                break;
            case babel::BabelcOptions::Directory:
                theValues = "either an existing or non-existing directory (in case it will be created)";
                break;
            case babel::BabelcOptions::Compiler:
                theValues = "either an absolute or relative path or pathless name (in which case the environment variable PATH will be used). Default is \"g++\"";
                break;
            default:
                throw std::runtime_error("Internal: Option value documentation mismatch.");
            }
            theDescription.append(theValues);
            theDescription.append(".");
        }

        std::string::const_iterator theMatchStart = theDescription.begin();
        std::string::const_iterator theMatchEnd   = theDescription.end();

        while (regex_search(theMatchStart, theMatchEnd, theRegexpMatch, theRegexp )) {
            (void)fprintf( theStream, "%*s  %.*s\n",
                            -static_cast<int>(theMaxOptionLength),
                            (theMatchStart == theDescription.begin()) ? theBabelcOption.first.c_str() : "",
                            static_cast<int>(theRegexpMatch[1].second-theRegexpMatch[1].first),
                            &*theRegexpMatch[1].first);
            theMatchStart = theRegexpMatch[0].second;
        }

    }

    (void)fprintf(theStream, "\n");

    exit(theError != nullptr);
}

/**
 * This function attempts to return the path of the compiler executable.
 * @param theCompiler typically argv[0]
 * @return The path of the compiler executable or an exception is thrown
 */
static FilePath getCompilerPath( const std::string &theCompiler ) {
    {
        const FilePath theExecutable(theCompiler);

        if (theExecutable.sansDirectory().string() != theExecutable.string()) {
            return theExecutable;
        }
    }
    // Yikes! We'll have to look in the path.

    const std::string thePath = []{ ;
        const char * const theRawPath( std::getenv("PATH") );

        return theRawPath ? theRawPath : "";
    }();

    std::string::size_type theStart = 0;

    for (;;) {
        const auto theEnd = thePath.find(':', theStart );

        const FilePath theExecutable( thePath.substr( theStart, theEnd ) + "/" + theCompiler );

        if (theExecutable.exists()) {
            return theExecutable;
        }

        if (theEnd == std::string::npos) {
            throw std::runtime_error("Failed to determine modified time for compiler '" + theCompiler + "'");
        }
        
        theStart = theEnd + 1;
    } 
}

static auto getGenerators( const std::set<std::string> &theGeneratorNames, 
                           const BabelcOptionSet       &theGivenBabelcOptions, 
                           const FilePath              &theInputFile,
                           const FilePath              &theOutputDirectory,
                           const FilePaths             &theDependentFiles ) {
    auto theResult =  babel::plugins::getGenerators(&theGeneratorNames);
    
    if (theGivenBabelcOptions.find(BabelcOptions::Force) == theGivenBabelcOptions.end()) {
        theResult.remove_if([&theGivenBabelcOptions,&theInputFile,&theOutputDirectory,&theDependentFiles]( plugins::GeneratorInterface *theGenerator){
           return ! theGenerator->needsRealization(theGivenBabelcOptions,theInputFile,theOutputDirectory,theDependentFiles); 
        });
    }
    return theResult;
}

void runUnitTests() {
    const babel::unittest::GrandTotal theGrandTotal = babel::unittest::UnitTest::runAllTests();

    for (auto theUnitTest : theGrandTotal.itsTests) {
        (void)fprintf( stderr, "%s : %s\n",
                        theUnitTest->getItsName().c_str(),
                        theUnitTest->getItsExceptionReason().empty() ?
                                babel::util::format("Succeeded %lu of total %lu tests",
                                                    theUnitTest->getItsTotalNumberOfSucceededTests(),
                                                    theUnitTest->getItsTotalNumberOfTests()).c_str() :
                                babel::util::format("Threw exception '%s'", theUnitTest->getItsExceptionReason().c_str()).c_str());
    }
    if (!theGrandTotal.itsSuccess) {
        throw std::runtime_error("Unit testing failed!");
    }
}

} // namespace babel


/**
 * @brief Main entry point to the babel compiler.
 *
 * @param theNoOfArguments The length of the argument vector, argv
 * @param theArguments The argument vector.
 * @return The exit value for the program. 0 on success and non-zero otherwise
 */
int main( int theNoOfArguments, char *theArguments[] ) {

    const std::array<const char *,3> theHelpOptions = { "--help", "-h", "-?" };

    const std::string theCompiler(theArguments[0]);

    try {
        if (babel::BabelcOptions_LENGTH != babel::theBabelcOptions.size()) {
            throw std::runtime_error("Internal error: The documentation for the options is wrong!");
        }

        if (theNoOfArguments == 1) {
            babel::usage("No input files!");
        }
        else if ((theNoOfArguments == 2) &&
                 (1 == std::count_if(BABEL_UTIL_RANGE(theHelpOptions),
                                    [&theArguments](const char*s){return 0 == std::strcmp(s,theArguments[1]);}))) {
            babel::usage( nullptr);
        }
        else {
            // Sort the arguments to have the babelc options last
            //
            char ** const  theLastArgument       = &theArguments[theNoOfArguments-1];
            char ** const  theArgumentEnd        = &theArguments[theNoOfArguments];
            char ** const  theGccOptionBegin     = &theArguments[1];
            char ** const  theBabelcOptionEnd    = (**theLastArgument == '-') ? theArgumentEnd : theLastArgument;
            char ** const  theGccOptionEnd       = std::remove_if( theGccOptionBegin, theBabelcOptionEnd,
                                                                        []( const char *theOption ){
                                                                            return std::strstr(theOption, BABELC_OPTION_MARKER) == theOption;
                                                                        });
            char ** const &theBabelcOptionBegin  = theGccOptionEnd;


            babel::BabelcOptionSet            theGivenBabelcOptions;
            std::set< std::string >           theGeneratorNames;
            std::unique_ptr<babel::FilePath>  theOutputDirectoryOption;
            std::unique_ptr<std::string>      theGccCompiler;

            for (auto theBabelcOption = theBabelcOptionBegin; theBabelcOption != theBabelcOptionEnd; theBabelcOption++) {
                const std::string theOption( *theBabelcOption );
                const auto        theEqualPosition = theOption.find('=');
                const auto        theConfig        = babel::theBabelcOptions.find(theOption.substr(0, theEqualPosition));

                if (theConfig == babel::theBabelcOptions.end()) {
                    babel::usage(babel::util::format("unknown option '%s'", theOption.c_str()).c_str());
                }
                else if ((theEqualPosition == std::string::npos) && theConfig->second.hasValue) {
                    babel::usage(babel::util::format("Option '%s' must have an associated value.", theOption.c_str()).c_str());
                }
                else if ((theEqualPosition != std::string::npos) && !theConfig->second.hasValue) {
                    babel::usage(babel::util::format("Option '%s' must not have an associated value.", theOption.c_str()).c_str());
                }
                else if (theConfig->second.hasValue) {
                    switch (theConfig->second.itsName) {
                    case babel::BabelcOptions::Output:
                        theGeneratorNames.insert(theOption.substr((theEqualPosition+1)));
                        break;
                    case babel::BabelcOptions::Directory:
                        theOutputDirectoryOption = std::make_unique<babel::FilePath>(theOption.substr(theEqualPosition+1));
                        break;
                    case babel::BabelcOptions::Compiler:
                        theGccCompiler = std::make_unique<std::string>(theOption.substr(theEqualPosition+1));
                        break;
                    default:
                        throw std::runtime_error("Internal: Options parsing incorrect.");
                    }
                }
                else {
                    theGivenBabelcOptions.insert(theConfig->second.itsName);
                }
            }

            if (theGivenBabelcOptions.find(babel::BabelcOptions::Log) != theGivenBabelcOptions.end()) {
                babel::logger::enable();
            }
            if (theGivenBabelcOptions.find(babel::BabelcOptions::UnitTest) != theGivenBabelcOptions.end()) {
                BABEL_LOG_SCOPE();
                BABEL_LOG("Starting unit tests...");
                babel::runUnitTests();
            }
            else {
                const std::vector<std::string> acceptedExensions{ ".h", ".hpp", ".hxx", ".H" };
                const babel::FilePath  theInputFile( *theLastArgument );
                
                if ( std::find(BABEL_UTIL_RANGE(acceptedExensions), theInputFile.extension()) == std::end(acceptedExensions)) {
                    babel::usage( ("Last argument must be a header file with one of the extensions " +  
                                    std::accumulate((std::begin(acceptedExensions)+1), std::end(acceptedExensions), 
                                                     acceptedExensions.at(0),
                                                     []( const std::string &left, const std::string &right ){
                                                        return left + ", " + right; 
                                                     }) + 
                                    ": " + theInputFile.string()).c_str());
                }

                std::vector< char *> theGivenGccOptions( theGccOptionBegin, theGccOptionEnd );

                if (theGivenBabelcOptions.find(babel::BabelcOptions::NoColor) != theGivenBabelcOptions.end()) {
                    theGivenGccOptions.push_back( strdup("-fdiagnostics-color=never") );
                }

		{
		  bool hasStandard = false;
            
		  for (const auto theOption : theGivenGccOptions ) {
		    if ((strstr(theOption, "-std=c++") == theOption) || (strstr(theOption, "-std=gnu++") == theOption)) {
		      hasStandard = true;
		      break;
		    }
		  }
            
		  if (!hasStandard) {
		    theGivenGccOptions.push_back( strdup("-std=c++14") ); // At least!
		  }
		}


                if (!theGccCompiler) {
                    theGccCompiler = std::make_unique<std::string>("g++");
                }
                auto theResult = babel::preprocessor::preProcessFile( theInputFile, 
                                                                     *theGccCompiler, 
                                                                      theGivenGccOptions, 
                                                                      theGivenBabelcOptions );
                
                theResult.theDependentFiles.emplace_back(babel::getCompilerPath(theCompiler));

                const auto theOutputDirectory = theOutputDirectoryOption ? *theOutputDirectoryOption : theInputFile.directoryPart();
                const auto theGenerators      = babel::getGenerators(  theGeneratorNames, 
                                                                       theGivenBabelcOptions,
                                                                       theInputFile,
                                                                       theOutputDirectory,
                                                                       theResult.theDependentFiles );

                if ( ! theGenerators.empty()) {
                    auto const theCompilationUnit = babel::parser::parse(
                            theInputFile,
                            theResult.thePreProcessedLines,
                            *theGccCompiler,
                            theGivenGccOptions,
                            theGivenBabelcOptions,
                            theResult.theDependentFiles 
                    );

                    for (auto theGenerator : theGenerators) {
                        theGenerator->realize( theCompilationUnit, 
                                               theGivenBabelcOptions, 
                                               theOutputDirectory );
                    }
                }
            }
        }
    }
    catch (const std::exception &theException) {
        (void)fprintf( stderr, "%s: %s\n", babel::theCompilerName, theException.what());
        exit(1);
    }
    return 0;
}
