/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2016/08/28
 */
#ifndef PREPROCESSOR_BABEL_PREPROCESSOR_H_
#define PREPROCESSOR_BABEL_PREPROCESSOR_H_

#include <vector>
#include <common/babel_common.h>

namespace babel { namespace preprocessor {

struct Result {
    LinesPtr  thePreProcessedLines;
    FilePaths theDependentFiles;
};
/**
 * @brief Preprocessed the passed input file and returns the resulting lines
 * or throws an exception if not successful.
 */
Result preProcessFile( const FilePath                &theInputFile,
                       const std::string             &theCompiler,
                       const std::vector< char *>    &theGccOptions,
                       const BabelcOptionSet         &theBabelcOptions );


}}


#endif /* PREPROCESSOR_BABEL_PREPROCESSOR_H_ */
