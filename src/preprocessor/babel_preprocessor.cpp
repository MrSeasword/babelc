/*
 * Copyright (C) 2016 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2016/08/28
 */

#include "babel_preprocessor.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <set>
#include <util/babel_util.h>
#include <util/babel_logger.h>
#include <common/babel_common.h>

namespace babel { namespace preprocessor {

struct SubString {

    SubString( std::string::const_iterator theBegin, std::string::const_iterator theEnd )
        :itsBegin(theBegin),itsEnd(theEnd) {
    }
    bool operator==( const SubString &theOther ) const {
        return std::equal(itsBegin, itsEnd, theOther.itsBegin, theOther.itsEnd );
    }
    bool operator <( const SubString &theOther) const {
        return std::lexicographical_compare(itsBegin, itsEnd, theOther.itsBegin, theOther.itsEnd);
    }
    std::string toString() const { return std::string(itsBegin,itsEnd); }

private:
    std::string::const_iterator itsBegin;
    std::string::const_iterator itsEnd;
};

void saveFileName( const std::string &thePreprocessedLine, std::set<SubString> &theFileNames ) {
    std::string::const_iterator theIter = thePreprocessedLine.begin();
    
    if ((thePreprocessedLine.size() > 5) && (*theIter++ == '#') && (*theIter++ == ' ')) {
        
        do {
            if ((*theIter < '0') || ('9' < *theIter))   return;
            if (++theIter == thePreprocessedLine.end()) return;
        } while (*theIter != ' ');
        
        if ((++theIter == thePreprocessedLine.end()) || (*theIter++ != '"')) return;
        
        const std::string::const_iterator theBegin = theIter;
        
        for (;;) {
            if (theIter == thePreprocessedLine.end()) return;
            if (*theIter == '"') break;
            theIter++;
        }
    
        theFileNames.emplace(theBegin,theIter);
    }
}
extern Result preProcessFile( const FilePath              &theInputFile,
                                const std::string           &theCompiler,
                                const std::vector< char *>  &theGccOptions,
                                const BabelcOptionSet       &theBabelcOptions ) {
    int  thePipe[2];

    if (pipe(thePipe) != 0) {
        throw std::runtime_error("Failed to create pipe to preprocessor!");
    }

    const int &thePipeReadEnd  = thePipe[0];
    const int &thePipeWriteEnd = thePipe[1];

    const pid_t thePid = fork();

    if (thePid < 0) {
        throw std::runtime_error("Failed to fork to preprocessor!");
    }
    else if (thePid == 0) {
        // This is the child process.
        //
        if (close( thePipeReadEnd ) || (dup2( thePipeWriteEnd, STDOUT_FILENO ) < 0)) {
            throw std::runtime_error("Failed to redirect standard out!");
        }

        // Build an argument vector for gcc. The strdup's aren't leaks since we will exec.
        //
        auto theArgumentVector( theGccOptions );

        util::push_front( theArgumentVector, strdup( theCompiler.c_str() ) );

        theArgumentVector.push_back( strdup("-D__BABELC__") );
        theArgumentVector.push_back( strdup("-D__BABELC_VERSION_=1") );
        theArgumentVector.push_back( strdup("-E") );
        theArgumentVector.push_back( strdup(theInputFile.string().c_str()));
        theArgumentVector.push_back( nullptr );

        (void)execvp(theArgumentVector[0], &theArgumentVector[0]);

        throw std::runtime_error("Failed to exec preprocessor!"); // Otherwise we wouldn't be here!
    }
    else {
        (void)close(thePipeWriteEnd);

        const LinesPtr theLines(new Lines());
        std::set<SubString>     theFilenames;
        {
            FILE * const            thePreprocessorOutput = fdopen(thePipeReadEnd, "r");
            char                   *theLineBufferPtr      = nullptr;
            std::size_t             theLineBufferSize     = 0;
            ssize_t                 theLineLength;
            
            while (0 < (theLineLength = getline(&theLineBufferPtr, &theLineBufferSize, thePreprocessorOutput))) {
                theLines->push_back(Line( std::string(
                                          theLineBufferPtr,
                                          theLineBufferPtr + (theLineLength - ((theLineBufferPtr[theLineLength - 1] == '\n') ? 1 : 0) ))));
                saveFileName(theLines->back().getItsContents(),theFilenames);
            }

            fclose(thePreprocessorOutput);

            free(theLineBufferPtr);
        }
        {
            int theExitStatus = 0;

            const pid_t theDeceasedPreProcessor = waitpid( thePid, &theExitStatus, 0 );

            if ((theDeceasedPreProcessor != thePid) || !WIFEXITED(theExitStatus) || WEXITSTATUS(theExitStatus)) {
                throw std::runtime_error("The preprocessing failed!");
            }
        }

        dumpLines(theBabelcOptions, theInputFile, ".ii", *theLines );
        
        Result theResult;
        
        theResult.thePreProcessedLines = theLines;
        
        for (auto theFilename : theFilenames) {
            FilePath theFile( theFilename.toString() );
            
            if (!theFile.exists()) {
                BABEL_LOGF("%s does not exist and will not participate in dependency check", theFile.string().c_str());
            }
            else if (theFile.isDirectory()) {
                BABEL_LOGF("%s is a directory and will not participate in dependency check", theFile.string().c_str());
            }
            else {
                theResult.theDependentFiles.emplace_back(theFile);
            }
        }
        return theResult;
    }
}


}}
