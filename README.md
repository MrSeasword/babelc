# README #

### What is this repository for? ###

This repository contains the source code for babelc, a tool for serializing and communicating C++ data structures to persistent storage or via IPC (e.g. d-bus).
A brief introduction can be read at http://www.babelc.com .

### How do I get set up? ###

See User's Guide in pdf format.

### Contribution guidelines ###

Contributions are welcome.

### Who do I talk to? ###

mr@seasword.com