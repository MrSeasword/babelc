#ifndef SERVER_H__
#define SERVER_H__

#include <cstdint>

namespace server {
  enum class FD : std::int32_t  [[babelc::ipc_type("FileDescriptor")]] {};

  using FileDescriptor = FD [[babelc::ipc_type("FileDescriptor")]];

  struct Server {
    virtual FileDescriptor doIt( FileDescriptor theFileDescriptor ) = 0;
    virtual ~Server() {}
  };
   
}

#endif
