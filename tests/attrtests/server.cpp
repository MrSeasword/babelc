#include "./babel/server.ipc.h"
#include <sdbus-policy.h>
#include <memory>

struct Server : server::Server {
  server::FileDescriptor doIt( server::FileDescriptor theFileDescriptor ) override final {
      return theFileDescriptor;
   }
};

int main() {
   const auto thePolicy = babel::sdbus::Policy::createPolicy( 
                               babel::sdbus::Policy::Sdbus::Session,
                               "com.seasword.server",
                               [](const std::weak_ptr<babel::sdbus::Policy>&){});


    const auto theServer = server::registerServerImplementation(
                               thePolicy,
                               "/theServer",
                               std::make_shared<Server>());
    
    thePolicy->runMainLoop();

   return 0;
}

