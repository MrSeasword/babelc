/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2017/06/01
 */
#include <iostream>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <sstream>
#include <limits>

#include "generated/streamtest.stream.h"

#include "gtest/gtest.h"

TEST (TestStruct, StreamTest) {
  using namespace stream::test;

  const Struct theOriginal = {
      1
     ,2
     ,static_cast<Byte>(3)
     ,true
     ,Enum::Second
     ,"A \"strange\" one\n"
     ,std::make_shared<stream::test::SubStruct>(stream::test::SubStruct{
           "A \\t is equal to \t.",
           std::numeric_limits<std::uint64_t>::max()
       }
      )
     ,stream::test::SubStructUPtr()
     ,{ 1, 2, 3, 4, 5 }
     ,{ "apa", "bepa", "cepa" }
     ,{}
#ifdef HAS_BOOST_VARIANT
     ,{ static_cast<std::int32_t>(42) }
#endif
#ifdef HAS_BOOST_OPTIONAL
      ,{}
#endif
  };

  {
     std::stringstream ss;


     EXPECT_TRUE( (ss << theOriginal) );
     {
        Struct theRevived;
        
        EXPECT_TRUE( (ss >> theRevived) );
        EXPECT_EQ( theRevived, theOriginal ); 
     }
  }
  
}


TEST (TestStruct, FormattedStreamTest) {
  using namespace stream::test;

  const Struct theOriginal = {
       std::numeric_limits<decltype(theOriginal.itsUint32)>::max()
      ,std::numeric_limits<decltype(theOriginal.itsUint8)>::max()
      ,static_cast<Byte>(std::numeric_limits<std::underlying_type_t<decltype(theOriginal.itsByte)>>::min())
      ,true
      ,Enum::Third
      ,""
      ,stream::test::SubStructPtr()
      ,std::make_unique<stream::test::SubStruct>( stream::test::SubStruct{
           "A normal string",
           12345678
        }
      )
      ,{ 5, 4, 3, 2, 1 }
      ,{ "one" }
      ,{
        std::make_pair(1,stream::test::SubStruct{ "The First",  2786 } ),
        std::make_pair(2,stream::test::SubStruct{ "The Second",  762 } ),
        std::make_pair(3,stream::test::SubStruct{ "The Third",   828 } )
       }
#ifdef HAS_BOOST_VARIANT
     ,{ "This is the string variant" }
#endif
#ifdef HAS_BOOST_OPTIONAL
       ,stream::test::MaybeInt(6108096758)
#endif
  };

  {
     std::stringstream ss;


     EXPECT_TRUE( stream::test::appendValue( ss, theOriginal, babel::stream::Indent(1) ) );
     {
        Struct theRevived;
        
        EXPECT_TRUE( (ss >> theRevived) );
        EXPECT_EQ( theRevived, theOriginal ); 
     }
  }
  
}


TEST (TestStruct, BadStreamTest) {
  using namespace stream::test;

  const Struct theOriginal = {
       std::numeric_limits<decltype(theOriginal.itsUint32)>::min()
      ,std::numeric_limits<decltype(theOriginal.itsUint8)>::min()
      ,static_cast<Byte>(std::numeric_limits<std::underlying_type_t<decltype(theOriginal.itsByte)>>::max())
      ,false
      ,Enum::First
      ,"Nisse Hult"
      ,std::make_shared<stream::test::SubStruct>(stream::test::SubStruct{
        "Was here",
        std::numeric_limits<std::uint64_t>::max()/3
       }
      )
      ,std::make_unique<stream::test::SubStruct>( stream::test::SubStruct{
           "and his brother too",
           987654321
        }
       )
      ,{ 0, 0xFFFF, 0xAAAA, 1234, 999 }
      ,{}
      ,{
          std::make_pair(23,stream::test::SubStruct{ "Anton",  0 } ),
          std::make_pair(67,stream::test::SubStruct{ "Bruno",  1 } ),
          std::make_pair(-1,stream::test::SubStruct{ "Caesar", 2 } ),
          std::make_pair(-1,stream::test::SubStruct{ "Dora",   3 } )
       }
#ifdef HAS_BOOST_VARIANT
       ,Multiple(Enum::Third)
#endif
#ifdef HAS_BOOST_OPTIONAL
       ,stream::test::MaybeInt(-3)
#endif
       
  };

  {
     std::stringstream ss;


     EXPECT_TRUE( stream::test::appendValue( ss, theOriginal, babel::stream::Indent(1) ) );

     const auto theString = ss.str();
     bool insideString = false;
     
     for (std::size_t i = 0; i < theString.size(); i++) {
        if (theString[i] == '"') {
           insideString = !insideString;
        }
        
        std::string theBadOne( theString );

        theBadOne[i] = insideString ? '\\' : 0x07;

        
        std::stringstream theBadStream( theBadOne );
        
        Struct theRevived;

        EXPECT_FALSE( (theBadStream >> theRevived) );
     }
  }
  
}

int main( int argc, char **argv ) {
  ::testing::InitGoogleTest( &argc, argv );

  return RUN_ALL_TESTS();
}
