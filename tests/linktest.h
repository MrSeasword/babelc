/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2017/09/14
 */

#ifndef __LINKTEST_H__
#define __LINKTEST_H__

namespace linktest {
   void dragItIn();
}

#endif
