/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2019/03/09
 */
#ifndef EXCEPTIONTEST_H_
#define EXCEPTIONTEST_H_

#include <cstdint>
#include <array>

namespace babelc { namespace tests { 


    enum class Exception : std::uint8_t {
        Runtime,
        BadAlloc,
        Overflow,
        Underflow,
        InvalidArgument,
        DomainError,
        LengthError,
        OutOfRange,
        RangeError,
        LogicError
    };

#ifndef __BABELC__

  constexpr std::array<Exception, 10> EXCEPTION_VALUES = {
    Exception::Runtime,
    Exception::BadAlloc,
    Exception::Overflow,
    Exception::Underflow,
    Exception::InvalidArgument,
    Exception::DomainError,
    Exception::LengthError,
    Exception::OutOfRange,
    Exception::RangeError,
    Exception::LogicError
  };

#endif

    struct ExceptionTest {
      virtual void throwIt( Exception theException ) = 0;
      virtual void exit() = 0;
      virtual ~ExceptionTest() {}
    };
}}
#endif
