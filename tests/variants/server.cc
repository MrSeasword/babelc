#include <iostream>
#include <sstream>

#include "interface.ipc.h"
#include "interface.stream.h"
#include <sdbus-policy.h>

namespace Impl {
  struct Server : public Test::Interface {
     std::string add( const Test::Contact &theContact ) override {
        using namespace Test;

        std::ostringstream theOutput;

        appendValue( theOutput, theContact, babel::stream::Indent(1) );
	appendValue( std::cout, theContact, babel::stream::Indent(1) ) << std::endl;
        return theOutput.str();
     }
  };
}

int main() {
    const auto thePolicy = babel::sdbus::Policy::createPolicy( 
                               babel::sdbus::Policy::Sdbus::Session,
                               "com.seasword.Test",
                               [](const std::weak_ptr<babel::sdbus::Policy>&){});

    const auto theImplementation = 
      Test::registerInterfaceImplementation(
					    thePolicy,
					    "/contents",
					    std::make_shared<Impl::Server>() );

    thePolicy->runMainLoop();
    
    return 0;
}
