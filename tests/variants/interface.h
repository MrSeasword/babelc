#ifndef _INTERFACE_H_
#define _INTERFACE_H_

#include <boost/optional.hpp>
#include <boost/optional/optional_io.hpp>
#include <boost/variant.hpp>
#include <vector>
#include <string>
#include <cstdint>

namespace Test {
   using CountryCode = boost::optional< std::uint8_t >;
   
   struct PhoneNumber {
      CountryCode   itsCountryCode;
      std::uint64_t itsNumber;
   };
   struct Email {
      std::string   itsAddress;
   };

   using ContactDetail = boost::variant<PhoneNumber,Email>;

   struct Contact {
      std::string   itsName;
      ContactDetail itsContactDetail;
   };
   
   struct Interface {
      virtual std::string add( const Contact &theContact ) = 0;
      virtual ~Interface() {}
  };

   #ifndef __BABELC__

   bool operator==( const PhoneNumber &theLeft, const PhoneNumber &theRight ) {
      return (theLeft.itsCountryCode == theRight.itsCountryCode) &&
         (theLeft.itsNumber == theRight.itsNumber);
   }
   
   bool operator==( const Email &theLeft, const Email &theRight ) {
      return theLeft.itsAddress == theRight.itsAddress;
   }
   
   bool operator==( const Contact &theLeft, const Contact &theRight ) {
      return (theLeft.itsName == theRight.itsName) &&
         (theLeft.itsContactDetail == theRight.itsContactDetail);
   }
   #endif
}

#endif
