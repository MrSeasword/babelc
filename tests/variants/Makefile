#
# Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
#
# This file is part of babelc.
#
# babelc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# babelc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with babelc.  If not, see <http://www.gnu.org/licenses/>.
#
#  Created on: 2017/05/14
#

SOURCES   	:= 	client.cc \
			server.cc
BABELCHEADERS	:= interface.h

OBJECTS   	:= $(SOURCES:%.cc=%.o)
DEPENDS   	:= $(SOURCES:%.cc=%.d) $(BABELCHEADERS:%.h=%.dep.h)
PROGRAMS 	:= $(SOURCES:%.cc=%)

BABELCROOT	:= ../..
RSWDLROOT	:= ../../../rswdl-controller

CPPFLAGS := 	-I. 						\
		-I$(BABELCROOT)/policies			\
		-I$(RSWDLROOT)/include/ipc			\
		-D__BABEL_SDBUS_DEFAULT_REPORTING__		\
		-Wall -Wextra 					\
		-std=c++14 -g -fdiagnostics-color=never

BABELC		:= $(BABELCROOT)/babelc

ifneq ($(VERBOSE),)
BABELCLOG=-babelcLog
endif

.PHONY: FORCE

%.ipc.h:$(RSWDLROOT)/include/ipc/%.h
	if [ ! -d $(@D) ] ; then mkdir $(@D); fi
	$(BABELC) $(CPPFLAGS) $(BABELCLOG) -babelcOutput=Dep -babelcOutput=Ipc -babelcDirectory=. $(<)

%.dep.h:%.h
	if [ ! -d $(@D) ] ; then mkdir $(@D); fi
	$(BABELC) $(CPPFLAGS) $(BABELCLOG) -babelcOutput=Dep $(<)

%.ipc.h:%.h
	if [ ! -d $(@D) ] ; then mkdir $(@D); fi
	$(BABELC) $(CPPFLAGS) $(BABELCLOG) -babelcOutput=Dep -babelcOutput=Ipc $(<)

%.stream.h:%.h
	if [ ! -d $(@D) ] ; then mkdir $(@D); fi
	$(BABELC) $(CPPFLAGS) $(BABELCLOG) -babelcOutput=Dep -babelcOutput=Stream $(<)

%.d:%.cc
	g++  $(CPPFLAGS) -MM -MG $(<) | sed -e 's@^\(.*:\)@$(@D)/\1@' > $(@)


%.i:%.cc FORCE
	g++ -g -c -E -o $(@) $(CPPFLAGS) $(<)

%.o:%.cc
	g++ -g -c -o $(@) $(CPPFLAGS) $(<)

all: $(PROGRAMS)

%: %.o
	g++ -o $(@) -g $(CPPFLAGS) $(<) -lpthread `pkg-config --libs libsystemd` 

googletest/googletest/src/gtest-all.cc:
	git clone --quiet 'https://github.com/google/googletest.git'

$(BABELC): FORCE
	if [ ! -d $(@D) ] ; then mkdir $(@D) ; fi ;	\
	cd $(@D) ; 					\
	cmake . ; 					\
	make 


clean:
	rm -rf $(PROGRAMS) *.o *.d *.ipc.h *.stream.h *.dep.h *~ */*~


FORCE:

.PRECIOUS: %.test.h %.ipc.h

$(OBJECTS) $(DEPENDS) $(PROGRAM): Makefile $(BABELC)


ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),distclean)
-include $(DEPENDS)
endif
endif
