#include <iostream>
#include <sstream>
#include "interface.ipc.h"
#include "interface.stream.h"
#include <sdbus-policy.h>

template <typename ServerPtr >
void runTest( ServerPtr theServer, const Test::Contact &theContact ) {
   using namespace Test;
   
   const auto         theReplyString = theServer->add( theContact );
   std::istringstream theInput( theReplyString );
   Test::Contact      theReply;

   if (theInput >> theReply) {
      if (!(theReply == theContact)) {
         std::cerr << "Object\n  " << theContact <<
            "\nwhich was returned as\n  " << theReplyString <<
            "\nwas resurrected as\n  " << theReply << "\n";
      }
   }
   else {
      std::cerr << "Failed to read back\n  " << theContact << "\nwhich was returned as\n  " << theReplyString << "\n";
   }

   
}

int main() {
   const auto thePolicy = babel::sdbus::Policy::createPolicy( 
                                                             babel::sdbus::Policy::Sdbus::Session,
                                                             "does.not.matter.here",
                                                             [](const std::weak_ptr<babel::sdbus::Policy>&){});

   const auto theServer = Test::lookupInterfaceImplementation(
                                                              thePolicy,
                                                              "com.seasword.Test:/contents" );


   runTest( theServer, Test::Contact{ "One", Test::ContactDetail{ Test::PhoneNumber{Test::CountryCode{46}, 708140598 }}} );
   runTest( theServer, Test::Contact{ "Two", Test::ContactDetail{ Test::PhoneNumber{Test::CountryCode{}, 708350898 }}} );
   runTest( theServer, Test::Contact{ "Three", Test::ContactDetail{ Test::Email{"mr@seasword.com"}}} );

   return 0;
}
