/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2017/10/30
 */

#ifndef JSONTEST_H_
#define JSONTEST_H_

#include <cstdint>
#include <string>
#include <memory>
#include <array>
#include <vector>
#include <list>
#include <map>

#ifdef __has_include
       #if __has_include(<boost/variant.hpp>)
           #include <boost/variant.hpp>
           #define HAS_BOOST_VARIANT
       #endif
       #if __has_include(<variant>)
           #include <variant>
           #define HAS_STD_VARIANT
       #endif
       #if __has_include(<boost/optional.hpp>)
           #include <boost/optional.hpp>
           #define HAS_BOOST_OPTIONAL
       #endif
       #if __has_include(<optional>)
           #include <variant>
           #define HAS_STD_OPTIONAL
       #endif
#endif

namespace json { namespace test {

    enum struct Enum : std::uint8_t {
      First,
      Second,
      Third
    };

    enum struct Byte : std::int8_t {};
    
    struct SubStruct {
       std::string   itsString;
       std::uint64_t itsUint64;
    };

    using SubStructPtr  = std::shared_ptr<SubStruct>;
    using SubStructUPtr = std::unique_ptr<SubStruct>;
    using ByteArray     = std::array< std::uint8_t, 8 >;
    using Uint16Array   = std::array< std::uint16_t, 5 >;
    using StringList    = std::list< std::string >;
    using Map           = std::map< std::int32_t, SubStruct >;
    using DoubleVector  = std::vector< double >;

    #ifdef HAS_BOOST_VARIANT
    using Multiple = boost::variant< std::int32_t, std::string > [[babelc::intentionally_incomplete_variant]];
    #endif

    #ifdef HAS_BOOST_OPTIONAL
    using MaybeInt = boost::optional< std::int32_t >;
    //enum struct MaybeInt : std::int32_t {};
    #endif
      
    struct Struct {
       std::uint32_t itsUint32    = 0;
       std::uint8_t  itsUint8     = 0;
       Byte          itsByte      = static_cast<Byte>(0);
       bool          itsBool      = false;
       Enum          itsEnum      = Enum::First;
       std::string   itsString    = "";
       SubStructPtr  itsSubStruct;
       SubStructUPtr itsSubStructU;
       ByteArray     itsByteArray;
       Uint16Array   itsUint16Array;
       StringList    itsStringList;
       Map           itsMap;
       #ifdef HAS_BOOST_VARIANT
       Multiple      itsMultiple;
       #endif
       #ifdef HAS_BOOST_OPTIONAL
       MaybeInt      itsMaybeInt;
       #endif
    };


#ifndef __BABELC__
#define FIELD_IS_EQUAL( theField ) (theLeft.theField == theRight.theField)
#define PTR_FIELD_IS_EQUAL( theField ) ((!theLeft.theField && !theRight.theField) || (theLeft.theField && theRight.theField && (*theLeft.theField == *theRight.theField)))
      
    bool operator==( const json::test::SubStruct &theLeft, const json::test::SubStruct &theRight ) {
       return
          FIELD_IS_EQUAL( itsString ) &&
          FIELD_IS_EQUAL( itsUint64 );
    }

    bool operator!=( const json::test::SubStruct &theLeft, const json::test::SubStruct &theRight ) {
       return !(theLeft == theRight);
    }

    bool operator==( const json::test::Struct &theLeft, const json::test::Struct &theRight ) {
       return
             FIELD_IS_EQUAL    ( itsUint32 )     
          && FIELD_IS_EQUAL    ( itsUint8 )
          && FIELD_IS_EQUAL    ( itsByte )
          && FIELD_IS_EQUAL    ( itsBool )
          && FIELD_IS_EQUAL    ( itsEnum )
          && PTR_FIELD_IS_EQUAL( itsSubStruct )
          && PTR_FIELD_IS_EQUAL( itsSubStructU )
          && FIELD_IS_EQUAL    ( itsByteArray )
          && FIELD_IS_EQUAL    ( itsUint16Array )
          && FIELD_IS_EQUAL    ( itsStringList )
          && FIELD_IS_EQUAL    ( itsMap )
#ifdef HAS_BOOST_VARIANT
          && FIELD_IS_EQUAL( itsMultiple )
#endif
#ifdef HAS_BOOST_OPTIONAL
          && FIELD_IS_EQUAL( itsMaybeInt )
#endif
          ;
    }

    bool operator !=( const json::test::Struct &theLeft, const json::test::Struct &theRight ) {
       return !(theLeft == theRight);
    }

#undef FIELD_IS_EQUAL
#undef PTR_FIELD_IS_EQUAL
      
#endif
}}


#endif
