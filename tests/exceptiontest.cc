/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2019/03/09
 */
#include "gtest/gtest.h"

#include <iostream>
#include <algorithm>
#include <iterator>

#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>

#include "generated/exceptiontest.ipc.h"
#include "../policies/sdbus-policy.h"

using namespace babelc::tests;

#define theServerName "babelc.test.thrower"

struct Thrower : public ExceptionTest {
  std::shared_ptr<babel::sdbus::Policy> itsPolicy;

  explicit Thrower( std::shared_ptr<babel::sdbus::Policy> thePolicy ) {
    itsPolicy = thePolicy;
  }

  void exit() override {
    itsPolicy->stopMainLoop();
    itsPolicy.reset();
  }

  void throwIt( Exception theException ) override {
    char theNumber[2]{};

    theNumber[0] = '0' + static_cast<int>(theException);

    switch (theException) {
    case Exception::DomainError: {
      throw std::domain_error( theNumber );
    }
    case Exception::InvalidArgument: {
      throw std::invalid_argument( theNumber );
    }
    case Exception::LengthError: {
      throw std::length_error( theNumber );
    }
    case Exception::OutOfRange: {
      throw std::out_of_range( theNumber );
    }
    case Exception::Overflow: {
      throw std::overflow_error( theNumber );
    }
    case Exception::Underflow: {
      throw std::underflow_error( theNumber );
    }
    case Exception::Runtime: {
      throw std::runtime_error( theNumber );
    }
    case Exception::LogicError: {
      throw std::logic_error( theNumber );
    }
    case Exception::BadAlloc: {
      throw std::bad_alloc();
    }
    case Exception::RangeError: {
      throw std::range_error(theNumber);
    }
  }
  }

};


//========================================================================
// The greeting message to synch server and client.
//
static const char theServerGreeting[] = "SERVER STARTED\n";


void runServer() {
    const auto thePolicy = babel::sdbus::Policy::createPolicy(
			            babel::sdbus::Policy::Sdbus::Session,
				    theServerName,
				    [](const std::weak_ptr<babel::sdbus::Policy> &){} );

    const auto theThrower = registerExceptionTestImplementation
      (
       thePolicy,
       "/nisse/hult",
       std::make_shared<Thrower>(thePolicy)
      ); 
     

    // Do a loop of processing to register the servers before
    // acknowledging on standard out.
    //
    thePolicy->processEvent();

    std::cout << theServerGreeting << std::flush;

    thePolicy->runMainLoop();

    std::cerr << "SERVER STOPPED" << std::endl;
}


//========================================================================
// A helper class to fork, kill and wait for a child process
//
struct ServerProcess {
  static const char *argv0;

  ServerProcess() 
    : itsPid(0) {

    int thePipe[2];

    if (pipe(thePipe) != 0) {
      std::cerr << "Failed to create pipe: " << strerror(errno) << std::endl;
      exit(1);
    }

    const int &thePipeReadEnd  = thePipe[0];
    const int &thePipeWriteEnd = thePipe[1];

    itsPid = fork();

    if (itsPid < 0) {
      std::cerr << "Failed to fork: " << strerror(errno) << std::endl;
      exit(1);
    }
    else if (itsPid > 0) {
      // The parent; wait for start OK
      
      (void)close(thePipeWriteEnd);

      FILE * const  theServerOutput = fdopen(thePipeReadEnd, "r");
      char         *theLineBufferPtr      = nullptr;
      std::size_t   theLineBufferSize     = 0;
      ssize_t       theLineLength;

      while ((0 < (theLineLength = getline(&theLineBufferPtr, &theLineBufferSize, theServerOutput))) &&
	     (strcmp( theServerGreeting, theLineBufferPtr ) != 0)) {
      }

      fclose(theServerOutput);
      free(theLineBufferPtr);
    }
    else {
      if (close(thePipeReadEnd) || (dup2( thePipeWriteEnd, STDOUT_FILENO) < 0)) {
	std::cerr << "Setting up output failed: " << strerror(errno) << std::endl;
	exit(1);
      }

      char *argv[] = { strdup(argv0), 
		       strdup("-server"),
		       static_cast<char*>(nullptr) };
      
      execvp( argv[0], argv );

      std::cerr << "Failed to exec " << argv[0] << ": " << strerror(errno) <<
	std::endl;
      
    }
  }
  ~ServerProcess() {
    if (itsPid > 0) {
      if (kill(itsPid, SIGTERM) < 0) {
	std::cerr << "Kill said: " << strerror(errno) << std::endl;
      }

      const auto killedPid = waitpid( itsPid, nullptr, 0 );

      if (killedPid != itsPid) {
	std::cerr << "Wait said: " << strerror(errno) << std::endl;
      }
    }
  }
  
  ServerProcess( const ServerProcess& )          = delete;
  ServerProcess &operator=(const ServerProcess&) = delete;

private:
  pid_t itsPid;
};

const char *ServerProcess::argv0 = nullptr;

constexpr Exception NO_EXCEPTION   = static_cast<Exception>(-1);
constexpr Exception BAD_EXCEPTION  = static_cast<Exception>(-2);


TEST( Exceptions, AllTests ) {
  const auto thePolicy = babel::sdbus::Policy::createPolicy(
			            babel::sdbus::Policy::Sdbus::Session,
				    nullptr,
				    [](const std::weak_ptr<babel::sdbus::Policy> &){} );

  const auto theRemoteThrower = lookupExceptionTestImplementation( thePolicy, theServerName ":/nisse/hult" ) ;


  ASSERT_NE( theRemoteThrower, nullptr );

  ASSERT_THROW( theRemoteThrower->throwIt( Exception::LogicError ), std::runtime_error );
  ASSERT_THROW( theRemoteThrower->throwIt( Exception::LogicError ), babel::sdbus::DbusRuntimeError );

  // Start server....
  ServerProcess theServerProcess;

  for (const auto theException : EXCEPTION_VALUES) {
    Exception theThrownException = NO_EXCEPTION;

    try {
      theRemoteThrower->throwIt( theException );
    }
    catch ( const babel::sdbus::DbusRuntimeError &e )    { theThrownException = Exception::Runtime; }
    catch ( const babel::sdbus::DbusBadAlloc &e )        { theThrownException = Exception::BadAlloc; }
    catch ( const babel::sdbus::DbusOverflowError &e )   { theThrownException = Exception::Overflow; }
    catch ( const babel::sdbus::DbusUnderflowError &e )  { theThrownException = Exception::Underflow; }
    catch ( const babel::sdbus::DbusInvalidArgument &e ) { theThrownException = Exception::InvalidArgument; }
    catch ( const babel::sdbus::DbusDomainError &e )     { theThrownException = Exception::DomainError; }
    catch ( const babel::sdbus::DbusLengthError &e )     { theThrownException = Exception::LengthError; }
    catch ( const babel::sdbus::DbusOutOfRange &e )      { theThrownException = Exception::OutOfRange; }
    catch ( const babel::sdbus::DbusRangeError &e )      { theThrownException = Exception::RangeError; }
    catch ( const babel::sdbus::DbusLogicError &e )      { theThrownException = Exception::LogicError; }
    catch (...)                                          { theThrownException = BAD_EXCEPTION; }

    ASSERT_EQ( theException, theThrownException );
  }

  for (const auto theException : EXCEPTION_VALUES) {
    Exception theThrownException = NO_EXCEPTION;

    try {
      theRemoteThrower->throwIt( theException );
    }
    catch ( const std::bad_alloc &e )        { theThrownException = Exception::BadAlloc; }
    catch ( const std::overflow_error &e )   { theThrownException = Exception::Overflow; }
    catch ( const std::underflow_error &e )  { theThrownException = Exception::Underflow; }
    catch ( const std::invalid_argument &e ) { theThrownException = Exception::InvalidArgument; }
    catch ( const std::domain_error &e )     { theThrownException = Exception::DomainError; }
    catch ( const std::length_error &e )     { theThrownException = Exception::LengthError; }
    catch ( const std::out_of_range &e )     { theThrownException = Exception::OutOfRange; }
    catch ( const std::range_error &e )      { theThrownException = Exception::RangeError; }
    catch ( const std::logic_error &e )      { theThrownException = Exception::LogicError; }
    catch ( const std::runtime_error &e )    { theThrownException = Exception::Runtime; }
    catch (...)                              { theThrownException = BAD_EXCEPTION; }

    ASSERT_EQ( theException, theThrownException );
  }

  for (const auto theException : EXCEPTION_VALUES) {
    ASSERT_THROW( theRemoteThrower->throwIt( theException ), babel::sdbus::DbusError );
  }
}


int main( int argc, char **argv ) {
  bool isServer = false;

  ServerProcess::argv0 = *argv;

  for (char **theArgumentPtr = argv; theArgumentPtr < (argv + argc); theArgumentPtr++) {
    printf("'%s'\n", *theArgumentPtr );
    if (isServer) {
      *(theArgumentPtr-1) = *theArgumentPtr;
    }
    else if (!strcmp(*theArgumentPtr, "-server")) {
      isServer = true;
      argc -= 1;
    }
  }

  *(argv+argc) = nullptr;

  ::testing::InitGoogleTest( &argc, argv );

  if (isServer) {
    runServer();
    return 0;
  }
  else {
    return RUN_ALL_TESTS();  
  }

}
