/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2017/05/14
 */
#include <iostream>
#include <algorithm>
#include <iterator>

#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>

#include "gtest/gtest.h"
#include "test_traits.h"
#include "implementation_base.h"
#include "linktest.h"

//======================================================================
// Basic types
//
#include "generated/case.uint8_t.test.ipc.h"
#include "generated/case.uint16_t.test.ipc.h"
#include "generated/case.uint32_t.test.ipc.h"
#include "generated/case.uint64_t.test.ipc.h"
// DBUS does not support signed 8 bits! 
#include "generated/case.int16_t.test.ipc.h"
#include "generated/case.int32_t.test.ipc.h"
#include "generated/case.int64_t.test.ipc.h"

#include "generated/case.UnsignedByte.test.ipc.h"
#include "generated/case.UnsignedWord.test.ipc.h"
#include "generated/case.UnsignedLongWord.test.ipc.h"
// DBUS does not support signed 8 bits! 
#include "generated/case.SignedWord.test.ipc.h"
#include "generated/case.SignedLongWord.test.ipc.h"

#include "generated/case.String.test.ipc.h"
#include "generated/case.Bool.test.ipc.h"
#include "generated/case.Struct.test.ipc.h"

//======================================================================
// Arrays of basic types
//
#include "generated/case.Uint8Array.test.ipc.h"
#include "generated/case.Uint16Array.test.ipc.h"
#include "generated/case.Uint32Array.test.ipc.h"
#include "generated/case.Uint64Array.test.ipc.h"
#include "generated/case.Int16Array.test.ipc.h"
#include "generated/case.Int32Array.test.ipc.h"
#include "generated/case.Int64Array.test.ipc.h"

#include "generated/case.StringArray.test.ipc.h"

#include "generated/case.UnsignedByteArray.test.ipc.h"
#include "generated/case.UnsignedWordArray.test.ipc.h"
#include "generated/case.UnsignedLongWordArray.test.ipc.h"
// DBUS does not support signed 8 bits! 
#include "generated/case.SignedWordArray.test.ipc.h"
#include "generated/case.SignedLongWordArray.test.ipc.h"

// DBUS does not support arrays of boolean /&%%!/%¤!%&¤!

// Containers of file descriptors

#include "generated/case.FileDescriptorArray.test.ipc.h"
#include "generated/case.FileDescriptorList.test.ipc.h"
#include "generated/case.FileDescriptorVector.test.ipc.h"

//======================================================================
// Vectors of basic types
//
#include "generated/case.Uint8Vector.test.ipc.h"
#include "generated/case.Uint16Vector.test.ipc.h"
#include "generated/case.Uint32Vector.test.ipc.h"
#include "generated/case.Uint64Vector.test.ipc.h"
// DBUS does not support signed 8 bits! 
#include "generated/case.Int16Vector.test.ipc.h"
#include "generated/case.Int32Vector.test.ipc.h"
#include "generated/case.Int64Vector.test.ipc.h"

#include "generated/case.StringVector.test.ipc.h"

#include "generated/case.UnsignedByteVector.test.ipc.h"
#include "generated/case.UnsignedWordVector.test.ipc.h"
#include "generated/case.UnsignedLongWordVector.test.ipc.h"
// DBUS does not support signed 8 bits! 
#include "generated/case.SignedWordVector.test.ipc.h"
#include "generated/case.SignedLongWordVector.test.ipc.h"


//======================================================================
// Lists of basic types
//
#include "generated/case.Uint8List.test.ipc.h"
#include "generated/case.Uint16List.test.ipc.h"
#include "generated/case.Uint32List.test.ipc.h"
#include "generated/case.Uint64List.test.ipc.h"
// DBUS does not support signed 8 bits! 
#include "generated/case.Int16List.test.ipc.h"
#include "generated/case.Int32List.test.ipc.h"
#include "generated/case.Int64List.test.ipc.h"

#include "generated/case.StringList.test.ipc.h"

#include "generated/case.UnsignedByteList.test.ipc.h"
#include "generated/case.UnsignedWordList.test.ipc.h"
#include "generated/case.UnsignedLongWordList.test.ipc.h"
// DBUS does not support signed 8 bits! 
#include "generated/case.SignedWordList.test.ipc.h"
#include "generated/case.SignedLongWordList.test.ipc.h"

//======================================================================
// File descriptor
//
#include "generated/case.FileDescriptor.test.ipc.h"

//======================================================================
// Vacuous type
//
#include "generated/case.Vacuous.test.ipc.h"


//======================================================================
// Some map types
//
#include "generated/case.Uint32Uint32Map.test.ipc.h"
#include "generated/case.Int32Int32Map.test.ipc.h"
#include "generated/case.StringStringMap.test.ipc.h"
#include "generated/case.UnsignedWordStructMap.test.ipc.h"
#include "generated/case.StringSimpleBoostVariantMap.test.ipc.h"
#include "generated/case.SimpleBoostVariantVector.test.ipc.h"
#include "generated/case.StringStringMapVector.test.ipc.h"

//======================================================================
// The test main program could be run in any of three flavours;
// * As a server  - The program will register all test interfaces
// * As a client  - The program will fork a server and test all interfaces
// * As both      - The program will run them both in separate threads,
//
enum struct Flavour {
  Server,
  Client,
  Both
};

static const char * theOptions[] = {
  "-sdbusServer",
  "-sdbusClient",
  "-sdbusBoth"
};

static Flavour getFlavour( int &argc, char **argvBegin ) {
  const auto argvEnd     = argvBegin + argc;
  const auto optionStart = 
    std::remove_if( argvBegin, 
		    argvEnd,
		    [](const char *arg) {
		      return 
			std::end(theOptions) != 
			std::find_if( std::begin(theOptions),
				      std::end(theOptions),
				      [arg](const char *option) {
					return strcmp(arg,option) == 0;
				      });
		    } );

  if ((optionStart == argvEnd) || ((argvEnd - optionStart) > 1)) {
    std::cerr << argvBegin[0] << ": You must specify exactly one of ";
    std::copy( std::begin(theOptions), std::end(theOptions), 
	       std::ostream_iterator<const char*>( std::cerr, " " ) );
    std::cerr << std::endl;
    exit(1);
  }
  else {
    const char * const theOption = *optionStart;

    *optionStart =  nullptr;
    argc         -= 1;

    return static_cast<Flavour>(
             std::find_if( std::begin(theOptions),
			   std::end(theOptions),
			   [theOption](const char *v){
			     return strcmp(theOption,v) == 0;
			   })
	     - std::begin(theOptions));
  }
}

//========================================================================
// The greeting message to synch server and client.
//
static const char theServerGreeting[] = "SERVER STARTED\n";

//========================================================================
// A helper class to fork, kill and wait for a child process
//
struct ServerProcess {
  ServerProcess( const char *argv0 ) 
    : itsPid(0) {

    int thePipe[2];

    if (pipe(thePipe) != 0) {
      std::cerr << "Failed to create pipe: " << strerror(errno) << std::endl;
      exit(1);
    }

    const int &thePipeReadEnd  = thePipe[0];
    const int &thePipeWriteEnd = thePipe[1];

    itsPid = fork();

    if (itsPid < 0) {
      std::cerr << "Failed to fork: " << strerror(errno) << std::endl;
      exit(1);
    }
    else if (itsPid > 0) {
      // The parent; wait for start OK
      
      (void)close(thePipeWriteEnd);

      FILE * const  theServerOutput = fdopen(thePipeReadEnd, "r");
      char         *theLineBufferPtr      = nullptr;
      std::size_t   theLineBufferSize     = 0;
      ssize_t       theLineLength;

      while ((0 < (theLineLength = getline(&theLineBufferPtr, &theLineBufferSize, theServerOutput))) &&
	     (strcmp( theServerGreeting, theLineBufferPtr ) != 0)) {
      }

      fclose(theServerOutput);
      free(theLineBufferPtr);
    }
    else {
      if (close(thePipeReadEnd) || (dup2( thePipeWriteEnd, STDOUT_FILENO) < 0)) {
	std::cerr << "Setting up output failed: " << strerror(errno) << std::endl;
	exit(1);
      }

      char *argv[] = { strdup(argv0), 
		       strdup(theOptions[static_cast<int>(Flavour::Server)]),
		       static_cast<char*>(nullptr) };
      
      execvp( argv[0], argv );

      std::cerr << "Failed to exec " << argv[0] << ": " << strerror(errno) <<
	std::endl;
      
    }
  }
  ~ServerProcess() {
    if (itsPid > 0) {
      if (kill(itsPid, SIGTERM) < 0) {
	std::cerr << "Kill said: " << strerror(errno) << std::endl;
      }

      const auto killedPid = waitpid( itsPid, nullptr, 0 );

      if (killedPid != itsPid) {
	std::cerr << "Wait said: " << strerror(errno) << std::endl;
      }
    }
  }
  
  ServerProcess( const ServerProcess& )          = delete;
  ServerProcess &operator=(const ServerProcess&) = delete;

private:
  pid_t itsPid;
};

//========================================================================
// Runs the main loop of an already created server
//
static void runServer() {
  // Do a loop of processing to register the servers before
  // acknowledging on standard out.
  //
  (void)ImplementationBase::getPolicy()->processEvent();

  std::cout << theServerGreeting << std::flush;

  ImplementationBase::getPolicy()->runMainLoop();

  std::cerr << "SERVER STOPPED" << std::endl;
}

//========================================================================
//
static void startServer() {
  ImplementationBase::registerAll();
}

//========================================================================

int main( int argc, char **argv ) {
  const auto theFlavour = getFlavour( argc, argv );

  switch( theFlavour ) {
  case Flavour::Server: 
    {
      startServer();
      runServer();
      return 0;
    }
  case Flavour::Both:
    {
      startServer();
      ::testing::InitGoogleTest( &argc, argv );
      return RUN_ALL_TESTS();
    }
  case Flavour::Client:
    {
      ServerProcess theServer( argv[0] );

      ::testing::InitGoogleTest( &argc, argv );

      return RUN_ALL_TESTS();
    }
  }

  linktest::dragItIn();
  
  return RUN_ALL_TESTS();
}
