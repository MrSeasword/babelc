/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2017/11/05
 */
#include <iostream>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <sstream>
#include <limits>

#include "generated/jsontest.json.h"

#include "gtest/gtest.h"

template <typename T>
struct IOResult {
  bool writeOK;
  bool readOK;
  T    original;
  T    revived;
};

template <typename CharT>
struct Reader {
  using StringType = std::basic_string<CharT>;
  using StreamType = std::basic_stringstream<CharT>;

  template <typename T>
  static T testRead( const char *theCharImage ) {
    StringType theImage( theCharImage, (theCharImage + std::strlen(theCharImage)));
    StreamType ss(theImage);
    T          theValue;
    (void)json::test::readJSON( ss, theValue );
    return theValue;
  }
};

#define TESTREAD( returntype, arg ) testRead<CharT,returntype>(BABEL_JSON_PARSER_STRING_LITERAL(CharT,arg))

template <typename CharT, typename T> 
struct IOResult<T> testJSONIO( T theValue ) {
  struct IOResult<T> theResult{ false, false, theValue, T{} };

  std::basic_stringstream<CharT> ss;
  
  if ((theResult.writeOK = !!babel::JSON::writeJSON( ss,  theResult.original, babel::JSON::Indent(0)))) {
    babel::JSON::readJSON( ss, theResult.revived );
    theResult.readOK  = true;
  }

  return theResult;
}

#define TEST_A_TYPE( theValue )                                \
  do {                                                         \
    const auto theResult = testJSONIO<CharT>( theValue );      \
                                                               \
    EXPECT_TRUE( theResult.writeOK );                          \
    EXPECT_TRUE( theResult.readOK );                           \
    EXPECT_EQ( theResult.original, theResult.revived );        \
} while (0)

#define TEST_A_NUMERIC_TYPE( theType )                                 \
  do {                                                                 \
    using Limits = std::numeric_limits<theType>;                       \
    TEST_A_TYPE( static_cast<theType>(-1) );                           \
    TEST_A_TYPE( static_cast<theType>(0) );                            \
    TEST_A_TYPE( static_cast<theType>(1) );                            \
    TEST_A_TYPE( static_cast<theType>(2) );                            \
    TEST_A_TYPE( static_cast<theType>(4) );                            \
    TEST_A_TYPE( static_cast<theType>(8) );                            \
    TEST_A_TYPE( static_cast<theType>(16) );                           \
    TEST_A_TYPE( static_cast<theType>(32) );                           \
    TEST_A_TYPE( static_cast<theType>(64) );                           \
    if (Limits::has_quiet_NaN) {                                       \
      const auto theResult = testJSONIO<CharT>( Limits::quiet_NaN());  \
      EXPECT_TRUE( theResult.writeOK );                                \
      EXPECT_TRUE( theResult.readOK );                                 \
      EXPECT_TRUE( std::isnan(theResult.revived) );                    \
    }                                                                  \
    else {                                                             \
      TEST_A_TYPE( Limits::lowest() );                                 \
      TEST_A_TYPE( Limits::lowest()/3 );                               \
      TEST_A_TYPE( Limits::max()/3 );                                  \
      TEST_A_TYPE( Limits::max() );                                    \
    }                                                                  \
  } while (0)


template <typename T> 
class CharDependentTest : public ::testing::Test {
};

TYPED_TEST_CASE_P( CharDependentTest );

TYPED_TEST_P( CharDependentTest, BasicTest ) {
  using CharT = TypeParam;

  TEST_A_NUMERIC_TYPE( std::int8_t );
  TEST_A_NUMERIC_TYPE( std::int16_t );
  TEST_A_NUMERIC_TYPE( std::int32_t );
  TEST_A_NUMERIC_TYPE( std::int64_t );
  TEST_A_NUMERIC_TYPE( std::uint8_t );
  TEST_A_NUMERIC_TYPE( std::uint16_t );
  TEST_A_NUMERIC_TYPE( std::uint32_t );
  TEST_A_NUMERIC_TYPE( std::uint64_t );
  TEST_A_NUMERIC_TYPE( double );
  TEST_A_NUMERIC_TYPE( float );
  TEST_A_TYPE( true );
  TEST_A_TYPE( false );

  for (auto theString : { "", "apa", "bepa", "ce\npa", "de\\\\pa", "fe\x02pa" }) {
    TEST_A_TYPE( std::string(theString) );
  }

  {
    const std::vector< std::vector< double > > theDoubleVectors = 
      {
        {},
        { 1.3, 5.6 },
        { 2828, 92837, -276, 987 }
      }; 

    for (const auto &theDoubleVector : theDoubleVectors) {
      TEST_A_TYPE( theDoubleVector );
    }
  }

  {
    const std::vector< std::vector< int > > theIntVectors = 
      {
        {},
        { 1, 5 },
        { 282, 92837, -276, 987 }
      }; 

    for (const auto &theIntVector : theIntVectors) {
      TEST_A_TYPE( theIntVector );
    }
  }

  {
    const std::vector< std::vector< std::int16_t > > theIntVectors = 
      {
        {},
        { 1, 5 },
        { 282, 9283, -276, 987 }
      }; 

    for (const auto &theIntVector : theIntVectors) {
      TEST_A_TYPE( theIntVector );
    }
  }

  {
    const std::vector< std::map<std::basic_string<CharT>,int> > theIntMaps = {
      {},
      { {BABEL_JSON_PARSER_STRING_LITERAL(CharT,"one"), 1} },
      { {BABEL_JSON_PARSER_STRING_LITERAL(CharT,"one"), 1}, {BABEL_JSON_PARSER_STRING_LITERAL(CharT,"two"), -2} },
      { {BABEL_JSON_PARSER_STRING_LITERAL(CharT,"one"), 1}, {BABEL_JSON_PARSER_STRING_LITERAL(CharT,"two"), -2}, {BABEL_JSON_PARSER_STRING_LITERAL(CharT,"three"), 3} },
      { {BABEL_JSON_PARSER_STRING_LITERAL(CharT,"one"), 1}, {BABEL_JSON_PARSER_STRING_LITERAL(CharT,"two"), -2}, {BABEL_JSON_PARSER_STRING_LITERAL(CharT,"three"), 3}, {BABEL_JSON_PARSER_STRING_LITERAL(CharT,"four"), -4}}
    };

    for (const auto &theMap : theIntMaps) {
      TEST_A_TYPE( theMap );
    }
  }

  using ThisReader = Reader<CharT>;

  EXPECT_EQ( ThisReader:: template testRead<std::uint8_t>( "2e1" ),   static_cast<uint8_t>(20) );
  EXPECT_EQ( ThisReader:: template testRead<std::uint8_t>( "2.0" ),   static_cast<uint8_t>(2) );
  EXPECT_EQ( ThisReader:: template testRead<std::uint8_t>( "1.0e1" ), static_cast<uint8_t>(10) );

  EXPECT_THROW( ThisReader:: template testRead<std::uint8_t>(   "-1" ),  std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<std::uint8_t>(  "2.3" ),  std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<std::uint8_t>(  "256" ),  std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<std::uint8_t>(  "1e3" ),  std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<std::uint8_t>( "1e-1" ),  std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<std::uint8_t>( "\"1\"" ), std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<std::uint8_t>( "null" ),  std::invalid_argument );

  EXPECT_EQ( ThisReader:: template testRead<json::test::Enum>( "\"First\"" ),  json::test::Enum::First );
  EXPECT_EQ( ThisReader:: template testRead<json::test::Enum>( "\"Second\"" ), json::test::Enum::Second );
  EXPECT_EQ( ThisReader:: template testRead<json::test::Enum>( "\"Third\"" ),  json::test::Enum::Third );

  EXPECT_THROW( ThisReader:: template testRead<json::test::Enum>( "\"\"" ),      std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<json::test::Enum>( "[ 0 ]" ),     std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<json::test::Enum>( "\"first\"" ), std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<json::test::Enum>( "1" ),         std::invalid_argument );

  EXPECT_EQ( ThisReader:: template testRead<json::test::Byte>("-128"), static_cast<json::test::Byte>(-128) );
  EXPECT_EQ( ThisReader:: template testRead<json::test::Byte>( "127"), static_cast<json::test::Byte>(127) );
  EXPECT_EQ( ThisReader:: template testRead<json::test::Byte>(   "0"), static_cast<json::test::Byte>(0) );
  EXPECT_EQ( ThisReader:: template testRead<json::test::Byte>( "3.0"), static_cast<json::test::Byte>(3) );
  EXPECT_EQ( ThisReader:: template testRead<json::test::Byte>( "1e2"), static_cast<json::test::Byte>(100) );
  EXPECT_EQ( ThisReader:: template testRead<json::test::Byte>("-2e1"), static_cast<json::test::Byte>(-20) );

  EXPECT_THROW( ThisReader:: template testRead<json::test::Byte>( "\"\"" ),      std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<json::test::Byte>( "-129" ),      std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<json::test::Byte>(  "128" ),      std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<json::test::Byte>(  "2.2" ),      std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<json::test::Byte>(  "null" ),      std::invalid_argument );

  EXPECT_EQ( ThisReader:: template testRead<json::test::MaybeInt>( "2" ),    json::test::MaybeInt(2) );
  EXPECT_EQ( ThisReader:: template testRead<json::test::MaybeInt>( "-23" ),  json::test::MaybeInt(-23) );
  EXPECT_EQ( ThisReader:: template testRead<json::test::MaybeInt>( "null" ), json::test::MaybeInt() );

  EXPECT_THROW( ThisReader:: template testRead<json::test::MaybeInt>( "\"\"" ), std::invalid_argument );
  EXPECT_THROW( ThisReader:: template testRead<json::test::MaybeInt>( "22.5" ), std::invalid_argument );

  EXPECT_EQ( ThisReader:: template testRead<json::test::Multiple>("33"), json::test::Multiple(33) ); 
  EXPECT_EQ( ThisReader:: template testRead<json::test::Multiple>("\"333\""), json::test::Multiple("333") ); 
  
  EXPECT_THROW( ThisReader:: template testRead<json::test::Multiple>("true"), std::invalid_argument ); 
  EXPECT_THROW( ThisReader:: template testRead<json::test::Multiple>("2.5"), std::invalid_argument ); 
  EXPECT_THROW( ThisReader:: template testRead<json::test::Multiple>("[3]"), std::invalid_argument ); 
  EXPECT_THROW( ThisReader:: template testRead<json::test::Multiple>("{\"nisse\" : \"hult\"}"), std::invalid_argument ); 
  EXPECT_THROW( ThisReader:: template testRead<json::test::Multiple>("null"), std::invalid_argument );

  EXPECT_EQ( ThisReader:: template testRead<std::string>("\"apa\""), std::string("apa") );
  EXPECT_EQ( ThisReader:: template testRead<std::string>("\"a\\\"pa\""), std::string("a\"pa") );
  EXPECT_EQ( ThisReader:: template testRead<std::string>("\"\\npa\""), std::string("\npa") );

  {
    const auto theAttemptPtr  = ThisReader:: template testRead<json::test::SubStructPtr>(R"({"itsString":"the string","itsUint64":7633})");
    
    json::test::SubStruct  theResult{ "the string", 7633ul };

    EXPECT_EQ( *theAttemptPtr, theResult );
  }

  {
    const auto theAttemptPtr  = ThisReader:: template testRead<json::test::SubStructPtr>("null");
    
    EXPECT_EQ( theAttemptPtr, json::test::SubStructPtr() );
  }

}

TYPED_TEST_P( CharDependentTest, JsonTest ) {
  using CharT = TypeParam;

  using namespace json::test;

  const Struct theOriginal = {
      1
     ,2
     ,static_cast<Byte>(3)
     ,true
     ,Enum::Second
     ,"A \"strange\" one\n"
     ,std::make_shared<json::test::SubStruct>(json::test::SubStruct{
           "A \\t is equal to \t.",
           std::numeric_limits<std::uint64_t>::max()
       }
      )
     ,json::test::SubStructUPtr()
     ,{ 8, 7, 6, 5, 4, 3, 2, 1 }
     ,{ 1, 2, 3, 4, 5 }
     ,{ "apa", "bepa", "cepa" }
     ,{}
#ifdef HAS_BOOST_VARIANT
     ,{ static_cast<std::int32_t>(42) }
#endif
#ifdef HAS_BOOST_OPTIONAL
      ,{}
#endif
  };

  {
     std::basic_stringstream<CharT> ss;

     const char theAnswer[] = R"({"itsUint32":1,"itsUint8":2,"itsByte":3,"itsBool":true,"itsEnum":"Second","itsString":"A \"strange\" one\n","itsSubStruct":{"itsString":"A \\t is equal to \t.","itsUint64":18446744073709551615},"itsSubStructU":null,"itsByteArray":[8,7,6,5,4,3,2,1],"itsUint16Array":[1,2,3,4,5],"itsStringList":["apa","bepa","cepa"],"itsMap":{})"
#ifdef HAS_BOOST_VARIANT
       R"(,"itsMultiple":42)"
#endif
#ifdef HAS_BOOST_OPTIONAL
       R"(,"itsMaybeInt":null)"
#endif
       R"(})";

     EXPECT_TRUE( writeJSON( ss, theOriginal, babel::JSON::Indent(0) ) );
     EXPECT_EQ( ss.str(), ::babel::JSON::parser::CharTraits<CharT>::adjust( std::string( theAnswer ) ) );

     {
        Struct theRevived;
        
        EXPECT_TRUE( readJSON( ss, theRevived ) );
        EXPECT_EQ( theRevived, theOriginal ); 
     }
  }
}

TYPED_TEST_P( CharDependentTest, FormattedJsonTest ) {
  using CharT = TypeParam;

  using namespace json::test;

  const Struct theOriginal = {
       std::numeric_limits<decltype(theOriginal.itsUint32)>::max()
      ,std::numeric_limits<decltype(theOriginal.itsUint8)>::max()
      ,static_cast<Byte>(std::numeric_limits<std::underlying_type_t<decltype(theOriginal.itsByte)>>::min())
      ,true
      ,Enum::Third
      ,""
      ,json::test::SubStructPtr()
      ,std::make_unique<json::test::SubStruct>( json::test::SubStruct{
           "A normal string",
           12345678
        }
      )

      ,{ 1, 2, 3, 4, 5, 6, 7, 8 }
      ,{ 5, 4, 3, 2, 1 }
      ,{ "one" }
      ,{
        std::make_pair(1,json::test::SubStruct{ "The First",  2786 } ),
        std::make_pair(2,json::test::SubStruct{ "The Second",  762 } ),
        std::make_pair(3,json::test::SubStruct{ "The Third",   828 } )
       }
#ifdef HAS_BOOST_VARIANT
     ,{ "This is the string variant" }
#endif
#ifdef HAS_BOOST_OPTIONAL
       ,json::test::MaybeInt(6108096758)
#endif
  };

  {
     std::basic_stringstream<CharT> ss;


     EXPECT_TRUE( writeJSON( ss, theOriginal, babel::JSON::Indent(1) ) );
     {
        Struct theRevived;
        
        json::test::readJSON(ss,theRevived);
        EXPECT_EQ( theRevived, theOriginal ); 
     }
  }
}

TYPED_TEST_P( CharDependentTest, BadJsonTest ) {
  using CharT = TypeParam;

  using namespace json::test;

  const Struct theOriginal = {
       std::numeric_limits<decltype(theOriginal.itsUint32)>::min()
      ,std::numeric_limits<decltype(theOriginal.itsUint8)>::min()
      ,static_cast<Byte>(std::numeric_limits<std::underlying_type_t<decltype(theOriginal.itsByte)>>::max())
      ,false
      ,Enum::First
      ,"Nisse Hult"
      ,std::make_shared<json::test::SubStruct>(json::test::SubStruct{
        "Was here",
        std::numeric_limits<std::uint64_t>::max()/3
       }
      )
      ,std::make_unique<json::test::SubStruct>( json::test::SubStruct{
           "and his brother too",
           987654321
        }
       )
      ,{ 0xFF, 0xF0, 0x0F, 0x00, 0xAA, 0x55, 0xA5, 0x5A }
      ,{ 0, 0xFFFF, 0xAAAA, 1234, 999 }
      ,{}
      ,{
          std::make_pair(23,json::test::SubStruct{ "Anton",  0 } ),
          std::make_pair(67,json::test::SubStruct{ "Bruno",  1 } ),
          std::make_pair(-1,json::test::SubStruct{ "Caesar", 2 } ),
          std::make_pair(-1,json::test::SubStruct{ "Dora",   3 } )
       }
#ifdef HAS_BOOST_VARIANT
      ,{-33}
#endif
#ifdef HAS_BOOST_OPTIONAL
       ,json::test::MaybeInt(-3)
#endif
       
  };

  {
     std::basic_stringstream<CharT> ss;


     EXPECT_TRUE( json::test::writeJSON( ss, theOriginal, babel::JSON::Indent(0) ) );

     const auto theString = ss.str();
     
     for (std::size_t i = 0; i < (theString.size()-1); i++) {
       std::basic_stringstream<CharT> theBadStream( theString.substr(0,i) );
        
        Struct theRevived;

        EXPECT_THROW( json::test::readJSON(theBadStream, theRevived), std::invalid_argument );
     }
  }
}

REGISTER_TYPED_TEST_CASE_P( CharDependentTest, BasicTest, JsonTest, FormattedJsonTest, BadJsonTest );

using CharTypes = ::testing::Types< char, wchar_t >;

INSTANTIATE_TYPED_TEST_CASE_P( CharDependentTests, CharDependentTest, CharTypes );

#if 0
TEST (TestStruct, JsonTest) {
  using namespace json::test;

  const Struct theOriginal = {
      1
     ,2
     ,static_cast<Byte>(3)
     ,true
     ,Enum::Second
     ,"A \"strange\" one\n"
     ,std::make_shared<json::test::SubStruct>(json::test::SubStruct{
           "A \\t is equal to \t.",
           std::numeric_limits<std::uint64_t>::max()
       }
      )
     ,json::test::SubStructUPtr()
     ,{ 1, 2, 3, 4, 5 }
     ,{ "apa", "bepa", "cepa" }
     ,{}
#ifdef HAS_BOOST_VARIANT
     ,{ static_cast<std::int32_t>(42) }
#endif
#ifdef HAS_BOOST_OPTIONAL
      ,{}
#endif
  };

  {
     std::stringstream ss;

     const char theAnswer[] = R"({"itsUint32":1,"itsUint8":2,"itsByte":3,"itsBool":true,"itsEnum":"Second","itsString":"A \"strange\" one\n","itsSubStruct":{"itsString":"A \\t is equal to \t.","itsUint64":18446744073709551615},"itsSubStructU":null,"itsByteArray":[8,7,6,5,4,3,2,1],"itsUint16Array":[1,2,3,4,5],"itsStringList":["apa","bepa","cepa"],"itsMap":{})"
#ifdef HAS_BOOST_VARIANT
       R"(,"itsMultiple":42)"
#endif
#ifdef HAS_BOOST_OPTIONAL
       R"(,"itsMaybeInt":null)"
#endif
       R"(})";

     EXPECT_TRUE( writeJSON( ss, theOriginal, babel::JSON::Indent(0) ) );
     EXPECT_EQ( ss.str(), theAnswer );

     {
        Struct theRevived;
        
        EXPECT_TRUE( readJSON( ss, theRevived ) );
        EXPECT_EQ( theRevived, theOriginal ); 
     }
  }
  
}

TEST (TestStruct, FormattedJsonTest) {
  using namespace json::test;

  const Struct theOriginal = {
       std::numeric_limits<decltype(theOriginal.itsUint32)>::max()
      ,std::numeric_limits<decltype(theOriginal.itsUint8)>::max()
      ,static_cast<Byte>(std::numeric_limits<std::underlying_type_t<decltype(theOriginal.itsByte)>>::min())
      ,true
      ,Enum::Third
      ,""
      ,json::test::SubStructPtr()
      ,std::make_unique<json::test::SubStruct>( json::test::SubStruct{
           "A normal string",
           12345678
        }
      )
      ,{ 5, 4, 3, 2, 1 }
      ,{ "one" }
      ,{
        std::make_pair(1,json::test::SubStruct{ "The First",  2786 } ),
        std::make_pair(2,json::test::SubStruct{ "The Second",  762 } ),
        std::make_pair(3,json::test::SubStruct{ "The Third",   828 } )
       }
#ifdef HAS_BOOST_VARIANT
     ,{ "This is the string variant" }
#endif
#ifdef HAS_BOOST_OPTIONAL
       ,json::test::MaybeInt(6108096758)
#endif
  };

  {
     std::stringstream ss;


     EXPECT_TRUE( writeJSON( ss, theOriginal, babel::JSON::Indent(1) ) );
     {
        Struct theRevived;
        
        json::test::readJSON(ss,theRevived);
        EXPECT_EQ( theRevived, theOriginal ); 
     }
  }
  
}


TEST (TestStruct, BadJsonTest) {
  using namespace json::test;

  const Struct theOriginal = {
       std::numeric_limits<decltype(theOriginal.itsUint32)>::min()
      ,std::numeric_limits<decltype(theOriginal.itsUint8)>::min()
      ,static_cast<Byte>(std::numeric_limits<std::underlying_type_t<decltype(theOriginal.itsByte)>>::max())
      ,false
      ,Enum::First
      ,"Nisse Hult"
      ,std::make_shared<json::test::SubStruct>(json::test::SubStruct{
        "Was here",
        std::numeric_limits<std::uint64_t>::max()/3
       }
      )
      ,std::make_unique<json::test::SubStruct>( json::test::SubStruct{
           "and his brother too",
           987654321
        }
       )
      ,{ 0, 0xFFFF, 0xAAAA, 1234, 999 }
      ,{}
      ,{
          std::make_pair(23,json::test::SubStruct{ "Anton",  0 } ),
          std::make_pair(67,json::test::SubStruct{ "Bruno",  1 } ),
          std::make_pair(-1,json::test::SubStruct{ "Caesar", 2 } ),
          std::make_pair(-1,json::test::SubStruct{ "Dora",   3 } )
       }
#ifdef HAS_BOOST_VARIANT
      ,{-33}
#endif
#ifdef HAS_BOOST_OPTIONAL
       ,json::test::MaybeInt(-3)
#endif
       
  };

  {
     std::stringstream ss;


     EXPECT_TRUE( json::test::writeJSON( ss, theOriginal, babel::JSON::Indent(0) ) );

     const auto theString = ss.str();
     
     for (std::size_t i = 0; i < (theString.size()-1); i++) {
       std::stringstream theBadStream( theString.substr(0,i) );
        
        Struct theRevived;

        EXPECT_THROW( json::test::readJSON(theBadStream, theRevived), std::invalid_argument );
     }
  }
  
}

#endif


int main( int argc, char **argv ) {
  ::testing::InitGoogleTest( &argc, argv );

  return RUN_ALL_TESTS();
}
