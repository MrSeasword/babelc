/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2017/05/14
 */
#ifndef _TEST_TYPES_H_
#define _TEST_TYPES_H_

#include <cstdint>
#include <string>
#include <array>
#include <vector>
#include <list>
#include <map>
#include <sys/types.h>
#include <unistd.h>

#ifdef __has_include
       #if __has_include(<boost/variant.hpp>)
           #include <boost/variant.hpp>
           #define HAS_BOOST_VARIANT
       #endif
       #if __has_include(<variant>)
           #include <variant>
           #define HAS_STD_VARIANT
       #endif
       #if __has_include(<boost/optional.hpp>)
           #include <boost/optional.hpp>
           #define HAS_BOOST_OPTIONAL
       #endif
       #if __has_include(<optional>)
           #include <variant>
           #define HAS_STD_OPTIONAL
       #endif
#endif

namespace ipc { namespace test {
    using uint8_t  = std::uint8_t;
    using uint16_t = std::uint16_t;
    using uint32_t = std::uint32_t;
    using uint64_t = std::uint64_t;
    using int8_t   = std::int8_t;
    using int16_t  = std::int16_t;
    using int32_t  = std::int32_t;
    using int64_t  = std::int64_t;

    enum struct UnsignedByte : std::uint8_t {
      First   = 0x01,
      Zero    = 0x00,
      Nominal = 0x55,
      Last    = 0xFF
    };

    enum struct UnsignedWord : std::uint16_t {
      First   = 0x0001,
      Zero    = 0x0000,
      Nominal = 0x5555,
      Last    = 0xFFFF
    };

    enum struct UnsignedLongWord : std::uint32_t {
      First   = 0x00000001,
      Zero    = 0x00000000,
      Nominal = 0x55555555,
      Last    = 0xFFFFFFFF
    };

    enum struct SignedWord : std::int16_t {
      First   = -0x8000,
      Zero    =  0x0000,
      Nominal =  0x5555,
      Last    =  0x7F00
    };

    enum struct SignedLongWord : std::int32_t {
      First   = -0x80000000L,
      Zero    =  0x00000000,
      Nominal =  0x55555555,
      Last    =  0x7FFFFFFF
    };

    enum struct FileDescriptor : std::int32_t [[babelc::ipc_type("FileDescriptor")]] {};

#ifndef __BABELC__
    template <std::size_t N>
    ssize_t readArray( ipc::test::FileDescriptor fd, std::array< std::uint8_t, N > &theData ) {
      if (lseek(static_cast<int>(fd), 0, SEEK_SET) == (off_t)-1) {
        return -1;
      }
      else {
        return read( static_cast<int>(fd), theData.begin(), theData.size() );
      }
    }

    bool operator==( ipc::test::FileDescriptor left, ipc::test::FileDescriptor right ) {
      std::array<std::uint8_t,1024> theLeftData;
      decltype(theLeftData)         theRightData;
      const ssize_t                 theLeftLength  = readArray( left,  theLeftData );
      const ssize_t                 theRightLength = readArray( right, theRightData );
      return (theLeftLength >= 0) && (theRightLength >= 0) && (theLeftLength == theRightLength) && 
        std::equal( theLeftData.begin(), theLeftData.begin() + theLeftLength, theRightData.begin() );
    }
#endif

    using String = std::string;

    using Bool = bool;

    struct Struct {
      uint16_t       itsUint16         = 0;
      SignedLongWord itsSignedLongWord = SignedLongWord::First;
      Bool           itsBool           = false;
      String         itsString         = "";

#ifndef __BABELC__
      bool operator==( const ipc::test::Struct &right ) const {
	return 
	  (itsUint16         == right.itsUint16) &&
	  (itsSignedLongWord == right.itsSignedLongWord) &&
	  (itsBool           == right.itsBool) &&
	  (itsString         == right.itsString);
      }
#endif
    };

    struct Vacuous {
      // Intentionally empty
#ifndef __BABELC__
      bool operator==( const ipc::test::Vacuous & ) const {
	return true; // Pathetic, ain't it?
      }
#endif
    };

    using Uint8Array  = std::array<std::uint8_t,20>;
    using Uint16Array = std::array<std::uint16_t,20>;
    using Uint32Array = std::array<std::uint32_t,20>;
    using Uint64Array = std::array<std::uint64_t,20>;
    using Int16Array  = std::array<std::int16_t,20>;
    using Int32Array  = std::array<std::int32_t,20>;
    using Int64Array  = std::array<std::int64_t,20>;

    using UnsignedByteArray     = std::array<UnsignedByte, 20>;
    using UnsignedWordArray     = std::array<UnsignedWord, 20>;
    using UnsignedLongWordArray = std::array<UnsignedLongWord, 20>;
    using SignedWordArray       = std::array<SignedWord, 20>;
    using SignedLongWordArray   = std::array<SignedLongWord, 20>;

    using BoolArray = std::array< Bool, 20 >;
      
    using StringArray = std::array<String,13>;

    using FileDescriptorArray = std::array< FileDescriptor, 3 >;

    using Uint8Vector  = std::vector<std::uint8_t>;
    using Uint16Vector = std::vector<std::uint16_t>;
    using Uint32Vector = std::vector<std::uint32_t>;
    using Uint64Vector = std::vector<std::uint64_t>;
    using Int16Vector   = std::vector<std::int16_t>;
    using Int32Vector   = std::vector<std::int32_t>;
    using Int64Vector   = std::vector<std::int64_t>;
      
    using StringVector = std::vector<String>;

    using UnsignedByteVector     = std::vector<UnsignedByte>;
    using UnsignedWordVector     = std::vector<UnsignedWord>;
    using UnsignedLongWordVector = std::vector<UnsignedLongWord>;
    using SignedWordVector       = std::vector<SignedWord>;
    using SignedLongWordVector   = std::vector<SignedLongWord>;

    using FileDescriptorVector = std::vector< FileDescriptor>;

    using Uint8List  = std::list<std::uint8_t>;
    using Uint16List = std::list<std::uint16_t>;
    using Uint32List = std::list<std::uint32_t>;
    using Uint64List = std::list<std::uint64_t>;
    using Int16List   = std::list<std::int16_t>;
    using Int32List   = std::list<std::int32_t>;
    using Int64List   = std::list<std::int64_t>;
      
    using StringList = std::list<String>;

    using UnsignedByteList     = std::list<UnsignedByte>;
    using UnsignedWordList     = std::list<UnsignedWord>;
    using UnsignedLongWordList = std::list<UnsignedLongWord>;
    using SignedWordList       = std::list<SignedWord>;
    using SignedLongWordList   = std::list<SignedLongWord>;

    using FileDescriptorList = std::list<FileDescriptor>;

    using Uint32Uint32Map         = std::map<std::uint32_t,std::uint32_t>;
    using Int32Int32Map           = std::map<std::int32_t, std::int32_t>;
    using StringStringMap         = std::map<std::string, std::string>;
    using UnsignedWordStructMap   = std::map<UnsignedWord,Struct>;
    using StringStringMapVector   = std::vector<StringStringMap>;
  
#ifdef HAS_BOOST_VARIANT
      using SimpleBoostVariant = boost::variant<
         std::uint8_t,
         UnsignedWord,
         UnsignedLongWord,
         std::uint64_t,
         std::int16_t,
         std::int32_t,
         std::int64_t,
         String,
         Bool,
         Struct,
         Uint8Array,
         UnsignedWordArray,
         Uint32Vector,
         Uint64List,
         SignedWordArray,
         Int32Vector,
         Int64List
      >;

      template <class CharT, class Traits>
      std::basic_ostream<CharT,Traits>& operator <<( std::basic_ostream<CharT,Traits> &theStream, const SimpleBoostVariant & ) {
         return (theStream << "<SimpleBoostVariant is not printable>");
      }
      
      using SimpleBoostVariantVector = std::vector<SimpleBoostVariant>;

      using StringSimpleBoostVariantMap = std::map<std::string,SimpleBoostVariant>;
      
      // Due to the annoying limitation of 20 types in a
      // boost variant, the following variant is split
      // into three. 
      // I tried to reconfigure boost according to the
      // documentation (by defining 
      // BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS and 
      // various limits) but it didn't work.
      //
      using MikaelHansson1 = boost::variant<
         uint8_t,
         uint64_t,
         int64_t,
         UnsignedLongWord,
         Bool,
         Uint16Array,
         Int16Array,
         UnsignedByteArray,
         SignedWordArray,
         StringArray,
         Uint32Vector,
         Int32Vector,
         UnsignedByteVector,
         SignedWordVector,
         Uint16List,
         Int16List,
         StringList,
         UnsignedLongWordList
      > [[babelc::use_discriminated_variant]];
      using MikaelHansson2 = boost::variant<
         uint16_t,
         int16_t,
         UnsignedByte,
         SignedWord,
         Struct,
         Uint32Array,
         Int32Array,
         UnsignedWordArray,
         SignedLongWordArray,
         Uint8Vector,
         Uint64Vector,
         Int64Vector,
         UnsignedWordVector,
         SignedLongWordVector,
         Uint32List,
         Int32List,
         UnsignedByteList,
         SignedWordList
      > [[babelc::use_discriminated_variant]];
      using MikaelHansson3 = boost::variant<
         uint32_t,
         int32_t,
         UnsignedWord,
         SignedLongWord,
         Uint8Array,
         Uint64Array,
         Int64Array,
         UnsignedLongWordArray,
         // BoolArray, sdbus does not support boolean arrays!
         Uint16Vector,
         Int16Vector,
         StringVector,
         UnsignedLongWordVector,
         Uint8List,
         Uint64List,
         Int64List,
         UnsignedWordList,
         SignedLongWordList,
         Vacuous
      > [[babelc::use_discriminated_variant]];
#endif
      
}}


#endif

