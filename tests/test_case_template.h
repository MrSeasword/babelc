/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2017/05/14
 */

#ifndef _TEST_TYPENAME_H_
#define _TEST_TYPENAME_H_

#include "test_types.h"

#define MIKAEL_HANSSON_METHOD( N ) \
      MikaelHansson##N callDiscriminatedBoostTestMethod##N( const MikaelHansson##N &value )

namespace ipc { namespace test {

#ifdef HAS_BOOST_OPTIONAL
    using TYPENAME_Optional = boost::optional<TYPENAME>;
#endif

    struct Interface_TYPENAME {

#ifndef VACUOUS_TYPE
      virtual TYPENAME callTestMethod( TYPENAME value ) = 0;
#else
#ifndef HAS_BOOST_VARIANT
      virtual bool inThisCaseAtLeastOneMethodIsNeeded() = 0;
#endif
#endif

#ifdef HAS_BOOST_OPTIONAL
#ifndef VACUOUS_TYPE
#ifndef FILEDESCRIPTOR_TYPE
      virtual TYPENAME_Optional        callWithOptional( const TYPENAME_Optional &theValue ) = 0;
#endif
#endif
#endif

#ifdef HAS_BOOST_VARIANT
      virtual SimpleBoostVariant callSimpleBoostTestMethod( const SimpleBoostVariant &value ) = 0;
      virtual MIKAEL_HANSSON_METHOD(1) = 0;
      virtual MIKAEL_HANSSON_METHOD(2) = 0;
      virtual MIKAEL_HANSSON_METHOD(3) = 0;
#endif
      virtual ~Interface_TYPENAME() {}
    };
}}

#ifndef __BABELC__

//======================================================================
// Here are things not seen by the babelc compiler!
//

#include "implementation_base.h"
#include "test_traits.h"

#define TEST_OBJECT_PATH "/test/dbus/TYPENAME"

namespace ipc { namespace test {
    template <typename IpcPolicy>
    std::shared_ptr<ipc::test::Interface_TYPENAME>
    registerInterface_TYPENAMEImplementation(
        std::shared_ptr<IpcPolicy>                    theIpcPolicy,
        const char*                                   theObjectPath,
        std::shared_ptr<ipc::test::Interface_TYPENAME> theImplementation
    );

    template <typename IpcPolicy>
    std::shared_ptr<ipc::test::Interface_TYPENAME>
    lookupInterface_TYPENAMEImplementation( std::shared_ptr<IpcPolicy> theIpcPolicy, const char *theObjectPath );


    struct Implementation_TYPENAME : public Interface_TYPENAME, 
		    public std::enable_shared_from_this<Implementation_TYPENAME>, 
		    private ImplementationBase {


#ifndef VACUOUS_TYPE
      TYPENAME callTestMethod( TYPENAME value ) override {
	return TestTraits<TYPENAME>::transform(value);
      }
#else
#ifndef HAS_BOOST_VARIANT
      bool inThisCaseAtLeastOneMethodIsNeeded() override {}
#endif
#endif

#ifdef HAS_BOOST_OPTIONAL
#ifndef VACUOUS_TYPE
#ifndef FILEDESCRIPTOR_TYPE
      TYPENAME_Optional callWithOptional( const TYPENAME_Optional &theValue ) override {
	if (theValue) {
	  return TYPENAME_Optional( TestTraits<TYPENAME>::transform(*theValue) );
	}
	else {
	  return theValue;
	}
      }
#endif
#endif
#endif

#ifdef HAS_BOOST_VARIANT
      SimpleBoostVariant callSimpleBoostTestMethod ( const SimpleBoostVariant &value ) override {
         return transformVariant<TYPENAME>( value );
      }
      MIKAEL_HANSSON_METHOD(1) override {
        return transformVariant<TYPENAME>( value );
      }
      MIKAEL_HANSSON_METHOD(2) override {
        return transformVariant<TYPENAME>( value );
      }
      MIKAEL_HANSSON_METHOD(3) override {
        return transformVariant<TYPENAME>( value );
      }
       
#endif
      
      void registerInstance( std::shared_ptr<babel::sdbus::Policy> thePolicy ) override {
	itsProxy = registerInterface_TYPENAMEImplementation( thePolicy, TEST_OBJECT_PATH, shared_from_this() );
      }
    private:
      std::shared_ptr<Interface_TYPENAME> itsProxy;
    };

    extern const auto theImplementation_TYPENAME  [[gnu::weak]] = std::make_shared<Implementation_TYPENAME>();
}}

TEST (Test_TYPENAME, GeneralTest) {
  const auto theRemote = ipc::test::lookupInterface_TYPENAMEImplementation( ImplementationBase::getPolicy(), 
									    TEST_SERVER_NAME ":" TEST_OBJECT_PATH );

#ifndef VACUOUS_TYPE
  using traits = TestTraits<ipc::test::TYPENAME>;

  EXPECT_EQ(traits::transform(traits::min()),     theRemote->callTestMethod(traits::min()));
  EXPECT_EQ(traits::transform(traits::max()),     theRemote->callTestMethod(traits::max()));
  EXPECT_EQ(traits::transform(traits::zero()),    theRemote->callTestMethod(traits::zero()));
  EXPECT_EQ(traits::transform(traits::nominal()), theRemote->callTestMethod(traits::nominal()));
#endif

#ifdef HAS_BOOST_OPTIONAL
#ifndef VACUOUS_TYPE
#ifndef FILEDESCRIPTOR_TYPE
  EXPECT_EQ(ipc::test::TYPENAME_Optional(traits::transform(traits::min())),     theRemote->callWithOptional(ipc::test::TYPENAME_Optional(traits::min())));
  EXPECT_EQ(ipc::test::TYPENAME_Optional(traits::transform(traits::max())),     theRemote->callWithOptional(ipc::test::TYPENAME_Optional(traits::max())));
  EXPECT_EQ(ipc::test::TYPENAME_Optional(traits::transform(traits::zero())),    theRemote->callWithOptional(ipc::test::TYPENAME_Optional(traits::zero())));
  EXPECT_EQ(ipc::test::TYPENAME_Optional(traits::transform(traits::nominal())), theRemote->callWithOptional(ipc::test::TYPENAME_Optional(traits::nominal())));
  EXPECT_FALSE( theRemote->callWithOptional( ipc::test::TYPENAME_Optional() ) );
#endif
#endif  
#endif

  #ifdef HAS_BOOST_VARIANT

  #define MIKAEL_HANSSON_TEST( N ) \
  runVariantTest<ipc::test::TYPENAME,ipc::test::MikaelHansson##N>(*theRemote,&ipc::test::Interface_TYPENAME::callDiscriminatedBoostTestMethod##N)
 
  runVariantTest<ipc::test::TYPENAME,ipc::test::SimpleBoostVariant>(*theRemote,&ipc::test::Interface_TYPENAME::callSimpleBoostTestMethod);
  MIKAEL_HANSSON_TEST(1);
  MIKAEL_HANSSON_TEST(2);
  MIKAEL_HANSSON_TEST(3);

  #undef MIKAEL_HANSSON_TEST

  #endif
}

#undef TEST_OBJECT_PATH

#endif

#undef MIKAEL_HANSSON_METHOD

#ifdef VACUOUS_TYPE
#undef VACUOUS_TYPE
#endif

#ifdef FILEDESCRIPTOR_TYPE
#undef FILEDESCRIPTOR_TYPE
#endif

#endif
