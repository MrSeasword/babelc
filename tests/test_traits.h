/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2017/05/14
 */
#ifndef _TEST_TRAITS_H_
#define _TEST_TRAITS_H_

#include <type_traits>
#include <limits>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include "test_types.h"

template <typename... T>
struct firstInRest : public std::false_type {
};

template <typename U, typename T1, typename... REST>
struct firstInRest<U,T1,REST...> :
   public std::conditional_t<std::is_same<U,T1>::value, std::true_type, firstInRest<U,REST...>> {
};


template <typename TheType> 
struct ArithmeticTestTraits {
  using limits = std::numeric_limits<TheType>;

  static constexpr TheType max()     { return limits::max(); }
  static constexpr TheType min()     { return limits::min(); }
  static constexpr TheType zero()    { return 0; }                
  static constexpr TheType nominal() { return static_cast<TheType>(0x0123456789ABCDEFull); }

  static constexpr TheType transform( TheType v ) {
    return static_cast<TheType>(v+127);
  }

  static constexpr bool equal( TheType left, TheType right ) {
    return left == right;
  }
};

template <typename TheType> 
struct EnumTestTraits {
  using BaseType = std::underlying_type_t<TheType>;

  static constexpr TheType max()     { return TheType::Last; }
  static constexpr TheType min()     { return TheType::First; }
  static constexpr TheType zero()    { return TheType::Zero; }
  static constexpr TheType nominal() { return TheType::Nominal; }

  static constexpr TheType transform( TheType v ) {
    switch(v) {
    case TheType::Zero:
      return TheType::First;
    case TheType::First:
      return TheType::Nominal;
    case TheType::Nominal:
      return TheType::Last;
    case TheType::Last:
      return TheType::First;
    }
    return TheType::Zero;
  }

  static constexpr bool equal( TheType left, TheType right ) {
    return left == right;
  }
};


template <typename TheTestType>
struct TestTraits {};

template <> struct TestTraits<std::uint8_t>  : public ArithmeticTestTraits<std::uint8_t>  {};
template <> struct TestTraits<std::uint16_t> : public ArithmeticTestTraits<std::uint16_t> {};
template <> struct TestTraits<std::uint32_t> : public ArithmeticTestTraits<std::uint32_t> {};
template <> struct TestTraits<std::uint64_t> : public ArithmeticTestTraits<std::uint64_t> {};
template <> struct TestTraits<std::int8_t>   : public ArithmeticTestTraits<std::int8_t>   {};
template <> struct TestTraits<std::int16_t>  : public ArithmeticTestTraits<std::int16_t>  {};
template <> struct TestTraits<std::int32_t>  : public ArithmeticTestTraits<std::int32_t>  {};
template <> struct TestTraits<std::int64_t>  : public ArithmeticTestTraits<std::int64_t>  {};

template <> struct TestTraits<ipc::test::UnsignedByte>     : public EnumTestTraits<ipc::test::UnsignedByte> {};
template <> struct TestTraits<ipc::test::UnsignedWord>     : public EnumTestTraits<ipc::test::UnsignedWord> {};
template <> struct TestTraits<ipc::test::UnsignedLongWord> : public EnumTestTraits<ipc::test::UnsignedLongWord> {};
template <> struct TestTraits<ipc::test::SignedWord>       : public EnumTestTraits<ipc::test::SignedWord> {};
template <> struct TestTraits<ipc::test::SignedLongWord>   : public EnumTestTraits<ipc::test::SignedLongWord> {};

template <> 
struct TestTraits<ipc::test::String> {
  using TheType = ipc::test::String;

  static TheType max()     { return TheType(1024,'$'); }
  static TheType min()     { return TheType(""); }
  static TheType zero()    { return TheType("0"); }
  static TheType nominal() { return TheType("Nisse Hult"); }

  static TheType transform( TheType v ) {
    TheType theCopy(v);
    for (auto &c : theCopy) {
      c += 1;
    }
    return "[" + theCopy + "]";
  }

  static bool equal( TheType left, TheType right ) {
    return left == right;
  }
};


template <> 
struct TestTraits<bool> {
  using TheType = bool;

  static TheType max()     { return true; }
  static TheType min()     { return false; }
  static TheType zero()    { return false; }
  static TheType nominal() { return true; }

  static TheType transform( TheType v ) {
    return !v;
  }

  static bool equal( TheType left, TheType right ) {
    return left == right;
  }
};

template <>
struct TestTraits<ipc::test::FileDescriptor> {
  using TheType = ipc::test::FileDescriptor;

  static TheType writeData( const std::uint8_t *theData, std::size_t theSize) {
    const auto theFD = fileno( tmpfile() );
    if ((theFD < 0) || (write(theFD,theData,theSize) < 0)) {
      return static_cast<TheType>(-1);
    }
    else {
      return static_cast<TheType>(theFD);
    }
  }

  template <std::size_t  N>
  static TheType writeArray( const std::uint8_t (&theData)[N] ) {
    return writeData( &theData[0], N );
  }

  static TheType max() {
    static const std::uint8_t theData[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    return writeArray(theData);
  }
  static TheType min() {
    static const std::uint8_t theData[] = {232};
    return writeArray(theData);

  }
  static TheType zero() {
    return writeData(nullptr, 0);
  }

  static TheType nominal() {
    static const std::uint8_t theData[] = {0, 1, 2, 3, 4};
    return writeArray(theData);
  }

  static TheType transform( TheType v ) {
    std::array<std::uint8_t,1024> theData;
    ssize_t                       theDataSize = ipc::test::readArray( v, theData );

    if (theDataSize < 0) {
      return static_cast<TheType>(-1);
    }
    else {
      return writeData( theData.begin(), theDataSize );
    }
  }

  static bool equal( TheType left, TheType right ) {
    return left == right;
  }
};

template <> 
struct TestTraits<ipc::test::Struct> {
  using TheType = ipc::test::Struct;

  static TheType max() { 
    return TheType{ 
      TestTraits<ipc::test::uint16_t>::max(), 
      TestTraits<ipc::test::SignedLongWord>::max(), 
      TestTraits<ipc::test::Bool>::max(), 
      TestTraits<ipc::test::String>::max() 
     }; 
  }
  static TheType min() { 
    return TheType{ 
      TestTraits<ipc::test::uint16_t>::min(), 
      TestTraits<ipc::test::SignedLongWord>::min(), 
      TestTraits<ipc::test::Bool>::min(), 
      TestTraits<ipc::test::String>::min() 
     }; 
  }
  static TheType zero() { 
    return TheType{ 
      TestTraits<ipc::test::uint16_t>::zero(), 
      TestTraits<ipc::test::SignedLongWord>::zero(), 
      TestTraits<ipc::test::Bool>::zero(), 
      TestTraits<ipc::test::String>::zero() 
     }; 
  }
  static TheType nominal() { 
    return TheType{ 
      TestTraits<ipc::test::uint16_t>::nominal(), 
      TestTraits<ipc::test::SignedLongWord>::nominal(), 
      TestTraits<ipc::test::Bool>::nominal(), 
      TestTraits<ipc::test::String>::nominal() 
     }; 
  }


  static TheType transform( TheType v ) {
    return TheType{ 
      TestTraits<ipc::test::uint16_t>::transform(v.itsUint16), 
      TestTraits<ipc::test::SignedLongWord>::transform(v.itsSignedLongWord), 
      TestTraits<ipc::test::Bool>::transform(v.itsBool), 
      TestTraits<ipc::test::String>::transform(v.itsString) 
     }; 
  }

  static bool equal( TheType left, TheType right ) {
    return left == right;
  }
};

template <> 
struct TestTraits<ipc::test::Vacuous> {
  using TheType = ipc::test::Vacuous;

  static TheType max() { 
    return TheType{}; 
  }
  static TheType min() { 
    return TheType{}; 
  }
  static TheType zero() { 
    return TheType{}; 
  }
  static TheType nominal() { 
    return TheType{}; 
  }
  static TheType transform( TheType v ) {
    return v; 
  }
  static bool equal( TheType left, TheType right ) {
    return left == right;
  }
};

template <typename ElementType, std::size_t N> 
struct TestTraits<std::array<ElementType,N>> {
  using TheType = std::array<ElementType,N>;

  static TheType max()     { TheType r; for (auto &v : r ) v = TestTraits<ElementType>::max();  return r; }
  static TheType min()     { TheType r; for (auto &v : r ) v = TestTraits<ElementType>::min();  return r; }
  static TheType zero()    { TheType r; for (auto &v : r ) v = TestTraits<ElementType>::zero(); return r; }
  static TheType nominal() { 
    TheType r; 
    r[0] = TestTraits<ElementType>::min();
    for (std::size_t i = 1; i < N; i++) {
      r[i] = TestTraits<ElementType>::transform(r[i-1]);
    }
    return r;
  }

  static TheType transform( TheType v ) {
    TheType theCopy(v);
    for (auto &c : theCopy) {
      c = TestTraits<ElementType>::transform(c);
    }
    return theCopy;
  }

};

template <typename ElementType> 
struct TestTraits<std::vector<ElementType>> {
  using TheType = std::vector<ElementType>;

  static constexpr int theMaxCount     = std::is_same<ElementType,ipc::test::FileDescriptor>::value ? 3 : 20;
  static constexpr int theMinCount     = std::is_same<ElementType,ipc::test::FileDescriptor>::value ? 1 : 1;
  static constexpr int theNominalCount = std::is_same<ElementType,ipc::test::FileDescriptor>::value ? 2 : 10;

  static TheType max()     { TheType r; for (int i = 0; i < theMaxCount; i++) r.emplace_back( TestTraits<ElementType>::max() );  return r; }
  static TheType min()     { TheType r; for (int i = 0; i < theMinCount;  i++) r.emplace_back( TestTraits<ElementType>::min() );  return r; }
  static TheType zero()    { TheType r; return r; }
  static TheType nominal() { 
    TheType r; 
    r.push_back( TestTraits<ElementType>::min() );
    for (std::size_t i = 1; i < theNominalCount; i++) {
       r.emplace_back( TestTraits<ElementType>::transform(r.back()) );
    }
    return r;
  }

  static TheType transform( TheType v ) {
    TheType theCopy(v);
    for (auto &c : theCopy) {
      c = TestTraits<ElementType>::transform(c);
    }
    return theCopy;
  }

};


template <typename ElementType> 
struct TestTraits<std::map<ElementType,ElementType>> {
 using TheType       = std::map<ElementType,ElementType>;
 using ElementTraits = TestTraits<ElementType>;
 
 static TheType max()     {
    TheType r;
    r.insert( std::make_pair(ElementTraits::max(),     ElementTraits::transform(ElementTraits::max()) ) );
    r.insert( std::make_pair(ElementTraits::min(),     ElementTraits::transform(ElementTraits::min()) ) );
    r.insert( std::make_pair(ElementTraits::nominal(), ElementTraits::transform(ElementTraits::nominal()) ) );
    r.insert( std::make_pair(ElementTraits::zero(),    ElementTraits::transform(ElementTraits::zero()) ) );
    return r;
 }
 static TheType min()     {
    TheType r;
    r.insert( std::make_pair(ElementTraits::min(),     ElementTraits::transform(ElementTraits::min()) ) );
    return r;
 }
 static TheType zero()    { TheType r; return r; }
 static TheType nominal() { 
    TheType r;
    r.insert( std::make_pair(ElementTraits::nominal(),     ElementTraits::transform(ElementTraits::nominal()) ) );
    return r;
 }

 static TheType transform( TheType v ) {
    TheType theCopy;
    for (const auto &thePair : v ) {
       theCopy.insert( std::make_pair( thePair.second, thePair.first ) );
    }
    return theCopy;
  }
};


template <typename KeyType> 
struct TestTraits<std::map<KeyType,ipc::test::Struct>> {
   using TheType     = std::map<KeyType,ipc::test::Struct>;
   using KeyTraits   = TestTraits<KeyType>;
   using ValueTraits = TestTraits<ipc::test::Struct>;

 static TheType max()     {
    TheType r;
    r.insert( std::make_pair(KeyTraits::max(),     ValueTraits::max() ) );
    r.insert( std::make_pair(KeyTraits::min(),     ValueTraits::min() ) );
    r.insert( std::make_pair(KeyTraits::nominal(), ValueTraits::nominal() ) );
    r.insert( std::make_pair(KeyTraits::zero(),    ValueTraits::zero() ) );
    return r;
 }
 static TheType min()     {
    TheType r;
    r.insert( std::make_pair(KeyTraits::min(), ValueTraits::min() ) );
    return r;
 }
 static TheType zero()    { TheType r; return r; }
 static TheType nominal() { 
    TheType r;
    r.insert( std::make_pair(KeyTraits::nominal(), ValueTraits::nominal() ) );
    return r;
 }

 static TheType transform( TheType v ) {
    TheType theCopy;
    for (const auto &thePair : v ) {
       theCopy.insert( std::make_pair( KeyTraits::transform(thePair.first), ValueTraits::transform(thePair.second) ) );
    }
    return theCopy;
  }
};


template <typename ElementType> 
struct TestTraits<std::list<ElementType>> {
  using TheType = std::list<ElementType>;

  static constexpr int theMaxCount     = std::is_same<ElementType,ipc::test::FileDescriptor>::value ? 3 : 33;
  static constexpr int theMinCount     = std::is_same<ElementType,ipc::test::FileDescriptor>::value ? 1 : 1;
  static constexpr int theNominalCount = std::is_same<ElementType,ipc::test::FileDescriptor>::value ? 2 : 17;

  static TheType max()     { TheType r; for (int i = 0; i < theMaxCount; i++) r.emplace_back( TestTraits<ElementType>::max() );  return r; }
  static TheType min()     { TheType r; for (int i = 0; i < theMinCount;  i++) r.emplace_back( TestTraits<ElementType>::min() );  return r; }
  static TheType zero()    { TheType r; return r; }
  static TheType nominal() { 
    TheType r; 
    r.push_back( TestTraits<ElementType>::min() );
    for (std::size_t i = 1; i < theNominalCount; i++) {
       r.emplace_back( TestTraits<ElementType>::transform(r.back()) );
    }
    return r;
  }

  static TheType transform( TheType v ) {
    TheType theCopy(v);
    for (auto &c : theCopy) {
      c = TestTraits<ElementType>::transform(c);
    }
    return theCopy;
  }

};


#ifdef HAS_BOOST_VARIANT

template <typename U, typename T1, typename... REST>
constexpr bool variantSupports( const boost::variant<T1,REST...> & ) {
   return std::is_same<U,T1>::value || firstInRest<U,REST...>::value;
}

template <typename T, typename V, bool b>
struct VariantTest {
   static V transform( const V & ) {
      throw std::runtime_error("Unsupported variant type");
   }
   template <typename I>
   static void run(I &, V (I::*)(const V&)) {
   }
};

template <typename T, typename V>
struct VariantTest<T,V,true> {
   using traits = TestTraits<T>;
      
   static V transform( const V &theVariant ) {
      return V(traits::transform(boost::get<T>(theVariant)));
   }

   template <typename I>
   static void run(I &theInterface, V (I::*testMethod)(const V&)) {
      EXPECT_EQ(traits::transform(traits::min()),     boost::get<T>((theInterface.*testMethod)(V(traits::min()))));
      EXPECT_EQ(traits::transform(traits::max()),     boost::get<T>((theInterface.*testMethod)(V(traits::max()))));
      EXPECT_EQ(traits::transform(traits::zero()),    boost::get<T>((theInterface.*testMethod)(V(traits::zero()))));
      EXPECT_EQ(traits::transform(traits::nominal()), boost::get<T>((theInterface.*testMethod)(V(traits::nominal()))));
   }
};

template <typename T, typename V>
V transformVariant( const V &theVariant ) {
   V theV;
   return VariantTest<T,V,variantSupports<T>(theV)>::transform( theVariant );
}

template <typename T, typename V, typename I>
  void runVariantTest(I &theInterface, V (I::*testMethod)(const V&)) {
   V theV;
   VariantTest<T,V,variantSupports<T>(theV)>:: template run(theInterface, testMethod);
}


template <> 
struct TestTraits<ipc::test::SimpleBoostVariant> {
  using TheType = ipc::test::SimpleBoostVariant;

  static TheType max()     { return TheType(TestTraits<ipc::test::String>::max()); }
  static TheType min()     { return TheType(TestTraits<std::uint8_t>::min()); }
  static TheType zero()    { return TheType(TestTraits<ipc::test::Uint32Vector>::zero()); }
  static TheType nominal() { return TheType(TestTraits<ipc::test::Struct>::nominal()); }

  static TheType transform( TheType v ) {
     if (auto theValue = boost::relaxed_get<ipc::test::String>(&v)) {
        return TheType(TestTraits<ipc::test::String>::transform(*theValue));
     }
     if (auto theValue = boost::relaxed_get<std::uint8_t>(&v)) {
        return TheType(TestTraits<std::uint8_t>::transform(*theValue));
     }
     if (auto theValue = boost::relaxed_get<ipc::test::Uint32Vector>(&v)) {
        return TheType(TestTraits<ipc::test::Uint32Vector>::transform(*theValue));
     }
     if (auto theValue = boost::relaxed_get<ipc::test::Struct>(&v)) {
        return TheType(TestTraits<ipc::test::Struct>::transform(*theValue));
     }
     throw std::runtime_error("Yikes!");
  }

  static bool equal( TheType left, TheType right ) {
    return left == right;
  }
};


template <> 
struct TestTraits<ipc::test::SimpleBoostVariantVector> {
  using TheType = ipc::test::SimpleBoostVariantVector;

  static TheType max()     { return TheType(256,TestTraits<ipc::test::SimpleBoostVariant>::max()); }
  static TheType min()     { return TheType(1,TestTraits<ipc::test::SimpleBoostVariant>::min()); }
  static TheType zero()    { return TheType();}
  static TheType nominal() { return TheType{TestTraits<ipc::test::SimpleBoostVariant>::max(),TestTraits<ipc::test::SimpleBoostVariant>::nominal(),TestTraits<ipc::test::SimpleBoostVariant>::min(),TestTraits<ipc::test::SimpleBoostVariant>::zero()}; }

  static TheType transform( TheType v ) {
    TheType theReturnValue;
    for (const auto &theValue : v) {
      theReturnValue.push_back( TestTraits<ipc::test::SimpleBoostVariant>::transform(theValue) );
    }
    return theReturnValue;
  }

  static bool equal( TheType left, TheType right ) {
    return left == right;
  }
};


template <> 
struct TestTraits<ipc::test::StringSimpleBoostVariantMap> {
   using TheType     = ipc::test::StringSimpleBoostVariantMap;
   using KeyTraits   = TestTraits<TheType::key_type>;
   using ValueTraits = TestTraits<TheType::mapped_type>;

 static TheType max()     {
    TheType r;
    r.insert( std::make_pair(KeyTraits::max(),     ValueTraits::max() ) );
    r.insert( std::make_pair(KeyTraits::min(),     ValueTraits::min() ) );
    r.insert( std::make_pair(KeyTraits::nominal(), ValueTraits::nominal() ) );
    r.insert( std::make_pair(KeyTraits::zero(),    ValueTraits::zero() ) );
    return r;
 }
 static TheType min()     {
    TheType r;
    r.insert( std::make_pair(KeyTraits::min(), ValueTraits::min() ) );
    return r;
 }
 static TheType zero()    { TheType r; return r; }
 static TheType nominal() { 
    TheType r;
    r.insert( std::make_pair(KeyTraits::nominal(), ValueTraits::nominal() ) );
    return r;
 }

 static TheType transform( TheType v ) {
    TheType theCopy;
    for (const auto &thePair : v ) {
       theCopy.insert( std::make_pair( KeyTraits::transform(thePair.first), ValueTraits::transform(thePair.second) ) );
    }
    return theCopy;
  }
};


#endif



#endif
