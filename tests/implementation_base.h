/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc.
 *
 * babelc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * babelc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with babelc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 2017/05/14
 */
#ifndef __IMPLEMENTATION_BASE_H__
#define __IMPLEMENTATION_BASE_H__

#include <vector>
#include "../policies/sdbus-policy.h"

#define TEST_SERVER_NAME "babel.test.sdbus"

struct ImplementationBase {
  
  ImplementationBase() {
    ourInstances.push_back(this);
  }

  virtual void registerInstance( std::shared_ptr<babel::sdbus::Policy> thePolicy ) = 0;

  static void registerAll() {
    for (auto instance : ourInstances) {
      instance->registerInstance(getPolicy());
    }
  }

  static std::shared_ptr<babel::sdbus::Policy> getPolicy() {
    static const auto thePolicy = babel::sdbus::Policy::createPolicy(
			            babel::sdbus::Policy::Sdbus::Session,
				    TEST_SERVER_NAME,
				    [](const std::weak_ptr<babel::sdbus::Policy> &){} );

    return thePolicy;
  }

private:
  
  static std::vector<ImplementationBase*> ourInstances;
};

std::vector<ImplementationBase*> ImplementationBase::ourInstances [[gnu::weak]];

#endif
