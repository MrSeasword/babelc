#
# Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
#
# This file is part of babelc.
#
# babelc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# babelc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with babelc.  If not, see <http://www.gnu.org/licenses/>.
#
#  Created on: 2017/05/14
#
GOOGLEROOT	:= 	googletest/googletest

GENERATEDIR	:=	generated
ALREADYGEN	:= 	$(wildcard $(GENERATEDIR)/*.ipc.h $(GENERATEDIR)/*.stream.h $(GENERATEDIR)/*.json.h)
ALREADYDEPS	:=      $(shell echo $(ALREADYGEN) | sed -e 's/\.\(ipc\|stream\|json\)\./.dep./g')

MAINSOURCES	:= 	sdbustest.cc \
			streamtest.cc \
			jsontest.cc \
			orsedal-olsen.cc \
			exceptiontest.cc

BABELLIB	:= 	libbabelc.a

LIBSOURCES	:=      linktest.cc \
			$(GOOGLEROOT)/src/gtest-all.cc

SOURCES		:= 	$(MAINSOURCES) $(LIBSOURCES)
OBJECTS		:=	$(SOURCES:%.cc=%.o)

DEPENDS   	:= 	$(SOURCES:%.cc=%.d) $(ALREADYDEPS)

PROGRAMS  	:= 	$(MAINSOURCES:%.cc=%)

CPPFLAGS 	:= 	-I. 						\
			-I$(GOOGLEROOT)/include				\
			-I$(GOOGLEROOT)					\
			-D__BABEL_SDBUS_DEFAULT_REPORTING__		\
			-Wall -Wextra -Wno-attributes 			\
			-std=c++14 -g -fdiagnostics-color=never

BABELC		:= 	../babelc

ifneq ($(VERBOSE),)
BABELCLOG=-babelcLog
endif

.PHONY: FORCE

%.test.h: Makefile

$(GENERATEDIR)/%.test.h: test_case_template.h
	@echo "Generating $(@)..."
	@if [ ! -d $(@D) ] ; then mkdir $(@D); fi
	@TYPENAME=`echo $(@) | sed -e 's/[^.]*[.]\([^.]*\).test.h$$/\1/'` ; \
	( if [ "Vacuous" = "$$TYPENAME" ] ; then echo "#define VACUOUS_TYPE"; fi ; \
	  if [ "FileDescriptor" = "$$TYPENAME" ] ; then echo "#define FILEDESCRIPTOR_TYPE"; fi ; \
	  sed -e "s/TYPENAME/$$TYPENAME/g" < $(<) ) > $(@)

%.ipc.h:%.h
	@echo "Generating $(@)..."
	@if [ ! -d $(@D) ] ; then mkdir $(@D); fi
	$(BABELC) $(CPPFLAGS) $(BABELCLOG) -babelcOutput=Dep -babelcOutput=Ipc $(<)

$(GENERATEDIR)/%.ipc.h:%.h
	@echo "Generating $(@)..."
	@if [ ! -d $(@D) ] ; then mkdir $(@D); fi
	$(BABELC) $(CPPFLAGS) $(BABELCLOG) -babelcDirectory=$(GENERATEDIR) -babelcOutput=Dep -babelcOutput=Ipc $(<)

$(GENERATEDIR)/%.stream.h:%.h
	@echo "Generating $(@)..."
	@$(BABELC) $(CPPFLAGS) $(BABELCLOG) -babelcDirectory=$(GENERATEDIR) -babelcOutput=Dep -babelcOutput=Stream $(<)

$(GENERATEDIR)/%.json.h:%.h
	@echo "Generating $(@)..."
	@$(BABELC) $(CPPFLAGS) $(BABELCLOG) -babelcDirectory=$(GENERATEDIR) -babelcOutput=Dep -babelcOutput=Json $(<)


%.d:%.cc
	g++  $(CPPFLAGS) -MM -MG $(<) | sed -e 's@^\(.*:\)@$(@D)/\1@' > $(@)


%.o:%.cc
	g++ -g -c -o $(@) $(CPPFLAGS) $(<)

.PRECIOUS: $(GENERATEDIR)/%.test.h

%: %.o $(BABELLIB)
	g++ -o $(@) -g $(CPPFLAGS) $(<) -L. -lbabelc -lpthread `pkg-config --libs libsystemd` 

all: $(BABELC) $(PROGRAMS)
	@printf "\n=========== Test using internal unit tests  ==\n\n"
	$(BABELC) -babelcUnitTest
	@printf "\n=========== Test using IPC D-Bus =============\n\n"
	dbus-launch --exit-with-session ./sdbustest -sdbusClient
	@printf "\n=========== Test using IPC internal routing ==\n\n"
	dbus-launch --exit-with-session ./sdbustest -sdbusBoth
	@printf "\n=========== Test stream ======================\n\n"
	./streamtest
	@printf "\n=========== Test JSON ========================\n\n"
	./jsontest
	@printf "\n=========== Test Orsedal-Olsen ===============\n\n"
	./orsedal-olsen


googletest/googletest/src/gtest-all.cc:
	git clone --quiet 'https://github.com/google/googletest.git'

$(BABELLIB): $(LIBSOURCES:%.cc=%.o)
	ar -cr $(@) $(^)

$(BABELC): FORCE
	if [ ! -d $(@D) ] ; then mkdir $(@D) ; fi ;	\
	cd $(@D) ; 					\
	cmake . ; 					\
	make 

distclean: clean
	rm -rf googletest

clean:
	rm -rf $(PROGRAMS) *.o *.d $(GENERATEDIR) *~ */*~ case.*.test.h


FORCE:

.PRECIOUS: %.test.h %.ipc.h

$(OBJECTS) $(PROGRAM): Makefile 


ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),distclean)
-include $(DEPENDS)
endif
endif
