
#ifndef INCLUDE_ORSEDAL_OLSEN_H_
#define INCLUDE_ORSEDAL_OLSEN_H_

#include <vector>
#include <map>

namespace ocp {

  struct DB {
    std::string y;
  };

  struct Interface {
    virtual bool doIt(const DB &db) = 0;
    virtual ~Interface() {};
  };

  using S = std::vector<std::string>;
  
  struct D { 
    S s; 
  };

  using DId = std::uint32_t;

  using DMap = std::map<DId, D>;

  struct T { 
    S    s; 
    DMap d; 
  };

  struct Interface2 {
    virtual bool call(const T &t) = 0;
    virtual ~Interface2();
  };

}

#endif
