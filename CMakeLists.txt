cmake_minimum_required(VERSION 3.1)
project(babelc)

add_custom_command( OUTPUT          ${CMAKE_CURRENT_BINARY_DIR}/babel_json_parser.h.txt
                    COMMAND         awk "'BEGIN{print\"R\\\"====(\"}{print}END{print\")====\\\"\"}'" ${CMAKE_CURRENT_SOURCE_DIR}/src/json/babel_json_parser.h > ${CMAKE_CURRENT_BINARY_DIR}/babel_json_parser.h.txt
                    MAIN_DEPENDENCY ${CMAKE_CURRENT_SOURCE_DIR}/src/json/babel_json_parser.h )
 
add_executable( babelc
                ${CMAKE_CURRENT_BINARY_DIR}/babel_json_parser.h.txt
		src/driver/babelc.cpp
		src/common/babel_common.cpp
		src/common/babel_types.cpp
		src/parser/babel_parser.cpp
		src/plugins/babel_plugins.cpp
		src/preprocessor/babel_preprocessor.cpp
		src/unittest/babel_unittest.cpp
		src/util/babel_logger.cpp
		src/util/babel_util.cpp
		src/plugins/generators/babel_plugins_generator.cpp
		src/plugins/generators/babel_plugins_jsongenerator.cpp
		src/plugins/generators/babel_plugins_depgenerator.cpp
		src/plugins/generators/babel_plugins_streamgenerator.cpp
                src/plugins/generators/babel_plugins_ipcgenerator.cpp )
		

target_compile_definitions( babelc 
			    PRIVATE HIDE_FROM_ECLIPSE_CDT=1 BABEL_UNITTEST=1)


target_include_directories( babelc 
			    PRIVATE
			    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src> ${CMAKE_CURRENT_BINARY_DIR})

set_target_properties(babelc PROPERTIES
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS ON
)

if ( CMAKE_COMPILER_IS_GNUCC )
   set_property( TARGET babelc 
		 APPEND_STRING PROPERTY COMPILE_FLAGS " -Wall -Wextra -Wsuggest-attribute=pure -Wsuggest-attribute=const -Wsuggest-attribute=noreturn -Wsuggest-final-types -Wsuggest-final-methods -Wsuggest-override")
endif ( CMAKE_COMPILER_IS_GNUCC )

if ( CMAKE_COMPILER_IS_GNUCXX )
   set_property( TARGET babelc 
		 APPEND_STRING PROPERTY COMPILE_FLAGS " -Wall -Wextra -Wsuggest-attribute=pure -Wsuggest-attribute=const -Wsuggest-attribute=noreturn -Wsuggest-final-types -Wsuggest-final-methods -Wsuggest-override")
endif ( CMAKE_COMPILER_IS_GNUCXX )
