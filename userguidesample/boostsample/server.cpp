/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc but this particular file is 
 * not covered by the GPL2 license, but rather the MIT license as follows:
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */
#include <map>
#include <iostream>
#include <string>
#include <sdbus-policy.h>
#include <boost/variant/static_visitor.hpp>
#include "babel/ipcevent.ipc.h"
#include "babel/ipcevent.stream.h"


struct ServerImpl : public service::v1::Manager, public boost::static_visitor<std::string> {
  
   std::string process( const service::v1::Event &theEvent ) override final {
      using service::v1::operator <<;
      
      std::cout << "Server received event '" << theEvent << "'" << std::endl;

      return boost::apply_visitor( *this, theEvent );
   }
   
   std::string operator()( const service::v1::Login &theLogin )  const {
      return "User " + theLogin.theUserName + " is logged in";
   }
   
   std::string operator()( const service::v1::Logout & )  const {
      return "Last user is logged out";
   }
   
   std::string operator()( const service::v1::AccessRequest &theRequest )  const {
      return "Access is granted to resource " + theRequest.theResource +
         (theRequest.theOptionalTimeLimit ? (" with limit " + std::to_string(*theRequest.theOptionalTimeLimit)) :
                                             " with no limit");
   }
};


int main() {
    const auto thePolicy = babel::sdbus::Policy::createPolicy( 
                               babel::sdbus::Policy::Sdbus::Session,
                               "com.seasword.service",
                               [](const std::weak_ptr<babel::sdbus::Policy>&){});

    const auto theServer = service::v1::registerManagerImplementation(
                               thePolicy,
                               "/manager",
                               std::make_shared<ServerImpl>());
    
    thePolicy->runMainLoop();

    return 0;
}
