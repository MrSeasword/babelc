/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc but this particular file is 
 * not covered by the GPL2 license, but rather the MIT license as follows:
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */
#include <iostream>
#include <sdbus-policy.h>
#include <cstring>
#include "babel/ipcevent.ipc.h"

namespace {
   void doSomeCalls( service::v1::Manager &theManager ) {
      using namespace service::v1;
      
      std::cout << theManager.process( Event{ Login{"MrSeasword"} } )                                           << std::endl;
      std::cout << theManager.process( Event{ AccessRequest{ "CommonResource", OptionalTimeLimit{} } } )        << std::endl;
      std::cout << theManager.process( Event{ AccessRequest{ "ScarceResource", OptionalTimeLimit{60000ul} } } ) << std::endl;
      std::cout << theManager.process( Event{ Logout{} } )                                                      << std::endl;
   }
}

int main() {
    const auto thePolicy = babel::sdbus::Policy::createPolicy( 
                               babel::sdbus::Policy::Sdbus::Session,
                               "does.not.matter.here",
                               [](const std::weak_ptr<babel::sdbus::Policy>&){});

    const auto theServer = service::v1::lookupManagerImplementation(
                               thePolicy,
                               "com.seasword.service:/manager" );

    doSomeCalls( *theServer );
    
    return 0;
}
