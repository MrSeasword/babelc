/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc but this particular file is 
 * not covered by the GPL2 license, but rather the MIT license as follows:
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */
#include <map>
#include <iostream>
#include <fstream>
#include <sdbus-policy.h>
#include "babel/addressbook.ipc.h"
#include "babel/listener.ipc.h"
#include "babel/server.json.h"

static const char theDirectoryName[] = "phonedirectory.json";

struct ServerImpl : public addressbook::v1::Server {
    
    explicit ServerImpl( addressbook::listener::v1::ListenerPtr theListener ) 
        : itsListener(theListener) {

        try {
          std::ifstream theRegistryStream( theDirectoryName );
          addressbook::v1::readJSON( theRegistryStream, itsRegistry );
        }
        catch (const std::exception &e) {
          std::cerr << "Failed to read the directory!" << std::endl << e.what() << std::endl;
        }
    }

    void add( const addressbook::v1::Person &thePerson ) override final {

        if (thePerson.itsPhoneNumbers.empty()) throw std::invalid_argument("No phone numbers");

        itsRegistry[thePerson.itsName] = thePerson;
        itsListener->addition( thePerson );

        std::ofstream theOutput( theDirectoryName );

        if (!addressbook::v1::writeJSON( theOutput, 
                                         itsRegistry, 
                                         babel::JSON::Indent(1))) {
            std::cerr << "Failed to save the directory!\n";
        }
    }

private:
    const addressbook::listener::v1::ListenerPtr itsListener;
    addressbook::server::v1::Registry            itsRegistry;
};


int main() {
    const auto thePolicy = babel::sdbus::Policy::createPolicy( 
                               babel::sdbus::Policy::Sdbus::Session,
                               "com.seasword.addressbook",
                               [](const std::weak_ptr<babel::sdbus::Policy>&){});

    const auto theListener = 
        addressbook::listener::v1::getListenerBroadcastProxy( thePolicy, 
                                                              "/contents" );

    const auto theServer = addressbook::v1::registerServerImplementation(
                               thePolicy,
                               "/contents",
                               std::make_shared<ServerImpl>(theListener));
    
    thePolicy->runMainLoop();

    return 0;
}
