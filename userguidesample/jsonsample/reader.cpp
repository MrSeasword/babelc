/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc but this particular file is 
 * not covered by the GPL2 license, but rather the MIT license as follows:
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */

#include "babel/demo.json.h"
#include "babel/demo.stream.h"
#include <iostream>
#include <fstream>

void visit( std::size_t indent, const babel::JSON::parser::Value &theValue );

void visit( std::size_t indent, const babel::JSON::parser::Object<std::string> &theObject ) {
  std::cout << "{" << std::endl;
  for (const auto &thePair : theObject.itsValue ) {
    std::cout << std::string(3*(indent+1),' ') << thePair.first << " : ";
    visit( indent+1, thePair.second );
    std::cout << std::endl;
  }
  std::cout << std::string(3*indent,' ') << "}";
}

void visit( std::size_t, const babel::JSON::parser::String<std::string> &theObject ) {
  std::cout << '"' << theObject.itsValue << '"';
}

void visit( std::size_t, const babel::JSON::parser::Number<std::string> &theObject ) {
  std::cout << theObject.itsValue;
}

void visit( std::size_t indent, const babel::JSON::parser::Array  &theObject ) {
  std::cout << "[ ";
  for (const auto &theItem : theObject.itsValue) {
    visit( indent, theItem ); 
    std::cout << " ";
  }
  std::cout << ']';
}

void visit( std::size_t, const babel::JSON::parser::Boolean  &theObject ) {
  std::cout << (theObject.itsValue ? "true" : "false");
}

void visit( std::size_t, const babel::JSON::parser::Null  & ) {
  std::cout << "null";
}


template <class Class>
bool tryVisit( std::size_t indent, const babel::JSON::parser::Value &theValue ) {
  const auto theInstance = dynamic_cast<const Class*>(theValue.get());

  if (theInstance) {
    visit(indent, *theInstance);
  }
  return theInstance;
}

void visit( std::size_t indent, const babel::JSON::parser::Value &theValue ) {
  (void)(
         tryVisit<babel::JSON::parser::Object<std::string>>(indent, theValue) ||
         tryVisit<babel::JSON::parser::String<std::string>>(indent, theValue) ||
         tryVisit<babel::JSON::parser::Number<std::string>>(indent, theValue) ||
         tryVisit<babel::JSON::parser::Array >             (indent, theValue) ||
         tryVisit<babel::JSON::parser::Boolean >           (indent, theValue) ||
         tryVisit<babel::JSON::parser::Null >              (indent, theValue)
  );
}

void readDOM( const std::string &theFileName ) {
  try {
    std::ifstream              theInput( theFileName );
    babel::JSON::parser::Value theTargets = babel::JSON::parser::readValue( theInput );
    visit( 0, theTargets );
  }
  catch (const std::invalid_argument &theError ) {
    std::cerr << theError.what() << std::endl;
  }
}

void readWithSchema( const std::string &theFileName ) {
  try {
    std::ifstream       theInput( theFileName );
    demo::JSON::Targets theTargets;

    babel::JSON::readJSON( theInput, theTargets );
    babel::stream::appendValue( std::cout, theTargets, babel::stream::Indent(1) );
  }
  catch (const std::invalid_argument &theError ) {
    std::cerr << theError.what() << std::endl;
  }
}

int main() {
  const std::string theFileName( "input.json" );

  readWithSchema( theFileName );
 
  std::cout << std::endl << std::string( 64, '=' ) << std::endl;

  readDOM( theFileName );

  return 0;
}
