/*
 * Copyright (C) 2017 Mattias Sjösvärd (mr@seasword.com)
 *
 * This file is part of babelc but this particular file is 
 * not covered by the GPL2 license, but rather the MIT license as follows:
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */
#include <stdexcept>
#include <sdbus-policy.h>
#include <cstdlib>
#include <cstring>
#include "babel/addressbook.ipc.h"


void addPerson(addressbook::v1::ServerPtr theServer,char **begin,char **end) {
    addressbook::v1::Person theNewPerson;

    theNewPerson.itsName = *begin++;

    if (strcmp("Male", *begin) == 0) {
        theNewPerson.itsSex = addressbook::v1::Sex::Male;
    }
    else if (strcmp("Female", *begin) == 0) {
        theNewPerson.itsSex = addressbook::v1::Sex::Female;
    }
    else if (strcmp("Neither", *begin) == 0) {
        theNewPerson.itsSex = addressbook::v1::Sex::Neither;
    }
    else {
        theNewPerson.itsSex = addressbook::v1::Sex::Unknown;
    }

    while (++begin != end) {
        theNewPerson.itsPhoneNumbers.push_back(std::strtoull(*begin,nullptr,10));
    }

    try {
        theServer->add( theNewPerson );
    }
    catch (const std::invalid_argument &theException) {
       std::cerr << "Failed to add '" << theNewPerson.itsName << "' : " <<
          theException.what() << std::endl;
    }
}

int main(int argc, char **argv) {
    if (argc < 3) throw std::invalid_argument("To few arguments!");
    
    const auto thePolicy = babel::sdbus::Policy::createPolicy( 
                               babel::sdbus::Policy::Sdbus::Session,
                               "does.not.matter.here",
                               [](const std::weak_ptr<babel::sdbus::Policy>&){});

    const auto theServer = addressbook::v1::lookupServerImplementation(
                               thePolicy,
                               "com.seasword.addressbook:/contents" );
    
    addPerson( theServer, argv+1, argv+argc );

    return 0;
}
